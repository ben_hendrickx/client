import React from 'react';
import TabView from 'components/TabView.jsx';
import { FolderOpen, Edit, Assignment, ArtTrack, Group, CameraRoll, TransferWithinAStation } from '@material-ui/icons';

export default {
    title: 'Tabs',
};

function Component1() {
    return (
        <div>
            <h1 style={{ margin: 0, padding: 0 }}>Component1</h1>
        </div>
    );
}

function Component2() {
    return (
        <div>
            <h1 style={{ margin: 0, padding: 0 }}>Component2</h1>
        </div>
    );
}

export const normal = () => {
    return (
        <div
            style={{
                fontFamily: 'sans-serif',
                fontSize: 13,
            }}
        >
            <TabView
                tabs={[
                    { label: 'Project', component: Component1, icon: <FolderOpen /> },
                    { label: 'Inschrijvingen', component: Component2, icon: <Assignment /> },
                    { label: 'Rollen', component: Component2, icon: <ArtTrack /> },
                    { label: 'Groepen', component: Component2, icon: <Group /> },
                    { label: 'Scènes', component: Component2, icon: <CameraRoll /> },
                ]}
                actions={[
                    { tooltip: 'Bewerk', icon: <Edit /> },
                    { icon: <TransferWithinAStation />, tooltip: 'Kopiëer inschrijvingen', onClick: () => {} },
                ]}
            />
        </div>
    );
};
