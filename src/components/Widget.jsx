import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { ArrowForwardIos } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import { primaryColor } from 'assets/jss/material-kit-react';
import { secondaryColor } from 'assets/jss/material-kit-react';

const widgetStyle = {
    widget: {
        background: '#fff',
        flexGrow: 1,
        flexShrink: 0,
        flexBasis: 0,
        marginRight: 10,
        marginBottom: 10,
        minWidth: 300,
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        '@media (min-width: 960px)': {
            minWidth: '26%',
        },
        '& > header, & > main': {
            padding: '10px 20px',
        },
        '& > footer a': {
            padding: '5px 20px',
        },
        '& h1': {
            margin: 0,
            fontSize: '1.1em !important',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        '& main h1': {
            margin: 0,
            marginTop: -10,
            fontSize: '2.5em !important',
            fontWeight: 300,
            textAlign: 'center',
            color: '#444',
        },
        '& footer a': {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            fontSize: '.9em',
            borderTop: '1px dashed rgba(0,0,0,0.25)',
            transition: 'all linear 0.15s',
            '&:hover': {
                background: 'rgba(0,0,0,0.1)',
                '& svg': {
                    color: secondaryColor,
                    transform: 'translateX(10px)',
                },
            },
            '& svg': {
                transition: 'all linear 0.15s',
                color: primaryColor,
                fontSize: '1em',
            },
        },
    },
};

function Widget(props) {
    const { classes } = props;
    return (
        <div className={classes.widget}>
            <header>
                <h1>
                    {props.title} {props.icon || null}
                </h1>
            </header>
            <main>
                <h1>{props.number}</h1>
            </main>
            {props.link && (
                <footer>
                    <Link onClick={props.link.onClick || (() => {})} to={props.link.href}>
                        <span>{props.link.label}</span> <ArrowForwardIos />
                    </Link>
                </footer>
            )}
        </div>
    );
}

Widget.propTypes = {
    classes: PropTypes.object,
    title: PropTypes.string.isRequired,
    icon: PropTypes.object,
    number: PropTypes.number.isRequired,
};

export default withStyles(widgetStyle)(Widget);
