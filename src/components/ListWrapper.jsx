import { withStyles } from '@material-ui/styles';
import React from 'react';

const listWrapperStyles = {
    listWrapper: {
        listStyle: 'none',
        margin: '0',
        padding: '0',
    },
};

function ListWrapper({ classes, children }) {
    return <ul className={classes.listWrapper}>{children}</ul>;
}

export default withStyles(listWrapperStyles)(ListWrapper);
