import React from 'react';
import { FolderOpen } from "@material-ui/icons";

export function Project({ name }) {
    return <div style={{ display: 'flex', alignItems: 'center' }}>
        <FolderOpen style={{marginRight: 10}} />
        <span>{name}</span>
    </div>
}