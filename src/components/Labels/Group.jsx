import React from 'react';
import { Group as GroupIcon } from "@material-ui/icons";

export function Group({ identifier, name }) {
    return <div style={{ display: 'flex', alignItems: 'center' }}>
        <GroupIcon style={{marginRight: 10}} />
        <span>{identifier} - {name}</span>
    </div>
}