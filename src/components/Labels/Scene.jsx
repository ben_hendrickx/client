import React from 'react';
import { CameraRoll } from "@material-ui/icons";

export function Scene({ identifier, name }) {
    return <div style={{ display: 'flex', alignItems: 'center' }}>
        <CameraRoll style={{marginRight: 10}} />
        <span>{identifier} - {name}</span>
    </div>
}