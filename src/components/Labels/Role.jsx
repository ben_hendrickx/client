import React from 'react';
import { ArtTrack } from "@material-ui/icons";

export function Role({ identifier, name }) {
    return <div style={{ display: 'flex', alignItems: 'center' }}>
        <ArtTrack style={{marginRight: 10}} />
        <span>{identifier} - {name}</span>
    </div>
}