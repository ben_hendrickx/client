import React from 'react';
import { ArtTrack, CameraRoll, Subject, Group, FolderOutlined } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import { Tooltip } from '@material-ui/core';

const sceneStyle = {
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexGrow: 1,
        textAlign: 'left',
        '& > span': {
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
        },
        '& i, & svg': {
            marginRight: 10,
        },
    },
};

function Scene(props) {
    const { classes, scene, identifier, groups, roles, description, icon, project } = props;

    return (
        <div className={classes.container}>
            {project && <span style={{ width: 200, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>
                <FolderOutlined /> {scene.project.name}
            </span>}
            <span style={{ maxWidth: 300 }}>
                {identifier && <span style={{ marginRight: 10, width: 60, maxWidth: 60 }}>{scene.identifier}</span>}
                {icon !== false && <CameraRoll />}
                <Tooltip title={scene.name} placement="top">
                    <span
                        style={{
                            width: 190,
                            overflow: 'hidden',
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis',
                        }}
                    >
                        {scene.name}
                    </span>
                </Tooltip>
            </span>
            {groups && (
                <span>
                    <Group /> {scene.groups} groepen
                </span>
            )}
            {roles && (
                <span>
                    <ArtTrack /> {scene.roles} rollen
                </span>
            )}
            {description && scene.description ? (
                <span style={{ minWidth: 40 }}>
                    <Tooltip title={scene.description} placement="top">
                        <Subject />
                    </Tooltip>
                </span>
            ) : (
                <span style={{ minWidth: 40 }}></span>
            )}
        </div>
    );
}

export default withStyles(sceneStyle)(Scene);
