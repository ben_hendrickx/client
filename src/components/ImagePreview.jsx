import React, { useEffect } from 'react';
import { withStyles } from '@material-ui/styles';
import { useStore } from '../mobx';
import { Close, CloudDownload } from '@material-ui/icons';

const styles = {
    imageOverlay: {
        background: 'rgba(0,0,0,0.6)',
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& img': {
            cursor: 'pointer',
            maxWidth: '90vw',
            maxHeight: '90vh',
        },
    },
    close: {
        position: 'absolute',
        right: 20,
        top: 20,
        color: 'white',
        fontSize: 40,
        "&:hover": {
            cursor: 'pointer',
            color: "#fff7e6",
        },
    },
};

function ImagePreview(props) {
    const { classes } = props;
    const { ui } = useStore();

    useEffect(() => {
        function handler(e) {
            if (e.code && e.code.toLowerCase().includes('escape')) {
                delete ui.imagePreview;
            }
        }
        window.addEventListener('keyup', handler);

        return () => {
            window.removeEventListener('keyup', handler);
        };
    });

    return (
        <div className={classes.imageOverlay} onClick={() => delete ui.imagePreview}>
            <div className={classes.close}>
                {ui.imagePreview.onDownload && <CloudDownload fontSize="large" style={{marginRight: 20}} onClick={e => {
                    e.stopPropagation();
                    ui.imagePreview.onDownload();
                }} />}
                <Close fontSize="large" className="close" />
            </div>
            <img alt="Matenfiche" src={ui.imagePreview.src} />
        </div>
    )
}

export default withStyles(styles)(ImagePreview);
