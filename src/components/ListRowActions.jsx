import { withStyles } from '@material-ui/styles';
import { primaryColor, secondaryColor, white } from '../assets/jss/material-kit-react.jsx';
import React from 'react';

const listRowActionsStyles = {
    listRowActions: {
        height: 50,
        borderLeft: `1px solid ${primaryColor}`,
        padding: '10px 20px',
        display: 'inline-flex',
        alignItems: 'center',
        background: primaryColor,
        color: white,
        justifyContent: 'space-evenly',
        '& svg': {
            marginRight: 10,
            '&:last-child': {
                margin: 0,
            },
        },
        '& svg:hover': {
            cursor: 'pointer',
            color: secondaryColor,
        },
        '@media (min-width: 960px)': {
            flexGrow: 0,
        },
    },
};

function ListRowActions({ classes, children, style }) {
    return <div className={classes.listRowActions} style={style}>{children}</div>;
}

export default withStyles(listRowActionsStyles)(ListRowActions);
