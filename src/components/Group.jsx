import React, { useState } from 'react';
import { CameraRoll, FolderOutlined, People, PeopleOutline, Subject } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import { Tooltip } from '@material-ui/core';
import { useEffect } from 'react';

const groupStyle = {
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexGrow: 1,
        textAlign: 'left',
        '& > span': {
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
        },
        '& i, & svg': {
            marginRight: 10,
        },
    },
};

function Group(props) {
    const { classes, identifier, description, users, scene, project } = props;
    const [group, setGroup] = useState(props.group);

    useEffect(() => {
        setGroup(props.group);
    }, [props.group]);

    return (
        <div className={classes.container}>
            {project && <span style={{ width: 200, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>
                <FolderOutlined /> {group.project.name}
            </span>}
            {scene && <span style={{ width: 300 }}>
                {group.scene ? <><CameraRoll /><Tooltip title={`${group.scene.identifier} - ${group.scene.name} (Cast: ${group.cast.name})`} placement="top"><span style={{ width: 265, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>{group.scene.identifier} - {group.scene.name} (Cast: {group.cast.name})</span></Tooltip></> : <><CameraRoll /> Geen scène</>}
            </span>}
            <span style={{ maxWidth: 300 }}>
                {identifier && <span style={{ marginRight: 10, width: 60, maxWidth: 60 }}>{group.identifier}</span>}
                {props.icon !== false && <People />}
                <Tooltip title={group?.name} placement="top">
                    <span
                        style={{
                            width: 190,
                            overflow: 'hidden',
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis',
                        }}
                    >
                        {group?.name}
                    </span>
                </Tooltip>
            </span>
            {
                users && (
                    <Tooltip placement="top" title={<>{Object.entries(group?.usersPerCast).map(([key, value]) => <div>{key}: {value.toString()}</div>)}</>}>
                        <span style={{ minWidth: 250 }}>

                            <PeopleOutline /> {group?.users || 0} medewerkers

                        </span>
                    </Tooltip>
                )
            }
            <span style={{ paddingTop: 2, marginBottom: 5 }}>
                {description && group?.description ? (
                    <span style={{ minWidth: 40 }}>
                        <Tooltip title={group?.description} placement="top">
                            <Subject />
                        </Tooltip>
                    </span>
                ) : (
                    <span style={{ minWidth: 40 }}></span>
                )}
            </span>
        </div >
    );
}

export default withStyles(groupStyle)(Group);
