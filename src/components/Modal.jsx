import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Button from 'components/CustomButtons/Button.jsx';
import { withTranslation } from 'react-i18next';

const modalStyle = {
    overlay: {
        padding: 20,
        position: 'fixed',
        top: 50,
        left: 10,
        right: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& > div': {
            padding: '50px 100px',
            background: '#ffffff',
            boxShadow:
                '2px 1px 5px -1px rgba(0,0,0,0.3), 0px 0px 1px 0px rgba(0,0,0,0.2), 0px 3px 5px -2px rgba(0,0,0,0.1)',
            borderRadius: 3,
            boxSizing: 'border-box',
            '& h1': {
                margin: 0,
                marginBottom: 10,
                fontSize: '1.5rem',
            },
            '& button': {
                marginRight: 10,
            },
        },
    },
};

function Modal(props) {
    const { visible, classes, onClose, title, text, onConfirm, buttonText, t } = props;

    if (!visible) {
        return null;
    }

    return (
        <div className={classes.overlay} onClick={onClose}>
            <div onClick={e => e.stopPropagation()}>
                <h1>{title}</h1>
                <p>{text}</p>
                {onConfirm && (
                    <Button onClick={onConfirm} color="success" size="md">
                        {buttonText || t('form.button.confirm')}
                    </Button>
                )}
                <Button onClick={onClose} color="default" size="md">
                    {t('form.button.close')}
                </Button>
            </div>
        </div>
    );
}

export default withTranslation()(withStyles(modalStyle)(Modal));
