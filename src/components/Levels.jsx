import { observer } from 'mobx-react-lite';
import { useStore } from '../mobx';

const User = observer(function({ children }) {
    const { account } = useStore();

    return account.level === 0 ? children : null;
});

const Admin = observer(function({ children }) {
    const { account } = useStore();

    return [1, 2].includes(account.level) ? children : null;
});

const ClothingAdmin = observer(function({ children }) {
    const { account } = useStore();

    return [1, 2, 3].includes(account.level) ? children : null;
});

const Developer = observer(function({ children }) {
    const { account } = useStore();

    return account.level === 2 ? children : null;
});

export { User, Admin, Developer, ClothingAdmin };
