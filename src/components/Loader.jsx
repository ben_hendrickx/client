import React from 'react';
import { withStyles } from '@material-ui/styles';
import Loader from 'react-loaders';
import { primaryColor } from 'assets/jss/material-kit-react';

const loaderStyles = {
    container: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
};

function Loading(props) {
    const { classes } = props;
    if (props.loading !== undefined) {
        if (!props.loading) {
            return props.children ? props.children : null;
        }
    }

    return (
        <div className={classes.container}>
            <Loader type="ball-pulse" active color={primaryColor} />
        </div>
    );
}

export default withStyles(loaderStyles)(Loading);
