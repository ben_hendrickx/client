import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import withStyles from '@material-ui/core/styles/withStyles';

import { useStore } from '../mobx';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { User } from 'components/Levels.jsx';
import ArticleForm from '../views/ClothingManagement/Articles/Forms/Article';

import {
    AccountCircle,
    PersonOutlined,
    PersonAdd,
    ExitToApp,
    LockOutlined,
    Search,
    Notifications,
    QuestionAnswer,
    DateRange,
    Language,
} from '@material-ui/icons';
import { colors, gradients, divider, fonts } from 'assets/jss/main.jsx';
import { Badge } from '@material-ui/core';
import { ClothingAdmin } from './Levels';
import Notification from 'views/Notifications/Notification';
import { primaryColor } from 'assets/jss/material-kit-react';
import Brand from './Brand';
import { withTranslation } from 'react-i18next';
import timeAgoService from 'services/timeAgoService';

let timeout;

const topBarStyle = {
    searchContainer: {
        position: 'relative',
        '&:focus-within': {
            overflow: 'visible',
        },
    },
    results: {
        position: 'absolute',
        background: 'white',
        color: '#222',
        width: '100%',
        maxHeight: 250,
        overflowY: 'auto',
        borderRadius: '0 0 3px 3px',
        boxShadow: '0px 3px 5px #777',
        zIndex: 99,
        '& > div': {
            padding: '5px 10px',
            fontSize: 13,
            '&:hover': {
                background: 'rgba(253,175,0,.4)',
                cursor: 'pointer',
            },
        },
        '& > header': {
            padding: '5px',
            fontWeight: 'bolder',
            textAlign: 'center',
            background: 'rgba(0,0,0,0.15)',
        },
    },
    search: {
        position: 'relative',
        '& > input': {
            width: 300,
            fontSize: 13,
            padding: '8px 10px',
            borderRadius: 5,
            background: 'rgba(0,0,0,.2)',
            outline: 'none',
            border: '1px solid rgba(0,0,0,.3)',
            color: 'white',
        },
        '& > svg': {
            position: 'absolute',
            right: 3,
            top: 4,
        },
    },
    container: {
        ...gradients.main,
        color: colors.white,
        userSelect: 'none',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '0 10px',
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        '& h1': {
            fontFamily: fonts.logo,
            fontSize: '1.5rem',
            lineHeight: '0.8rem',
            margin: 0,
            '@media (min-width: 960px)': {
                fontSize: '2rem',
            },
        },
        '@media (min-width: 960px)': {
            paddingLeft: 30,
        },
        '@media print': {
            display: 'none',
        },
    },
    profile: {
        position: 'relative',
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        '& img': {
            borderRadius: '100%',
            maxHeight: 30,
            marginRight: 15,
        },
        '& svg': {
            marginRight: 15,
        },
        '& > span': {
            position: 'relative',
            fontWeight: '400',
            '& + span': {
                right: -25,
                width: 0,
                height: 0,
                borderLeft: '6px solid transparent',
                borderRight: '6px solid transparent',
                borderTop: `6px solid rgba(255,255,255,.9)`,
            },
            '@media (min-width: 960px)': {
                display: 'inline-block',
                width: 120,
                textOverflow: 'ellipsis',
                overflow: 'hidden',
                whiteSpace: 'nowrap',
            },
        },
        '&:hover': {
            background: 'rgba(255, 255, 255, .3)',
        },
        background: 'rgba(255, 255, 255, .2)',
        borderLeft: '1px solid rgba(0,0,0,0.1)',
        borderRight: '1px solid rgba(0,0,0,0.1)',
        padding: '12px 40px 12px 15px',
        '@media (min-width: 960px)': {
            padding: '12px 50px 12px 30px',
        },
    },
    profilePicker: {
        position: 'absolute',
        top: '100%',
        right: 0,
        background: colors.white,
        color: 'initial',
        width: '100%',
        borderRadius: '0 0 5px 5px',
        fontSize: '0.9em',
        overflow: 'hidden',
        borderTop: '1px solid rgba(0,0,0,.4 )',
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        zIndex: 9999,
        fontWeight: 400,
        '& ul': {
            margin: 0,
            padding: 0,
            listStyle: 'none',
            '& li': {
                display: 'flex',
                alignItems: 'center',
                padding: '10px 15px',
                '&:hover': {
                    background: colors.primary,
                    color: colors.white,
                },
            },
        },
        '& footer': {
            '&:hover': {
                background: colors.primary,
            },
            '& span': {
                marginRight: 15,
            },
            '& a, & p': {
                margin: 0,
                '&:hover': {
                    color: colors.white,
                },
                display: 'flex',
                alignItems: 'center',
                padding: '10px 15px',
            },
        },
        '@media (min-width: 960px)': {
            width: 'calc(100% - 10px)',
            right: 10,
        },
    },
    messages: {
        position: 'relative',
        padding: '14px 30px',
        borderRight: '1px solid rgba(255,255,255,.3)',
        '&:hover': {
            cursor: 'pointer',
            background: 'rgba(255,255,255,0.2)',
            borderRight: '1px solid rgba(255,255,255,.3)',
        },
    },
    all: {
        transition: 'all linear 0.1s',
        '&:hover': {
            background: primaryColor,
            color: 'white',
            cursor: 'pointer',
        },
    },
};

const GlobalSearch = withStyles(topBarStyle)(
    observer(props => {
        const { classes } = props;
        const [search, setSearch] = useState('');
        const { account, general, articles, clothingManagement } = useStore();
        const history = useHistory();

        useEffect(() => {
            if (timeout) {
                clearTimeout(timeout);
                timeout = undefined;
            }

            timeout = setTimeout(() => {
                search && general.search(search);
            }, 500);
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [search]);

        function pad(value, length) {
            if ((value || '').toString().length >= length) {
                return value;
            }

            return pad('0' + value, length);
        }

        return (
            <div className={classes.searchContainer} style={props.style || {}}>
                <div className={classes.search}>
                    <input type="text" onChange={e => setSearch(e.target.value)} value={search} id="search" />
                    <Search />
                </div>
                {general.searchOpen && (
                    <div className={classes.results}>
                        {/* eslint-disable-next-line */}
                        {Object.entries(general.searchResults).map(([type, results]) => {
                            switch (type) {
                                case 'user':
                                    return (
                                        <>
                                            <header>Medewerkers</header>
                                            {results.slice(0, 5).map((user, i) => (
                                                <div
                                                    className={`search-user-${i}`}
                                                    key={`user-${user.id}`}
                                                    onClick={e => {
                                                        history.push('/medewerker/' + user.id + '/overzicht');
                                                        general.searchOpen = false;
                                                        setSearch('');
                                                    }}
                                                >
                                                    {user.firstName} {user.lastName}
                                                </div>
                                            ))}
                                        </>
                                    );
                                case 'project':
                                    return account.rights.canReadProjects ? (
                                        <>
                                            <header>Projecten</header>
                                            {results.slice(0, 5).map((project, i) => (
                                                <div
                                                    className={`search-project-${i}`}
                                                    key={`project-${project.id}`}
                                                    onClick={e => {
                                                        history.push('/projecten/' + project.id + '/overzicht');
                                                        general.searchOpen = false;
                                                        setSearch('');
                                                    }}
                                                >
                                                    {project.name}
                                                </div>
                                            ))}
                                        </>
                                    ) : null;
                                case 'article':
                                    return (
                                        <>
                                            <header>Artikels</header>
                                            {results.slice(0, 5).map((article, i) => (
                                                <div
                                                    className={`search-article-${i}`}
                                                    key={`articles-${article.id}`}
                                                    onClick={() => {
                                                        clothingManagement.categories.resource.fetch();
                                                        clothingManagement.sizes.resource.fetch();
                                                        clothingManagement.colors.resource.fetch();
                                                        clothingManagement.patterns.resource.fetch();
                                                        clothingManagement.fabrics.resource.fetch();
                                                        clothingManagement.racks.resource.fetch();
                                                        const {
                                                            id,
                                                            active,
                                                            clothingSizeId,
                                                            patternId,
                                                            fabricId,
                                                            colorId,
                                                            rackId,
                                                            tags,
                                                            categoryId,
                                                        } = article;
                                                        let modal;
                                                        const closeModal = () => modal.close();
                                                        articles.data = {
                                                            id,
                                                            active,
                                                            clothingSizeId,
                                                            patternId,
                                                            fabricId,
                                                            colorId,
                                                            rackId,
                                                            tags: tags?.split('|'),
                                                            categoryId,
                                                        };
                                                        modal = general.addModal({
                                                            type: 'window',
                                                            width: 1100,
                                                            title: `Bewerk artikel: ${pad(articles.data.id, 6)}`,
                                                            onMinimize: async modal => {
                                                                articles.data.id &&
                                                                    (modal.title = `Bewerk artikel: ${pad(
                                                                        articles.data.id,
                                                                        6
                                                                    )}`);
                                                                return articles.data;
                                                            },
                                                            onMaximize: modal => {
                                                                articles.data = { ...modal.data };
                                                            },
                                                            body: (
                                                                <ArticleForm
                                                                    onCancel={closeModal}
                                                                    onConfirm={closeModal}
                                                                />
                                                            ),
                                                        });
                                                    }}
                                                >
                                                    {pad(article.id, 6)} {article.category?.name} (Maat:{' '}
                                                    {article.size.size})
                                                </div>
                                            ))}
                                        </>
                                    );
                                default:
                                    break;
                            }
                        })}
                        <header
                            className={classes.all}
                            onClick={() => {
                                history.push('/zoekresultaten');
                                general.searchOpen = false;
                                setSearch('');
                            }}
                        >
                            Bekijk alle {general.resultCount} resultaten
                        </header>
                    </div>
                )}
            </div>
        );
    })
);

function TopBar(props) {
    const { classes, i18n, t } = props;
    const { account, general, ui, profile, notifications, threads, projects } = useStore();
    const history = useHistory();
    const [profilePickerOpen, setProfilePickerOpen] = useState(false);
    const [messagesOpen, setMessagesOpen] = useState(false);
    const [languagePickerOpen, setLanguagePickerOpen] = useState(false);
    const [language, setLanguage] = useState(i18n.language);

    function onLogoutClick() {
        account.logout();
        history.push('/');
    }

    useEffect(() => {
        if (language) {
            document.getElementsByTagName('html')[0].setAttribute('lang', language);
            timeAgoService().set(language);
            window.localStorage.setItem('FVD_LANG', language);
            i18n.changeLanguage(language);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [language]);

    useEffect(() => {
        account.rights.canReadThreads && threads.listen();
        account.rights.canReadNotificationsGlobal && notifications.listen_unread();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.id, account.rights.canReadThreads, account.rights.canReadNotificationsGlobal]);

    const ProfilePicture = props => {
        return props.picture ? (
            <img src={props.picture} alt={'Profile'} />
        ) : (
            <AccountCircle
                style={{
                    fontSize: 30,
                    width: 30,
                }}
            />
        );
    };

    const ProfileName = () => {
        return (
            <span>
                {account.firstName} {!ui.isMobile && account.lastName}
            </span>
        );
    };

    const Item = props => {
        const onClick = props.onClick || (() => {});

        return (
            <footer onClick={onClick}>
                {props.to ? (
                    <Link to={props.to}>
                        {props.icon} {props.label}
                    </Link>
                ) : (
                    <p>
                        {props.icon} {props.label}
                    </p>
                )}
            </footer>
        );
    };

    const { profiles } = account;

    const hasProfiles = profiles.length > 0;

    return (
        <>
            <div
                className={classes.container}
                onMouseLeave={() => {
                    setProfilePickerOpen(false);
                    setMessagesOpen(false);
                    setLanguagePickerOpen(false);
                }}
            >
                <h1>
                    <Brand
                        height={40}
                        style={{ background: 'none', color: 'white', fontSize: ui.isMobile ? '.8em' : '.9em' }}
                    />
                </h1>
                <ClothingAdmin>{!ui.isMobile && <GlobalSearch />}</ClothingAdmin>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
                    {!ui.isMobile && account.rights.canSeeCalendar && (
                        <div
                            className={classes.messages}
                            style={messagesOpen ? { background: 'rgba(255,255,255,.2)' } : {}}
                            onClick={() => {
                                if (!window.location.pathname.includes('projecten')) {
                                    return history.push('/kalender');
                                }

                                const projectId = window.location.pathname.split('/')[2];

                                if (!projectId) {
                                    return history.push('/kalender');
                                }

                                return history.push(`/projecten/${projectId}/kalender`);
                            }}
                        >
                            <Badge>
                                <DateRange style={{ marginTop: -4 }} className="calendar" />
                            </Badge>
                        </div>
                    )}
                    {!ui.isMobile && process.env.REACT_APP_CHAT_ENABLED && account.rights.canReadThreads && (
                        <div
                            className={classes.messages}
                            style={messagesOpen ? { background: 'rgba(255,255,255,.2)' } : {}}
                            onClick={() => {
                                history.push('/gesprekken/overzicht');
                            }}
                        >
                            <Badge badgeContent={threads.unread} color="secondary">
                                <QuestionAnswer className="threads" />
                            </Badge>
                        </div>
                    )}
                    {!ui.isMobile &&
                        (account.rights.canReadNotificationsGlobal ||
                            (projects.id && account.rights.canReadNotifications)) && (
                            <div
                                className={classes.messages}
                                style={messagesOpen ? { background: 'rgba(255,255,255,.2)' } : {}}
                                onClick={() => {
                                    setMessagesOpen(!messagesOpen);
                                    setProfilePickerOpen(false);
                                    setLanguagePickerOpen(false);
                                }}
                            >
                                <Badge badgeContent={notifications.unread.length} color="secondary">
                                    <Notifications className="notifications" />
                                </Badge>
                                {messagesOpen && (
                                    <div className={classes.profilePicker} style={{ minWidth: 250, left: 0 }}>
                                        {notifications.unread.slice(0, 10).map(notification => {
                                            return (
                                                <Item
                                                    onClick={() => {
                                                        notifications.markRead(notification.id);
                                                        general.addModal({
                                                            title: notification?.label,
                                                            body: <Notification notification={notification} />,
                                                        });
                                                    }}
                                                    label={notification.label}
                                                />
                                            );
                                        })}
                                        <div style={divider}></div>
                                        <Item
                                            to={
                                                !account.rights.canReadNotificationsGlobal && projects.id
                                                    ? `/projecten/${projects.id}/meldingen`
                                                    : '/meldingen'
                                            }
                                            label={t('form.button.viewNotifications')}
                                        />
                                    </div>
                                )}
                            </div>
                        )}
                    <div className={classes.profile} onClick={() => setProfilePickerOpen(!profilePickerOpen)}>
                        <ProfilePicture picture={profile.picture} />
                        <ProfileName />
                        <span></span>
                        {profilePickerOpen && (
                            <div className={classes.profilePicker}>
                                {ui.isMobile && account.rights.canSeeCalendar && (
                                    <Item to="/kalender" icon={<DateRange />} label={t('form.button.calendar')} />
                                )}
                                {account.rights.canReadThreads && ui.isMobile && (
                                    <Item
                                        to="/gesprekken/overzicht"
                                        icon={<QuestionAnswer />}
                                        label={t('form.button.threads')}
                                    />
                                )}
                                {ui.isMobile && (
                                    <ClothingAdmin>
                                        <Item
                                            to="/meldingen"
                                            icon={<Notifications />}
                                            label={t('form.button.notification')}
                                        />
                                    </ClothingAdmin>
                                )}
                                {hasProfiles && (
                                    <ul>
                                        {profiles.map((p, key) => (
                                            <li key={key} onClick={() => account.changeProfile(p.id)}>
                                                <ProfilePicture picture={p.image} /> {p.firstName} {p.lastName}
                                            </li>
                                        ))}
                                    </ul>
                                )}
                                {hasProfiles && <div className={divider}></div>}
                                <User>
                                    <Item
                                        to="/profiel"
                                        icon={<PersonOutlined />}
                                        label={t('form.button.viewProfile')}
                                    />
                                </User>
                                <Item
                                    to="/verander-wachtwoord"
                                    icon={<LockOutlined />}
                                    label={t('form.button.changeYourPassword')}
                                />
                                <User>
                                    <Item
                                        to="/medewerkers/nieuw"
                                        icon={<PersonAdd />}
                                        label={t('form.button.createProfile')}
                                    />
                                </User>
                                <Item
                                    onClick={onLogoutClick}
                                    to="/"
                                    icon={<ExitToApp />}
                                    label={t('page.title.signOff')}
                                />
                            </div>
                        )}
                    </div>
                    {!ui.isMobile && process.env.REACT_APP_I18N_ENABLED && (
                        <div
                            className={classes.profile}
                            style={{ maxWidth: 120, minHeight: 55 }}
                            onClick={() => setLanguagePickerOpen(!languagePickerOpen)}
                        >
                            <Language />
                            {i18n.language.toUpperCase()}
                            <span></span>
                            {languagePickerOpen && (
                                <div className={classes.profilePicker}>
                                    <Item onClick={() => setLanguage('nl')} label="NL" />
                                    <Item onClick={() => setLanguage('fr')} label="FR" />
                                    <Item onClick={() => setLanguage('en')} label="EN" />
                                </div>
                            )}
                        </div>
                    )}
                </div>
            </div>
            {/* {ui.isMobile && (
                <Admin>
                    <GlobalSearch style={{ padding: 10, background: '#fafafa' }} />
                </Admin>
            )} */}
        </>
    );
}

export default withTranslation()(withStyles(topBarStyle)(observer(TopBar)));
