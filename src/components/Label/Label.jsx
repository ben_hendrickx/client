import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import labelStyles from './label.style.jsx';

function Label(props) {
    const { style, classes, value } = props;
    return (
        <span style={style || {}} className={classes.label}>
            {value}
        </span>
    );
}

Label.propTypes = {
    classes: PropTypes.object,
    value: PropTypes.string,
};

export default withStyles(labelStyles)(Label);
