const labelStyles = {
    label: {
        display: 'block',
        fontWeight: 400,
    },
};

export default labelStyles;
