import React from 'react';
import { useHistory, useLocation } from 'react-router';
import { withStyles } from '@material-ui/styles';
import { observer } from 'mobx-react-lite';
import { useStore } from '../mobx';
import { Link } from 'react-router-dom';
import { User, Admin, ClothingAdmin } from 'components/Levels.jsx';

import { colors } from 'assets/jss/main.jsx';

import {
    FolderOpen,
    Assignment,
    SupervisedUserCircle,
    MyLocation,
    FormatSize,
    ExitToApp,
    DashboardOutlined,
    AccountTree,
    ArtTrack,
    Accessibility,
    ShoppingBasket,
    VpnKey,
    Settings,
    Message,
} from '@material-ui/icons';
import { primaryColor } from 'assets/jss/material-kit-react';
import { withTranslation } from 'react-i18next';
import { useEffect } from 'react';
import { Badge } from '@material-ui/core';

const navigationStyles = {
    menu: {
        '@media (max-width: 768px)': {
            width: '100%',
            padding: '15px 30px',
        },
        background: '#fff',
        boxShadow: '2px 5px 5px rgba(0,0,0,.3)',
        minWidth: 200,
        marginTop: 1,
        '& > ul': {
            position: 'relative',
            margin: 0,
            padding: 0,
            marginTop: 25,
            listStyle: 'none',
            height: '96%',
            '& > li': {
                paddingLeft: 20,
                '&:hover': {
                    background: primaryColor,
                    '& a': {
                        color: 'white !important',
                    },
                },
                '& > a, & > span': {
                    padding: '10px 0',
                    display: 'flex',
                    alignItems: 'center',
                    fontWeight: 'bold',
                    '& svg': {
                        marginRight: '20px',
                    },
                },
                '&.active': {
                    '& svg': {
                        color: colors.primary,
                    },
                },
                '&.disabled': {
                    background: 'transparent',
                    cursor: 'not-allowed',
                    '& > a, & > span': {
                        color: colors.disabled,
                    },
                },
            },
        },
        '@media print': {
            display: 'none',
        },
    },
};

function Navigation(props) {
    const { classes, t } = props;
    const { account, ui, messages } = useStore();
    const history = useHistory();
    const location = useLocation();

    useEffect(() => {
        messages.getUnreadMessageCount();
    }, [account?.id]);

    useEffect(() => {
        if (location.pathname.includes('berichten/')) {
            setTimeout(() => messages.getUnreadMessageCount(), 500);
        }
    }, [location]);

    const Item = props => {
        const onClick = props.onClick || (() => {});
        return (
            <li style={props.style || {}} onClick={onClick} className={props.disabled ? 'disabled' : ''}>
                {!props.disabled ? (
                    <Link to={props.to}>
                        <Badge
                            badgeContent={props.badgeContent}
                            color="secondary"
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                        >
                            {props.icon} {props.label}
                        </Badge>
                    </Link>
                ) : (
                    <Badge badgeContent={props.badgeContent}>
                        <span>
                            {props.icon} {props.label}
                        </span>
                    </Badge>
                )}
            </li>
        );
    };

    const UserItem = props => (
        <User>
            <Item {...props} />
        </User>
    );

    const AdminItem = props => (
        <Admin>
            <Item {...props} />
        </Admin>
    );

    const ClothingAdminItem = props => (
        <ClothingAdmin>
            <Item {...props} />
        </ClothingAdmin>
    );

    return (
        <div className={classes.menu} id="menu">
            <ul>
                <AdminItem
                    to={ui.isMobile ? '/overzicht' : '/overzicht'}
                    icon={<DashboardOutlined />}
                    label={t('page.title.overview')}
                />
                <UserItem
                    disabled={account.level === 0 && account.shouldVerify}
                    to="/berichten"
                    icon={<Message />}
                    label={t('page.title.news')}
                    badgeContent={messages.unread}
                />
                <UserItem
                    disabled={account.level === 0 && account.shouldVerify}
                    to="/projecten"
                    icon={<FolderOpen />}
                    label={t('page.title.projects')}
                />
                {account.rights.canReadProjects && (
                    <ClothingAdminItem to="/projecten" icon={<FolderOpen />} label={t('page.title.projects')} />
                )}
                <UserItem
                    disabled={account.level === 0 && account.shouldVerify}
                    to="/rollen"
                    icon={<ArtTrack />}
                    label={t('page.title.myRoles')}
                />
                <UserItem
                    disabled={account.level === 0 && account.shouldVerify}
                    to="/repetities"
                    icon={<ArtTrack />}
                    label={t('page.title.myRepetitions')}
                />
                {ui.isMobile && (
                    <UserItem
                        disabled={account.shouldVerify}
                        to="/inschrijvingen"
                        icon={<Assignment />}
                        label={t('page.title.registrations')}
                    />
                )}
                {account.rights.canReadUsers && (
                    <Item to="/medewerkers" icon={<SupervisedUserCircle />} label={t('page.title.users')} />
                )}
                {account.rights.canReadCasts && (
                    <Item to="/casts" icon={<AccountTree />} label={t('page.title.casts')} />
                )}
                {account.rights.canReadRepetitionTypes && (
                    <Item to="/repetitietypes" icon={<FormatSize />} label={t('page.title.repetitionTypes')} />
                )}
                {account.rights.canReadLocations && (
                    <Item to="/locaties" icon={<MyLocation />} label={t('page.title.locations')} />
                )}
                {account.rights.canReadArticles && (
                    <Item
                        to="/kledijbeheer/artikels"
                        icon={<Accessibility />}
                        label={t('page.title.clothingManagement')}
                    />
                )}
                {account.rights.canReadOrders && (
                    <Item
                        to="/retributiebeheer/bestellingen"
                        icon={<ShoppingBasket />}
                        label={t('page.title.renting')}
                    />
                )}
                {!account.rights.canReadOrders && account.rights.canReadCategories && (
                    <Item
                        to="/retributiebeheer/categorieen"
                        icon={<ShoppingBasket />}
                        label={t('page.title.renting')}
                    />
                )}
                {account.rights.canReadAccessRoles && (
                    <Item to="/toegangsrollen" icon={<VpnKey />} label={t('page.title.accessRoles')} />
                )}
                {account.rights.canReadSettings && (
                    <Item to="/instellingen" icon={<Settings />} label={t('page.title.settings')} />
                )}
                <Item
                    // style={{ position: 'absolute', bottom: 0, right: 0, left: 0 }}
                    onClick={() => {
                        account.logout();
                        history.push('/');
                    }}
                    to="/"
                    icon={<ExitToApp />}
                    label={t('page.title.signOff')}
                />
            </ul>
        </div>
    );
}

export default withTranslation()(withStyles(navigationStyles)(observer(Navigation)));
