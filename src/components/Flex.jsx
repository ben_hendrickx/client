import React from 'react';
import { withStyles } from '@material-ui/styles';

const flexStyle = {
    flex: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        //alignItems: 'center',
    },
};

function Flex(props) {
    const { classes } = props;
    return <div className={classes.flex}>{props.children || null}</div>;
}

export default withStyles(flexStyle)(Flex);
