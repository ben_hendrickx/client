/*!

=========================================================
* Material Kit React - v1.7.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-kit-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-kit-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
/*eslint-disable*/
import React from "react";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

// @material-ui/icons
import { Twitter, Facebook, Instagram } from "@material-ui/icons";

// core components
import Button from "components/CustomButtons/Button.jsx";
import { useStore } from "../../mobx";

import headerLinksStyle from "./headerlinks.style.jsx";

function HeaderLinks({ ...props }) {
    const { ui } = useStore();
    const { classes } = props;
    return (
        <List className={classes.list}>
            {ui.isMobile && <ListItem className={classes.listItem}>
                <Link className={classes.navLinkIcon} to="/">Aanmelden</Link>
            </ListItem>}
            {ui.isMobile && <ListItem className={classes.listItem}>
                <Link className={classes.navLinkIcon} to="/account-aanmaken">Account aanmaken</Link>
            </ListItem>}
            {ui.isMobile && <ListItem className={classes.listItem}>
                <Link className={classes.navLinkIcon} to="/wachtwoord-vergeten">Wachtwoord vergeten</Link>
            </ListItem>}
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-twitter"
                    title="Volg ons op twitter"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{ tooltip: classes.tooltip }}
                >
                    <Button
                        href="https://twitter.com/ToerismeGeel"
                        target="_blank"
                        color="transparent"
                        className={classes.navLinkIcon}
                    >

                        <Twitter className={classes.socialIcons} style={{height:24, width:24}} /> {ui.isMobile && " Volg ons op twitter"}
                    </Button>
                </Tooltip>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-facebook"
                    title="Volg ons op facebook"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{ tooltip: classes.tooltip }}
                >
                    <Button
                        color="transparent"
                        href="https://www.facebook.com/toerismegeel/"
                        target="_blank"
                        className={classes.navLinkIcon}
                    >
                        <Facebook className={classes.socialIcons} style={{height: 24, width: 24}} /> {ui.isMobile && " Volg ons op facebook"}
                    </Button>
                </Tooltip>
            </ListItem>
            <ListItem className={classes.listItem}>
                <Tooltip
                    id="instagram-tooltip"
                    title="Volg ons op instagram"
                    placement={window.innerWidth > 959 ? "top" : "left"}
                    classes={{ tooltip: classes.tooltip }}
                >
                    <Button
                        color="transparent"
                        href="http://instagram.com/toerismegeel"
                        target="_blank"
                        className={classes.navLinkIcon}
                    >
                        <Instagram className={classes.socialIcons} style={{height: 24, width: 24}} /> {ui.isMobile && " Volg ons op instagram"}
                    </Button>
                </Tooltip>
            </ListItem>
        </List>
    );
}

export default withStyles(headerLinksStyle)(HeaderLinks);
