const toolbarStyles = {
    toolbar: {
        '@media (min-width: 960px)': {
            padding: '20px 0px',
        },
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        '& > h1': {
            fontSize: '1.8em',
            lineHeight: '60px',
            margin: 0,
        },
        '@media print': {
            display: 'none',
        },
    },
    noPrint: {
        '@media print': {
            display: 'none',
        },
    },
    root: {
        padding: '6px 12px',
        display: 'flex',
        alignItems: 'center',
        '@media (max-width: 960px)': {
            flexGrow: 1,
        },
        '@media (min-width: 960px)': {
            borderRadius: '2px',
            minWidth: '300px',
            float: 'right',
        },
        '@media print': {
            display: 'none',
        },
    },
    smallRoot: {
        padding: '6px 12px',
        display: 'flex',
        alignItems: 'center',
        '@media (max-width: 960px)': {
            flexGrow: 1,
        },
        '@media (min-width: 960px)': {
            borderRadius: '2px',
            minWidth: '60px',
            float: 'right',
        },
    },
    input: {
        flex: 1,
    },
    icon: {
        color: 'rgba(0, 0, 0, 0.54)',
    },
    divider: {
        height: '28px',
        margin: '4px 10px 4px 15px',
        width: '1px',
    },
};

export default toolbarStyles;
