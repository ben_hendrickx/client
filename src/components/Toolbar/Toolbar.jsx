import React from 'react';
import { useHistory } from 'react-router';

import {
    Mail,
    Search,
    Add,
    GetApp,
    NotListedLocation,
    ExitToApp,
    ImportExport,
    CreateNewFolderOutlined,
} from '@material-ui/icons';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import Tooltip from '@material-ui/core/Tooltip';
import withStyles from '@material-ui/core/styles/withStyles';

import { useStore } from '../../mobx';

import toolbarStyles from './toolbar.styles.jsx';
import { secondaryColor } from 'assets/jss/material-kit-react';

function Toolbar(props) {
    const { classes, actions, title, searchProps, small } = props;
    const { value, placeholder, onChange } = searchProps || {};
    const { ui, account } = useStore();
    const history = useHistory();

    function Icon({ icon }) {
        switch (icon) {
            case 'back':
                return <ExitToApp style={{ color: secondaryColor }} />;
            case 'mail':
                return <Mail style={{ color: secondaryColor }} />;
            case 'add':
                return <Add style={{ color: secondaryColor }} />;
            case 'addGroup':
                return <CreateNewFolderOutlined style={{ color: secondaryColor }} />;
            case 'getApp':
                return <GetApp style={{ color: secondaryColor }} />;
            case 'importExport':
                return <ImportExport style={{ color: secondaryColor }} />;
            case 'notListed':
                return <NotListedLocation style={{ color: secondaryColor }} />;
            default:
                return icon;
        }
    }

    return (
        <div className={props.noPrint ? classes.noPrint : ''}>
            <div style={props.style || {}} className={!small && classes.toolbar}>
                {!ui.isMobile && <h1>{title}</h1>}
                {(searchProps || !!actions?.length) && (
                    <Paper square className={searchProps ? classes.root : classes.smallRoot}>
                        {searchProps && (
                            <>
                                <InputBase
                                    className={classes.input}
                                    placeholder={placeholder}
                                    inputProps={{ 'aria-label': placeholder }}
                                    value={value}
                                    onChange={onChange}
                                />
                                <Search className={classes.icon} />
                            </>
                        )}
                        {searchProps &&
                            actions &&
                            actions?.filter(v => v).some(({ level }) => !level || level === account.level) && (
                                <Divider className={classes.divider} orientation="vertical" />
                            )}
                        {actions &&
                            actions
                                ?.filter(v => v)
                                .filter(
                                    ({ level }) =>
                                        !level || (level === account.level || (account.level === 2 && level >= 1))
                                )
                                .map((action, key) => (
                                    <Tooltip key={key} placement="top" title={action.tooltip}>
                                        <IconButton
                                            onClick={() => {
                                                action.onClick && action.onClick();
                                                action.route && history.push(action.route);
                                                action.href &&
                                                    window.open(
                                                        `${window.location.protocol}//${
                                                            window.location.hostname
                                                        }:${process.env.REACT_APP_PORT || 9998}${action.href}?token=${
                                                            account.token
                                                        }${action.addition ? '&' + action.addition : ''}`,
                                                        '_blank'
                                                    );
                                            }}
                                            color="primary"
                                            size="medium"
                                        >
                                            <Icon icon={action.icon} />
                                        </IconButton>
                                    </Tooltip>
                                ))}
                    </Paper>
                )}
            </div>
        </div>
    );
}

export default withStyles(toolbarStyles)(Toolbar);
