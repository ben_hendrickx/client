import { container, primaryColor, darkColor } from 'assets/jss/material-kit-react.jsx';

const footerStyle = {
    block: {
        color: 'inherit',
        padding: '0.9375rem',
        fontWeight: '500',
        fontSize: '12px',
        textTransform: 'uppercase',
        borderRadius: '3px',
        textDecoration: 'none',
        position: 'relative',
        display: 'block',
        '@media (max-width: 450px)': {
            padding: '0px',
        },
    },
    left: {
        float: 'left!important',
        display: 'block',
    },
    right: {
        margin: '0',
        float: 'right!important',
    },
    center: {
        width: '100%',
        textAlign: 'center',
    },
    footer: {
        padding: '0.9375rem 0',
        textAlign: 'center',
        display: 'flex',
        zIndex: 9999,
        position: 'relative',
        '@media (max-width: 450px)': {
            width: '100%',
            padding: 0,
            position: 'fixed',
            bottom: 0,
        },
    },
    a: {
        color: primaryColor,
        textDecoration: 'none',
        backgroundColor: 'transparent',
    },
    footerGrayFont: {
        '&,&:hover,&:focus': {
            color: darkColor,
        },
    },
    container: {
        ...container,
        padding: '15px',
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        '@media (max-width: 450px)': {
            background: darkColor,
            color: '#ffffff',
        },
    },
    list: {
        marginBottom: '0',
        padding: '0',
        marginTop: '0',
    },
    inlineFlex: {
        display: 'inline-flex',
        padding: '0px',
        width: 'auto',
        '@media (max-width: 450px)': {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'baseline',
        },
    },
    icon: {
        width: '18px',
        height: '18px',
        position: 'relative',
        top: '3px',
    },
};
export default footerStyle;
