import { withStyles } from '@material-ui/core';
import { darkColor, white, defaultFont } from 'assets/jss/material-kit-react';
import React from 'react';
import { Link } from 'react-router-dom';

const styles = {
    title: {
        ...defaultFont,
        background: darkColor,
        color: white,
        lineHeight: '30px',
        textTransform: 'none',
        padding: '0em 0.2em',
        fontSize: '3.25em',
        letterSpacing: '0.05em',
        borderRadius: '6px',
        fontWeight: 400,
        fontFamily: 'Oleo Script, serif',
        '&:hover, &:focus': {
            background: darkColor,
        },
        '@media (max-width: 450px)': {
            fontSize: '1.5rem',
        },
    },
};

function Brand({ classes, height = 80, style }) {
    return (
        <Link to="/" className={process.env.REACT_APP_LOGO_NAME ? '' : classes.title} style={style || {}}>
            {process.env.REACT_APP_LOGO_NAME ? <img alt="logo" src="https://historalia.testing74.8trust.com/wp-content/uploads/2019/06/Historalia_Finaal_Logo_RGB-brownwhite.png" height={height} /> : process.env.REACT_APP_SITE_TITLE}
        </Link>
    );
}

export default withStyles(styles)(Brand);
