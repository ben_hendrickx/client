import { withStyles } from '@material-ui/styles';
import { white } from '../assets/jss/material-kit-react.jsx';
import React from 'react';

const listRowStyles = {
    listRow: {
        display: 'flex',
        justifyContent: 'space-between',
        background: white,
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        marginBottom: 10,
        minHeight: 50,
        flexDirection: 'column',
        '& > span, & > div > span': {
            display: 'flex',
            alignItems: 'center',
            margin: '10px 10px',
            '& svg': {
                marginRight: 10,
            },
        },
        '@media (min-width: 960px)': {
            alignItems: 'center',
            flexDirection: 'row',
        },
    },
};

function ListRow({ key = '', style = {}, classes, children, onClick = () => {} }) {
    return (
        <div key={key} style={style} className={classes.listRow}>
            {children}
        </div>
    );
}

export default withStyles(listRowStyles)(ListRow);
