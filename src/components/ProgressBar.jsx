import React from 'react';
import { withStyles } from '@material-ui/styles';

const progressBarStyles = {
    container: {
        position: 'relative',
        width: '100%',
        boxSizing: 'border-box',
        margin: '20px 0px',
        background: 'rgba(0,0,0,.1)',
        border: '1px solid rgba(0,0,0,.3)',
        borderRadius: 3,
        height: 40,
        overflow: 'hidden',
        boxShadow: '0px 1px 2px rgba(0,0,0,.3)',
    },
    inner: {
        position: 'absolute',
        overflow: 'visible',
        background: 'rgba(20, 150, 20, .7)',
        height: '100%',
        transition: 'all linear 1s',
        zIndex: 1,
    },
    text: {
        position: 'relative',
        width: '100%',
        textAlign: 'center',
        lineHeight: '36px',
        zIndex: 99,
    },
};

function ProgressBar(props) {
    const { classes, text, progress, style } = props;

    return (
        <div className={classes.container}>
            <div className={classes.inner} style={{ ...(style || {}), width: `${progress || 0}%` }}></div>
            <div className={classes.text}>{text !== '0 %' ? text : 'Voorbereiden'} </div>
        </div>
    );
}

export default withStyles(progressBarStyles)(ProgressBar);
