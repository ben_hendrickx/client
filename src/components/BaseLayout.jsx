import React from 'react';
import Header from 'components/Header/Header.jsx';
import Footer from 'components/Footer/Footer.jsx';
import GridContainer from 'components/Grid/GridContainer.jsx';
import { withStyles } from '@material-ui/styles';
import { container } from 'assets/jss/material-kit-react.jsx';

const styles = {
    container: {
        zIndex: '12',
        color: '#FFFFFF',
        ...container,
        padding: '50px !important',
        background: '#ffffff',
        borderRadius: '5px',
        '@media (max-width: 450px)': {
            boxSizing: 'border-box',
            minHeight: '85vh',
            padding: '100px 50px 150px !important',
            background: 'transparent',
        },
    },
};

function BaseLayout(props) {
    const { children, classes, ...rest } = props;

    return <div>
        <Header fixed color="transparent" {...rest} />
        <div className={classes.container}>
            <GridContainer>
                {children}
            </GridContainer>
        </div>
        <Footer />
    </div>;
}

export default withStyles(styles)(BaseLayout);