import React, { useEffect } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import { observer } from 'mobx-react-lite';
import { useStore } from '../mobx';

import { ArrowBackIos, DateRange, QuestionAnswer } from '@material-ui/icons';
import { useHistory } from 'react-router';
import useReactRouter from 'use-react-router';

import Toolbar from 'components/Toolbar/Toolbar.jsx';
import Loader from 'components/Loader.jsx';

import { primaryCardHeader } from '../assets/jss/material-kit-react.jsx';
import { Badge } from '@material-ui/core';

const pageStyle = {
    container: {
        flexGrow: 1,
        minWidth: '100%',
        height: '100%',
        '& h2': {
            textAlign: 'left',
            fontSize: '1.5em',
            '@media print': {
                fontSize: '1.2em',
            },
        },
    },
    header: {
        ...primaryCardHeader,
        position: 'relative',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 15,
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
    },
    content: {
        marginTop: 10,
        //padding: '0px 10px',
        flexGrow: 1,
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        '@media print': {
            padding: 0,
        },
    },
    messages: {
        position: 'relative',
        padding: '14px 30px',
        borderRight: '1px solid rgba(255,255,255,.3)',
        '&:hover': {
            cursor: 'pointer',
            background: 'rgba(255,255,255,0.2)',
            borderRight: '1px solid rgba(255,255,255,.3)',
        },
    },
};

function Page(props) {
    const {
        classes,
        title = '',
        loading = false,
        toolbar,
        containerStyle = {},
        style = {},
        children,
        small = false,
        showThreads,
    } = props;
    const { ui, threads, account } = useStore();
    const history = useHistory();
    const { location } = useReactRouter();

    useEffect(() => {
        if (showThreads && ui.isMobile) {
            threads.listen();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.id]);

    return (
        <>
            <div className={classes.container} style={containerStyle}>
                {ui.isMobile && !!title && (
                    <div
                        className={classes.header}
                        style={location.pathname === '/overzicht' ? { marginBottom: 10 } : {}}
                    >
                        <ArrowBackIos
                            onClick={() => history.replace('/')}
                            style={{ padding: 0, paddingRight: 15, paddingLeft: 15, boxSizing: 'content-box' }}
                        />
                        <span>{title}</span>
                        <div style={{ display: 'flex' }}>
                            {account.rights.canSeeCalendar && (
                                <div
                                    className={classes.messages}
                                    style={{ border: 0, padding: 0, paddingLeft: 15, paddingRight: 15 }}
                                    onClick={() => {
                                        const projectId = window.location.pathname.split('/')[2];
                                        console.log(window.location);
                                        if (!projectId) {
                                            return history.push('/kalender');
                                        }

                                        return history.push(`/projecten/${projectId}/kalender`);
                                    }}
                                >
                                    <Badge>
                                        <DateRange style={{ marginTop: -4 }} className="calendar" />
                                    </Badge>
                                </div>
                            )}
                            {process.env.REACT_APP_CHAT_ENABLED && account.rights.canReadThreads && showThreads ? (
                                <div
                                    className={classes.messages}
                                    style={{ border: 0, padding: 0, paddingLeft: 15, paddingRight: 15 }}
                                    onClick={() => {
                                        history.push('/gesprekken/overzicht');
                                    }}
                                >
                                    <Badge badgeContent={threads.unread} color="secondary">
                                        <QuestionAnswer className="threads" />
                                    </Badge>
                                </div>
                            ) : (
                                <div style={{ width: 30 }}></div>
                            )}
                        </div>
                    </div>
                )}
                {(!ui.isMobile || toolbar) && !!title && (
                    <Toolbar
                        title={title}
                        style={toolbar ? toolbar?.style : { padding: '10px 0' }}
                        searchProps={toolbar?.searchProps}
                        actions={toolbar?.actions}
                        small={small}
                    />
                )}
                <Loader loading={loading}>
                    <div
                        style={!(toolbar || title) ? { paddingTop: ui.isMobile ? 0 : 20, ...style } : style}
                        className={ui.isMobile ? classes.content : ''}
                    >
                        {children}
                    </div>
                </Loader>
            </div>
        </>
    );
}

export default withStyles(pageStyle)(observer(Page));
