import React, { useState } from 'react';
import { ArtTrack, CameraRoll, FolderOutlined, Group, MusicNote, MusicOff, Person, Subject } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import { Tooltip } from '@material-ui/core';
import { useEffect } from 'react';

const sceneStyle = {
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexGrow: 1,
        textAlign: 'left',
        '& > span': {
            marginRight: 10,
            display: 'flex',
            alignItems: 'center',
        },
        '& i, & svg': {
            marginRight: 10,
        },
    },
};

function Role(props) {
    const { classes, identifier, description, type, users, icon, scene, project } = props;
    const [role, setRole] = useState(props.role);

    useEffect(() => {
        setRole(props.role);
    }, [props.role]);

    const hasUsers = Object.keys(role.users || {}).length > 0;

    return (
        <div className={classes.container}>
            {project && <span style={{ width: 200, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}>
                <FolderOutlined /> {role.project.name}
            </span>}
            {scene && <span style={{ width: 300 }}>
                {role.scene || role.sceneIdentifier ? <><CameraRoll /> <Tooltip title={`${role.scene?.identifier || role.sceneIdentifier} - ${role.scene?.name || role.sceneName} (Cast: ${role.cast.name})`} placement="top">
                    <span
                        style={{
                            width: 265,
                            overflow: 'hidden',
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis',
                        }}
                    >{role.scene?.identifier || role.sceneIdentifier} - {role.scene?.name || role.sceneName} (Cast: {role.cast.name})
                    </span></Tooltip></> : <><CameraRoll /> Geen scène</>}
            </span>}
            <span style={{ maxWidth: 300 }}>
                {icon !== false && (icon === 'group' ? <Group /> : <ArtTrack />)}
                {identifier && <span style={{ marginRight: 10, width: 60, maxWidth: 60 }}>{role.identifier}</span>}
                <Tooltip title={role.name} placement="top">
                    <span
                        style={{
                            width: 190,
                            overflow: 'hidden',
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis',
                        }}
                    >
                        {role.name}
                    </span>
                </Tooltip>
            </span>
            {users &&
                (hasUsers ? (
                    <Tooltip
                        placement="top"
                        title={Object.entries(role.users)
                            .map(
                                ([key, users]) =>
                                    `${key}: ${users.map(user => `${user.lastName} ${user.firstName}`).join(', ')}`
                            )
                            .join(' | ')}
                    >
                        <span
                            style={{
                                width: 200,
                                overflow: 'hidden',
                                whiteSpace: 'nowrap',
                                textOverflow: 'ellipsis',
                            }}
                            className="highlightOnHover"
                        >
                            <Person /> {Object.values(role.users).reduce((acc, users) => (acc += users.length), 0)}{' '}
                            {Object.values(role.users).reduce((acc, users) => (acc += users.length), 0) === 1
                                ? 'medewerker'
                                : 'medewerkers'}
                        </span>
                    </Tooltip>
                ) : (
                    <span
                        style={{
                            width: 200,
                            overflow: 'hidden',
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis',
                        }}
                    >
                        {' '}
                    </span>
                ))}
            <span style={{ paddingTop: 2, marginBottom: 5 }}>
                {description && role.description ? (
                    <span style={{ minWidth: 40 }}>
                        <Tooltip title={role.description} placement="top">
                            <Subject />
                        </Tooltip>
                    </span>
                ) : (
                    <span style={{ minWidth: 40 }}></span>
                )}
                {type && (
                    <span style={{ minWidth: 40, maxWidth: 40 }}>
                        {role.speaking === 1 ? (
                            <Tooltip title="Sprekende rol" placement="top">
                                <MusicNote style={{ color: 'green' }} />
                            </Tooltip>
                        ) : (
                            <Tooltip title="Niet-sprekende rol" placement="top">
                                <MusicOff style={{ color: 'tomato' }} />
                            </Tooltip>
                        )}
                    </span>
                )}
            </span>
        </div>
    );
}

export default withStyles(sceneStyle)(Role);
