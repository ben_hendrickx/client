import { Cancel } from '@material-ui/icons';
import React from 'react';

const Notification = props => {
    if (!props.message) {
        return null;
    }

    return (
        <div
            style={{
                position: 'relative',
                padding: '10px 15px',
                background: props.color,
                color: '#fff',
                marginBottom: 20,
            }}
        >
            {props.message}
            {props.onClose && (
                <div style={{ position: 'absolute', right: 15, top: 9, cursor: 'pointer' }}>
                    <Cancel onClick={() => props.onClose()} />
                </div>
            )}
        </div>
    );
};

const Success = props => <Notification color="#54ab25" {...props} />;
const Warning = props => <Notification color="#fdaf00" {...props} />;
const Danger = props => <Notification color="#f44336" {...props} />;
const Info = props => <Notification color="#64cbd6" {...props} />;

export { Success, Warning, Danger, Info };
