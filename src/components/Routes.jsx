import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useStore } from '../mobx';
import { Switch, Route } from 'react-router-dom';
import useReactRouter from 'use-react-router';

import Home from 'views/Unauthenticated/Home.jsx';
import ChangePasswordPage from 'views/Unauthenticated/ChangePasswordPage.jsx'; //TODO: rewrite
import ForgotPasswordPage from 'views/Unauthenticated/ForgotPasswordPage.jsx'; //TODO: rewrite
import SignUpPage from 'views/Unauthenticated/SignUpPage.jsx';

import Overview from 'views/Dashboard/Overview.jsx';
import Settings from 'views/Settings.jsx';
import ThreadOverview from 'views/Threads/index.jsx';
import Notifications from 'views/Notifications/Overview.jsx';
import Casts from 'views/Casts/Overview.jsx';
import Types from 'views/Types/Overview.jsx';
import Locations from 'views/Locations/Overview.jsx';
import Projects from 'views/Projects/Overview.jsx';
import Project from 'views/Projects/Detail.jsx';
import RegisterProject from 'views/Projects/Register.jsx';
import Registrations from 'views/Registrations/Overview.jsx';
import Users from 'views/Users/Overview.jsx';
import OnlineUsers from 'views/Users/Online.jsx';
import NewUser from 'views/Users/New.jsx';
import Mail from 'views/Users/Mail.jsx';
import ImportUsers from 'views/Users/Import.jsx';
import UserDetail from 'views/Users/Detail.jsx';
import Profile from 'views/Users/Profile.jsx';
import RegistrationDetail from 'views/Registrations/UserDetail.jsx';
import ChangePassword from 'views/Users/ChangePassword.jsx';
import MyRoles from 'views/Roles/My.jsx';
import MyRepetitions from 'views/Repetitions/My.jsx';
import ClothingManagement from 'views/ClothingManagement/Overview.jsx';
import Retributions from 'views/Retributions/Overview.jsx';
import SearchOverview from 'views/SearchOverview';
import NotFound from 'views/NotFound';
import Loader from './Loader';
import Rights from 'views/Rights';
import NewRight from 'views/Rights/New';
import AccessRoleUsers from 'views/Rights/Users';
import Calendar from 'views/Calendar';
import MessagesOverview from 'views/Messages/Overview';
import Message from 'views/Messages/Detail';

const Anonymous = observer(function({ children }) {
    const { account } = useStore();

    return account.loggedIn ? null : children;
});

const Authenticated = observer(function({ children }) {
    const { account } = useStore();

    return account.loggedIn ? children : null;
});

const LoggedOff = function() {
    return (
        <>
            <Route path="/account-aanmaken" component={SignUpPage} />
            <Route path="/nieuw-wachtwoord" component={ChangePasswordPage} />
            <Route path="/wachtwoord-vergeten" component={ForgotPasswordPage} />
            <Route exact path="/" component={Home} />
        </>
    );
};

const LoggedIn = observer(function() {
    const { ui, account } = useStore();
    const { rights } = account;
    const { location } = useReactRouter();

    useEffect(() => {
        if (account.level === 0) {
            if (window.location.pathname.includes('/projecten/')) {
                const projectId = window.location.pathname.split('/').find(piece => !isNaN(parseInt(piece)));
                rights.fetchRightsForProject(parseInt(projectId));
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [window.location.pathname, account.id]); // TODO: check if location.pathname works

    if (ui.isMobile && location.pathname === '/') {
        return null;
    }

    return (
        <Switch>
            <Route exact path="/overzicht" component={Overview} />

            <Route exact path="/projecten/inschrijven/:id" component={RegisterProject} />
            <Route exact path="/projecten/inschrijven/:id/:userId" component={RegisterProject} />
            <Route exact path="/projecten/:id/meldingen" component={Notifications} />

            {(rights.canReadProjects || account.level === 0) && <Route exact path="/projecten" component={Projects} />}
            {rights.canReadProjects && <Route path="/projecten/:id" component={Project} />}

            <Route exact path="/meldingen" component={Notifications} />

            {process.env.REACT_APP_CHAT_ENABLED && <Route path="/gesprekken" component={ThreadOverview} />}

            <Route exact path="/casts" component={Casts} />
            <Route exact path="/repetitietypes" component={Types} />
            <Route exact path="/locaties" component={Locations} />

            <Route path="/kalender" component={Calendar} />
            <Route exact path="/berichten" component={MessagesOverview} />
            <Route path="/berichten/:id" component={Message} />
            <Route path="/rollen" component={MyRoles} />
            <Route path="/repetities" component={MyRepetitions} />

            {account.level === 0 && (
                <Route
                    exact
                    path="/inschrijvingen"
                    component={() => <Registrations apiPrefix={`/user/${account.id}`} />}
                />
            )}
            {account.level === 0 && (
                <Route exact path="/inschrijvingen/:registrationId" component={RegistrationDetail} />
            )}

            {rights.canReadUsers && <Route exact path="/medewerkers" component={Users} />}
            {rights.canReadUsers && <Route exact path="/medewerkers/online" component={OnlineUsers} />}

            {/* TODO: make sure also admins can use this route to create an extra profile for a login */}
            <Route exact path="/medewerkers/nieuw" component={NewUser} />
            <Route exact path="/medewerkers/mail" component={Mail} />
            {rights.canCreateUsers && <Route exact path="/medewerkers/importeren" component={ImportUsers} />}
            {rights.canReadUsers && <Route path="/medewerker/:id" component={UserDetail} />}

            <Route exact path="/profiel" component={Profile} />
            <Route exact path="/verander-wachtwoord" component={ChangePassword} />

            <Route path="/kledijbeheer" component={ClothingManagement} />
            <Route path="/retributiebeheer" component={Retributions} />
            <Route path="/zoekresultaten" component={SearchOverview} />
            <Route exact path="/toegangsrollen" component={Rights} />
            <Route exact path="/toegangsrollen/:id/gebruikers" component={AccessRoleUsers} />
            <Route exact path="/toegangsrollen/nieuw" component={NewRight} />
            <Route exact path="/toegangsrollen/:id/bewerk" component={NewRight} />
            {rights.canReadSettings && <Route exact path="/instellingen" component={Settings} />}
            {account.rights.loading && <Route path="/" component={Loader} />}
            {!ui.isMobile && !account.rights.loading && <Route path="/" component={NotFound} />}
        </Switch>
    );
});

export { Anonymous, Authenticated, LoggedOff, LoggedIn };
