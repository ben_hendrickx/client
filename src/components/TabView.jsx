import React, { useRef, useState } from 'react';
import { withStyles } from '@material-ui/styles';
import { colors } from 'assets/jss/main.jsx';
import { Icon, Tab, Tabs, Tooltip } from '@material-ui/core';
import { Switch, Route } from 'react-router-dom';
import useReactRouter from 'use-react-router';
import NotFound from 'views/NotFound';
import { useEffect } from 'react';
import { useStore } from '../mobx';
import { observer } from 'mobx-react-lite';

const tabStyles = {
    container: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        borderBottom: '1px solid rgba(0,0,0,.1)',
        '@media print': {
            display: 'none',
            border: 0,
        },
        '@media screen and (min-width: 768px)': {
            maxHeight: 38,
        },
    },
    tabs: {
        display: 'flex',
        flexGrow: 1,
        flexWrap: 'wrap',
        '@media print': {
            display: 'block',
        },
    },
    actions: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        '@media print': {
            display: 'none',
        },
        '@media screen and (max-width: 768px)': {
            marginTop: 5,
        },
    },
    action: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: 37,
        width: 37,
        padding: 5,
        marginRight: 5,
        boxSizing: 'border-box',

        border: '1px solid rgba(0,0,0,.1)',
        background: 'rgba(0,0,0,.05)',
        color: '#444',
        borderBottom: 0,
        borderRadius: '3px 3px 0 0',

        '& svg': {
            maxHeight: 27,
        },
        '&:hover': {
            cursor: 'pointer',
            background: colors.primary,
            color: colors.white,
            borderRadius: '3px 3px 0 0',
            border: `1px solid ${colors.primary}`,
            marginBottom: -1,
        },
        '@media screen and (max-width: 768px)': {
            flexGrow: 1,
        },
    },
    content: {
        border: '1px solid rgba(0,0,0,.2)',
        borderTop: 0,
        background: '#eee',
        overflowY: 'auto',
        maxWidth: '100vw',
        '@media print': {
            padding: '0 !important',
            background: 'transparent',
            border: 'none',
            overflowY: 'visible',
        },
    },
    tab: {
        display: 'flex',
        alignItems: 'center',
        padding: '5px 20px',
        borderRadius: '3px 3px 0 0',
        cursor: 'pointer',
        marginRight: 5,
        border: '1px solid rgba(0,0,0,.1)',
        background: 'rgba(0,0,0,.05)',
        color: '#444',
        borderBottom: 0,
        flexGrow: 1,
        gap: 10,
        maxHeight: 38,
        overflowY: 'hidden',
        '& svg': {
            marginTop: 4,
            display: 'block',
            fontSize: 20,
        },
        '@media print': {
            display: 'none',
            border: 0,
        },
        '@media screen and (max-width: 768px)': {
            marginTop: 5,
        },
    },
    activeTab: {
        display: 'flex',
        alignItems: 'center',
        padding: '5px 20px',
        borderRadius: '3px 3px 0 0',
        cursor: 'pointer',
        marginRight: 5,
        border: `1px solid ${colors.primary}`,
        background: colors.primary,
        color: colors.white,
        marginBottom: -1,
        flexGrow: 1,
        gap: 10,
        maxHeight: 38,
        overflowY: 'hidden',
        '& svg': {
            marginTop: 4,
            display: 'block',
            fontSize: 20,
        },
        '& > span': {
            '@media print': {
                display: 'none',
                maxWidth: 0,
            },
        },
        '@media print': {
            display: 'flex',
            alignItems: 'center',
            background: 'transparent',
            margin: 0,
            padding: '20px 0',
            border: 0,
            color: '#666',
            fontSize: '1.5em',
        },
        '@media screen and (max-width: 768px)': {
            marginTop: 5,
        },
    },
};

const InnerTab = withStyles(tabStyles)(props => {
    const { classes } = props;
    return (
        <Tab className={props.active ? classes.activeTab : classes.tab} icon={props.icon ? <Icon>{props.icon}</Icon> : undefined} label={props.label} onClick={props.onClick}/>
    );
});

function TabView(props) {
    const { account } = useStore();
    const { classes, tabs } = props;
    const [heightOffset, setHeightOffset] = useState(170);
    const [tab, setTab] = useState(0);
    const router = useReactRouter();
    const tabViewContentRef = useRef();

    useEffect(() => {
        if (tabViewContentRef.current?.offsetTop) {
            setHeightOffset(tabViewContentRef.current?.offsetTop);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [tabViewContentRef.current]);

    if (!tabs || !tabs.length) {
        return null;
    }

    const currentLocation = router.location.pathname.replace(/ë/, 'e');

    return (
        <div style={props.style || {}} className="tabview">
            <div className={classes.container}>
                <Tabs value={tab} variant="scrollable" scrollButtons="auto" allowScrollButtonsMobile>
                    {tabs
                        .filter(v => v)
                        .map((tab, index) => {
                            const cleanTabName = tab.label.toLowerCase().replace(/ë/, 'e');

                            return (
                                <InnerTab
                                    index={index}
                                    key={`tab-${index}`}
                                    active={
                                        (currentLocation.includes('/' + cleanTabName.toLowerCase()) &&
                                            !currentLocation.includes('/' + cleanTabName.toLowerCase() + '-')) ||
                                        (tab.as && currentLocation.includes('/' + tab.as.toLowerCase()))
                                    }
                                    label={tab.label}
                                    icon={tab.icon}
                                    onClick={() => {
                                        setTab(index);
                                        tab.onClick();
                                    }}
                                />
                            );
                        })}
                </Tabs>
                <div className={classes.actions}>
                    {props.actions &&
                        props.actions
                            .filter(v => v)
                            .map((action, index) => {
                                return (
                                    <Tooltip placement="top" title={action.tooltip} key={`action-${index}`}>
                                        <span className={classes.action} onClick={() => {
                                            action.href &&
                                            window.open(
                                                `${window.location.protocol}//${
                                                    window.location.hostname
                                                }:${process.env.REACT_APP_PORT || 9998}${action.href}?token=${
                                                    account.token
                                                }${action.addition ? '&' + action.addition : ''}`,
                                                '_blank'
                                            );
                                            action.onClick && action.onClick()
                                        }}>
                                            {action.icon}
                                        </span>
                                    </Tooltip>
                                );
                            })}
                </div>
            </div>
            <div ref={tabViewContentRef} className={classes.content} style={{ padding: '1rem', height: `calc(100vh - ${(heightOffset) + (heightOffset > 250 ? 30 : 10)}px - 1rem)` }}>
                <Switch>
                    {props.routes &&
                        props.routes
                            .filter(r => r)
                            .map((route, key) => (
                                <Route
                                    key={`route-${key}`}
                                    exact={route.exact === false ? route.exact : true}
                                    {...route}
                                />
                            ))}
                    <Route path="/" component={NotFound} />
                </Switch>
            </div>
        </div>
    );
}

export default withStyles(tabStyles)(observer(TabView));
