import { Tooltip } from '@material-ui/core';
import { ClearAll, Sort } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import { observer } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react';
import { AutoSizer, List, InfiniteLoader } from 'react-virtualized';
import FilterBar from './FilterBar/FilterBar';
import ListWrapper from './ListWrapper';
import Page from './Page';
import Button from 'components/CustomButtons/Button.jsx';
import { secondaryColor } from '../assets/jss/material-kit-react.jsx';
import { Success } from './Notifications';
import { primaryColor } from 'assets/jss/material-kit-react';
import { useStore } from '../mobx';
import { useDebounce } from 'use-debounce';
import { withTranslation } from 'react-i18next';
import { t } from 'i18next';

const resourceOverviewStyles = {
    clearFilters: {
        minWidth: 'auto !important',
        padding: '15px 5px',
        marginTop: 10,
        marginLeft: 10,
        '& svg:hover': {
            cursor: 'pointer',
            color: secondaryColor,
        },
    },
    sort: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start !important',
        marginTop: 10,
        paddingRight: 20,
        '& > svg': {
            marginRight: 20,
        },
        '@media (min-width: 960px)': {
            marginLeft: 0,
            paddingLeft: 20,
            borderLeft: '1px solid rgba(0,0,0,.1)',
            '& > div': {
                marginTop: -9,
            },
        },
    },
    list: {
        listStyle: 'none',
        margin: '0',
        padding: '0',
    },
};

function ResourceOverview(props) {
    const { account, general } = useStore();
    const [text, setText] = useState('');
    const [debouncedText] = useDebounce(text, 300);

    const {
        classes,
        store,
        rowRenderer,
        heightOffset = 0,
        baseHeight = '100vh',
        search = true,
        actions = [],
        filters,
        rowHeight = 60,
        apiPrefix,
        sort,
        renderBefore = null,
        noAutoSizer = false,
        checkable = false,
        data,
        onClearFilters = () => {},
        backend = false,
        keepFilters = false,
        title,
    } = props;

    const { loading, isFiltering, total, filtered, resource, success, items } = store;

    useEffect(() => {
        return () => {
            store.reset(keepFilters);
            store.success = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (debouncedText !== '' || store.filters[backend ? 'text' : 'plain']) {
            store.filters[backend ? 'text' : 'plain'] = debouncedText;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [debouncedText]);

    useEffect(() => {
        if (account.id) {
            store.offset = 0;
            store.data = [];
            store.status = [];
            store.fetch(apiPrefix);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        // TODO: find way to automate this
        store.filters.inactive,
        store.filters.archive,

        store.filters.read,
        store.filters.category,

        store.filters.text,
        store.filters.sex,
        store.filters.picture,
        store.filters.minAge,
        store.filters.maxAge,

        store.filters.onStage,
        store.filters.auditioning,
        store.filters.remarks,
        store.filters.talents,

        store.filters.project,
        store.filters.scene,
        store.filters.role,
        store.filters.group,
        store.filters.cast,

        store.filters.past,

        store.filters.return,
        store.filters.billed,
        store.filters.returned,

        store.filters.clothingId,
        store.filters.clothingSizeId,
        store.filters.colorId,
        store.filters.fabricId,
        store.filters.patternId,
        store.filters.rackId,
        store.filters.tags,

        store.sort,

        account.id,
    ]);

    const filteredRowCount = items || (data || filtered).length;

    function onExport() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            title: `Exporteer gegevens`,
            body: (
                <>
                    <p>Wenst u alle gegevens te exporteren of enkel voor de geselecteerde filters?</p>
                    <Button
                        onClick={() => {
                            window.location = store.exportUrlWithoutFilters;
                            closeModal();
                        }}
                        color="primary"
                    >
                        Alle gegevens
                    </Button>
                    <Button
                        onClick={() => {
                            window.location = store.exportUrlWithFilters;
                            closeModal();
                        }}
                        color="success"
                    >
                        Enkel filters
                    </Button>
                </>
            ),
        });
    }

    return (
        <>
            <Page
                title={title || t(`resource.${resource.plural}`)}
                loading={loading || props.loading}
                toolbar={{
                    searchProps: search && {
                        value: text,
                        onChange: e => {
                            setText(e.target.value);
                        },
                        placeholder: `${t('form.labels.search')} ${t(`resource.${resource.single}`).toLowerCase()}`,
                    },
                    actions: [
                        ...actions,
                        props.export && {
                            icon: 'getApp',
                            onClick: store.isFiltering
                                ? onExport
                                : () => (window.location = store.exportUrlWithoutFilters),
                            tooltip: 'Download rapport',
                        },
                    ],
                }}
                containerStyle={props.style}
            >
                <Success message={success} onClose={() => (store.success = '')} />
                <FilterBar total={total} filtered={filteredRowCount}>
                    {filters ? (
                        <>
                            {filters}
                            <div className={classes.clearFilters}>
                                <Tooltip
                                    title={t('tooltip.filters.delete')}
                                    placement={window.innerWidth > 959 ? 'top' : 'left'}
                                >
                                    <ClearAll
                                        htmlColor={primaryColor}
                                        fontSize="large"
                                        onClick={() => {
                                            setText('');
                                            onClearFilters();
                                            store.filters = {};
                                        }}
                                        className="remove-filters"
                                    />
                                </Tooltip>
                            </div>
                        </>
                    ) : null}
                    {sort ? (
                        <div className={classes.sort}>
                            <Sort />
                            {sort}
                        </div>
                    ) : null}
                </FilterBar>
                {renderBefore}
                {total === 0 && !props.isFiltering && !isFiltering && (
                    <h2 style={{ textAlign: 'center', marginTop: 70 }}>
                        {t('empty.resource', { resource: t(`resource.${resource.plural}`).toLowerCase() })}
                    </h2>
                )}
                {(props.isFiltering || isFiltering) && (data || filtered).length === 0 && (
                    <h2 style={{ textAlign: 'center', marginTop: 70 }}>
                        {t('empty.resourceWithFilters', { resource: t(`resource.${resource.plural}`).toLowerCase() })}
                    </h2>
                )}
                {!noAutoSizer &&
                    (data || filtered).length > 0 &&
                    (backend ? (
                        <InfiniteLoader
                            isRowLoaded={({ index }) => {
                                return !!store.status[index];
                            }}
                            loadMoreRows={async ({ startIndex, stopIndex }) => {
                                if (store.stop) {
                                    return;
                                }
                                store.offset = startIndex;
                                store.batchSize = stopIndex - startIndex + 1;

                                for (let i = startIndex; i < stopIndex; i++) {
                                    store.status[startIndex] = 'loading';
                                }

                                store.stop = true;
                                await store.fetch(apiPrefix, false);
                                store.stop = false;
                            }}
                            rowCount={filteredRowCount}
                        >
                            {({ registerChild, onRowsRendered }) => (
                                <AutoSizer>
                                    {({ width }) => (
                                        <ListWrapper>
                                            <List
                                                ref={registerChild}
                                                onRowsRendered={onRowsRendered}
                                                width={width}
                                                height={window.innerHeight - heightOffset - (success ? 64 : 0)}
                                                rowCount={filteredRowCount}
                                                rowHeight={rowHeight}
                                                rowRenderer={rowRenderer}
                                            />
                                        </ListWrapper>
                                    )}
                                </AutoSizer>
                            )}
                        </InfiniteLoader>
                    ) : (
                        <AutoSizer>
                            {({ width }) => (
                                <ListWrapper>
                                    <List
                                        width={width}
                                        height={window.innerHeight - heightOffset - (success ? 64 : 0)}
                                        rowCount={filteredRowCount}
                                        rowHeight={rowHeight}
                                        rowRenderer={rowRenderer}
                                    />
                                </ListWrapper>
                            )}
                        </AutoSizer>
                    ))}
                {noAutoSizer && (
                    <div
                        className="scrollContainer"
                        style={{
                            height: `calc(${baseHeight} - ${
                                typeof heightOffset === 'number' ? `${heightOffset}px` : heightOffset
                            })`,
                        }}
                    >
                        <ul className={classes.list}>
                            {(data || filtered).map((_, index) =>
                                rowRenderer({
                                    key: `${resource.single}-${index}`,
                                    index,
                                    style: {
                                        height: rowHeight,
                                    },
                                    checkable,
                                })
                            )}
                        </ul>
                    </div>
                )}
            </Page>
        </>
    );
}

export default withTranslation()(withStyles(resourceOverviewStyles)(observer(ResourceOverview)));
