import { primaryCardHeader, white } from '../../assets/jss/material-kit-react.jsx';

const repetitionStyle = {
    noPrint: {
        '@media print': {
            display: 'none',
        },
    },
    repetition: {
        width: '100%',
        position: 'relative',
        background: white,
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        margin: '26px 10px',
        '@media print': {
            boxShadow: 'none',
            border: '1px solid rgba(0,0,0,.15) !important',
            margin: '15px 3px',
            pageBreakInside: 'avoid',
        },
        '& > h2': {
            ...primaryCardHeader,
            fontSize: '1.2em !important',
            margin: '0 !important',
            fontWeight: 500,
            padding: '15px 20px !important',
            boxShadow: 0,
        },
        '& > section': {
            ...primaryCardHeader,
            boxShadow: 0,
            display: 'flex',
            justifyContent: 'space-between',
            padding: '3px 0px 3px 20px',
            borderBottom: '1px solid rgba(0,0,0,.15)',
            fontWeight: 500,
            flexWrap: 'wrap',
            '& > div': {
                display: 'flex',
                alignItems: 'center',
                color: white,
                padding: '10px 0',
                flexWrap: 'wrap',
                '& svg': {
                    marginRight: 10,
                    '@media print': {
                        display: 'none',
                    },
                },
                '& span': {
                    marginRight: 30,
                    whiteSpace: 'nowrap',
                },
                '@media print': {
                    minWidth: '50%',
                },
            },
        },
        '& > main': {
            padding: '10px 20px',
            '& > div': {
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'space-between',
                background: 'rgba(0,0,0,.05)',
                borderRadius: 3,
                borer: '1px solid rgba(0,0,0,.1)',
                margin: '10px 0',
                padding: 10,
                gap: 10,
                '& > span': {
                    minWidth: 200,
                    maxWidth: '50%',
                    '@media screen and (min-width: 768px)': {
                        '& > span': {
                            maxWidth: '100%',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                            flexGrow: 1,
                        },
                    },
                    display: 'flex',
                    alignItems: 'center',
                    '& svg': {
                        marginRight: 10,
                    },
                },
                '@media print': {
                    '& > span': {
                        display: 'flex',
                        minWidth: '100%',
                        whiteSpace: 'normal',
                    },
                },
            },
        },
    },
};

export default repetitionStyle;
