import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import {
    DateRange,
    Schedule,
    Public,
    CameraRoll,
    ArtTrack,
    Group,
    Visibility,
    VisibilityOff,
} from '@material-ui/icons';
import repetitionStyle from './repetition.style.jsx';
import Button from '../../components/CustomButtons/Button.jsx';

import { getTime } from 'utils.js';
import { Admin, User } from 'components/Levels.jsx';
import { Tooltip } from '@material-ui/core';
import moment from 'moment';
import localization from 'moment/locale/nl-be';

function Repetition(props) {
    const { classes, repetition, markPresent, canCancel = false } = props;

    const isGeneral = !!repetition.allChecked || !!repetition.castChecked;

    return (
        <div className={classes.repetition}>
            <section>
                <div>
                    {repetition.name} ({repetition.type?.name})
                </div>
                <div>
                    <DateRange />{' '}
                    <span>
                        {moment(repetition.startDt)
                            .locale('nl', localization)
                            .format('dddd DD-MM-YYYY')}
                    </span>{' '}
                    <Schedule />
                    <span>
                        {getTime(repetition.startDt)} - {getTime(repetition.endDt)}
                    </span>
                </div>
                <div style={{ paddingRight: 20, display: 'flex', alignItems: 'center' }}>
                    <Public />{' '}
                    <span>
                        {repetition.location?.name} <br className={classes.noPrint} /> {repetition.location?.streetName}{' '}
                        {repetition.location?.houseNumber}, {repetition.location?.zipCode}{' '}
                        {repetition.location?.cityName}
                    </span>
                </div>
                <Admin>
                    <div style={{ borderLeft: '1px solid rgba(0,0,0,.1)', padding: '0 10px' }}>
                        {repetition.open ? (
                            <Visibility style={{ color: 'green' }} />
                        ) : (
                            <VisibilityOff style={{ color: 'tomato' }} />
                        )}
                    </div>
                </Admin>
            </section>
            {(isGeneral || !!['scenes', 'groups', 'roles'].some(key => Object.keys(repetition).includes(key))) && (
                <main>
                    {isGeneral && (
                        <h2 style={{ marginTop: 10 }}>{repetition.description || 'Dit is een algemene repetitie'}</h2>
                    )}
                    {!isGeneral &&
                        repetition.scenes &&
                        repetition.scenes.map(scene => (
                            <>
                                {!scene.groups && !scene.roles && (
                                    <div>
                                        <Tooltip placement="top" title={`${scene.identifier} - ${scene.name}`}>
                                            <span>
                                                <CameraRoll />{' '}
                                                <span>
                                                    {scene.identifier} - {scene.name}
                                                </span>
                                            </span>
                                        </Tooltip>
                                    </div>
                                )}
                                {scene.groups &&
                                    scene.groups.map(group => (
                                        <div>
                                            <Tooltip placement="top" title={`${scene.identifier} - ${scene.name}`}>
                                                <span>
                                                    <CameraRoll />{' '}
                                                    <span>
                                                        {scene.identifier} - {scene.name}
                                                    </span>
                                                </span>
                                            </Tooltip>
                                            <Tooltip placement="top" title={`${group.identifier} - ${group.name}`}>
                                                <span>
                                                    <Group />{' '}
                                                    <span>
                                                        {group.identifier} - {group.name}
                                                    </span>
                                                </span>
                                            </Tooltip>
                                        </div>
                                    ))}
                                {scene.roles &&
                                    scene.roles.map(role => (
                                        <div>
                                            <Tooltip placement="top" title={`${scene.identifier} - ${scene.name}`}>
                                                <span>
                                                    <CameraRoll />{' '}
                                                    <span>
                                                        {scene.identifier} - {scene.name}
                                                    </span>
                                                </span>
                                            </Tooltip>
                                            <Tooltip placement="top" title={`${role.identifier} - ${role.name}`}>
                                                <span>
                                                    <ArtTrack />{' '}
                                                    <span>
                                                        {role.identifier} - {role.name}
                                                    </span>
                                                </span>
                                            </Tooltip>
                                        </div>
                                    ))}
                            </>
                        ))}
                    {!isGeneral &&
                        repetition.groups &&
                        repetition.groups.map(group => (
                            <div>
                                <Tooltip placement="top" title={`${group.identifier} - ${group.name}`}>
                                    <span>
                                        <Group />{' '}
                                        <span>
                                            {group.identifier} - {group.name}
                                        </span>
                                    </span>
                                </Tooltip>
                            </div>
                        ))}
                    {!isGeneral &&
                        repetition.roles &&
                        repetition.roles.map(role => (
                            <div>
                                <Tooltip placement="top" title={`${role.identifier} - ${role.name}`}>
                                    <span>
                                        <ArtTrack />{' '}
                                        <span>
                                            {role.identifier} - {role.name}
                                        </span>
                                    </span>
                                </Tooltip>
                            </div>
                        ))}
                </main>
            )}
            {canCancel && new Date(repetition.startDt).getTime() > new Date().getTime() + 30 * 60 * 1000 && (
                <User>
                    <div
                        className={classes.noPrint}
                        style={{
                            background: 'rgba(0,0,0,.05)',
                            padding: '10px 20px',
                            borderTop: '1px solid rgba(0,0,0,.1)',
                        }}
                    >
                        {repetition.presence ? (
                            <div>
                                <b style={{ marginRight: 20 }}>Klik op deze knop als je niet aanwezig kan zijn:</b>
                                <Button
                                    onClick={async () => {
                                        await markPresent(false);
                                    }}
                                    color="danger"
                                    size="sm"
                                >
                                    Ik kan niet aanwezig zijn
                                </Button>
                            </div>
                        ) : (
                            <div>
                                <b style={{ marginRight: 20 }}>Klik op deze knop als je aanwezig kan zijn:</b>
                                <Button
                                    onClick={async () => {
                                        await markPresent(true);
                                    }}
                                    color="success"
                                    size="sm"
                                >
                                    Ik kan aanwezig zijn
                                </Button>
                            </div>
                        )}
                    </div>
                </User>
            )}
        </div>
    );
}

export default withStyles(repetitionStyle)(Repetition);
