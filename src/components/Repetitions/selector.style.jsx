import { primaryCardHeader } from '../../assets/jss/material-kit-react.jsx';

const selectorStyle = {
    row: {
        display: 'flex',
        justifyContent: 'space-between',
        textAlign: 'center',
        padding: '0 10px',
        '& h2': {
            fontSize: '1em !important',
            margin: '0 !important',
            textAlign: 'justify',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            alignItems: 'center',
            display: 'flex',
            '& svg': {
                marginRight: 10,
            },
        },
        '& > div': {
            borderRight: '1px solid #ddd',
            textAlign: 'center',
            flexGrow: 1,
        },
        '& > div:first-child': {
            width: 200,
        },
        '& > div:last-child': {
            border: 0,
        },
    },
    column: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    toggle: {
        position: 'absolute',
        right: 20,
        top: 10,
        padding: '5px 10px',
        borderRadius: '50%',
        height: 40,
        '&:hover': {
            background: 'rgba(0,0,0,.1)',
            cursor: 'pointer',
        },
    },
    selector: {
        marginBottom: 30,
        '& > h2': {
            ...primaryCardHeader,
            padding: '10px 20px',
            marginBottom: 0,
            position: 'relative',
        },
    },
    content: {},
};

export default selectorStyle;
