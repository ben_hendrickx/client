import React, { useState, useEffect } from 'react';
import selectorStyle from './selector.style.jsx';
import { ArrowDropUp, ArrowDropDown, CameraRoll, Group as GroupIcon, ArtTrack } from '@material-ui/icons';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Checkbox from '@material-ui/core/Checkbox';
import withStyles from '@material-ui/core/styles/withStyles';
import Loader from 'components/Loader.jsx';

const GroupUser = withStyles(selectorStyle)(
    observer(props => {
        const { classes, user, sceneIndex, groupIndex, index, onChange } = props;
        const { repetitions } = useStore();

        if (!repetitions.selectorData) {
            return null;
        }

        const Row = props => <div className={classes.row}>{props.children}</div>;
        const Column = props => (
            <div className={classes.column} style={props.style}>
                {props.children}
            </div>
        );

        const toggleCast = (castId, checked) => {
            const castIndex = user.casts.findIndex(cast => cast && cast.id === castId);
            const cast = { ...user.casts[castIndex], checked };
            const newCasts = user.casts.slice(0);
            newCasts.splice(castIndex, 1, cast);
            user.casts = newCasts;
            user.checked = newCasts.every(cast => !cast || cast.checked);
            onChange(user);
        };

        const toggleAllCasts = checked => {
            const newCasts = user.casts.slice(0);
            user.casts = newCasts.map(cast => {
                cast.checked = checked;
                return cast;
            });
            user.checked = checked;
            onChange(user);
        };

        return (
            <>
                <Row>
                    <div>
                        <Column style={{ paddingLeft: 40 }}>
                            <h2>
                                {user.lastName} {user.firstName}
                            </h2>
                            <Checkbox
                                checked={user.checked}
                                onChange={e => {
                                    toggleAllCasts(e.target.checked);
                                }}
                            />
                        </Column>
                    </div>
                    {user.casts.map((cast, i) => (
                        <div
                            style={{ maxWidth: 165, minWidth: 164 }}
                            key={`scene-${sceneIndex}-group-${groupIndex}-user-${index}-cast${i}`}
                        >
                            <div>
                                {cast && (
                                    <Checkbox
                                        checked={cast.checked}
                                        onChange={e => {
                                            toggleCast(cast.id, e.target.checked);
                                        }}
                                    />
                                )}
                            </div>
                        </div>
                    ))}
                    <div
                        style={{
                            maxWidth: 40,
                        }}
                    ></div>
                </Row>
            </>
        );
    })
);

const Group = withStyles(selectorStyle)(
    observer(props => {
        const { classes, group, sceneIndex, index, onChange } = props;
        const { repetitions } = useStore();

        if (!repetitions.selectorData) {
            return null;
        }

        const isOpen = repetitions.open === group.identifier;

        const Row = props => (
            <div className={classes.row} style={props.style}>
                {props.children}
            </div>
        );
        const Column = props => (
            <div className={classes.column} style={props.style}>
                {props.children}
            </div>
        );

        const toggleCast = (castId, checked) => {
            const castIndex = group.casts.findIndex(({ id }) => id === castId);
            const cast = { ...group.casts[castIndex], checked };
            const newCasts = group.casts.slice(0);
            newCasts.splice(castIndex, 1, cast);
            group.casts = newCasts;
            group.checked = newCasts.every(({ checked }) => checked);

            group.users.forEach(user => {
                user.checked = group.checked;
            });

            onChange(group);
        };

        const toggleAllCasts = checked => {
            const newCasts = group.casts.slice(0);
            group.casts = newCasts.map(cast => {
                cast.checked = checked;
                return cast;
            });
            group.checked = checked;
            onChange(group);
        };

        const calculate = user => {
            if (!user.checked) {
                group.checked = false;
            } else if (group.users.every(({ checked }) => checked)) {
                group.checked = true;
            }

            group.casts = group.casts.map(cast => {
                cast.checked = group.users.every(user => {
                    return !!user.casts.find(({ id, checked }) => id === cast.id && checked);
                });
                return cast;
            });
            onChange(group);
        };

        return (
            <>
                <Row style={repetitions.isPartiallyChecked({ group }) ? { background: 'rgba(247, 189, 69, .3)' } : {}}>
                    <div>
                        <Column style={{ paddingLeft: 20 }}>
                            <h2>
                                <GroupIcon /> {group.identifier} - {group.name}
                            </h2>
                            <Checkbox
                                checked={group.checked}
                                onChange={e => {
                                    toggleAllCasts(e.target.checked);
                                    group.users.forEach(user => {
                                        user.checked = e.target.checked;
                                        user.casts = user.casts.map(cast => {
                                            cast.checked = e.target.checked;
                                            return cast;
                                        });
                                    });
                                }}
                            />
                        </Column>
                    </div>
                    {group.casts.map((cast, i) => (
                        <div
                            style={{ maxWidth: 165, minWidth: 164 }}
                            key={`scene-${sceneIndex}-group-${index}-cast${i}`}
                        >
                            <div>
                                <Checkbox
                                    checked={cast.checked}
                                    onChange={e => {
                                        toggleCast(cast.id, e.target.checked);
                                        group.users.forEach(({ casts }) => {
                                            casts.find(({ id }) => id === cast.id).checked = e.target.checked;
                                        });
                                    }}
                                />
                            </div>
                        </div>
                    ))}
                    <div
                        onClick={() =>
                            group.users.length > 0 &&
                            (repetitions.open =
                                repetitions.open === props.group.identifier ? null : props.group.identifier)
                        }
                        style={{
                            maxWidth: 40,
                            paddingTop: 10,
                            fontSize: 30,
                            cursor: 'pointer',
                        }}
                    >
                        {group.users && group.users.length > 0 && (isOpen ? <ArrowDropUp /> : <ArrowDropDown />)}
                    </div>
                </Row>
                {isOpen && group.users && group.users.length > 0 && (
                    <div>
                        {group.users.map((user, i) => {
                            return (
                                <GroupUser
                                    sceneIndex={sceneIndex}
                                    groupIndex={index}
                                    index={i}
                                    user={user}
                                    key={`scene-${sceneIndex}-group-${index}-user-${i}`}
                                    onChange={calculate}
                                />
                            );
                        })}
                    </div>
                )}
            </>
        );
    })
);

const Role = withStyles(selectorStyle)(
    observer(props => {
        const { classes, role, sceneIndex, index, onChange } = props;
        const { repetitions } = useStore();

        if (!repetitions.selectorData) {
            return null;
        }

        const isOpen = repetitions.open === role.identifier;

        const Row = props => (
            <div className={classes.row} style={props.style}>
                {props.children}
            </div>
        );
        const Column = props => (
            <div className={classes.column} style={props.style}>
                {props.children}
            </div>
        );

        const toggleCast = (castId, checked) => {
            const castIndex = role.casts.findIndex(({ id }) => id === castId);
            const cast = { ...role.casts[castIndex], checked };
            const newCasts = role.casts.slice(0);
            newCasts.splice(castIndex, 1, cast);
            role.casts = newCasts;
            role.checked = newCasts.every(({ checked }) => checked);

            role.users.forEach(user => {
                user.checked = role.checked;
            });

            onChange(role);
        };

        const toggleAllCasts = checked => {
            const newCasts = role.casts.slice(0);
            role.casts = newCasts.map(cast => {
                cast.checked = checked;
                return cast;
            });
            role.checked = checked;
            onChange(role);
        };

        const calculate = user => {
            if (!user.checked) {
                role.checked = false;
            } else if (role.users.every(({ checked }) => checked)) {
                role.checked = true;
            }

            role.casts = role.casts.map(cast => {
                const usersWithCast = role.users.filter(user =>
                    user.casts.filter(v => v).some(userCast => userCast.id === cast.id)
                );
                cast.checked =
                    usersWithCast.length &&
                    usersWithCast.every(user =>
                        user.casts.filter(v => v).some(userCast => userCast.id === cast.id && userCast.checked)
                    );
                return cast;
            });
            onChange(role);
        };

        return (
            <>
                <Row style={repetitions.isPartiallyChecked({ role }) ? { background: 'rgba(247, 189, 69, .3)' } : {}}>
                    <div>
                        <Column style={{ paddingLeft: 20 }}>
                            <h2>
                                <ArtTrack /> {role.identifier} - {role.name}
                            </h2>
                            <Checkbox
                                checked={role.checked}
                                onChange={e => {
                                    toggleAllCasts(e.target.checked);
                                    role.users.forEach(user => {
                                        user.checked = e.target.checked;
                                        user.casts = user.casts.map(cast => {
                                            cast.checked = e.target.checked;
                                            return cast;
                                        });
                                    });
                                }}
                            />
                        </Column>
                    </div>
                    {role.casts.map((cast, i) => (
                        <div
                            style={{ maxWidth: 165, minWidth: 164 }}
                            key={`scene-${sceneIndex}-group-${index}-cast${i}`}
                        >
                            <div>
                                <Checkbox
                                    checked={cast.checked}
                                    onChange={e => {
                                        toggleCast(cast.id, e.target.checked);
                                        role.users.forEach(({ casts }) => {
                                            const userCast = casts.filter(v => v).find(({ id }) => id === cast.id);
                                            userCast && (userCast.checked = e.target.checked);
                                        });
                                    }}
                                />
                            </div>
                        </div>
                    ))}
                    <div
                        onClick={() =>
                            role.users &&
                            role.users.length > 0 &&
                            (repetitions.open =
                                repetitions.open === props.role.identifier ? null : props.role.identifier)
                        }
                        style={{
                            maxWidth: 40,
                            paddingTop: 10,
                            fontSize: 30,
                            cursor: 'pointer',
                        }}
                    >
                        {role.users && role.users.length > 0 && (isOpen ? <ArrowDropUp /> : <ArrowDropDown />)}
                    </div>
                </Row>
                {isOpen && role.users && role.users.length > 0 && (
                    <div>
                        {role.users.map((user, i) => {
                            return (
                                <GroupUser
                                    sceneIndex={sceneIndex}
                                    groupIndex={index}
                                    index={i}
                                    user={user}
                                    key={`scene-${sceneIndex}-group-${index}-user-${i}`}
                                    onChange={calculate}
                                />
                            );
                        })}
                    </div>
                )}
            </>
        );
    })
);

const Scene = withStyles(selectorStyle)(
    observer(props => {
        const { classes, scene, index, onChange } = props;
        const { repetitions } = useStore();

        if (!repetitions.selectorData) {
            return null;
        }

        const scenes = repetitions.selectorData.scenes;

        const isOpen = props.open === props.scene.id;
        const openStyle = isOpen
            ? {
                  background: '#fafafa',
                  borderBottom: '2px solid #ddd',
                  borderTop: '2px solid #ddd',
              }
            : repetitions.isPartiallyChecked({ scene })
            ? { background: 'rgba(247, 189, 69, .3)' }
            : {};

        const Row = props => <div className={classes.row}>{props.children}</div>;
        const Column = props => <div className={classes.column}>{props.children}</div>;

        const toggleCast = (castId, checked) => {
            const castIndex = scene.casts.findIndex(({ id }) => id === castId);
            const cast = { ...scene.casts[castIndex], checked };
            const newCasts = scene.casts.slice(0);
            newCasts.splice(castIndex, 1, cast);
            scene.casts = newCasts;
            scene.checked = newCasts.every(({ checked }) => checked);

            scene.groups.forEach(group => {
                group.checked = scene.checked;
                group.users.forEach(user => {
                    user.checked = scene.checked;
                });
            });

            scene.roles.forEach(role => {
                role.checked = scene.checked;
                role.users.forEach(user => {
                    user.checked = scene.checked;
                });
            });

            onChange(scene);
        };

        const toggleAllCasts = checked => {
            const newCasts = scene.casts.slice(0);
            scene.casts = newCasts.map(cast => {
                cast.checked = checked;
                return cast;
            });
            scene.checked = checked;
            onChange(scene);
        };

        const calculateGroup = group => {
            if (!group.checked) {
                scene.checked = false;
            } else if (scene.groups.every(({ checked }) => checked) && scene.roles.every(({ checked }) => checked)) {
                scene.checked = true;
            }

            scene.casts = scene.casts.map(cast => {
                cast.checked =
                    scene.groups.every(group => {
                        return !!group.casts.find(({ id, checked }) => id === cast.id && checked);
                    }) &&
                    scene.roles.every(role => {
                        return !!role.casts.find(({ id, checked }) => id === cast.id && checked);
                    });
                return cast;
            });
            onChange(scene);
        };

        return (
            <div style={openStyle}>
                <Row>
                    <div>
                        <Column>
                            <h2>
                                <CameraRoll /> {scene.identifier} - {scene.name}
                            </h2>
                            <Checkbox
                                checked={scenes[index].checked || false}
                                onChange={e => {
                                    toggleAllCasts(e.target.checked);
                                    scene.groups.forEach(group => {
                                        group.checked = e.target.checked;
                                        group.casts = group.casts.map(cast => {
                                            cast.checked = e.target.checked;
                                            return cast;
                                        });
                                        group.users.forEach(user => {
                                            user.checked = e.target.checked;
                                            user.casts = user.casts.map(cast => {
                                                cast.checked = e.target.checked;
                                                return cast;
                                            });
                                        });
                                    });

                                    scene.roles.forEach(role => {
                                        role.checked = e.target.checked;
                                        role.casts = role.casts.map(cast => {
                                            cast.checked = e.target.checked;
                                            return cast;
                                        });
                                        role.users.forEach(user => {
                                            user.checked = e.target.checked;
                                            user.casts = user.casts.map(cast => {
                                                cast.checked = e.target.checked;
                                                return cast;
                                            });
                                        });
                                    });
                                }}
                            />
                        </Column>
                    </div>
                    {scene.casts.map(cast => (
                        <div style={{ maxWidth: 165, minWidth: 164 }} key={`scene-${scene.id}-cast-${cast.id}`}>
                            <div>
                                <Checkbox
                                    checked={cast.checked}
                                    onChange={e => {
                                        toggleCast(cast.id, e.target.checked);
                                        scene.groups.forEach(({ casts, users }) => {
                                            casts.find(({ id }) => id === cast.id).checked = e.target.checked;
                                            users.forEach(({ casts }) => {
                                                casts.find(({ id }) => id === cast.id).checked = e.target.checked;
                                            });
                                        });

                                        scene.roles.forEach(({ casts, users }) => {
                                            casts.find(({ id }) => id === cast.id).checked = e.target.checked;
                                            users.forEach(({ casts }) => {
                                                const userCast = casts.filter(v => v).find(({ id }) => id === cast.id);
                                                userCast && (userCast.checked = e.target.checked);
                                            });
                                        });
                                    }}
                                />
                            </div>
                        </div>
                    ))}
                    <div
                        onClick={() =>
                            (scene.groups.length > 0 || scene.roles.length > 0) && props.onToggle(props.scene.id)
                        }
                        style={{ maxWidth: 40, paddingTop: 10, fontSize: 30, cursor: 'pointer' }}
                    >
                        {(scene.groups.length > 0 || scene.roles.length > 0) &&
                            (isOpen ? <ArrowDropUp /> : <ArrowDropDown />)}
                    </div>
                </Row>
                {isOpen && scene.groups && scene.groups.length > 0 && (
                    <div className={classes.row} style={{ background: '#ddd' }}>
                        <h2>
                            <b>Groepen</b>
                        </h2>
                    </div>
                )}
                {isOpen && scene.groups && scene.groups.length > 0 && (
                    <div>
                        {scene.groups.map((group, i) => {
                            return (
                                <Group
                                    sceneIndex={index}
                                    groupIndex={i}
                                    group={group}
                                    key={`scene-${index}-group-${i}`}
                                    onChange={calculateGroup}
                                />
                            );
                        })}
                    </div>
                )}
                {isOpen && scene.roles && scene.roles.length > 0 && (
                    <div className={classes.row} style={{ background: '#ddd' }}>
                        <h2>
                            <b>Rollen</b>
                        </h2>
                    </div>
                )}
                {isOpen && scene.roles && scene.roles.length > 0 && (
                    <div>
                        {scene.roles.map((role, i) => {
                            return (
                                <Role
                                    sceneIndex={index}
                                    roleIndex={i}
                                    role={role}
                                    key={`scene-${index}-role-${i}`}
                                    onChange={calculateGroup}
                                />
                            );
                        })}
                    </div>
                )}
            </div>
        );
    })
);

function Selector(props) {
    const { classes } = props;
    const { repetitions } = useStore();
    const { selectorData: data } = repetitions;

    const [allChecked, checkAll] = useState(false);
    const [casts, setCasts] = useState([]);

    const [scenesOpen, setScenesOpen] = useState(false);
    const [sceneOpen, setSceneOpen] = useState(false);

    useEffect(() => {
        console.log(casts.length);
        casts.length > 0 && repetitions.allChecked && checkAll(repetitions.allChecked);
        casts.length > 0 && repetitions.allChecked && toggleAllCasts(true);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [casts.length]);

    useEffect(() => {
        if (repetitions.allChecked) {
            data &&
                data.scenes.forEach(scene => {
                    scene.checked = repetitions.allChecked;

                    scene.casts.forEach(cast => {
                        cast.checked = repetitions.allChecked;
                    });
                    scene.groups.forEach(group => {
                        group.checked = repetitions.allChecked;
                        group.users.forEach(user => {
                            user.checked = repetitions.allChecked;

                            user.casts.forEach(cast => {
                                cast.checked = repetitions.allChecked;
                            });
                        });

                        group.casts.forEach(cast => {
                            cast.checked = repetitions.allChecked;
                        });
                    });

                    scene.roles.forEach(role => {
                        role.checked = repetitions.allChecked;
                        role.users.forEach(user => {
                            user.checked = repetitions.allChecked;

                            user.casts.forEach(cast => {
                                cast.checked = repetitions.allChecked;
                            });
                        });

                        role.casts.forEach(cast => {
                            cast.checked = repetitions.allChecked;
                        });
                    });
                });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data && data.scenes && data.scenes.length]);

    const toggleCast = (castId, checked, update = true) => {
        const castIndex = casts.findIndex(({ id }) => id === castId);
        const cast = { ...casts[castIndex], checked };
        const newCasts = casts.slice(0);
        newCasts.splice(castIndex, 1, cast);
        setCasts(newCasts);

        const allCastsChecked = newCasts.every(({ checked }) => checked);
        update && checkAll(allCastsChecked);

        update &&
            scenes.forEach(scene => {
                scene.checked = allCastsChecked;
                scene.groups.forEach(group => {
                    group.checked = allCastsChecked;
                    group.users.forEach(user => {
                        user.checked = allCastsChecked;
                    });
                });

                scene.roles.forEach(role => {
                    role.checked = allCastsChecked;
                    role.users.forEach(user => {
                        user.checked = allCastsChecked;
                    });
                });
            });
    };

    const toggleAllCasts = checked => {
        const newCasts = casts.slice(0);
        setCasts(
            newCasts.map(cast => {
                cast.checked = checked;
                return cast;
            })
        );
    };

    if (!repetitions.selectorData) {
        return <Loader loading={true} />;
    }

    if (!casts.length) {
        setCasts(data.casts);
        checkAll(data.casts.every(({ checked }) => checked));
    }

    const calculate = scene => {
        if (!scene.checked) {
            checkAll(false);
        } else if (scenes.every(({ checked }) => checked)) {
            checkAll(true);
            setCasts(
                casts.slice(0).map(cast => {
                    cast.checked = true;
                    return cast;
                })
            );
            return;
        }

        setCasts(
            casts.slice(0).map(cast => {
                cast.checked = scenes.every(scene => {
                    return !!scene.casts.find(({ id, checked }) => id === cast.id && checked);
                });
                return cast;
            })
        );
    };

    const Row = props => <div className={classes.row}>{props.children}</div>;
    const Column = props => <div className={classes.column}>{props.children}</div>;

    const scenes = repetitions.selectorData.scenes;

    const View = props => {
        return (
            <div className={classes.selector}>
                <h2>
                    <span>{props.title}</span>
                    <span className={classes.toggle} onClick={() => props.onToggle(!props.open)}>
                        {props.open ? <ArrowDropUp /> : <ArrowDropDown />}
                    </span>
                </h2>
                {props.open && <div className={classes.content}>{props.children}</div>}
            </div>
        );
    };

    return (
        <>
            <Row>
                <div style={{ flexGrow: 0, border: 0 }}>
                    <Column>
                        <h2>Iedereen</h2>
                        <Checkbox
                            checked={allChecked}
                            onChange={e => {
                                checkAll(e.target.checked);
                                toggleAllCasts(e.target.checked);
                                scenes.forEach(scene => {
                                    scene.checked = e.target.checked;
                                    scene.casts = scene.casts.map(cast => {
                                        cast.checked = e.target.checked;
                                        return cast;
                                    });

                                    scene.groups.forEach(group => {
                                        group.checked = e.target.checked;
                                        group.casts = group.casts.map(cast => {
                                            cast.checked = e.target.checked;
                                            return cast;
                                        });
                                        group.users.forEach(user => {
                                            user.checked = e.target.checked;
                                            user.casts = user.casts.map(cast => {
                                                cast.checked = e.target.checked;
                                                return cast;
                                            });
                                        });
                                    });

                                    scene.roles.forEach(role => {
                                        role.checked = e.target.checked;
                                        role.casts = role.casts.map(cast => {
                                            cast.checked = e.target.checked;
                                            return cast;
                                        });
                                        role.users.forEach(user => {
                                            user.checked = e.target.checked;
                                            user.casts = user.casts.map(cast => {
                                                cast.checked = e.target.checked;
                                                return cast;
                                            });
                                        });
                                    });
                                });
                            }}
                        />
                    </Column>
                </div>
                {casts.map((cast, i) => (
                    <div style={{ flexGrow: 0, border: 0 }} key={`cast-${i}`}>
                        <Column>
                            <h2>Cast {cast.name}</h2>
                            <Checkbox
                                checked={cast.checked || false}
                                onChange={e => {
                                    toggleCast(cast.id, e.target.checked);
                                    scenes.forEach(({ casts, groups, roles }) => {
                                        casts.find(({ id }) => id === cast.id).checked = e.target.checked;

                                        groups.forEach(({ casts, users }) => {
                                            casts.find(({ id }) => id === cast.id).checked = e.target.checked;

                                            users.forEach(({ casts }) => {
                                                casts.find(({ id }) => id === cast.id).checked = e.target.checked;
                                            });
                                        });

                                        roles.forEach(({ casts, users }) => {
                                            casts.find(({ id }) => id === cast.id).checked = e.target.checked;

                                            users.forEach(({ casts }) => {
                                                const userCast = casts.filter(v => v).find(({ id }) => id === cast.id);
                                                userCast && (userCast.checked = e.target.checked);
                                            });
                                        });
                                    });
                                }}
                            />
                        </Column>
                    </div>
                ))}
            </Row>
            <View
                title={`Scènes (${repetitions.checkedUsers} medewerkers geselecteerd`}
                open={scenesOpen}
                onToggle={open => setScenesOpen(open)}
            >
                <div style={{ border: '1px solid #ddd', borderTop: 0 }}>
                    <div className={classes.row} style={{ background: '#ddd' }}>
                        <div style={{ textAlign: 'left' }}>Scène-naam</div>
                        {casts.map(cast => (
                            <div key={`cast-${cast.id}`} style={{ maxWidth: 165, minWidth: 164 }}>
                                {cast.name}
                            </div>
                        ))}
                    </div>
                    {scenes.map((scene, i) => (
                        <Scene
                            index={i}
                            key={`scene-${scene.id}`}
                            scene={scene}
                            open={sceneOpen}
                            onToggle={id => setSceneOpen(sceneOpen !== id || sceneOpen === null ? id : null)}
                            onChange={calculate}
                        />
                    ))}
                </div>
            </View>
        </>
    );
}

export default withStyles(selectorStyle)(observer(Selector));
