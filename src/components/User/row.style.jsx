import { primaryColor, secondaryColor, white } from 'assets/jss/material-kit-react.jsx';

const userRowStyle = {
    container: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'flex-start',
        background: white,
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        margin: '26px 10px',
        flexWrap: 'wrap',
        '& span.highlightOnHover:hover': {
            cursor: 'pointer',
            fontWeight: 500,
            '& svg': {
                color: primaryColor,
            },
        },
        '& > span': {
            display: 'inline-flex',
            alignItems: 'center',
            minWidth: 200,
            minHeight: 54,
            '&.small': {
                minWidth: 100,
            },
            '&.big': {
                minWidth: 400,
            },
        },
        '& svg': {
            minWidth: 60,
            textAlign: 'center',
            marginRight: '10px',
            '@media (min-width: 960px)': {
                minWidth: 'auto',
                marginRight: '15px',
            },
        },
        '@media (min-width: 960px)': {
            padding: '0 0 0 85px',
            flexWrap: 'nowrap',
            justifyContent: 'space-between',
        },
    },
    thumb: {
        background: white,
        cursor: 'pointer',
        marginRight: 10,
        '& img': {
            display: 'block',
        },
        '& svg': {
            margin: '0 !important',
            display: 'block',
        },
        '@media (min-width: 960px)': {
            position: 'absolute',
            left: 0,
            top: -10,
            zIndex: 1,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            padding: 5,
            borderRadius: 5,
            margin: 0,
        },
    },
    actions: {
        borderLeft: `1px solid ${primaryColor}`,
        padding: '15px 15px 15px 30px',
        display: 'inline-flex',
        alignItems: 'center',
        background: primaryColor,
        color: white,
        flexGrow: 1,
        justifyContent: 'space-evenly',
        '& span:hover': {
            cursor: 'pointer',
            color: secondaryColor,
        },
        '@media (min-width: 960px)': {
            flexGrow: 0,
        },
    },
};

export default userRowStyle;
