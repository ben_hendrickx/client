import React from 'react';
import userRowStyle from './row.style.jsx';

import { AccountCircle, ContactMail, PregnantWoman, DateRange, RemoveRedEye, Schedule, Wc } from '@material-ui/icons';
import Tooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/styles';
import { useHistory } from 'react-router';
import Icon from '@material-ui/core/Icon';
import { Developer } from 'components/Levels.jsx';
import moment from 'moment';
import { withTranslation } from 'react-i18next';
import timeAgoService from 'services/timeAgoService.js';

function UserRow(props) {
    const { key, classes, showImage = true, t } = props;

    const dateOfBirth = moment(props.date_of_birth);
    const birthYear = dateOfBirth.year();
    const age = moment().diff(dateOfBirth, 'years');

    const history = useHistory();
    const watch = props.watch !== undefined ? props.watch : true;
    const details = props.details !== undefined ? props.details : true;

    function openProfile(userId) {
        if (props.projectId) {
            history.push(`/projecten/${props.projectId}/inschrijvingen/${props.registrationId}/gegevens`);
        } else {
            history.push(`/medewerker/${userId}/overzicht`);
        }
    }

    const Placeholder = ({ width = 50 }) => <div style={{ height: 15, width, background: 'rgba(0,0,0,.1)' }}></div>;

    const name = `${props.lastName || ''} ${props.firstName || ''}`;

    return (
        <li key={'li-' + key} className={classes.container} style={props.style || {}}>
            {showImage && (
                <div className={classes.thumb} onClick={props.onClick}>
                    {props.image ? (
                        <img alt="User" src={props.image} height="60" style={{ background: 'rgba(160,160,160,.6)' }} />
                    ) : (
                        <AccountCircle
                            style={{
                                fontSize: 60,
                                background: 'rgba(160,160,160,.6)',
                                color: 'white',
                            }}
                        />
                    )}
                </div>
            )}
            <span
                className={watch ? 'highlightOnHover' : ''}
                onClick={() => watch && openProfile(props.id)}
                style={{ maxWidth: 200 }}
            >
                {name.replace(/\s/g, '') ? name : <Placeholder width={150} />}
            </span>
            {props.since && (
                <span className="big">
                    <Schedule />{' '}
                    {timeAgoService()
                        .get()
                        .format(new Date(props.since))}
                </span>
            )}
            {props.lastActivity && (
                <span className="big">
                    <Schedule />{' '}
                    {timeAgoService()
                        .get()
                        .format(new Date(props.lastActivity))}
                </span>
            )}
            {details && (
                <span className={props.small ? 'small' : 'big'}>
                    <ContactMail /> {props.email || <Placeholder width={200} />}
                </span>
            )}

            {process.env.REACT_APP_IS_FVD === 'true' && details && props.date_of_birth !== false && (
                <span className="small">
                    <PregnantWoman />
                    {birthYear || <Placeholder width={100} />}
                </span>
            )}
            {process.env.REACT_APP_IS_FVD === 'false' && details && props.sex !== undefined && (
                <span className="small" style={{ minWidth: 100 }}>
                    <Wc />
                    {['Man', 'Vrouw', 'X'][props.sex] || <Placeholder width={100} />}
                </span>
            )}
            {props.cast && (
                <span className="small">
                    {t('resource.cast')}: {props.cast}
                </span>
            )}
            {details && props.date_of_birth !== false && (
                <span className="small">
                    <DateRange />
                    {age || <Placeholder />}
                </span>
            )}
            {(watch || (props.actions && props.actions.length)) && (
                <div className={classes.actions}>
                    {watch && (
                        <Tooltip title={t('tooltip.user.view')} placement="top">
                            <RemoveRedEye onClick={() => props.id && openProfile(props.id)} className="view-user" />
                        </Tooltip>
                    )}
                    {props.actions &&
                        props.actions
                            .filter(action => action)
                            .map(({ developer, tooltip, icon, callback }, index) => {
                                const Component = () => (
                                    <Tooltip title={tooltip} placement="top">
                                        <Icon onClick={callback}>{icon}</Icon>
                                    </Tooltip>
                                );

                                return developer ? (
                                    <Developer key={`action-${index}`}>
                                        <Component />
                                    </Developer>
                                ) : (
                                    <Component key={`action-${index}`} />
                                );
                            })}
                    {/* <Icon onClick={() => setImage(user.image)}>image</Icon> */}
                    {/* <Tooltip title="Bewerk gegevens" placement="top">
                    <Icon onClick={() => openEditProfile(props.id)}>edit</Icon>
                </Tooltip>
                <Tooltip title="Archiveer/verwijder gebruiker" placement="top">
                    <Icon onClick={() => setDeleteId(props.id)}>delete</Icon>
                </Tooltip> */}
                </div>
            )}
            {props.children}
        </li>
    );
}

export default withTranslation()(withStyles(userRowStyle)(UserRow));
