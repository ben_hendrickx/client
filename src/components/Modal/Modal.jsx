import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Button from 'components/CustomButtons/Button.jsx';
import { observer } from 'mobx-react-lite';
import { Close, Minimize } from '@material-ui/icons';
import { useStore } from '../../mobx';
import { withTranslation } from 'react-i18next';

const modalStyle = {
    overlay: {
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        background: 'rgba(0,0,0,0.7)',
        '& > div': {
            padding: '50px 100px',
            background: '#ffffff',
            boxShadow:
                '2px 1px 5px -1px rgba(0,0,0,0.3), 0px 0px 1px 0px rgba(0,0,0,0.2), 0px 3px 5px -2px rgba(0,0,0,0.1)',
            borderRadius: 3,
            boxSizing: 'border-box',
            '& h1': {
                margin: 0,
                marginBottom: 10,
                fontSize: '1.5rem',
            },
            '& button': {
                marginRight: 10,
            },
        },
    },
};

function Modal(props) {
    const { classes, modal, index, t } = props;
    const { onClose = () => {}, title, body, onConfirm, buttonText, width, type = 'modal', disabled } = modal;
    const { ui } = useStore();

    let style = { width, position: 'relative' };

    if (ui.isMobile) {
        style.width = '100%';
        style.padding = '50px 30px';
    }

    let overlayStyle = { zIndex: 900 + index };

    if (type === 'window') {
        style = {
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: ui.isMobile ? 0 : 200,
            backgroundColor: '#e7e7e7',
            borderRadius: 0,
            maxHeight: 'calc(100vh - 55px)',
            overflowY: 'auto',
            padding: '0 30px',
        };

        overlayStyle.top = 54;
    }

    return (
        <div className={classes.overlay} onClick={onClose} style={overlayStyle}>
            <div onClick={e => e.stopPropagation()} style={style}>
                <div
                    style={{
                        position: 'absolute',
                        top: 10,
                        right: 40,
                        width: 45,
                        height: 20,
                        cursor: 'pointer',
                        display: 'flex',
                    }}
                >
                    <Minimize onClick={() => modal.minimize()} />
                    <Close onClick={() => modal.close()} className="close" />
                </div>
                {!!title && <h1 style={type === 'window' ? { margin: '10px 0 30px' } : {}}>{title}</h1>}
                <main>{body}</main>
                {onConfirm && (
                    <Button
                        disabled={!!disabled}
                        onClick={async () => {
                            await onConfirm();
                            modal.close();
                        }}
                        color="success"
                        size="md"
                    >
                        {buttonText || t('form.button.confirm')}
                    </Button>
                )}
            </div>
        </div>
    );
}

export default withTranslation()(withStyles(modalStyle)(observer(Modal)));
