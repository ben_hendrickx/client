import React from 'react';
import ReactTags from 'react-tagsinput';
import 'react-tagsinput/react-tagsinput.css';
import Autosuggest from 'react-autosuggest';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function Tags(props) {
    const { suggestions, value, label, placeholder, t } = props;

    function autosuggestRenderInput({ addTag, ...props }) {
        const handleOnChange = (e, { method }) => {
            if (method === 'enter') {
                e.preventDefault();
            } else {
                props.onChange(e);
            }
        };

        return (
            <Autosuggest
                ref={props.ref}
                suggestions={suggestions?.filter(s => s.toLowerCase().includes((props.value || '').toLowerCase()))}
                shouldRenderSuggestions={value => value && value.trim().length > 0}
                getSuggestionValue={suggestion => suggestion.name}
                renderSuggestion={suggestion => <span>{suggestion.split(' (')[0]}</span>}
                inputProps={{ ...props, onChange: handleOnChange, onBlur: () => addTag(props.value) }}
                onSuggestionSelected={(e, { suggestion }) => {
                    addTag(suggestion);
                }}
                onSuggestionsClearRequested={() => {}}
                onSuggestionsFetchRequested={() => {}}
                style={{ display: 'inline-block' }}
            />
        );
    }

    return (
        <div
            style={{
                marginTop: 20,
                padding: 20,
                border: '1px solid rgba(0,0,0,.1)',
                background: 'rgba(0,0,0,.05)',
            }}
        >
            <label style={{ color: '#222' }}>{label || 'Tags:'}</label>
            <ReactTags
                inputProps={{
                    style: { minWidth: 110 },
                    placeHolder: placeholder || t('form.placeholder.tags'),
                }}
                value={value || []}
                renderInput={autosuggestRenderInput}
                onChange={props.onChange}
            />
        </div>
    );
}

export default withTranslation()(observer(Tags));
