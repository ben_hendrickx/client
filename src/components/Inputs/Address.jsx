import React, { useEffect, useState } from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';
import { geocodeByAddress } from 'react-places-autocomplete';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import { withStyles } from '@material-ui/styles';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';
import { t } from 'i18next';

const classes = {
    link: {
        '&:hover': {
            cursor: 'pointer',
            textDecoration: 'underline',
        },
    },
};

function Address(props) {
    const { store, classes } = props;
    const [address, setAddress] = useState('');
    const [addrTemp, setAddrTemp] = useState('');
    const [manual, setManual] = useState(false);

    useEffect(() => {
        const tmp = `${store.streetName} ${store.houseNumber}, ${store.zipCode} ${store.cityName}`
            .replace(/undefined/g, '')
            .replace(' ,  ', '');
        setAddrTemp(tmp);
    }, [store.streetName, store.houseNumber, store.zipCode, store.cityName]);

    useEffect(() => {
        address &&
            geocodeByAddress(address)
                .then(results => {
                    console.log(results);
                    if (results && results.length) {
                        const addr = results[0].address_components;

                        const getValue = key => (addr.find(({ types }) => types.includes(key)) || {}).long_name;

                        store.houseNumber = getValue('street_number') || (/(\d{1,3}),/.exec(address) || [])[1];
                        store.zipCode = getValue('postal_code');
                        store.streetName = getValue('route');
                        let cityName = getValue('sublocality');
                        if (!cityName) {
                            cityName = getValue('locality');
                        } else if (getValue('locality') !== getValue('sublocality')) {
                            cityName += `, ${getValue('locality')}`;
                        }
                        store.cityName = cityName;

                        const { streetName, houseNumber, zipCode, cityName: storeCityName } = store;

                        if (streetName && houseNumber && zipCode && storeCityName) {
                            delete store.error;
                            setAddrTemp(`${streetName} ${houseNumber}, ${zipCode} ${storeCityName}`);
                        } else {
                            setAddrTemp(address);
                            const missing = [];
                            !streetName && missing.push(t('form.labels.street'));
                            !houseNumber && missing.push(t('form.labels.houseNumber'));
                            (!storeCityName || !zipCode) && missing.push(t('form.labels.city'));
                            store.error = t('form.error.missingAddressFields') + missing.join(', ');
                        }
                    }
                })
                .catch(error => console.error('Error', error));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [address]);

    const { cityName, streetName, zipCode, houseNumber } = store;

    return (
        <>
            {!manual && (
                <PlacesAutocomplete
                    value={addrTemp}
                    onChange={setAddrTemp}
                    onSelect={e => {
                        setAddress(e);
                    }}
                >
                    {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => {
                        return (
                            <div className="places">
                                <CustomInput
                                    labelText={t('form.labels.address')}
                                    id="ad_FVD"
                                    formControlProps={{
                                        required: true,
                                        error:
                                            (!!cityName || !!streetName || !!zipCode || !!houseNumber) &&
                                            !(cityName && streetName && zipCode && houseNumber),
                                        fullWidth: true,
                                    }}
                                    inputProps={{
                                        ...getInputProps({
                                            placeholder: t('form.placeholder.fullAddress'),
                                            className: '',
                                        }),
                                        color: 'primary',
                                        name: 'address',
                                        autoComplete: 'street-address',
                                        required: true,
                                        type: 'text',
                                    }}
                                />
                                <div className={loading || suggestions.length ? 'autocomplete-dropdown-container' : ''}>
                                    {loading && <div>{t('form.labels.patient')}</div>}
                                    {!loading &&
                                        suggestions.map(suggestion => {
                                            const { active, description } = suggestion;
                                            const className = active ? 'suggestion-item--active' : 'suggestion-item';
                                            const style = active
                                                ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                                : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                            return (
                                                <div
                                                    {...getSuggestionItemProps(suggestion, {
                                                        className,
                                                        style,
                                                    })}
                                                >
                                                    <span>{description}</span>
                                                </div>
                                            );
                                        })}
                                </div>
                            </div>
                        );
                    }}
                </PlacesAutocomplete>
            )}
            {manual && (
                <>
                    <div style={{ display: 'flex' }}>
                        <div style={{ width: 250, marginRight: 10 }}>
                            <CustomInput
                                labelText={t('form.labels.street')}
                                id="ad_FVD"
                                formControlProps={{
                                    required: true,
                                    error:
                                        (!!cityName || !!streetName || !!zipCode || !!houseNumber) &&
                                        !(cityName && streetName && zipCode && houseNumber),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    name: 'address',
                                    autoComplete: 'street-address',
                                    required: true,
                                    type: 'text',
                                    onChange: e => (store.streetName = e.target.value),
                                    value: store.streetName,
                                }}
                            />
                        </div>
                        <div style={{ maxWidth: 80, marginRight: 10 }}>
                            <CustomInput
                                labelText={t('form.labels.houseNumber')}
                                id="ad_FVD"
                                formControlProps={{
                                    required: true,
                                    error:
                                        (!!cityName || !!streetName || !!zipCode || !!houseNumber) &&
                                        !(cityName && streetName && zipCode && houseNumber),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    name: 'address',
                                    autoComplete: 'street-address',
                                    required: true,
                                    type: 'text',
                                    onChange: e => (store.houseNumber = e.target.value),
                                    value: store.houseNumber,
                                }}
                            />
                        </div>
                        <div style={{ maxWidth: 250 }}>
                            <CustomInput
                                labelText={t('form.labels.busNumber')}
                                id="ad_FVD"
                                formControlProps={{
                                    error:
                                        (!!cityName || !!streetName || !!zipCode || !!houseNumber) &&
                                        !(cityName && streetName && zipCode && houseNumber),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    name: 'address',
                                    autoComplete: 'street-address',
                                    required: true,
                                    type: 'text',
                                    onChange: e => (store.bus = e.target.value),
                                    value: store.bus,
                                }}
                            />
                        </div>
                    </div>
                    <div style={{ display: 'flex' }}>
                        <div style={{ flexGrow: 1, marginRight: 10 }}>
                            <CustomInput
                                labelText={t('form.labels.zipCode')}
                                id="ad_FVD"
                                formControlProps={{
                                    required: true,
                                    error:
                                        (!!cityName || !!streetName || !!zipCode || !!houseNumber) &&
                                        !(cityName && streetName && zipCode && houseNumber),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    name: 'address',
                                    autoComplete: 'street-address',
                                    required: true,
                                    type: 'text',
                                    onChange: e => (store.zipCode = e.target.value),
                                    value: store.zipCode,
                                }}
                            />
                        </div>
                        <div style={{ flexGrow: 1 }}>
                            <CustomInput
                                labelText={t('form.labels.city')}
                                id="ad_FVD"
                                formControlProps={{
                                    required: true,
                                    error:
                                        (!!cityName || !!streetName || !!zipCode || !!houseNumber) &&
                                        !(cityName && streetName && zipCode && houseNumber),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    name: 'address',
                                    autoComplete: 'street-address',
                                    required: true,
                                    type: 'text',
                                    onChange: e => (store.cityName = e.target.value),
                                    value: store.cityName,
                                }}
                            />
                        </div>
                    </div>
                </>
            )}
            <small className={classes.link} onClick={() => setManual(!manual)}>
                {!manual ? t('form.button.addressManual') : t('form.button.addressGoogle')}
            </small>
        </>
    );
}

export default withTranslation()(withStyles(classes)(observer(Address)));
