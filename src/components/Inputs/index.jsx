import React, { useEffect, useState } from 'react';
import CustomInput from 'components/CustomInput/CustomInput';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Address from 'components/Inputs/Address';
import Dropzone from 'components/Dropzone/Dropzone.jsx';
import Tags from './Tags';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import {
    FormControl,
    InputLabel,
    Input as MaterialInput,
    MenuItem,
    Checkbox,
    ListItemText,
    Select,
} from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { withTranslation } from 'react-i18next';
import { Delete } from '@material-ui/icons';

const styles = {
    formControl: {
        margin: 16,
        minWidth: 120,
        maxWidth: 300,
    },
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const MultiSelect = withStyles(styles)(
    observer(function MultiSelect(props) {
        return (
            <FormControl className={props.classes.formControl}>
                <InputLabel id="demo-mutiple-checkbox-label">{props.label}</InputLabel>
                <Select
                    labelId="demo-mutiple-checkbox-label"
                    id="demo-mutiple-checkbox"
                    multiple
                    value={props.value || []}
                    onChange={e => props.onChange(e.target.value)}
                    input={<MaterialInput />}
                    renderValue={selected =>
                        props.options
                            .filter(({ value }) => selected.includes(value))
                            .map(({ label }) => label)
                            .join(', ')
                    }
                    MenuProps={MenuProps}
                >
                    {props.options.map(({ label, value }) => (
                        <MenuItem key={value} value={value}>
                            <Checkbox checked={!!props.value?.includes(value)} />
                            <ListItemText primary={label} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        );
    })
);

function Input(props) {
    const [error, setError] = useState(false);

    const formControlProps = {
        required: props.required,
        disabled: props.disabled || false,
        error,
        fullWidth: props.fullWidth || false,
        style: props.style || {},
    };

    let value = props.value;

    if (!value) {
        props.type.includes('date') && (value = null);
        props.type === 'checkbox' && (value = false);
        props.type === 'select' && props.multi && (value = []);
        !['date', 'datetime', 'checkbox', 'select'].includes(props.type) &&
            [null, undefined].includes(value) &&
            (value = '');
    }

    const inputProps = {
        ...(props.inputProps || {}),
        endAdornment: props.endAdornment,
        autoComplete: props.autoComplete,
        placeholder: props.placeholder,
        inputRef: props.inputRef,
        autoFocus: props.autoFocus,
        name: props.name || (typeof props.label === 'string' ? props.label?.toLowerCase().replace(/\s/g, '_') : ''),
        onBlur: e => {
            if (Array.isArray(props.validations)) {
                setError(validate(e.target.value, props.validations, props.required));
            }
            props.onBlur && props.onBlur();
        },
        onChange: e => {
            const key = props.type === 'checkbox' ? 'checked' : 'value';
            let value = props.type === 'date' ? e : e.target[key];

            if (props.type === 'number') {
                if (!isNaN(parseFloat(value))) {
                    value = parseFloat(value);
                }
            }

            if (props.type === 'select') {
                if (!isNaN(parseInt(value))) {
                    value = parseInt(value);
                }

                if (props.multi) {
                    value = [...e.target.options]
                        .filter(option => option.selected)
                        .map(option => {
                            if (!isNaN(parseInt(option.value))) {
                                return parseInt(option.value);
                            }

                            return option.value;
                        });
                }
            }
            props.onChange(value);
        },
        value,
    };

    if (props.type === 'select') {
        inputProps.native = true;
        inputProps.useValue = true;
        inputProps.options = (props.options || []).filter(v => v);
        inputProps.multiple = props.multi || false;
        inputProps.displayEmpty = props.multi ? false : true;
        props.multi && (inputProps.inputProps = { style: { minHeight: props.options.length * 20, paddingTop: 25 } });
    }

    if (props.type === 'checkbox' && props.checked) {
        inputProps.value = props.checked();
    }

    let label = props.label;

    if (['checkbox', 'datetime'].includes(props.type) && props.required) {
        label = (
            <div style={{ display: 'flex' }}>
                {[
                    label,
                    <span key="required" style={{ color: 'tomato', marginLeft: 5 }}>
                        *
                    </span>,
                ]}
            </div>
        );
    }

    return (
        <>
            <CustomInput
                id={props.id}
                type={props.type}
                labelText={label}
                formControlProps={formControlProps}
                inputProps={inputProps}
                multiline={props.multiline || false}
                placeholder={props.placeholder}
            />
            {error && (
                <small
                    style={{
                        marginTop: -20,
                        display: 'block',
                        color: 'red',
                        fontSize: 11,
                    }}
                >
                    {error}
                </small>
            )}
        </>
    );
}

function validate(v, validations, required) {
    if (required && !v.toString().length) {
        return 'Veld mag niet leeg zijn';
    }

    function _length(v = '', min = 0, max = 0) {
        return v.length >= min && v.length <= max;
    }

    function _number(v, min, max) {
        return v >= min && v <= max;
    }

    function _between(v, min, max) {
        return v === '' || (parseInt(v) >= min && parseInt(v) <= max);
    }

    if (!validations?.length) {
        return;
    }

    return validations
        .filter(v => v)
        .map(validation => {
            switch (validation.type) {
                case 'between':
                    if (!_between(v, validation.min, validation.max)) {
                        return `Geef een waarde tussen ${validation.min} en ${validation.max}`;
                    }
                    return false;
                case 'length':
                    if (!_length(v, validation.min, validation.max)) {
                        return `min. ${validation.min} en max. ${validation.max} karakters`;
                    }
                    return false;
                case 'number':
                    if (!_number(parseInt(v), validation.min, validation.max)) {
                        return `min. ${validation.min} en max. ${validation.max}`;
                    }
                    return false;
                case 'email':
                    if (!(v && v.length > 6 && v.includes('@') && v.includes('.'))) {
                        return 'Ongeldig mailadres';
                    }
                    return false;
                default:
                    return false;
            }
        })
        .find(v => v);
}

function ResourceInput(props) {
    const { validations, id } = props;

    if (props.type === 'wysiwyg') {
        const data = props.store.form?.data || props.store.data;
        return (
            <>
                <label>{props.label}</label>
                <ReactQuill
                    modules={{
                        toolbar: [
                            [{ header: '1' }, { header: '2' }, { font: [] }],
                            [{ size: [] }],
                            ['bold', 'italic', 'underline', 'strike', 'blockquote'],
                            [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
                            ['link', 'image'],
                            ['clean'],
                        ],
                        clipboard: {
                            // toggle to add extra line breaks when pasting HTML:
                            matchVisual: false,
                        },
                    }}
                    formats={[
                        'header',
                        'font',
                        'size',
                        'bold',
                        'italic',
                        'underline',
                        'strike',
                        'blockquote',
                        'list',
                        'bullet',
                        'indent',
                        'link',
                        'image',
                        'video',
                    ]}
                    theme="snow"
                    value={data[props.id]}
                    onChange={v => (data[props.id] = v)}
                />
            </>
        );
    }

    if (props.type === 'file') {
        const data = props.store.form?.data || props.store.data;
        return (
            <>
                <label>{props.label}</label>
                {data.files?.length > 0 &&
                    data.files.map((file, index) => (
                        <div className="fileuploaded">
                            <span>{file.name}</span>
                            <Delete onClick={() => data.files.splice(index, 1)} />
                        </div>
                    ))}
                {(props.multiple || !data.files) && (
                    <Dropzone
                        className="fileupload"
                        store={data}
                        onData={file => {
                            if (data.files) {
                                data.files.push(file);
                            } else {
                                data.files = [file];
                            }
                        }}
                    />
                )}
            </>
        );
    }

    if (props.type === 'address') {
        return <Address store={props.store.form.data} />;
    }

    if (props.type === 'tags') {
        const data = props.store.form?.data || props.store.data;

        return (
            <Tags
                label={props.label}
                placeholder={props.placeholder}
                value={data[props.id]?.filter(v => v)}
                suggestions={props.options}
                onChange={tags => {
                    tags = tags.reduce((acc, tag) => {
                        if (tag && !acc.includes(tag)) {
                            acc.push(tag);
                        }
                        return acc;
                    }, []);
                    if (props.onChange) {
                        const values = props.onChange(tags);
                        if (values !== undefined) {
                            tags = values;
                        }
                    }
                    console.log(tags);
                    data[props.id] = tags;
                }}
            />
        );
    }

    return (
        <Input
            {...props}
            value={props.store.form.data[id]}
            onChange={v => {
                props.onChange && props.onChange(v);
                props.store.form.data[id] = v;
            }}
            validations={validations}
        />
    );
}

function InactiveProject({ t }) {
    const { projects } = useStore();
    const { resource } = projects;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.inactive')}
            type="select"
            value={filters.inactive}
            onChange={v => (filters.inactive = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function Category({ t }) {
    const { notifications } = useStore();
    const { resource } = notifications;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.notifications.category')}
            type="select"
            value={filters.category}
            onChange={v => (filters.category = v)}
            options={notifications.categoryAutoComplete}
        />
    );
}

function Accessoire({ t }) {
    const { articles } = useStore();
    const { resource } = articles;
    const { filters } = resource;

    return (
        <Input
            label="Is accessoire"
            type="select"
            value={filters.tags}
            onChange={v => (filters.tags = v)}
            options={[
                {
                    value: 'accessoire',
                    label: t('form.labels.yes'),
                },
                {
                    value: 'false',
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function ClothingSize({ t }) {
    const { articles, clothingManagement } = useStore();
    const { resource } = articles;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.clothing.clothingSize')}
            type="select"
            value={filters.clothingSizeId}
            onChange={v => (filters.clothingSizeId = v)}
            options={clothingManagement.sizes.autoComplete}
        />
    );
}

function Color({ t }) {
    const { articles, clothingManagement } = useStore();
    const { resource } = articles;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.clothing.color')}
            type="select"
            value={filters.colorId}
            onChange={v => (filters.colorId = v)}
            options={clothingManagement.colors.autoComplete}
        />
    );
}

function Pattern({ t }) {
    const { articles, clothingManagement } = useStore();
    const { resource } = articles;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.clothing.pattern')}
            type="select"
            value={filters.patternId}
            onChange={v => (filters.patternId = v)}
            options={clothingManagement.patterns.autoComplete}
        />
    );
}

function Fabric({ t }) {
    const { articles, clothingManagement } = useStore();
    const { resource } = articles;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.clothing.fabric')}
            type="select"
            value={filters.fabricId}
            onChange={v => (filters.fabricId = v)}
            options={clothingManagement.fabrics.autoComplete}
        />
    );
}

function Rack({ t }) {
    const { articles, clothingManagement } = useStore();
    const { resource } = articles;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.clothing.rack')}
            type="select"
            value={filters.rackId}
            onChange={v => (filters.rackId = v)}
            options={clothingManagement.racks.autoComplete}
        />
    );
}

function ReadMessages({ storeName, t }) {
    const store = useStore(storeName);
    const { resource } = store;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.read')}
            type="select"
            value={filters.read}
            onChange={v => (filters.read = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function Sex({ storeName, t }) {
    const store = useStore(storeName);
    const { resource } = store;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.sex')}
            type="select"
            value={filters.sex}
            onChange={v => (filters.sex = v)}
            options={[
                {
                    value: 1,
                    label: t('form.options.female'),
                },
                {
                    value: 0,
                    label: t('form.options.male'),
                },
                {
                    value: 2,
                    label: t('form.options.genderX'),
                },
            ]}
        />
    );
}

function Archive({ storeName, t }) {
    const store = useStore(storeName);
    const { resource } = store;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.archived')}
            type="select"
            value={filters.archive}
            onChange={v => (filters.archive = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function MinAge({ storeName, t }) {
    const store = useStore(storeName);
    const { resource } = store;
    const { filters, filterValues } = resource;

    return (
        <Input
            label={t('form.labels.minAge')}
            type="select"
            value={filters.minAge}
            onChange={v => (filters.minAge = v)}
            options={filterValues.age?.map(age => ({ label: age, value: age })) || []}
        />
    );
}

function MaxAge({ storeName, t }) {
    const store = useStore(storeName);
    const { resource } = store;
    const { filters, filterValues } = resource;

    return (
        <Input
            label={t('form.labels.maxAge')}
            type="select"
            value={filters.maxAge}
            onChange={v => (filters.maxAge = v)}
            options={filterValues.age?.map(age => ({ label: age, value: age })) || []}
        />
    );
}

function SortUsers({ t }) {
    const { users } = useStore();
    const { resource } = users;
    const { sort } = resource;

    return (
        <Input
            label={t('form.labels.sort')}
            type="select"
            value={sort}
            onChange={v => (resource.sort = v)}
            options={[
                {
                    label: 'A-Z',
                    value: 'lastName|ASC',
                },
                {
                    label: 'Z-A',
                    value: 'lastName|DESC',
                },
                {
                    label: t('form.options.creationDt|DESC'),
                    value: 'creationDt|DESC',
                },
                {
                    label: t('form.options.creationDt|ASC'),
                    value: 'creationDt|ASC',
                },
                {
                    label: t('form.options.date_of_birth|DESC'),
                    value: 'date_of_birth|DESC',
                },
                {
                    label: t('form.options.date_of_birth|ASC'),
                    value: 'date_of_birth|ASC',
                },
            ]}
        />
    );
}

function SortProjectUsers({ t }) {
    const { projectUsers } = useStore();
    const { resource } = projectUsers;
    const { sort } = resource;

    return (
        <Input
            label={t('form.labels.sort')}
            type="select"
            value={sort}
            onChange={v => (resource.sort = v)}
            options={[
                {
                    label: 'A-Z',
                    value: 'u.lastName|ASC',
                },
                {
                    label: 'Z-A',
                    value: 'u.lastName|DESC',
                },
                {
                    label: t('form.options.pU.updatedDt|ASC'),
                    value: 'pU.updatedDt|ASC',
                },
                {
                    label: t('form.options.pU.updatedDt|DESC'),
                    value: 'pU.updatedDt|DESC',
                },
                {
                    label: t('form.labels.project.project'),
                    value: 'pU.projectId|DESC',
                },
            ]}
        />
    );
}

function SortRegistrations({ t }) {
    const { registrations } = useStore();
    const { resource } = registrations;
    const { sort } = resource;

    return (
        <Input
            label={t('form.labels.sort')}
            type="select"
            value={sort}
            onChange={v => (resource.sort = v)}
            options={[
                {
                    label: 'A-Z',
                    value: 'user.lastName|ASC',
                },
                {
                    label: 'Z-A',
                    value: 'user.lastName|DESC',
                },
                {
                    label: t('form.options.registered|ASC'),
                    value: 'registered|ASC',
                },
                {
                    label: t('form.options.registered|DESC'),
                    value: 'registered|DESC',
                },
                {
                    label: t('form.options.date_of_birth|DESC'),
                    value: 'user.age|ASC',
                },
                {
                    label: t('form.options.date_of_birth|ASC'),
                    value: 'user.age|DESC',
                },
            ]}
        />
    );
}

function OnStage({ t }) {
    const { registrations } = useStore();
    const { resource } = registrations;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.onStage')}
            type="select"
            value={filters.onStage}
            onChange={v => (filters.onStage = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function Auditioning({ t }) {
    const { registrations } = useStore();
    const { resource } = registrations;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.auditioning')}
            type="select"
            value={filters.auditioning}
            onChange={v => (filters.auditioning = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function Remarks({ t }) {
    const { registrations } = useStore();
    const { resource } = registrations;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.hasRemarks')}
            type="select"
            value={filters.remarks}
            onChange={v => (filters.remarks = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function Talents({ t }) {
    const { registrations } = useStore();
    const { resource } = registrations;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.hasTalents')}
            type="select"
            value={filters.talents}
            onChange={v => (filters.talents = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function SortRolesAndGroups({ storeName, t }) {
    const store = useStore(storeName);
    const { resource } = store;
    const { sort } = resource;

    return (
        <Input
            label={t('form.labels.sort')}
            type="select"
            value={sort}
            onChange={v => (resource.sort = v)}
            options={[
                {
                    label: 'A-Z',
                    value: 'name|ASC',
                },
                {
                    label: 'Z-A',
                    value: 'name|DESC',
                },
                {
                    label: t('form.options.identifier|ASC'),
                    value: 'identifier|ASC',
                },
                {
                    label: t('form.options.identifier|DESC'),
                    value: 'identifier|DESC',
                },
                {
                    label: t('form.options.userCount|ASC'),
                    value: 'userCount|ASC',
                },
                {
                    label: t('form.options.userCount|DESC'),
                    value: 'userCount|DESC',
                },
            ]}
        />
    );
}

const Scene = observer(function Scene({ storeName, projectId, t }) {
    const store = useStore(storeName);
    const { projects, scenes } = useStore();
    const { resource } = store;
    const { filters } = resource;

    useEffect(() => {
        scenes.resource.fetch(`/project/${projects.id || projectId}`);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Input
            label={t('form.labels.scene')}
            type="select"
            value={filters.scene}
            onChange={v => (filters.scene = v)}
            options={(scenes.resource.data || [])
                .filter(v => v)
                .map(scene => ({
                    label: `${scene.identifier} - ${scene.name}`,
                    value: scene.id,
                }))}
        />
    );
});

const Project = observer(function Project({ storeName, t }) {
    const store = useStore(storeName);
    const { projects } = useStore();
    const { resource } = store;
    const { filters } = resource;

    useEffect(() => {
        delete projects.resource.filters.inactive;
        projects.resource.fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Input
            style={{ maxWidth: 300 }}
            label={t('form.labels.project.project')}
            type="select"
            value={filters.project}
            onChange={v => (filters.project = v)}
            options={(projects.resource.data || [])
                .filter(v => v)
                .map(project => ({
                    label: project.name,
                    value: project.id,
                }))}
        />
    );
});

const ActiveProject = observer(function Project({ storeName, t }) {
    const store = useStore(storeName);
    const { projects, scenes, projectRoles, groups } = useStore();
    const { resource } = store;
    const { filters } = resource;

    useEffect(() => {
        projects.resource.batchSize = 'all';
        projects.resource.fetch().then(() => {
            scenes.resource.filters = { project: filters.project || projects.resource.data.map(({ id }) => id) };
            projectRoles.resource.filters = { project: filters.project || projects.resource.data.map(({ id }) => id) };
            groups.resource.filters = { project: filters.project || projects.resource.data.map(({ id }) => id) };
        });

        return () => {
            projects.resource.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <MultiSelect
            label={t('form.labels.project.project')}
            type="select"
            value={filters.project}
            onChange={v => (filters.project = v)}
            options={(projects.resource.data || [])
                .filter(v => v)
                .map(project => ({
                    label: project.name,
                    value: project.id,
                }))}
        />
    );
});

const SceneMultiple = observer(function SceneMultiple({ storeName, t }) {
    const store = useStore(storeName);
    const { scenes } = useStore();
    const { resource } = store;
    const { filters } = resource;

    useEffect(() => {
        scenes.resource.filters?.project && scenes.resource.fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [scenes.resource.filters]);

    return (
        <MultiSelect
            label={t('form.labels.scene')}
            type="select"
            value={filters.scene}
            onChange={v => (filters.scene = v)}
            options={(scenes.resource.data || [])
                .filter(v => v)
                .map(scene => ({
                    label: `${scene.identifier}: ${scene.name}`,
                    value: scene.id,
                }))}
        />
    );
});

const RoleMultiple = observer(function SceneMultiple({ storeName, t }) {
    const store = useStore(storeName);
    const { projectRoles } = useStore();
    const { resource } = store;
    const { filters } = resource;

    useEffect(() => {
        projectRoles.resource.filters?.project && projectRoles.resource.fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [projectRoles.resource.filters]);

    return (
        <MultiSelect
            label={t('form.labels.role')}
            type="select"
            value={filters.role}
            onChange={v => (filters.role = v)}
            options={(projectRoles.resource.data || [])
                .filter(v => v)
                .map(projectRole => ({
                    label: `${projectRole.identifier}: ${projectRole.name}`,
                    value: projectRole.id,
                }))}
        />
    );
});

const GroupMultiple = observer(function SceneMultiple({ storeName, t }) {
    const store = useStore(storeName);
    const { groups } = useStore();
    const { resource } = store;
    const { filters } = resource;

    useEffect(() => {
        groups.resource.filters?.project && groups.resource.fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [groups.resource.filters]);

    return (
        <MultiSelect
            label={t('form.labels.group')}
            type="select"
            value={filters.group}
            onChange={v => (filters.group = v)}
            options={(groups.resource.data || [])
                .filter(v => v)
                .map(group => ({
                    label: `${group.identifier}: ${group.name}`,
                    value: group.id,
                }))}
        />
    );
});

const CastMultiple = observer(function SceneMultiple({ storeName, t }) {
    const store = useStore(storeName);
    const { casts } = useStore();
    const { resource } = store;
    const { filters } = resource;

    useEffect(() => {
        casts.resource.fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <MultiSelect
            label={t('form.labels.cast')}
            type="select"
            value={filters.cast}
            onChange={v => (filters.cast = v)}
            options={(casts.resource.data || [])
                .filter(v => v)
                .map(cast => ({
                    label: cast.name,
                    value: cast.id,
                }))}
        />
    );
});

function PastRepetition({ t }) {
    const { repetitions } = useStore();
    const { resource } = repetitions;
    const { filters } = resource;

    return (
        <Input
            label={t('form.labels.past')}
            type="select"
            value={filters.past}
            onChange={v => (filters.past = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function OrderType({ t }) {
    const { clothingManagement } = useStore();
    const { resource } = clothingManagement.orders;
    const {
        form: { data },
    } = resource;

    return (
        <Input
            fullWidth
            label={t('form.labels.order.renterType')}
            type="select"
            value={data.type || ''}
            onChange={v => (data.type = v)}
            options={[
                {
                    value: 1,
                    label: t('form.options.order.private'),
                },
                {
                    value: 2,
                    label: t('form.options.order.association'),
                },
                {
                    value: 3,
                    label: t('form.options.order.company'),
                },
            ]}
        />
    );
}

function OrderReturnDate({ t }) {
    const { clothingManagement } = useStore();
    const { resource } = clothingManagement.orders;
    const { filters } = resource;

    return (
        <Input
            style={{ minWidth: 200 }}
            label={t('form.labels.order.return')}
            type="select"
            value={filters.return}
            onChange={v => (filters.return = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.order.day'),
                },
                {
                    value: 7,
                    label: t('form.labels.order.week'),
                },
                {
                    value: 14,
                    label: t('form.labels.order.twoWeeks'),
                },
                {
                    value: 30,
                    label: t('form.labels.order.month'),
                },
            ]}
        />
    );
}

function OrderIsBilled({ t }) {
    const { clothingManagement } = useStore();
    const { resource } = clothingManagement.orders;
    const { filters } = resource;

    return (
        <Input
            style={{ minWidth: 200 }}
            label={t('form.labels.order.billed')}
            type="select"
            value={filters.billed}
            onChange={v => (filters.billed = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function OrderIsReturned({ t }) {
    const { clothingManagement } = useStore();
    const { resource } = clothingManagement.orders;
    const { filters } = resource;

    return (
        <Input
            style={{ minWidth: 200 }}
            label={t('form.labels.order.returned')}
            type="select"
            value={filters.returned}
            onChange={v => (filters.returned = v)}
            options={[
                {
                    value: 1,
                    label: t('form.labels.yes'),
                },
                {
                    value: 0,
                    label: t('form.labels.no'),
                },
            ]}
        />
    );
}

function withStore(storeName, Component) {
    return props => <Component storeName={storeName} {...props} />;
}

export default {
    projects: {
        filters: {
            inactive: withTranslation()(observer(InactiveProject)),
        },
    },
    users: {
        sort: withTranslation()(observer(SortUsers)),
        filters: {
            sex: withTranslation()(observer(withStore('users', Sex))),
            minAge: withTranslation()(observer(withStore('users', MinAge))),
            maxAge: withTranslation()(observer(withStore('users', MaxAge))),
        },
    },
    articles: {
        filters: {
            accessoire: withTranslation()(observer(Accessoire)),
            size: withTranslation()(observer(ClothingSize)),
            color: withTranslation()(observer(Color)),
            pattern: withTranslation()(observer(Pattern)),
            fabric: withTranslation()(observer(Fabric)),
            rack: withTranslation()(observer(Rack)),
        },
    },
    notifications: {
        filters: {
            category: withTranslation()(observer(Category)),
            read: withTranslation()(observer(withStore('notifications', ReadMessages))),
        },
    },
    threads: {
        filters: {
            archive: withTranslation()(observer(withStore('threads', Archive))),
        },
    },
    registrations: {
        sort: withTranslation()(observer(SortRegistrations)),
        filters: {
            sex: withTranslation()(observer(withStore('registrations', Sex))),
            minAge: withTranslation()(observer(withStore('registrations', MinAge))),
            maxAge: withTranslation()(observer(withStore('registrations', MaxAge))),
            onStage: withTranslation()(observer(OnStage)),
            auditioning: withTranslation()(observer(Auditioning)),
            remarks: withTranslation()(observer(Remarks)),
            talents: withTranslation()(observer(Talents)),
        },
    },
    roles: {
        sort: withTranslation()(observer(withStore('projectRoles', SortRolesAndGroups))),
        filters: {
            scene: withTranslation()(observer(withStore('projectRoles', Scene))),
            project: withTranslation()(observer(withStore('projectRoles', Project))),
        },
    },
    groups: {
        sort: withTranslation()(observer(withStore('groups', SortRolesAndGroups))),
        filters: {
            scene: withTranslation()(observer(withStore('groups', Scene))),
            project: withTranslation()(observer(withStore('groups', Project))),
        },
    },
    scenes: {
        filters: {
            project: withTranslation()(observer(withStore('scenes', Project))),
        },
    },
    repetitions: {
        filters: {
            past: withTranslation()(observer(PastRepetition)),
        },
    },
    orders: {
        type: withTranslation()(observer(OrderType)),
        filters: {
            return: withTranslation()(observer(OrderReturnDate)),
            billed: withTranslation()(observer(OrderIsBilled)),
            returned: withTranslation()(observer(OrderIsReturned)),
        },
    },
    messages: {
        filters: {
            project: withTranslation()(observer(withStore('messages', ActiveProject))),
            read: withTranslation()(observer(withStore('messages', ReadMessages))),
        },
    },
    projectUsers: {
        filters: {
            project: withTranslation()(observer(withStore('projectUsers', ActiveProject))),
            scene: withTranslation()(observer(withStore('projectUsers', SceneMultiple))),
            role: withTranslation()(observer(withStore('projectUsers', RoleMultiple))),
            group: withTranslation()(observer(withStore('projectUsers', GroupMultiple))),
            cast: withTranslation()(observer(withStore('projectUsers', CastMultiple))),
        },
        sort: withTranslation()(observer(SortProjectUsers)),
    },
    transferProjectUsers: {
        filters: {
            scene: withTranslation()(observer(withStore('projectUsers.transfer', SceneMultiple))),
            role: withTranslation()(observer(withStore('projectUsers.transfer', RoleMultiple))),
            group: withTranslation()(observer(withStore('projectUsers.transfer', GroupMultiple))),
            cast: withTranslation()(observer(withStore('projectUsers.transfer', CastMultiple))),
        },
        sort: withTranslation()(observer(SortProjectUsers)),
    },
    resource: observer(ResourceInput),
    Input: Input,
    MultiSelect: MultiSelect,
};
