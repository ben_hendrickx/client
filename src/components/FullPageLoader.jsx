import React from 'react';
import { withStyles } from '@material-ui/styles';
import Loader from 'react-loaders';
import { primaryColor } from 'assets/jss/material-kit-react';
import Brand from './Brand';

const loaderStyles = {
    container: {
        display: 'flex',
        alignItems: 'center',
        background: '#444',
        height: '100vh',
        width: '100vw',
        position: 'fixed',
        flexDirection: 'row !important',
    },
    center: {
        width: '100vw',
        textAlign: 'center',
    },
    logo: {
        fontFamily: 'Oleo Script, serif',
        color: '#fff',
    },
    spinner: { maxHeight: '250px', maxWidth: '50vw' },
};

function FullPageLoader(props) {
    const { classes } = props;

    return (
        <div className={classes.container}>
            <div className={classes.center}>
                <h1 className={classes.logo}><Brand style={{fontSize: '1em', color: 'white'}} /></h1>
                <Loader type="ball-pulse" active color={primaryColor} style={{marginTop: 20, transform: 'scale(1.2)'}} />
            </div>
        </div>
    );
}

export default withStyles(loaderStyles)(FullPageLoader);
