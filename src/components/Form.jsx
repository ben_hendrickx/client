import React from 'react';
import { useHistory } from 'react-router';
import Inputs from 'components/Inputs';
import Button from 'components/CustomButtons/Button.jsx';
import { Save } from '@material-ui/icons';
import { observer } from 'mobx-react-lite';
import { useStore } from '../mobx';
import { withTranslation } from 'react-i18next';
import { t } from 'i18next';

function Form(props) {
    const { ui } = useStore();
    const history = useHistory();
    const style = {
        background: '#fff',
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: '10px 15px',
        marginBottom: 30,
    };

    const { store, noCancel = false, onConfirm = () => history.goBack(), id, save, apiPrefix } = props;
    const { form } = store;
    const { sections, fields } = form;

    return (
        <>
            <div style={{ display: 'flex', justifyContent: 'space-between', flexWrap: 'wrap' }}>
                {sections
                    .filter(section => fields.some(field => field && field.section === section.id))
                    .map((section, i) => {
                        const inputs = fields.filter(field => field && field.section === section.id);

                        return (
                            <div
                                key={`form-section${i}`}
                                style={{
                                    flexDirection: 'column',
                                    flexGrow: 1,
                                    maxWidth: (props.fullWidth || ui.isMobile ? 100 : 49) + '%',
                                    marginRight: (props.fullWidth || ui.isMobile ? 0 : 1) + '%',
                                }}
                            >
                                <h1 style={{ fontSize: '1.5em' }}>
                                    {section.translationKey ? t(section.translationKey) : section.title}
                                </h1>
                                <div style={style}>
                                    {inputs.map((input, j) => {
                                        if (input.component) {
                                            return input.component();
                                        }

                                        return (
                                            <Inputs.resource
                                                key={`form-section${i}-input${j}`}
                                                fullWidth
                                                store={props.store}
                                                {...input}
                                                label={input.translationKey ? t(input.translationKey) : input.label}
                                                placeholder={t(input.placeholder)}
                                            />
                                        );
                                    })}
                                </div>
                            </div>
                        );
                    })}
            </div>
            {props.children || null}
            {props.confirmButton ? (
                props.confirmButton
            ) : (
                <Button
                    color={!form.isValid || props.disabled ? 'disabled' : 'success'}
                    onClick={async () => {
                        if (form.isValid) {
                            props.onSave && (await props.onSave());
                            save
                                ? await save(apiPrefix, undefined, id)
                                : await props.store.save(apiPrefix, undefined, id);
                            onConfirm();
                        } else {
                            store.error = form.invalidMessage || 'Gelieve alle verplichte velden in te vullen';
                        }
                    }}
                    size="md"
                >
                    <Save style={{ marginRight: '5px' }} />
                    <span style={{ marginTop: '1px', marginLeft: '5px' }}>{t('form.button.save')}</span>
                </Button>
            )}
            {!noCancel && (
                <Button onClick={() => history.goBack()} style={{ marginLeft: '20px' }} color="white" size="md">
                    {t('form.button.cancel')}
                </Button>
            )}
        </>
    );
}

export default withTranslation()(observer(Form));
