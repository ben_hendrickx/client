import React, { useState, useEffect } from 'react';

import withStyles from '@material-ui/core/styles/withStyles';
import { Edit, Close, Save } from '@material-ui/icons';
import { observer } from 'mobx-react-lite';
import { useStore } from '../mobx';
import { primaryColor } from 'assets/jss/material-kit-react';

const editableCardStyles = {
    card: {
        position: 'relative',
        '& img': {
            maxWidth: '100%',
        },
        background: '#fff',
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: '10px 15px',
        margin: '26px 0',
        '& > span': {
            marginTop: 10,
        },
        '& > span:firstChild': {
            marginTop: 0,
        },
    },
    cardAction: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 5,
        cursor: 'pointer',
        '&:hover': {
            color: primaryColor,
        },
    },
};

function EditableCard(props) {
    const { classes } = props;
    const { account } = useStore();
    const [edit, setEdit] = useState(props.edit);
    const [editable] = useState(props.editable !== undefined ? props.editable : true);

    useEffect(() => {
        setEdit(props.edit);
    }, [props.edit]);

    const EditCard = props => {
        return (
            <div {...props} className={classes.cardAction}>
                <Edit className="edit"/>
            </div>
        );
    };

    const SaveCard = props => {
        return (
            <div
                {...props}
                className={classes.cardAction}
                style={{ top: 30 }}
                onClick={() => (!props.disabled ? props.onClick() : () => {})}
            >
                <Save style={props.disabled ? { cursor: 'not-allowed', color: '#a4a4a4' } : {}} className="save" />
            </div>
        );
    };

    const CloseEditCard = props => {
        return (
            <div {...props} className={classes.cardAction}>
                <Close />
            </div>
        );
    };

    return (
        <div className={classes.card} style={{ paddingRight: edit ? 45 : 15, ...(props.style || {}) }}>
            {!edit && editable && (!props.forAdmin || (props.forAdmin && account.level > 0)) && (
                <EditCard
                    onClick={() => {
                        props.onEdit && props.onEdit(true);
                        setEdit(true);
                    }}
                />
            )}
            {edit && editable && (
                <SaveCard
                    disabled={!(props.canSave === undefined ? true : props.canSave)}
                    onClick={async () => {
                        console.error('WTF');
                        try {
                            await props.onSave();
                        } catch (e) {
                            console.error(e);
                        } finally {
                            setEdit(false);
                            props.onEdit && props.onEdit(false);
                        }
                    }}
                />
            )}
            {edit && editable && (
                <CloseEditCard
                    onClick={() => {
                        props.onCancel && props.onCancel();
                        props.onEdit && props.onEdit(false);
                        setEdit(false);
                    }}
                />
            )}

            {props.children({ edit })}
        </div>
    );
}

export default withStyles(editableCardStyles)(observer(EditableCard));
