import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Select from '@material-ui/core/Select';
import MomentUtils from '@date-io/moment';

import { MuiPickersUtilsProvider, KeyboardDatePicker, KeyboardDateTimePicker, KeyboardTimePicker } from '@material-ui/pickers';
import { Checkbox, FormControlLabel } from '@material-ui/core';

import customInputStyle from './customInput.style.jsx';

function CustomInput({ ...props }) {
    const {
        classes,
        formControlProps,
        labelText,
        id,
        labelProps,
        inputProps,
        error,
        white,
        inputRootCustomClasses,
        success,
        type,
    } = props;

    const useValue = (inputProps || {}).useValue;
    delete inputProps.useValue;
    delete inputProps.endAdornment;

    const labelClasses = classNames({
        [' ' + classes.labelRootError]: error,
        [' ' + classes.labelRootSuccess]: success && !error,
    });
    const underlineClasses = classNames({
        [classes.underlineError]: error,
        [classes.underlineSuccess]: success && !error,
        [classes.underline]: true,
        [classes.whiteUnderline]: white,
    });
    const marginTop = classNames({
        [inputRootCustomClasses]: inputRootCustomClasses !== undefined,
    });
    const inputClasses = classNames({
        [classes.input]: true,
        [classes.whiteInput]: white,
    });
    var formControlClasses;
    if (formControlProps !== undefined) {
        formControlClasses = classNames(formControlProps.className, classes.formControl);
    } else {
        formControlClasses = classes.formControl;
    }

    if (type && type.toLowerCase() === 'checkbox') {
        return (
            <div style={{ width: formControlProps.fullWidth ? '100%' : 'auto' }}>
                <FormControlLabel
                    style={{alignItems: 'flex-start'}}
                    control={<Checkbox checked={inputProps.value} onChange={inputProps.onChange} />}
                    label={labelText}
                />
            </div>
        );
    }

    return (
        <FormControl {...formControlProps} className={formControlClasses}>
            {!(type || '').includes('date') && !(type || '').includes('time') && labelText !== undefined ? (
                <InputLabel className={classes.labelRoot + ' ' + labelClasses} htmlFor={id} {...labelProps}>
                    {labelText}
                </InputLabel>
            ) : null}
            {(!type || ['text', 'number', 'password', 'email'].includes(type.toLowerCase())) && (
                <Input
                    type={type}
                    classes={{
                        input: inputClasses,
                        root: marginTop,
                        disabled: classes.disabled,
                        underline: underlineClasses,
                    }}
                    id={id}
                    {...inputProps}
                />
            )}
            {type && type.toLowerCase() === 'select' && (
                <Select native className={underlineClasses} {...inputProps}>
                    {(inputProps.displayEmpty === undefined || inputProps.displayEmpty === true) && <option value="" />}
                    {(inputProps.options || []).map(({ label, value }, i) => (
                        <option key={`option-${i}`} value={useValue ? value : label}>
                            {label}
                        </option>
                    ))}
                </Select>
            )}
            {type && type.toLowerCase() === 'date' && (
                <MuiPickersUtilsProvider utils={MomentUtils}>
                    <KeyboardDatePicker
                        autoOk
                        label={labelText}
                        className="datePicker"
                        variant="inline"
                        format="DD/MM/YYYY"
                        margin="normal"
                        invalidDateMessage={"Ongeldige datum"}
                        {...inputProps}
                    />
                </MuiPickersUtilsProvider>
            )}
            {type && type.toLowerCase() === 'time' && (
                <MuiPickersUtilsProvider utils={MomentUtils}>
                    <KeyboardTimePicker
                        autoOk
                        ampm={false}
                        label={labelText}
                        className="datePicker"
                        variant="inline"
                        format="HH:mm"
                        margin="normal"
                        invalidDateMessage={"Ongeldig tijdstip"}
                        {...inputProps}
                    />
                </MuiPickersUtilsProvider>
            )}
            {type && type.toLowerCase() === 'datetime' && (
                <MuiPickersUtilsProvider utils={MomentUtils}>
                    <KeyboardDateTimePicker
                        variant="inline"
                        ampm={false}
                        label={labelText}
                        {...inputProps}
                        onChange={v => inputProps.onChange({ target: { value: v } })}
                        format="DD/MM/YYYY HH:mm"
                    />
                </MuiPickersUtilsProvider>
            )}
        </FormControl>
    );
}

CustomInput.propTypes = {
    classes: PropTypes.object.isRequired,
    labelText: PropTypes.node,
    labelProps: PropTypes.object,
    id: PropTypes.string,
    inputProps: PropTypes.object,
    formControlProps: PropTypes.object,
    inputRootCustomClasses: PropTypes.string,
    error: PropTypes.any,
    success: PropTypes.bool,
    white: PropTypes.bool,
    type: PropTypes.string,
};

export default withStyles(customInputStyle, {
    defaultTheme: {
        MuiInput: {
            underline: customInputStyle.underline,
        },
    },
})(CustomInput);
