import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import { observer } from 'mobx-react-lite';
import resizeImage from 'smart-img-resize';
import { withStyles } from '@material-ui/styles';
import { withTranslation } from 'react-i18next';

const dropzoneStyles = {
    noPrint: {
        '@media print': {
            display: 'none',
        },
    },
};

const DropZone = props => {
    const { t } = props;
    const maxSize = 10485760;

    const onDrop = useCallback(
        acceptedFiles => {
            acceptedFiles &&
                resizeImage(
                    acceptedFiles[0],
                    {
                        outputFormat: 'jpeg',
                        targetWidth: 200,
                        targetHeight: 200,
                        crop: true,
                    },
                    (err, b64img) => {
                        if (err) {
                            console.error(err);
                            return;
                        }

                        props.onData(b64img);
                        // do what you have to with the b64img
                    }
                );
        },
        // eslint-disable-next-line react-hooks/exhaustive-deps
        [props.store.image]
    );

    const { getRootProps, getInputProps, isDragReject, rejectedFiles } = useDropzone({
        onDrop,
        accept: 'image/jpeg, image/png',
        minSize: 0,
        maxSize,
    });

    const isFileTooLarge = rejectedFiles.length > 0 && rejectedFiles[0].size > maxSize;

    const showImage = (props.edit && props.store.newImage) || (!props.edit && props.store.image);
    const image = props.edit ? props.store.newImage : props.store.image;

    return (
        <div className={!showImage ? props.className : props.classes.noPrint} style={props.style}>
            {showImage && (
                <div style={{ marginTop: props.edit ? 0 : 27 }}>
                    <img alt="Profile" src={image} height="204" />
                </div>
            )}
            {!showImage && (
                <div {...getRootProps()} className="dropzone">
                    <input {...getInputProps()} />
                    {!isFileTooLarge && !isDragReject && t('form.placeholder.profilePicture')}
                    {isDragReject && t('form.error.fileType')}
                    {isFileTooLarge && <div className="text-danger mt-2">{t('form.error.fileSize')}</div>}
                </div>
            )}
        </div>
    );
};

export default withTranslation()(withStyles(dropzoneStyles)(observer(DropZone)));
