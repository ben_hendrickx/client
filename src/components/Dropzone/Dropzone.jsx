import React, { useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

const DropZone = props => {
    const { t } = props;
    const maxSize = 10485760;

    const onDrop = useCallback(([file] = []) => {
        props.onData && props.onData(file);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const { getRootProps, getInputProps, isDragReject, rejectedFiles } = useDropzone({
        onDrop,
        minSize: 0,
        maxSize,
    });

    const isFileTooLarge = rejectedFiles.length > 0 && rejectedFiles[0].size > maxSize;

    return (
        <div className={props.className || ''}>
            <div {...getRootProps()} className="dropzone">
                <input {...getInputProps()} />
                {!isFileTooLarge && !isDragReject && t('form.placeholder.profilePicture')}
                {isDragReject && t('form.error.fileType')}
                {isFileTooLarge && <div className="text-danger mt-2">{t('form.error.fileSize')}</div>}
            </div>
        </div>
    );
};

export default withTranslation()(observer(DropZone));
