import React, { useCallback } from 'react';
import { useStore } from '../../mobx';
import { useDropzone } from 'react-dropzone';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

const CsvDropZone = props => {
    const { t } = props;
    const maxSize = 10485760;
    const { account } = useStore();

    const onDrop = useCallback(acceptedFiles => {
        if (acceptedFiles) {
            const reader = new FileReader();
            reader.onload = function(e) {
                props.onData && props.onData(e.target.result);
            };
            reader.readAsText(acceptedFiles[0]);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const { getRootProps, getInputProps, isDragReject, rejectedFiles } = useDropzone({
        onDrop,
        accept: '.csv',
        minSize: 0,
        maxSize,
    });

    const isFileTooLarge = rejectedFiles.length > 0 && rejectedFiles[0].size > maxSize;

    return (
        <div className={!account.image ? props.className : ''}>
            {account.image && (
                <div style={{ marginTop: 27 }}>
                    <img alt="Profile" src={account.image} height="204" />
                </div>
            )}
            {!account.image && (
                <div {...getRootProps()} className="dropzone">
                    <input {...getInputProps()} />
                    {!isFileTooLarge && !isDragReject && t('form.placeholder.profilePicture')}
                    {isDragReject && t('form.error.fileType')}
                    {isFileTooLarge && <div className="text-danger mt-2">{t('form.error.fileSize')}</div>}
                </div>
            )}
        </div>
    );
};

export default withTranslation()(observer(CsvDropZone));
