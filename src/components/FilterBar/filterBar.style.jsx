const filterBarStyle = {
    filters: {
        minHeight: 50,
        background: '#fff',
        boxShadow:
            '0px 1px 2px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: 10,
        marginBottom: 10,
        '@media (min-width: 960px)': {
            margin: '0',
            marginBottom: 10,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            padding: '0 0 0 20px',
        },
        '@media print': {
            display: 'none',
        },
    },
    results: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        '& > div': {
            display: 'flex',
            alignItems: 'center',
            '& > svg': {
                marginRight: 20,
            },
            '& > span': {
                minWidth: 150,
            },
        },
    },
    elements: {
        marginTop: -10,
        '& > div, & > div > div': {
            display: 'flex',
            
            minWidth: 100,
            marginRight: 20,
            '&:last-child': {
                marginRight: 0,
            },
        },
        '@media (min-width: 960px)': {
            display: 'flex',
            flexGrow: 1,
            marginLeft: 20,
            alignItems: 'center',
            justifyContent: 'flex-end',
            '& > svg': {
                marginRight: 20,
            },
        },
    },
    filter: {
        minWidth: '45%',
        '@media (min-width: 960px)': {
            minWidth: 100,
            marginTop: -9,
            marginRight: 20,
        },
        '@media (min-width: 1440px)': {
            minWidth: 110,
        },
    },
    sort: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start !important',
        '& > svg': {
            marginRight: 20,
        },
        '@media (min-width: 960px)': {
            marginLeft: 0,
            paddingLeft: 20,
            borderLeft: '1px solid rgba(0,0,0,.1)',
            '& > div': {
                marginTop: -9,
            },
        },
    },
};

export default filterBarStyle;
