import React from 'react';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Tooltip from '@material-ui/core/Tooltip';
import { FilterList, ClearAll } from '@material-ui/icons';
import { useStore } from '../../mobx';
import withStyles from '@material-ui/core/styles/withStyles';
import filterBarStyle from './filterBar.style.jsx';
import { observer } from 'mobx-react-lite';
import { primaryColor } from 'assets/jss/material-kit-react';
import Inputs from 'components/Inputs';

function FilterBar(props) {
    const { classes } = props;
    const { clothingManagement, ui } = useStore();

    const count = props.count !== undefined ? props.count : clothingManagement.filteredArticles.length;
    const totalCount = props.totalCount !== undefined ? props.totalCount : clothingManagement.articles.length;

    return (
        <div className={classes.filters}>
            <div className={classes.results}>
                <div>
                    <FilterList />
                    <span>
                        {count ? count : 0}
                        {count === totalCount ? '' : `/${totalCount}`} resultaten
                    </span>
                </div>
                {ui.isMobile && (
                    <Tooltip
                        id="instagram-twitter"
                        title="Verwijder filters"
                        placement={window.innerWidth > 959 ? 'top' : 'left'}
                        classes={{ tooltip: classes.tooltip }}
                    >
                        <ClearAll
                            htmlColor={primaryColor}
                            fontSize="large"
                            onClick={() => {
                                clothingManagement.articleFilter = '';
                                clothingManagement.filters.sex = '';
                                clothingManagement.filters.hasPicture = '';
                                clothingManagement.filters.minAge = '';
                                clothingManagement.filters.maxAge = '';
                                clothingManagement.filters.linked = '';
                            }}
                        />
                    </Tooltip>
                )}
            </div>
            <div className={classes.elements}>
                <div>
                    <Inputs.Input
                        type="select"
                        label="Kledingstuk"
                        value={clothingManagement.filters.clothingId}
                        onChange={v => (clothingManagement.filters.clothingId = v)}
                        options={clothingManagement.autoCompleteClothing}
                    />
                </div>
                <div>
                    <Inputs.Input
                        type="select"
                        label="Maat"
                        value={clothingManagement.filters.clothingSizeId}
                        onChange={v => (clothingManagement.filters.clothingSizeId = v)}
                        options={clothingManagement.autoCompleteClothingSizes}
                    />
                    <Inputs.Input
                        type="select"
                        label="Stof"
                        value={clothingManagement.filters.fabricId}
                        onChange={v => (clothingManagement.filters.fabricId = v)}
                        options={clothingManagement.autoCompleteFabrics}
                    />
                    <Inputs.Input
                        type="select"
                        label="Motief"
                        value={clothingManagement.filters.patternId}
                        onChange={v => (clothingManagement.filters.patternId = v)}
                        options={clothingManagement.autoCompletePatterns}
                    />
                    <Inputs.Input
                        type="select"
                        label="Kleur"
                        value={clothingManagement.filters.colorId}
                        onChange={v => (clothingManagement.filters.colorId = v)}
                        options={clothingManagement.autoCompleteColors}
                    />
                    {!props.noRack && (
                        <Inputs.Input
                            type="select"
                            label="Rek"
                            value={clothingManagement.filters.rack}
                            onChange={v => (clothingManagement.filters.rack = v)}
                            options={clothingManagement.autoCompleteRacks}
                        />
                    )}
                </div>
                {!ui.isMobile && (
                    <Tooltip
                        id="instagram-twitter"
                        title="Verwijder filters"
                        placement={window.innerWidth > 959 ? 'top' : 'left'}
                        classes={{ tooltip: classes.tooltip }}
                    >
                        <ClearAll
                            htmlColor={primaryColor}
                            fontSize="large"
                            onClick={() => {
                                clothingManagement.articleFilter = '';
                                clothingManagement.filters.clothingId = '';
                                clothingManagement.filters.clothingSizeId = '';
                                clothingManagement.filters.patternId = '';
                                clothingManagement.filters.fabricId = '';
                                clothingManagement.filters.colorId = '';
                                clothingManagement.filters.rack = '';
                            }}
                        />
                    </Tooltip>
                )}
                {/* <div className={classes.sort}>
                    <Sort />
                    <Inputs.Input
                        type="select"
                        label="Sortering"
                        value={clothingManagement.sort}
                        onChange={v => (clothingManagement.sort = v)}
                        options={[
                            { label: 'A-Z', value: 0 },
                            { label: 'Z-A', value: 1 },
                            { label: 'Geregistreerd oplopend', value: 2 },
                            { label: 'Geregistreerd aflopend', value: 3 },
                            { label: 'Leeftijd oplopend', value: 4 },
                            { label: 'Leeftijd aflopend', value: 5 }
                        ]}
                    />
                </div> */}
            </div>
        </div>
    );
}

export default withStyles(filterBarStyle)(observer(FilterBar));
