import { FilterList } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import React from 'react';
import { withTranslation } from 'react-i18next';
import filterBarStyle from './filterBar.style.jsx';

function FilterBar(props) {
    const { classes, loading, total = 0, filtered = 0, children, t } = props;

    return (
        <div className={classes.filters}>
            <div className={classes.results}>
                <div>
                    <FilterList />
                    {loading ? (
                        <span>0 {t('form.labels.results').toLowerCase()}</span>
                    ) : (
                        <span>
                            {filtered}
                            {filtered === total ? '' : `/${total}`}{' '}
                            {total === 1
                                ? t('form.labels.result').toLowerCase()
                                : t('form.labels.results').toLowerCase()}
                        </span>
                    )}
                </div>
            </div>
            {children && <div className={classes.elements}>{children}</div>}
        </div>
    );
}

export default withTranslation()(withStyles(filterBarStyle)(FilterBar));
