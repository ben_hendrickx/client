import { primaryColor, primaryColorDark } from "./material-kit-react";

const colors = {
    white: 'white',
    black: 'black',
    primary: primaryColor,
    primary_darker: primaryColorDark,
    secondary: '#ee7101',
    disabled: 'rgba(0,0,0,.1)',
};

const gradients = {
    main: {
        background: `linear-gradient(180deg, ${colors.primary}, ${colors.primary_darker})`,
    },
};

const shadow = { boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)' };

const divider = {
    height: 1,
    margin: '0 5px',
    background: 'rgba(0,0,0,0.1)',
};

const fonts = {
    logo: 'Oleo script, sans-serif',
};

export { colors, gradients, shadow, divider, fonts };
