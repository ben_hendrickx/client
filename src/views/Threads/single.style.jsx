import { primaryColor } from 'assets/jss/material-kit-react.jsx';
import { white } from '../../assets/jss/material-kit-react.jsx';

const styles = {
    container: {
        height: '100%',
        background: '#f3f3f3',
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        marginTop: 1,
        flexGrow: 1,
        '& header': {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: '18px 21px 18px',
            background: white,
            borderBottom: '1px solid rgba(0,0,0,.15)',
            boxShadow: '0 0 1px rgba(0,0,0,.2)',
            '& svg:hover': {
                color: primaryColor,
                cursor: 'pointer',
            },
        },
        '& main': {
            flexGrow: 1,
            overflowY: 'scroll',
            padding: 20,
            display: 'flex',
            flexDirection: 'column-reverse',
        },
        '& footer': {
            width: '100%',
            padding: '8px 20px 3px',
            background: white,
            borderTop: '1px solid rgba(0,0,0,.15)'
        },
    },
};

export default styles;
