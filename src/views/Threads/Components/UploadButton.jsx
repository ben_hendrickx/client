import React from 'react';
import Button from 'components/CustomButtons/Button.jsx';
import { AttachFile } from '@material-ui/icons';
import { withTranslation } from 'react-i18next';

function UploadButton(props) {
    return (
        <div>
            <input
                accept="image/*,.pdf,.xlsx,.docx,.xls,.doc"
                style={{ display: 'none' }}
                id="contained-button-file"
                type="file"
                onChange={props.onFileSelected}
            />
            <label htmlFor="contained-button-file">
                <Button
                    component="span"
                    style={{ marginLeft: 20, maxHeight: 40, marginTop: 15 }}
                    color="success"
                    size="md"
                >
                    <AttachFile style={{ marginRight: 10 }} /> {props.t('form.button.attachment')}
                </Button>
            </label>
        </div>
    );
}

export default withTranslation()(UploadButton);
