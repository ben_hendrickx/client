import React, { useState, useEffect, useRef } from 'react';
import Inputs from 'components/Inputs';
import { InsertEmoticon } from '@material-ui/icons';
import Picker from 'emoji-picker-react';
import { withStyles } from '@material-ui/styles';
import { primaryColor } from 'assets/jss/material-kit-react';
import { useStore } from '../../../mobx';
import classNames from 'classnames';
import { withTranslation } from 'react-i18next';

const styles = {
    emoji: {
        marginTop: 25,
        marginLeft: -35,
        cursor: 'pointer',
        color: 'rgba(0,0,0,.7)',
        '&:hover': {
            color: primaryColor,
        },
    },
};

function ChatInput(props) {
    const { classes, t } = props;
    const [picker, setPicker] = useState(false);
    const inputRef = useRef(null);
    const {
        general: { modals },
    } = useStore();

    useEffect(() => {
        function upHandler(e) {
            if (e.code && e.code.toLowerCase().includes('escape')) {
                setPicker(false);
            }
        }

        function downHandler(e) {
            const code = (e.code || '').toLowerCase();
            const isEscape = code.includes('escape');
            const isEnter = code.includes('enter');

            if (!modals.length && !(isEscape || isEnter)) {
                inputRef.current.focus();
            }
        }

        window.addEventListener('keyup', upHandler);
        if (!props.disableAutoFocus) {
            window.addEventListener('keydown', downHandler);
        }

        return () => {
            window.removeEventListener('keyup', upHandler);
            window.removeEventListener('keydown', downHandler);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.disableAutoFocus]);

    return (
        <>
            <Inputs.Input
                inputRef={inputRef}
                autoFocus
                style={{ flexGrow: 1, paddingRight: 40 }}
                label={t('form.labels.thread.message')}
                type="text"
                value={props.value}
                onChange={props.onChange}
            />
            <div style={{ position: 'relative' }}>
                <InsertEmoticon className={classNames(classes.emoji, 'emojis')} onClick={() => setPicker(!picker)} />
                {picker && (
                    <Picker
                        onEmojiClick={(e, emoji) => {
                            props.onChange((props.value || '') + emoji.emoji);
                            setPicker(false);
                        }}
                    />
                )}
            </div>
        </>
    );
}

export default withTranslation()(withStyles(styles)(ChatInput));
