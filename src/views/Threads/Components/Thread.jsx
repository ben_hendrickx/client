import React from 'react';
import { withStyles } from '@material-ui/styles';
import classNames from 'classnames';
import { Tooltip } from '@material-ui/core';
import { Avatar } from 'react-chat-elements';
import { useStore } from '../../../mobx';
import timeAgoService from 'services/timeAgoService';

const styles = {
    thread: {
        position: 'relative',
        display: 'flex',
        padding: '10px',
        cursor: 'pointer',
        transition: 'all .3s ease-in-out',
        '&:hover': {
            background: 'rgba(250, 195, 0, .7)',
        },
    },
    unread: {
        '&::before': {
            content: `''`,
            display: 'block',
            width: 3,
            height: '100%',
            background: '#fdc300',
            // background: '#497db0',
            top: 0,
            left: 0,
            position: 'absolute',
        },
        background: 'rgba(250, 195, 0, .4)',
    },

    title: {
        color: 'rgba(0,0,0,.7)',
        maxWidth: 130,
        overflow: 'hidden',
        fontWeight: 500,
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
    },
    message: {
        fontSize: '15px',
        color: 'rgba(0,0,0,.5)',
        maxWidth: 130,
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
    },
    flex: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    timeAgo: {
        fontSize: '13px',
    },
    selected: {
        '&::after': {
            content: `''`,
            display: 'block',
            width: 3,
            height: '100%',
            background: '#fdc300',
            // background: '#497db0',
            top: 0,
            right: 0,
            position: 'absolute',
        },
    },
};

function Thread(props) {
    const { classes, title, onClick, lastMessage, project, className: innerClass = 'thread' } = props;

    const { threads } = useStore();
    const { firstName = '', lastName = '' } = lastMessage?.user || {};
    const initials = `${firstName.slice(0, 1)}${lastName.slice(0, 1)}`.toUpperCase();

    const UserAvatar = () => (
        <Tooltip title={`${firstName} ${lastName}`} placement="bottom">
            <div style={{ maxHeight: 30, userSelect: 'none' }}>
                <Avatar
                    letterItem={{
                        id: initials,
                        letter: initials,
                    }}
                    size="large"
                    type="circle flexible"
                />
            </div>
        </Tooltip>
    );

    const className = classNames({
        [classes.thread]: true,
        [classes.unread]: props.unread,
        [classes.selected]: props.id && props.id === threads.selectedThread?.id,
    });

    const content = (
        <div className={className} onClick={onClick}>
            {!!props.icon && props.icon}
            {lastMessage && lastMessage.user && <UserAvatar />}
            <div
                style={{ marginLeft: 10, display: 'flex', flexDirection: 'column', flexGrow: 1 }}
                className={innerClass}
            >
                <div className={classes.flex}>
                    <div className={classes.title}>{title}</div>
                    {lastMessage && lastMessage.createdDt && (
                        <div className={classes.timeAgo}>
                            {timeAgoService()
                                .get()
                                .format(new Date(lastMessage.createdDt), 'twitter-minute-now')}
                        </div>
                    )}
                </div>
                {lastMessage && <div className={classes.message}>{lastMessage?.message}</div>}
            </div>
        </div>
    );

    if (props.noTooltip) {
        return content;
    }

    return (
        <Tooltip
            title={
                <>
                    <p>
                        <b>{props.title}</b>
                    </p>
                    <p>{project?.name || 'Geen Project'}</p>
                </>
            }
            placement="right"
        >
            {content}
        </Tooltip>
    );
}

export default withStyles(styles)(Thread);
