import React from 'react';
import { apiUrl, useStore } from '../../../mobx';
import { Tooltip } from '@material-ui/core';
import { Avatar, MessageBox } from 'react-chat-elements';
import { withStyles } from '@material-ui/styles';
import timeAgoService from 'services/timeAgoService';

const Url = withStyles({
    url: {
        color: 'blue',
        '&:hover': {
            cursor: 'pointer',
            color: '#8484f3',
            fontWeight: 500,
        },
    },
})(function({ classes, url }) {
    return (
        <span className={classes.url} onClick={() => window.open(url)} style={{ color: 'blue' }}>
            {url}
        </span>
    );
});

function Message(props) {
    const { user, replyOn, message, file, status, createdDt, className } = props;
    const { ui, threads, account } = useStore();

    const self = user.id === account.id;
    let reply;

    const { selectedThread: thread } = threads;

    if (replyOn) {
        const replyMessage = thread.messages.find(message => message.id === replyOn);
        const replyUser = replyMessage.user;

        reply = {
            title: `${replyUser.firstName} ${replyUser.lastName}`,
            message: replyMessage.message,
            titleColor: '#8717ae',
            photoUrl: replyMessage.file ? `${apiUrl}/api/uploads/${replyMessage.file}` : undefined,
        };
    }

    const { firstName, lastName } = user;

    const initials = `${firstName.slice(0, 1)}${lastName.slice(0, 1)}`.toUpperCase();

    const UserAvatar = () => (
        <Tooltip title={`${firstName} ${lastName}`} placement="bottom">
            <div style={{ maxHeight: 30, userSelect: 'none' }}>
                <Avatar
                    letterItem={{
                        id: initials,
                        letter: initials,
                    }}
                    size="large"
                    type="circle flexible"
                />
            </div>
        </Tooltip>
    );

    const position = self ? 'left' : 'right';
    const type = !file ? 'text' : 'png|gif|jpeg|jpg'.includes(file.split('.').pop()) ? 'photo' : 'file';
    const data = file
        ? {
              uri: `${apiUrl}/api/uploads/${file}`,
              status: {
                  click: false,
                  loading: 0,
              },
          }
        : null;

    function onDownload() {
        !!file && window.open(`${apiUrl}/api/download?file=${file}`);
    }

    function onReply() {
        threads.replying = props;
    }

    function onOpen() {
        ui.imagePreview = {
            src: data.uri,
            onDownload,
        };
    }

    const regexUrl = /(http|ftp|https):\/\/([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?/g;

    function replaceUrls(message) {
        if (!message || !regexUrl.test(message)) {
            return message;
        }

        const matches = message.match(regexUrl);
        const pieces = [];

        matches.forEach(url => {
            const urlIndex = message.indexOf(url);
            const piece = message.slice(0, urlIndex);
            message = message.slice(urlIndex + url.length);
            pieces.push(<span>{piece}</span>);
            pieces.push(<Url url={url} />);
        });
        message && pieces.push(<span>{message}</span>);

        return pieces;
    }

    return (
        <div style={{ display: 'flex', justifyContent: self ? 'flex-start' : 'flex-end' }} className={className}>
            {!!self && <UserAvatar />}
            <div style={{ paddingTop: 15, width: '100%' }}>
                <MessageBox
                    reply={reply}
                    position={position}
                    type={type}
                    data={data}
                    onOpen={onOpen}
                    onDownload={onDownload}
                    text={replaceUrls(message)}
                    dateString={timeAgoService()
                        .get()
                        .format(new Date(createdDt))}
                    replyButton={true}
                    status={self ? status : null}
                    onReplyClick={onReply}
                />
            </div>
            {!self && <UserAvatar />}
        </div>
    );
}

export default Message;
