import React from 'react';
import { Delete, PlaylistAdd, Edit } from '@material-ui/icons';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Tooltip from '@material-ui/core/Tooltip';
import ResourceOverview from 'components/ResourceOverview.jsx';
import ListRow from 'components/ListRow.jsx';
import ListRowActions from 'components/ListRowActions.jsx';
import ActionForm from './Forms/Action';
import { secondaryColor } from 'assets/jss/material-kit-react';
import { withTranslation } from 'react-i18next';
import { t } from 'i18next';

function MessagesOverview() {
    const { account, threads, general } = useStore();

    function rowRenderer({ key, style, index }) {
        const action = threads.actions.resource.filtered[index];

        return (
            <li key={key} style={{ ...style, display: 'flex' }}>
                <ListRow style={{ flexGrow: 1 }}>
                    <span style={{ width: 400 }}>{action.name}</span>
                    {(account.rights.canUpdateThreadActions || account.rights.canDeleteThreadActions) && (
                        <ListRowActions>
                            {account.rights.canUpdateThreadActions && (
                                <Tooltip placement="top" title={t('tooltip.thread.editTopic')}>
                                    <Edit
                                        onClick={async () => {
                                            await threads.actions.fetch(action.id);
                                            let modal;
                                            const closeModal = () => modal.close();
                                            modal = general.addModal({
                                                type: 'window',
                                                width: 1100,
                                                title: t('modal.title.threads.topic.edit'),
                                                body: <ActionForm onCancel={closeModal} onConfirm={closeModal} />,
                                            });
                                        }}
                                    />
                                </Tooltip>
                            )}
                            {account.rights.canDeleteThreadActions && (
                                <Tooltip placement="top" title={t('tooltip.thread.deleteTopic')}>
                                    <Delete
                                        onClick={() => {
                                            general.addModal({
                                                title: t('modal.title.threads.topic.delete'),
                                                body: t('modal.body.threads.topic.delete'),
                                                buttonText: t('form.button.delete'),
                                                onConfirm: async () => {
                                                    await threads.actions.resource.delete(`/threads`, action.id);
                                                    threads.actions.resource.status = [];
                                                    threads.actions.resource.data = [];
                                                    threads.actions.resource.fetch(`/threads`);
                                                },
                                            });
                                        }}
                                    />
                                </Tooltip>
                            )}
                        </ListRowActions>
                    )}
                </ListRow>
            </li>
        );
    }

    function onCreateAction() {
        let modal;
        const closeModal = () => {
            modal.close();
        };
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.threads.topic.new'),
            onMinimize: modal => {
                threads.actions.form.data.title &&
                    (modal.title = `Nieuw gespreksonderwerp: ${threads.actions.form.data.title}`);
                return threads.actions.form.data;
            },
            onMaximize: modal => {
                threads.actions.form.data = { ...modal.data };
            },
            body: <ActionForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <>
            <ResourceOverview
                checkable
                apiPrefix={`/threads`}
                store={threads.actions.resource}
                rowRenderer={rowRenderer}
                heightOffset={300}
                actions={[
                    account.rights.canCreateThreadActions && {
                        icon: <PlaylistAdd style={{ color: secondaryColor }} />,
                        onClick: onCreateAction,
                        tooltip: t('tooltip.thread.createTopic'),
                    },
                ]}
            />
        </>
    );
}

export default withTranslation()(observer(MessagesOverview));
