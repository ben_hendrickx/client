import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewThread(props) {
    const { threads, account, projects } = useStore();

    useEffect(() => {
        threads.actions.resource.fetch('/threads');
        threads.actions.getSuggestions();
        projects.resource.filters = {
            inactive: 0,
            all: true,
        };
        projects.resource.fetch();

        return () => {
            threads.data = {};
            delete projects.resource.filters.all;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        !threads.data.id &&
            threads.data.actionId &&
            threads.actions.fetch(threads.data.actionId).then(() => {
                threads.data = {
                    ...threads.data,
                    ...threads.actions.data,
                    id: threads.data.id,
                };
                threads.actions.data = {};
            });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [threads.data.actionId]);

    return (
        <Form
            apiPrefix={`/user/${account.id}`}
            store={threads}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                threads.resource.data = [];
                threads.resource.status = [];
                threads.resource.fetch(`/user/${account.id}`);
            }}
        />
    );
}

NewThread.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewThread);
