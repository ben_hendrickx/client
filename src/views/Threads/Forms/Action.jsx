import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewAction(props) {
    const { threads } = useStore();

    useEffect(() => {
        threads.actions.getSuggestions();

        return () => {
            threads.actions.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            store={threads.actions}
            noCancel
            onConfirm={() => {
                props.onConfirm();
            }}
        />
    );
}

NewAction.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewAction);
