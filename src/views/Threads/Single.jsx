import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { Edit, Archive, ArrowBackIos } from '@material-ui/icons';
import styles from './single.style.jsx';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import { Close, Send } from '@material-ui/icons';

import Button from 'components/CustomButtons/Button.jsx';
import filesize from 'filesize';
import Message from './Components/Message.jsx';
import ChatInput from './Components/ChatInput.jsx';
import UploadButton from './Components/UploadButton.jsx';
import ThreadForm from './Forms/Thread';
import { withTranslation } from 'react-i18next';

function Thread(props) {
    const { classes, searchOpen, t } = props;
    const { general, threads, account, ui } = useStore();
    const { selectedThread: thread } = threads;

    useEffect(() => {
        thread && threads.get(thread.id);
        // eslint-disable-next-line react-hooks/exhaustive-deps
        return () => {
            delete threads.id;
            delete threads.title;
            threads.messages = [];
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.id, thread?.id]);

    useEffect(() => {
        function handler(e) {
            if (e.code && e.code.toLowerCase().includes('enter')) {
                !!threads.message && !threads.sending && threads.send();
            }
        }
        window.addEventListener('keyup', handler);

        return () => {
            window.removeEventListener('keyup', handler);
        };
    }, []);

    if (!thread) {
        return null;
    }

    function onArchiveThread() {
        general.addModal({
            title: t('modal.title.threads.archive'),
            body: t('modal.body.threads.archive'),
            buttonText: t('form.button.archive'),
            onConfirm: async () => {
                await threads.resource.delete(undefined, thread.id, 'gearchiveerd');
                delete threads.selectedThread;
                threads.resource.data = [];
                threads.resource.status = [];
                await threads.resource.fetch(`/user/${account.id}`);
            },
        });
    }

    function onEditThread() {
        let modal;
        const closeModal = () => modal.close();
        threads.data = {
            ...thread,
        };
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: `${t('modal.title.threads.edit')}: ${thread.title}`,
            body: (
                <ThreadForm
                    onConfirm={() => {
                        closeModal();
                        delete threads.selectedThread;
                        threads.resource.success = t('success.thread.edit', { thread: thread.title });
                        threads.resource.data = [];
                        threads.resource.status = [];
                        threads.resource.fetch(`/users/${account.id}`);
                    }}
                    onClose={closeModal}
                />
            ),
            onMinimize: modal => {
                <threads className="data title"></threads> && (modal.title = `${t('modal.title.threads.edit')}: ${threads.data.title}`);
                return threads.data;
            },
            onMaximize: modal => {
                threads.data = { ...modal.data };
            },
        });
    }

    return (
        <div className={classes.container}>
            <header>
                {ui.isMobile && <ArrowBackIos onClick={() => (threads.selectedThread = undefined)} />}
                <span>
                    {!ui.isMobile ? `${t('resource.gesprek')}: ` : ''}
                    {thread.title}
                </span>
                <div>
                    <Edit onClick={() => onEditThread()} style={{ marginRight: 10 }} />
                    <Archive onClick={() => onArchiveThread()} />
                </div>
            </header>
            <main>
                {!!thread.messages?.length &&
                    thread.messages
                        .slice(0)
                        .reverse()
                        .map((message, index) => <Message {...message} className={`message-${index}`} />)}
            </main>
            <footer>
                {threads.replying && (
                    <div style={{ padding: 20, position: 'relative', border: '1px dotted rgba(0,0,0,.15)' }}>
                        <div style={{ cursor: 'pointer', position: 'absolute', right: 10, top: 10 }}>
                            <Close onClick={() => delete threads.replying} />
                        </div>
                        <b>{t('form.labels.thread.replyOn')}:</b>
                        <p>{threads.replying.message}</p>
                    </div>
                )}
                {threads.selectedFile && (
                    <div style={{ padding: 20, position: 'relative', border: '1px dotted rgba(0,0,0,.15)' }}>
                        <div style={{ cursor: 'pointer', position: 'absolute', right: 10, top: 10 }}>
                            <Close onClick={() => delete threads.selectedFile} />
                        </div>
                        <b>{t('form.labels.thread.attach')}:</b>
                        <div style={{ display: 'flex', alignItems: 'center', marginTop: 10 }}>
                            {'png|gif|jpeg|jpg'.includes(threads.selectedFile.name.split('.').pop()) && (
                                <img
                                    alt="uploaded"
                                    style={{ marginRight: 10 }}
                                    src={URL.createObjectURL(threads.selectedFile)}
                                    width={40}
                                    height={40}
                                />
                            )}
                            <span>
                                {threads.selectedFile.name} ({filesize(threads.selectedFile.size)})
                            </span>
                        </div>
                    </div>
                )}
                <div style={{ display: 'flex', position: 'relative' }}>
                    <ChatInput
                        disableAutoFocus={searchOpen}
                        value={threads.message}
                        onChange={v => (threads.message = v)}
                    />
                    <Button
                        disabled={threads.sending || !threads.message}
                        onClick={() => threads.send()}
                        style={{ marginLeft: 20, maxHeight: 40, marginTop: 15 }}
                        color="primary"
                        size="md"
                        loading={threads.sending}
                    >
                        {t('form.button.send')} <Send style={{ marginLeft: 10 }} />
                    </Button>
                    {!ui.isMobile && (
                        <UploadButton
                            onFileSelected={e => {
                                threads.selectedFile = e.target.files[0];
                            }}
                        />
                    )}
                </div>
            </footer>
        </div>
    );
}

Thread.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(styles)(observer(Thread)));
