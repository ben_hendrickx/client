import React, { useEffect, useState, useRef } from 'react';
import { white, primaryColor, secondaryColor } from 'assets/jss/material-kit-react';
import { VisibilityOff, Search, Message } from '@material-ui/icons';
import { withStyles } from '@material-ui/styles';
import { InputBase } from '@material-ui/core';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';

import Thread from './Components/Thread';
import ThreadForm from './Forms/Thread';
import Single from './Single';
import { t } from 'i18next';

const styles = {
    container: {
        display: 'flex',
        height: 'calc(100% + 2rem)',
        backgroundColor: 'white',
        margin: '-1rem',
        overflow: 'hidden',
    },
    sidebar: {
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        minWidth: 300,
        maxWidth: 300,
        borderRight: '1px solid rgba(0,0,0,.1)',
        maxHeight: '100%',
    },
    newThread: {
        borderBottom: '1px solid rgba(0,0,0,.1)',
        boxShadow: '0 0 1px rgba(0,0,0,.2)',
    },
    threads: {
        display: 'flex',
        flexDirection: 'column',
        maxHeight: 'calc(100% - 68px - 45px)',
        height: '100%',
        overflowY: 'auto',
        background: '#f3f3f3',
    },
    search: {
        width: '100%',
        padding: '5px 10px',
        background: '#dadada',
    },
    input: {
        flex: 1,
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        width: '100%',
        borderTop: '1px solid rgba(0,0,0,.1)',
        boxShadow: '-1px 0 1px rgba(0,0,0,.2)',
        display: 'flex',
        flexWrap: 'wrap',
        '& > div': {
            textAlign: 'center',
            flexGrow: 1,
            padding: 10,
            height: 44,
            '&:hover': {
                background: primaryColor,
                color: white,
                cursor: 'pointer',
            },
            '&:first-child': {
                borderRight: '1px solid rgba(0,0,0,.1)',
            },
        },
    },
};

const MessageIcon = () => (
    <div
        style={{
            marginTop: 6,
            marginLeft: 4,
            marginRight: 10,
            padding: '9px 6px 1px 7px',
            background: secondaryColor,
            color: white,
            borderRadius: '100%',
            maxWidth: 35,
            maxHeight: 35,
        }}
    >
        <Message fontSize="small" />
    </div>
);

function MessagesOverview({ classes }) {
    const [search, setSearch] = useState('');
    const [searchOpen, setSearchOpen] = useState(false);
    const [unread, setUnread] = useState(false);
    const inputRef = useRef(null);
    const { threads, account, general, ui } = useStore();

    useEffect(() => {
        threads.resource.fetch(`/user/${account.id}`);

        return () => {
            delete threads.selectedThread;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (searchOpen) {
            inputRef.current.focus();
        }
    }, [searchOpen]);

    function onCreateThread() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.threads.new'),
            onMinimize: modal => {
                threads.data.title && (modal.title = `${t('modal.title.threads.new')}: ${threads.data.title}`);
                return threads.data;
            },
            onMaximize: modal => {
                threads.data = { ...modal.data };
            },
            body: <ThreadForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <div className={classes.container}>
            {(!ui.isMobile || !threads.selectedThread) && (
                <div className={classes.sidebar}>
                    <div className={classes.newThread}>
                        <Thread
                            className="new"
                            title={t('form.button.newThread')}
                            lastMessage={{ message: t('form.button.newThreadBody') }}
                            icon={<MessageIcon />}
                            onClick={onCreateThread}
                            noTooltip
                        />
                    </div>
                    <div className={classes.threads} style={search ? { maxHeight: 'calc(100% - 68px - 87px)' } : {}}>
                        {threads.resource.filtered
                            .filter(thread => {
                                if (unread && !thread.unread) {
                                    return false;
                                }
                                return (
                                    !searchOpen || !search || thread.title.toLowerCase().includes(search.toLowerCase())
                                );
                            })
                            .map((thread, threadIndex) => (
                                <Thread
                                    className={`thread-${threadIndex}`}
                                    {...thread}
                                    onClick={() => {
                                        threads.selectedThread = thread;
                                        thread.unread = false;
                                    }}
                                />
                            ))}
                    </div>
                    <footer className={classes.footer}>
                        {searchOpen && (
                            <section className={classes.search}>
                                <InputBase
                                    inputRef={inputRef}
                                    className={classes.input}
                                    placeholder={t('form.placeholder.searchTopic')}
                                    inputProps={{ 'aria-label': t('form.placeholder.searchTopic') }}
                                    value={search}
                                    onChange={e => setSearch(e.target.value)}
                                />
                            </section>
                        )}
                        <div
                            onClick={() => {
                                searchOpen && setSearch('');
                                setSearchOpen(!searchOpen);
                            }}
                            style={
                                searchOpen
                                    ? {
                                          background: primaryColor,
                                          color: 'white',
                                          borderRight: `1px solid ${secondaryColor}`,
                                      }
                                    : {}
                            }
                        >
                            <Search />
                        </div>
                        <div
                            onClick={() => setUnread(!unread)}
                            style={unread ? { background: primaryColor, color: 'white' } : {}}
                        >
                            <VisibilityOff />
                        </div>
                    </footer>
                </div>
            )}
            {threads.selectedThread && <Single searchOpen={searchOpen} />}
        </div>
    );
}

export default withStyles(styles)(observer(MessagesOverview));
