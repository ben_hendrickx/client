import React from 'react';
import PropTypes from 'prop-types';

import useReactRouter from 'use-react-router';
import { observer } from 'mobx-react-lite';
import TabView from 'components/TabView.jsx';

import Overview from 'views/Threads/Overview.jsx';
import SingleThread from 'views/Threads/Single.jsx';

import { AccountBalance, Assignment } from '@material-ui/icons';
import Page from 'components/Page';
import ActionsOverview from './ActionsOverview';
import { Success } from 'components/Notifications';
import { useStore } from '../../mobx';
import { withTranslation } from 'react-i18next';
import { t } from 'i18next';

function SingleThreadNoPadding(props) {
    return <SingleThread style={{ margin: -20 }} {...props} />;
}

function Threads() {
    const { account, threads } = useStore();
    const { history } = useReactRouter();

    return (
        <Page style={{ marginTop: 30 }}>
            <Success message={threads.resource.success} onClose={() => (threads.resource.success = '')} />
            <TabView
                big
                tabs={[
                    {
                        label: t('resource.gesprekken'),
                        icon: <AccountBalance />,
                        onClick: () => history.push(`/gesprekken/overzicht`),
                    },
                    account.rights.canReadThreadActions && {
                        label: t('resource.acties'),
                        icon: <Assignment />,
                        onClick: () => history.push(`/gesprekken/onderwerpen`),
                    },
                ]}
                routes={[
                    account.rights.canReadThreadActions && {
                        path: '/gesprekken/onderwerpen',
                        component: ActionsOverview,
                    },
                    { path: '/gesprekken/overzicht', component: Overview },
                    { path: '/gesprekken/:id', component: SingleThreadNoPadding },
                ]}
            />
        </Page>
    );
}

Threads.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(observer(Threads));
