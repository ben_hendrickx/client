import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { Delete } from '@material-ui/icons';
import '../Users/rights.scss';
import ResourceOverview from 'components/ResourceOverview';
import ListRow from 'components/ListRow';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import Inputs from 'components/Inputs';

const AccessRoleModalContent = observer(function AccessRoleModalContent() {
    const { accessRoles, projects } = useStore();

    useEffect(() => {
        async function fetchData() {
            accessRoles.data = (await accessRoles.resource.fetchAndReturn(`/project/${projects.id}`)).data.map(
                ({ id, name }) => ({
                    value: id,
                    label: name,
                })
            );
        }

        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <Inputs.Input
                fullWidth
                label="Toegangsrol"
                type="select"
                value={accessRoles.selected}
                onChange={v => (accessRoles.selected = v)}
                options={accessRoles.data}
            />
        </>
    );
});

function Rights() {
    const { projects, users, accessRoles, general } = useStore();

    useEffect(() => {
        return () => {
            users.success = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function rowRenderer({ key, index, style }) {
        const accessRole = accessRoles.resource.filtered[index];

        const { id, name } = accessRole;

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span>{name}</span>
                    <ListRowActions>
                        <Tooltip title="Verwijder toegangsrol van gebruiker" placement="top">
                            <Delete
                                onClick={() => {
                                    general.addModal({
                                        title: 'Verwijder toegangsrol van gebruiker',
                                        body: 'Bent u zeker dat u deze toegangsrol wil verwijderen van deze gebruiker?',
                                        buttonText: 'Verwijder',
                                        onConfirm: async () => {
                                            await accessRoles.resource.delete(
                                                `/project/${projects.id}/user/${users.selectedUser.id}`,
                                                id
                                            );
                                            await accessRoles.resource.fetch(
                                                `/project/${projects.id}/user/${users.selectedUser.id}`,
                                                id
                                            );
                                        },
                                    });
                                }}
                            />
                        </Tooltip>
                    </ListRowActions>
                </ListRow>
            </li>
        );
    }

    return (
        <>
            <ResourceOverview
                apiPrefix={`/project/${projects.id}/user/${users.selectedUser.id}`}
                store={accessRoles.resource}
                rowRenderer={rowRenderer}
                heightOffset={250}
                actions={[
                    //TODO: check rights and remove level property
                    {
                        icon: 'add',
                        onClick: () => {
                            general.addModal({
                                title: 'Voeg toegangsrol toe aan gebruiker',
                                body: <AccessRoleModalContent />,
                                buttonText: 'Voeg toe',
                                onConfirm: async () => {
                                    await accessRoles.addToUser(users.selectedUser.id, `/project/${projects.id}`);
                                    setTimeout(async () => {
                                        accessRoles.resource.success = 'Toegangsrol succesvol toegevoegd ';
                                        accessRoles.resource.data = [];
                                        accessRoles.resource.status = [];
                                        await accessRoles.resource.fetch(
                                            `/project/${projects.id}/user/${users.selectedUser.id}`
                                        );
                                    }, 1000);
                                },
                            });
                        },
                        tooltip: 'Voeg toegangsrol toe aan gebruiker',
                    },
                ]}
            />
        </>
    );
}

export default observer(Rights);
