import { primaryColor } from 'assets/jss/material-kit-react.jsx';
import { white, primaryCardHeader } from '../../assets/jss/material-kit-react.jsx';

const registrationsDetailStyle = {
    header: {
        ...primaryCardHeader,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    container: {
        width: '100%',
        '& h2': {
            marginTop: 40,
            fontSize: '1.5em',
        },
    },
    title: {
        '& > h1': {
            fontSize: '2em',
            margin: '0 10px',
        },
    },
    content: {
        '& > h2': {
            marginTop: 0,
        },
        '& > div > div > div ': {
            background: white,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            padding: '10px 15px',
            margin: '26px 0',
            '& > span': {
                marginTop: 10,
            },
            '& > span:first-child': {
                marginTop: 0,
            },
        },
    },
    overlay: {
        background: 'rgba(0,0,0,0.6)',
        borderRadius: 5,
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& div': {
            padding: '20px 30px',
            borderRadius: '5px',
            background: '#ffffff',
            boxShadow:
                '2px 1px 5px -1px rgba(0,0,0,0.8), 0px 0px 1px 0px rgba(0,0,0,0.74), 0px 3px 5px -2px rgba(0,0,0,0.72)',
            '& h1': {
                fontSize: '1.5rem',
            },
        },
    },
    cardAction: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 5,
        cursor: 'pointer',
        '&:hover': {
            color: primaryColor,
        },
    },
    card: {
        position: 'relative',
        '& img': {
            maxWidth: '100%',
        },
    },
};

export default registrationsDetailStyle;
