import React, { useEffect } from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import UserRow from '../../components/User/Row.jsx';
import useReactRouter from 'use-react-router';

import Inputs from 'components/Inputs';
import Row from './Row';
import { withTranslation } from 'react-i18next';

function Filters() {
    const { registrations } = useStore();

    return (
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <Inputs.registrations.filters.sex />
            <Inputs.registrations.filters.minAge />
            <Inputs.registrations.filters.maxAge />
            <Inputs.registrations.filters.onStage />
            {registrations.resource.filters.onStage !== '0' && <Inputs.registrations.filters.auditioning />}
            <Inputs.registrations.filters.remarks />
            <Inputs.registrations.filters.talents />
        </div>
    );
}

function RegistrationsOverview(props) {
    const { account, registrations, projects, ui } = useStore();
    const { apiPrefix = `/project/${projects.id}`, t } = props;
    const { match } = useReactRouter();

    useEffect(() => {
        registrations.resource.findIn = 'user';
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function rowRenderer({ key, index, style }) {
        const registration = registrations.resource.filtered[index];
        const {
            canReadRegistrationsGeneral,
            canReadRegistrationsPreferred,
            canReadRegistrationsOther,
        } = account.rights;

        const canView = canReadRegistrationsGeneral || canReadRegistrationsPreferred || canReadRegistrationsOther;

        return (
            <UserRow
                style={{ ...style, height: 54, margin: '13px 0' }}
                details={true} //hasUsersRIghts
                key={key}
                {...registration.user}
                registrationId={registration.id}
                projectId={projects.id}
                onClick={() => {}}
                watch={canView}
            />
        );
    }

    function userRowRenderer({ key, index, style }) {
        const registration = registrations.resource.filtered[index];

        return <Row key={key} style={style} registration={registration} />;
    }

    return (
        <ResourceOverview
            keepFilters
            style={account.level === 0 ? { maxHeight: 'calc(50vh - 50px)' } : {}}
            noAutoSizer={account.level === 0}
            heightOffset={account.level === 0 ? '50vh - 400px' : 470}
            baseHeight={account.level === 0 ? '100%' : '100vh'}
            apiPrefix={apiPrefix}
            store={registrations.resource}
            rowRenderer={account.level || match.params.id ? rowRenderer : userRowRenderer}
            rowHeight={ui.isMobile ? 94 : 80}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateRegistrations && {
                    icon: 'notListed',
                    route: `/projecten/${projects.id}/inschrijvingen/niet-ingeschreven`,
                    tooltip: t('tooltip.project.notRegistered'),
                },
                account.rights.canExportRegistrations && {
                    icon: 'getApp',
                    href: '/api/reports/registrations.xlsx',
                    addition:
                        'projectId=' +
                        projects.id +
                        `${registrations.filters.getString() ? `&${registrations.filters.getString()}` : ''}`,
                    tooltip: t('tooltip.report.download'),
                },
            ]}
            filters={account.level ? <Filters /> : null}
            sort={account.level ? <Inputs.registrations.sort /> : null}
        />
    );
}

export default withTranslation()(observer(RegistrationsOverview));
