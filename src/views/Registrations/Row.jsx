import { Tooltip } from '@material-ui/core';
import ListRow from 'components/ListRow';
import React from 'react';
import { Edit, FolderOpen, RemoveRedEye } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';

function RegistrationRow(props) {
    const { account } = useStore();
    const { key, style, registration } = props;
    const history = useHistory();
    const { id, project } = registration;
    const { name } = project;

    return (
        <li key={key} style={style}>
            <ListRow key={key}>
                <Tooltip placement="top" title={name}>
                    <span style={{ width: 200 }}>
                        <FolderOpen />{' '}
                        <span
                            style={{
                                display: 'inline-block',
                                width: 170,
                                overflow: 'hidden',
                                textOverflow: 'ellipsis',
                                whiteSpace: 'nowrap',
                            }}
                        >
                            {name}
                        </span>
                    </span>
                </Tooltip>
                <ListRowActions>
                    <Tooltip title="Bekijk inschrijving" placement="top">
                        <RemoveRedEye
                            onClick={() => history.push(`/inschrijvingen/${id}`)}
                            className="view-registration"
                        />
                    </Tooltip>
                    {(account.rights.canReadProjects ||
                        project.rights.some(({ name, read }) => !!read && name === 'projects')) && (
                        <Tooltip title="Beheer project" placement="top">
                            <Edit onClick={() => history.push(`/projecten/${project.id}/overzicht`)} />
                        </Tooltip>
                    )}
                </ListRowActions>
            </ListRow>
        </li>
    );
}

export default observer(RegistrationRow);
