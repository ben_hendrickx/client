import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import UserRow from '../../components/User/Row.jsx';
import Inputs from 'components/Inputs';
import Register from 'views/Projects/Register';
import { withTranslation } from 'react-i18next';

function Filters() {
    return (
        <>
            <Inputs.users.filters.sex />
            <Inputs.users.filters.minAge />
            <Inputs.users.filters.maxAge />
        </>
    );
}

function NotRegisteredUsersOverview({ t }) {
    const { account, general, users, projects } = useStore();

    function rowRenderer({ key, index, style }) {
        const user = users.resource.filtered[index];
        return (
            <UserRow
                key={key}
                style={{ ...(style || {}), height: 'auto', width: '100%', margin: '10px 0px 10px 0px' }}
                onClick={() => {}}
                {...user}
                actions={[
                    // {
                    //     developer: true,
                    //     tooltip: 'Delete user',
                    //     icon: 'delete',
                    //     callback: () => {},
                    // },
                    account.rights.canCreateRegistrations && {
                        tooltip: t('tooltip.project.register'),
                        icon: 'note_add',
                        callback: () => {
                            let modal;
                            modal = general.addModal({
                                type: 'window',
                                body: (
                                    <Register
                                        userId={user.id}
                                        onConfirm={() => {
                                            modal.close();
                                            users.resource.data = [];
                                            users.resource.status = [];
                                            users.resource.fetch(`/project/${projects.id}/not-registered`);
                                        }}
                                    />
                                ),
                                width: 900,
                            });
                        },
                    },
                ]}
            />
        );
    }

    return (
        <ResourceOverview
            backend
            apiPrefix={`/project/${projects.id}/not-registered`}
            store={users.resource}
            rowRenderer={rowRenderer}
            heightOffset={260}
            rowHeight={80}
            actions={[
                {
                    icon: 'back',
                    route: '/projecten/' + projects.id + '/inschrijvingen',
                    tooltip: t('tooltip.project.registered'),
                },
                account.rights.canExportUsers && {
                    icon: 'getApp',
                    href: '/api/reports/notregistered.xlsx',
                    tooltip: t('tooltip.report.download'),
                },
            ]}
            filters={<Filters />}
            sort={<Inputs.users.sort />}
        />
    );
}

export default withTranslation()(observer(NotRegisteredUsersOverview));
