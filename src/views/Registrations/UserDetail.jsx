import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import registrationsDetailStyle from './userdetail.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Button from '../../components/CustomButtons/Button.jsx';
import Loader from '../../components/Loader.jsx';
import Label from '../../components/Label/Label.jsx';
import { Admin } from '../../components/Levels.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import EditableCard from 'components/EditableCard.jsx';
import { FormControl, FormLabel, FormGroup, FormControlLabel, Checkbox } from '@material-ui/core';
import { Danger } from 'components/Notifications.jsx';
import { Success } from 'components/Notifications.jsx';
import { User } from 'components/Levels.jsx';
import Page from 'components/Page.jsx';
import Profile from 'views/Users/Profile.jsx';
import Inputs from 'components/Inputs';
import moment from 'moment';

function SignOffBody({ name }) {
    return (
        <>
            <span>Ben je zeker dat je je wil uitschrijven voor {name}?</span>
            <div
                style={{
                    padding: 10,
                    background: 'orange',
                    marginTop: 10,
                    color: 'white',
                }}
            >
                <b>Opgelet:</b> uw profiels-gegevens worden niet verwijderd.
                <br />
                Indien u dit toch wenst neem contact op via{' '}
                <a href={`mailto:${process.env.REACT_APP_EMAIL}`} style={{ color: 'white', fontWeight: 'bold' }}>
                    {process.env.REACT_APP_EMAIL}
                </a>
            </div>
        </>
    );
}

function RegistrationsOverview(props) {
    const { classes } = props;
    const { registrations, ui, account, users, projects, roles: rolesStore, general } = useStore();
    const { match, history } = useReactRouter();

    const { selectedRegistration } = registrations;
    const { selectedUser = { sizes: {} } } = users;
    const [location, setLocation] = useState(false);
    const [editRoles, setEditRoles] = useState(false);
    const [editStage, setEditStage] = useState(false);

    useEffect(() => {
        async function fetchData() {
            await registrations.fetchRegistration(match.params.registrationId);
            registrations.fetchRolesForRegistration(match.params.registrationId);
            account.level === 0 &&
                (await account.rights.fetchRightsForProject(registrations.selectedRegistration.project.id));
        }

        projects.fetchPartsOfTown();

        if (match?.params?.registrationId) {
            fetchData();
        }

        rolesStore.fetch();

        return () => {
            delete registrations.selectedRegistration;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.token, match.params.id, match.params.registrationId]);

    if (!selectedRegistration || !selectedRegistration.id) {
        if (registrations.loading) {
            return <Loader />;
        }

        return (
            <Page title="Inschrijving niet gevonden">
                <div className={classes.content}>
                    <h2 style={{ margin: 0 }}>Voor deze gebruiker kon de inschrijving niet worden gevonden</h2>
                </div>
                <Button
                    onClick={() => {
                        history.push(ui.isMobile ? '/inschrijvingen' : '/projecten');
                    }}
                    style={{ marginTop: 30 }}
                    color="primary"
                    size="md"
                >
                    Ga terug naar overzicht
                </Button>
            </Page>
        );
    }

    // TODO: fetch roles separately
    const isOnStage = ((selectedRegistration.roles || [])[0] || {}).onStage === 1;
    const { project, roles = [], auditioning, talents, remarks, owner_remarks: ownerRemarks } = selectedRegistration;
    const { firstName = '', lastName = '' } = selectedUser;
    const { loading } = registrations;

    let canSignOff = false;

    if (!project.deleteRegistrationClosed) {
        canSignOff = isOnStage
            ? moment(project.closedFrom).isAfter(moment())
            : moment(project.closedOffStage).isAfter(moment());
    } else {
        canSignOff = moment(project.deleteRegistrationClosed).isAfter(moment());
    }

    const noMargin = { margin: 0 };

    let partOfTown;
    const projectData = projects.resource.item;
    if (projectData.extended) {
        partOfTown = (
            projects.partsOfTown.find(({ value }) => value === parseInt(selectedRegistration.part_of_town)) || {}
        ).label;
    }

    const onStageEnabled = moment(projectData.closedFrom).isAfter(moment());
    const offStageEnabled = new Date(projectData.closedOffStage).getTime() >= Date.now();
    let editEnabled = false;

    if (!projectData.editRegistrationClosed || moment(projectData.editRegistrationClosed).isAfter(moment())) {
        editEnabled = onStageEnabled || offStageEnabled;
    } else {
        editEnabled = isOnStage ? onStageEnabled : offStageEnabled;
    }

    if (account.level === 1) {
        editEnabled = true;
    }

    const canSwitchStage = (isOnStage && offStageEnabled) || (!isOnStage && onStageEnabled) || account.level === 1;

    //const hidePrefered = (projectData.extended === 1 && (location === 'on' || isOnStage) && location !== 'off');
    const hidePrefered = false;

    const PageWrapperConditional = props =>
        [1, 2, 3].includes(account.level) ? (
            <div className={classes.container}>
                <Loader loading={loading}>{props.children}</Loader>
            </div>
        ) : (
            <Page title={`Inschrijving`} loading={loading}>
                {props.children}
            </Page>
        );

    return (
        <>
            <PageWrapperConditional>
                <div className={classes.content}>
                    <Danger message={registrations.error} onClose={() => (registrations.error = '')} />
                    <Success message={registrations.success} onClose={() => (registrations.success = '')} />
                    <GridContainer>
                        {account.rights.canReadRegistrationsGeneral && (
                            <GridItem xs={12} sm={12} md={4}>
                                <h2 style={noMargin}>Algemene gegevens</h2>
                                <EditableCard
                                    editable={account.rights.canUpdateRegistrations && (editEnabled && canSwitchStage)}
                                    edit={editStage}
                                    onEdit={edit => {
                                        setEditStage(edit);
                                        setEditRoles(edit);
                                    }}
                                    onSave={async () => {
                                        await registrations.saveRoles();
                                        await registrations.fetchRegistration(match.params.registrationId);
                                        await registrations.fetchRolesForRegistration(match.params.registrationId);
                                    }}
                                >
                                    {({ edit }) => {
                                        if (!edit) {
                                            return (
                                                <>
                                                    <Label value="Waar wil je meewerken?" />{' '}
                                                    {isOnStage ? 'on-stage' : 'off-stage'}
                                                </>
                                            );
                                        }

                                        return (
                                            <FormControl fullWidth component="fieldset" className={classes.formControl}>
                                                <FormLabel component="legend">Waar wil je meewerken?</FormLabel>
                                                <FormGroup>
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                color="primary"
                                                                checked={
                                                                    location === false ? isOnStage : location === 'on'
                                                                }
                                                                onChange={e => {
                                                                    setLocation(e.target.value);
                                                                }}
                                                                value="on"
                                                            />
                                                        }
                                                        label="on-stage"
                                                    />
                                                    <FormControlLabel
                                                        control={
                                                            <Checkbox
                                                                color="primary"
                                                                checked={
                                                                    location === false ? !isOnStage : location === 'off'
                                                                }
                                                                onChange={e => {
                                                                    setLocation(e.target.value);
                                                                }}
                                                                value="off"
                                                            />
                                                        }
                                                        label="off-stage"
                                                    />
                                                </FormGroup>
                                            </FormControl>
                                        );
                                    }}
                                </EditableCard>
                            </GridItem>
                        )}
                        {!hidePrefered && account.rights.canReadRegistrationsPreferred && (
                            <GridItem xs={12} sm={12} md={4}>
                                <h2 style={noMargin}>Voorkeurs-gegevens</h2>
                                <EditableCard
                                    editable={account.rights.canUpdateRegistrations && editEnabled}
                                    edit={editRoles}
                                    onEdit={edit => {
                                        canSwitchStage && setEditStage(edit);
                                        setEditRoles(edit);
                                    }}
                                    onSave={async () => {
                                        await registrations.saveRoles();
                                        await registrations.fetchRegistration(match.params.registrationId);
                                        await registrations.fetchRolesForRegistration(match.params.registrationId);
                                    }}
                                >
                                    {({ edit }) => {
                                        if (!edit) {
                                            return (
                                                <>
                                                    <Label
                                                        value={`${isOnStage ? 'On-stage' : 'Off-stage'} voorkeur:`}
                                                    />
                                                    {roles.map(({ role }) => (
                                                        <span style={{ display: 'block' }}>{role}</span>
                                                    ))}
                                                    {isOnStage && !!projectData.auditions_enabled && (
                                                        <Label value="Wenst auditie te doen:" />
                                                    )}
                                                    {isOnStage && !!projectData.auditions_enabled && (
                                                        <span>{auditioning ? 'Ja' : 'Nee'}</span>
                                                    )}
                                                </>
                                            );
                                        }
                                        return (
                                            <>
                                                {
                                                    <>
                                                        {(location === 'on' || (!location && isOnStage)) && (
                                                            <Inputs.Input
                                                                label="On-stage voorkeur"
                                                                type="select"
                                                                multi
                                                                required
                                                                fullWidth
                                                                options={
                                                                    projectData.extended
                                                                        ? rolesStore.onStageAutoComplete
                                                                        : rolesStore.onStageAutoComplete.filter(
                                                                              ({ for_extended }) => !for_extended
                                                                          )
                                                                }
                                                                value={registrations.selectedRoles}
                                                                onChange={v => {
                                                                    registrations.selectedRoles = v;
                                                                }}
                                                            />
                                                        )}
                                                        {(location === 'off' || (!location && !isOnStage)) && (
                                                            <Inputs.Input
                                                                label="Off-stage voorkeur"
                                                                type="select"
                                                                multi
                                                                required
                                                                fullWidth
                                                                options={
                                                                    projectData.extended
                                                                        ? rolesStore.offStageAutoComplete
                                                                        : rolesStore.offStageAutoComplete.filter(
                                                                              ({ for_extended }) => !for_extended
                                                                          )
                                                                }
                                                                value={registrations.selectedRoles}
                                                                onChange={v => {
                                                                    registrations.selectedRoles = v;
                                                                }}
                                                            />
                                                        )}
                                                        {(location === 'on' || (!location && isOnStage)) &&
                                                            projectData.auditions_enabled !== 0 && (
                                                                <FormControl fullWidth>
                                                                    <FormControlLabel
                                                                        control={
                                                                            <Checkbox
                                                                                color="primary"
                                                                                checked={registrations.auditioning}
                                                                                onChange={e => {
                                                                                    registrations.auditioning =
                                                                                        e.target.checked;
                                                                                }}
                                                                                value={registrations.auditioning}
                                                                                inputProps={{
                                                                                    'aria-label':
                                                                                        'Ik wens graag auditie te doen',
                                                                                }}
                                                                            />
                                                                        }
                                                                        label={
                                                                            <p>
                                                                                Ik wens graag auditie te doen{' '}
                                                                                {`${
                                                                                    projectData.auditionHint
                                                                                        ? `(${projectData.auditionHint})`
                                                                                        : ''
                                                                                }`}
                                                                            </p>
                                                                        }
                                                                    />
                                                                </FormControl>
                                                            )}
                                                        <p style={{ marginTop: 20, fontSize: 11 }}>
                                                            Selecteer meerdere opties door de CTRL-toets ingedrukt te
                                                            houden
                                                        </p>
                                                    </>
                                                }
                                            </>
                                        );
                                    }}
                                </EditableCard>
                            </GridItem>
                        )}
                        {(account.rights.canReadRegistrationsGeneral || account.rights.canReadRegistrationsRemarks) && (
                            <GridItem xs={12} sm={12} md={4}>
                                <h2 style={noMargin}>Overige gegevens</h2>
                                <EditableCard
                                    editable={account.rights.canUpdateRegistrations && editEnabled}
                                    onSave={() => {
                                        registrations.saveOthers(selectedRegistration).then(() => {
                                            selectedRegistration.talents = selectedRegistration.newTalents;
                                            selectedRegistration.remarks = selectedRegistration.newRemarks;
                                            selectedRegistration.owner_remarks = selectedRegistration.newOwnerRemarks;
                                            selectedRegistration.part_of_town = selectedRegistration.newPartOfTown;
                                            delete selectedRegistration.newTalents;
                                            delete selectedRegistration.newRemarks;
                                            delete selectedRegistration.newOwnerRemarks;
                                            delete selectedRegistration.newPartOfTown;
                                        });
                                    }}
                                    onEdit={edit => {
                                        if (edit) {
                                            selectedRegistration.newTalents = selectedRegistration.talents;
                                            selectedRegistration.newRemarks = selectedRegistration.remarks;
                                            selectedRegistration.newOwnerRemarks = selectedRegistration.owner_remarks;
                                            selectedRegistration.newPartOfTown =
                                                selectedRegistration.part_of_town === ''
                                                    ? '0'
                                                    : selectedRegistration.part_of_town || '0';
                                        }
                                    }}
                                >
                                    {({ edit }) => {
                                        if (!edit) {
                                            return (
                                                <>
                                                    {account.rights.canReadRegistrationsGeneral && (
                                                        <>
                                                            <Label value="Ingeschreven op" />{' '}
                                                            {new Date(
                                                                selectedRegistration.creationDt
                                                            ).toLocaleDateString('nl-BE', {
                                                                weekday: 'long',
                                                                year: 'numeric',
                                                                month: 'long',
                                                                day: 'numeric',
                                                            })}
                                                            {projectData.extended > 0 && (
                                                                <>
                                                                    <Label value="Deeldorp" />{' '}
                                                                    {partOfTown ? partOfTown : 'Ik woon niet in Geel'}
                                                                </>
                                                            )}
                                                            <Label value="Heb je specifieke talenten?" />{' '}
                                                            {(talents || '').trim() === '' ? 'Geen' : talents}
                                                            <Label value="Heb je bijkomende opmerkingen?" />
                                                            {(remarks || '').trim() === '' ? 'Geen' : remarks}
                                                        </>
                                                    )}
                                                    {account.rights.canReadRegistrationsRemarks && (
                                                        <>
                                                            <Admin>
                                                                <Label value="Opmerkingen van beheerders" />
                                                                {(ownerRemarks || '').trim() === ''
                                                                    ? 'Geen'
                                                                    : ownerRemarks}
                                                            </Admin>
                                                        </>
                                                    )}
                                                </>
                                            );
                                        }

                                        return (
                                            <>
                                                {account.rights.canReadRegistrationsGeneral && (
                                                    <>
                                                        {projectData.extended === 1 && (
                                                            <CustomInput
                                                                type="select"
                                                                labelText="Deeldorp"
                                                                formControlProps={{
                                                                    required: true,
                                                                    fullWidth: true,
                                                                }}
                                                                inputProps={{
                                                                    useValue: true,
                                                                    native: true,
                                                                    value: selectedRegistration.newPartOfTown,
                                                                    onChange: e => {
                                                                        selectedRegistration.newPartOfTown =
                                                                            e.target.value === ''
                                                                                ? '0'
                                                                                : e.target.value;
                                                                    },
                                                                    name: 'partOfTown',
                                                                    inputProps: {
                                                                        name: 'partOfTown',
                                                                        id: 'partOfTown_FVD',
                                                                    },
                                                                    options: projects.partsOfTown,
                                                                }}
                                                            />
                                                        )}
                                                        <CustomInput
                                                            labelText="Heb je specifieke talenten?"
                                                            id="talents_FVD"
                                                            formControlProps={{
                                                                fullWidth: true,
                                                            }}
                                                            inputProps={{
                                                                multiline: true,
                                                                color: 'primary',
                                                                type: 'text',
                                                                autoComplete: 'off',
                                                                value: selectedRegistration.newTalents,
                                                                onChange: e =>
                                                                    (selectedRegistration.newTalents = e.target.value),
                                                            }}
                                                        />
                                                        <CustomInput
                                                            labelText="Heb je bijkomende opmerkingen?"
                                                            id="remarks_FVD"
                                                            formControlProps={{
                                                                fullWidth: true,
                                                            }}
                                                            inputProps={{
                                                                multiline: true,
                                                                color: 'primary',
                                                                type: 'text',
                                                                autoComplete: 'off',
                                                                value: selectedRegistration.newRemarks,
                                                                onChange: e =>
                                                                    (selectedRegistration.newRemarks = e.target.value),
                                                            }}
                                                        />
                                                    </>
                                                )}
                                                {account.rights.canReadRegistrationsRemarks && (
                                                    <CustomInput
                                                        labelText="Opmerkingen van beheerders"
                                                        id="remarks_FVD"
                                                        formControlProps={{
                                                            fullWidth: true,
                                                        }}
                                                        inputProps={{
                                                            inputProps: {
                                                                rows: 5,
                                                            },
                                                            multiline: true,
                                                            color: 'primary',
                                                            type: 'text',
                                                            autoComplete: 'off',
                                                            value: selectedRegistration.newOwnerRemarks || ' ',
                                                            onChange: e =>
                                                                (selectedRegistration.newOwnerRemarks = e.target.value),
                                                        }}
                                                    />
                                                )}
                                            </>
                                        );
                                    }}
                                </EditableCard>
                            </GridItem>
                        )}
                        {hidePrefered && <GridItem xs={12} sm={12} md={4}></GridItem>}
                    </GridContainer>
                </div>
                {match.params?.id && <Profile noName />}
                {account.level === 1 && account.rights.canDeleteRegistrations && (
                    <Button
                        onClick={() => {
                            general.addModal({
                                title: 'Uitschrijven',
                                body: `Ben je zeker dat je deze ${firstName} ${lastName} wil uitschrijven voor ${projectData.name}?`,
                                buttonText: 'Schrijf uit',
                                onConfirm: () => {
                                    registrations.signOff(selectedRegistration.id, selectedUser.id).then(() => {
                                        history.goBack();
                                        setTimeout(() => {
                                            registrations.resource.success = `${firstName} ${lastName} werd uitgeschreven voor ${projectData.name}`;
                                        }, 500);
                                    });
                                },
                            });
                        }}
                        style={{ marginLeft: '20px' }}
                        color="danger"
                        size="md"
                    >
                        Uitschrijven
                    </Button>
                )}
                <User>
                    {canSignOff && (
                        <Button
                            onClick={() => {
                                general.addModal({
                                    title: 'Uitschrijven',
                                    body: <SignOffBody name={projectData.name} />,
                                    buttonText: 'Schrijf uit',
                                    onConfirm: () => {
                                        registrations.signOff(selectedRegistration.id, selectedUser.id).then(() => {
                                            setTimeout(() => {
                                                projects.success = `Je werd uitgeschreven voor ${projectData.name}`;
                                                history.push('/projecten');
                                            }, 500);
                                        });
                                    },
                                });
                            }}
                            style={{ marginLeft: '20px' }}
                            color="danger"
                            size="md"
                        >
                            Uitschrijven
                        </Button>
                    )}
                </User>
                <Button
                    onClick={() => {
                        history.goBack();
                    }}
                    style={{ marginLeft: '20px' }}
                    color="primary"
                    size="md"
                >
                    Ga terug
                </Button>
            </PageWrapperConditional>
        </>
    );
}

RegistrationsOverview.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(registrationsDetailStyle)(observer(RegistrationsOverview));
