import React, { useEffect } from 'react';

import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowBackIos, AccountCircle } from '@material-ui/icons';

import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import Toolbar from '../../components/Toolbar/Toolbar.jsx';
import Loader from '../../components/Loader.jsx';

import { colors, gradients, shadow } from 'assets/jss/main.jsx';
import ImageDropzone from '../../components/Dropzone/ImageDropzone.jsx';
import { withTranslation } from 'react-i18next';

const badgesStyle = {
    header: {
        ...gradients.main,
        color: colors.white,
        ...shadow,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    content: {
        margin: 10,
        '& > div': {
            background: colors.white,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            padding: '10px 15px',
            marginBottom: 20,
            '& > b': {
                '@media print': {
                    display: 'none',
                },
            },
            '@media print': {
                boxShadow: 'none',
                padding: 0,
                margin: 0,
            },
        },
        '@media print': {
            margin: 0,
        },
    },
    dropzone: {
        marginTop: 27,
        marginBottom: 30,
        border: `2px dashed #888`,
        textAlign: 'center',
        color: '#888',
        borderRadius: '5px',
        fontSize: '0.9em',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& > div': {
            minHeight: '200px',
            width: '80%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        '@media print': {
            display: 'none',
        },
    },
    badge: {
        position: 'relative',
        boxSizing: 'border-box',
        width: '10cm',
        height: '7cm',
        border: '2px dashed rgba(0,0,0,.1)',
        borderRadius: 5,
        padding: 15,
        marginBottom: '5.4mm',
        pageBreakInside: 'avoid',
        '& h1': {
            margin: 0,
            fontSize: '1.5em',
        },
    },
};

const Pages = withStyles(badgesStyle)(
    observer(({ classes, data }) => {
        const { projects, registrations } = useStore();
        const slice = data.slice(0, 8);
        const nextPage = data.slice(8);

        return (
            <>
                <div className="page" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between' }}>
                    {slice.map((registration, i) => {
                        return (
                            <div className={classes.badge}>
                                <h1>{projects.resource.item.name}</h1>
                                {registration.user.firstName} {registration.user.lastName}
                                <div
                                    style={{
                                        paddingTop: 15,
                                        position: 'absolute',
                                        zIndex: 1,
                                        left: 15,
                                        bottom: 15,
                                    }}
                                >
                                    {!!registration.user.image && (
                                        <img alt="User" src={registration.user.image} height="110" />
                                    )}
                                    {!registration.user.image && (
                                        <AccountCircle
                                            style={{
                                                display: 'block',
                                                fontSize: 110,
                                                color: 'rgba(160,160,160,.1)',
                                                border: '1px dashed rgba(160,160,160,.2)',
                                                boxSizing: 'border-box',
                                                borderRadius: 3,
                                            }}
                                        />
                                    )}
                                </div>
                                <div
                                    style={{
                                        paddingTop: 15,
                                        position: 'absolute',
                                        zIndex: 1,
                                        right: 15,
                                        bottom: 15,
                                    }}
                                >
                                    {!!registrations.image && <img alt="User" src={registrations.image} height="110" />}
                                </div>
                            </div>
                        );
                    })}
                </div>
                {!!nextPage?.length && <Pages data={nextPage} />}
            </>
        );
    })
);

function Badges(props) {
    const { classes, t } = props;
    const { projects, registrations, ui } = useStore();
    const { history } = useReactRouter();

    useEffect(() => {
        async function fetchData() {
            await registrations.resource.fetch(`/project/${projects.id}`);
            //await Promise.all(registrations.resource.data.map(({ user }) => images.fetch(user)));
        }

        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div style={{ flexGrow: 1 }}>
            {ui.isMobile && (
                <div className={classes.header}>
                    <ArrowBackIos onClick={() => history.goBack()} />
                    <span>{t('tooltip.project.badges')}</span>
                </div>
            )}
            <Loader loading={projects.id && registrations.resource.loading}>
                <Toolbar title={t('tooltip.project.badges')} noPrint />
                <div className={classes.content}>
                    <div>
                        <b>{t('form.labels.choosePicture')}</b>
                        <ImageDropzone
                            className={classes.dropzone}
                            store={registrations}
                            onData={data => (registrations.image = data)}
                        />
                        <Pages data={registrations.resource.data.filter(v => v)} />
                    </div>
                </div>
            </Loader>
        </div>
    );
}

export default withTranslation()(withStyles(badgesStyle)(observer(Badges)));
