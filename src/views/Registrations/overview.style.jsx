import { primaryColor, secondaryColor, white } from '../../assets/jss/material-kit-react.jsx';

const registrationsOverviewStyle = {
    actions: {
        borderLeft: `1px solid ${primaryColor}`,
        padding: '15px 15px 15px 30px',
        display: 'inline-flex',
        alignItems: 'center',
        background: primaryColor,
        color: white,
        fontWeight: 'bold',
        '& > span': {
            marginRight: 15,
        },
        '& span:hover': {
            cursor: 'pointer',
            color: secondaryColor,
        },
    },
    registrations: {
        listStyle: 'none',
        margin: '0',
        padding: '0',
        paddingTop: 10,
        width: '100%',
        '& > li': {
            position: 'relative',
            display: 'flex',
            justifyContent: 'space-between',
            background: white,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            padding: '0 0 0 15px',
            margin: '26px 0px',
            '& span.highlightOnHover:hover': {
                cursor: 'pointer',
                fontWeight: 500,
                '& svg': {
                    color: primaryColor,
                },
            },
            '& > span': {
                display: 'inline-flex',
                alignItems: 'center',
                minWidth: 220,
            },
            '& svg': {
                marginRight: '15px',
            },
        },
    },
    imageOverlay: {
        background: 'rgba(0,0,0,0.6)',
        borderRadius: 5,
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& img': {
            cursor: 'pointer',
            maxWidth: '90vw',
            maxHeight: '90vh',
        },
    },
    filters: {
        background: '#fff',
        boxShadow:
            '0px 1px 2px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: 10,
        '@media (min-width: 960px)': {
            margin: '0 0px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            padding: '0 20px',
        },
    },
    results: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        '& > div': {
            display: 'flex',
            alignItems: 'center',
            '& > svg': {
                marginRight: 20,
            },
            '& > span': {
                minWidth: 150,
            },
        },
    },
    elements: {
        '& > div': {
            display: 'flex',
            justifyContent: 'space-between',
        },
        '@media (min-width: 960px)': {
            display: 'flex',
            flexGrow: 1,
            marginLeft: 20,
            alignItems: 'center',
            justifyContent: 'flex-end',
            '& > svg': {
                marginRight: 20,
            },
        },
    },
    filter: {
        minWidth: '45%',
        '@media (min-width: 960px)': {
            minWidth: 100,
            marginTop: -9,
            marginRight: 20,
        },
        '@media (min-width: 1440px)': {
            minWidth: 110,
        },
    },
    sort: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start !important',
        '& > svg': {
            marginRight: 20,
        },
        '@media (min-width: 960px)': {
            marginLeft: 0,
            paddingLeft: 20,
            borderLeft: '1px solid rgba(0,0,0,.1)',
            '& > div': {
                marginTop: -9,
            },
        },
    },
};

export default registrationsOverviewStyle;
