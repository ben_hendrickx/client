import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';
import useReactRouter from 'use-react-router';
import TabView from 'components/TabView.jsx';
import UserDetail from 'views/Registrations/UserDetail.jsx';

import Page from 'components/Page.jsx';
import Roles from './Roles';
import Repetitions from './Repetitions';
import Rights from './Rights';
import { ArtTrack, InfoOutlined, PanTool, Repeat } from '@material-ui/icons';

function RegistrationDetail() {
    const { account, users } = useStore();
    const { history, match } = useReactRouter();

    useEffect(() => {
        if (!users.selectedUser) {
            users.fetchUserForRegistration(match.params?.registrationId);
        }

        return () => {
            delete users.selectedUser;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const title = users.selectedUser
        ? `Inschrijving van ${users.selectedUser.firstName} ${users.selectedUser.lastName}`
        : null;

    if (!users.selectedUser?.id) {
        return null;
    }

    const { rights } = account;

    return (
        <Page title={title}>
            <TabView
                tabs={[
                    {
                        icon: <InfoOutlined />,
                        label: 'Gegevens',
                        onClick: () => {
                            history.replace(
                                `/projecten/${match.params.id}/inschrijvingen/${match.params.registrationId}/gegevens`
                            );
                        },
                    },
                    rights.canReadUserRoles && {
                        icon: <ArtTrack />,
                        label: 'Rollen',
                        as: 'rollen',
                        onClick: () => {
                            history.replace(
                                `/projecten/${match.params.id}/inschrijvingen/${match.params.registrationId}/rollen`
                            );
                        },
                    },
                    rights.canReadUserRepetitions && {
                        icon: <Repeat />,
                        label: 'Repetities',
                        as: 'repetities',
                        onClick: () => {
                            history.replace(
                                `/projecten/${match.params.id}/inschrijvingen/${match.params.registrationId}/repetities`
                            );
                        },
                    },
                    rights.canReadUserRights && {
                        icon: <PanTool />,
                        label: 'Toegangsrollen',
                        as: 'toegangsrollen',
                        onClick: () => {
                            history.replace(
                                `/projecten/${match.params.id}/inschrijvingen/${match.params.registrationId}/toegangsrollen`
                            );
                        },
                    },
                ]}
                routes={[
                    {
                        path: '/projecten/:id/inschrijvingen/:registrationId/gegevens',
                        component: UserDetail,
                    },
                    rights.canReadUserRoles && {
                        path: '/projecten/:id/inschrijvingen/:registrationId/rollen',
                        component: () => <Roles style={{ marginTop: -20 }} />,
                    },
                    rights.canReadUserRepetitions && {
                        path: '/projecten/:id/inschrijvingen/:registrationId/repetities',
                        component: Repetitions,
                    },
                    rights.canReadUserRights && {
                        path: '/projecten/:id/inschrijvingen/:registrationId/toegangsrollen',
                        component: Rights,
                    },
                ]}
            />
        </Page>
    );
}

export default observer(RegistrationDetail);
