import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import ListRow from 'components/ListRow';
// import { Delete } from '@material-ui/icons';
// import ListRowActions from 'components/ListRowActions';
import { Checkbox, Tooltip } from '@material-ui/core';
import { observer } from 'mobx-react-lite';
import Role from 'components/Role';
import Inputs from 'components/Inputs';
import Button from 'components/CustomButtons/Button.jsx';
import { CompareArrows } from '@material-ui/icons';
import { useEffect } from 'react';
import { NotificationManager } from 'react-notifications';

const ObservedCheckBox = observer(({ id, type }) => {
    const { projectRoles } = useStore();

    const key = type === 'role' ? 'selectedRoles' : 'selectedGroups';

    return (
        <div style={{ padding: '6px 0' }}>
            <Checkbox
                checked={projectRoles[key].includes(id)}
                onChange={e => {
                    if (!projectRoles[key].includes(id)) {
                        projectRoles[key].push(id);
                    } else {
                        projectRoles[key].splice(projectRoles[key].indexOf(id), 1);
                    }
                }}
            />
        </div>
    );
});

function RolesOverview({ style }) {
    const { projectRoles, projects, users, general, casts } = useStore();

    function rowRenderer({ key, index, style }) {
        const role = projectRoles.resource.filtered[index];

        return (
            <li key={key} style={{ ...style, display: 'flex' }}>
                <ObservedCheckBox id={role.id} type={role.type} />
                <ListRow key={key} style={{ flexGrow: 1 }}>
                    <Role role={role} identifier type description scene />
                    {/* <ListRowActions>
                        <Tooltip title="Haal gebruiker uit deze rol" placement="top">
                            <Delete onClick={() => {}} />
                        </Tooltip>
                    </ListRowActions> */}
                </ListRow>
            </li>
        );
    }

    useEffect(() => {
        casts.resource.fetch();
    }, []);

    return (
        <ResourceOverview
            style={style}
            apiPrefix={`/project/${projects.id}/user/${users.selectedUser.id}`}
            store={projectRoles.resource}
            rowRenderer={rowRenderer}
            heightOffset={600}
            filters={<Inputs.roles.filters.scene />}
            renderBefore={
                <div
                    style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', marginBottom: 10 }}
                >
                    <Button
                        color="primary"
                        onClick={() => {
                            if (projectRoles.selectedRoles.length || projectRoles.selectedGroups.length) {
                                projectRoles.selectedRoles = [];
                                projectRoles.selectedGroups = [];
                            } else {
                                projectRoles.selectedRoles = projectRoles.resource.filtered
                                    .filter(({ type }) => type === 'role')
                                    .map(({ id }) => id);
                                projectRoles.selectedGroups = projectRoles.resource.filtered
                                    .filter(({ type }) => type === 'group')
                                    .map(({ id }) => id);
                            }
                        }}
                    >
                        (De-)selecteer alle
                    </Button>
                    <Button
                        disabled={!projectRoles.selectedRoles.length && !projectRoles.selectedGroups.length}
                        color="success"
                        onClick={() => {
                            let modal;
                            const closeModal = () => modal.close();
                            modal = general.addModal({
                                title: `Wissel van cast`,
                                body: (
                                    <>
                                        <p>Naar welke cast wenst u deze gebruiker te wisselen?</p>
                                        <Inputs.Input
                                            type="select"
                                            fullWidth
                                            options={casts.resource.filtered.map(({ id, name }) => ({
                                                label: name,
                                                value: id,
                                            }))}
                                            onChange={v => (projectRoles.selectedCast = v)}
                                        />
                                        <Button
                                            color="success"
                                            onClick={async () => {
                                                const result = await projectRoles.switchCast();
                                                closeModal();
                                                if (result.success) {
                                                    NotificationManager.success('Succesvol gewisseld van cast');
                                                    projectRoles.resource.data = [];
                                                    projectRoles.resource.status = [];
                                                    projectRoles.selectedRoles = [];
                                                    projectRoles.selectedGroups = [];
                                                    await projectRoles.resource.fetch(
                                                        `/project/${projects.id}/user/${users.selectedUser.id}`
                                                    );
                                                }
                                            }}
                                        >
                                            Bevestig
                                        </Button>
                                    </>
                                ),
                            });
                        }}
                    >
                        <CompareArrows /> Wissel van cast
                    </Button>
                </div>
            }
        />
    );
}

export default observer(RolesOverview);
