import React from 'react';
import { useStore } from '../../../mobx';
import ListRow from 'components/ListRow';
import { Delete, Edit } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import LocationForm from '../Forms/Location';
import { withTranslation } from 'react-i18next';

function LocationRow({ key, style, location, t }) {
    const { name } = location;
    const { account, general, locations } = useStore();

    return (
        <li key={key} style={style}>
            <ListRow key={key}>
                <span>{name}</span>
                {(account.rights.canUpdateLocations ||
                    account.rights.canCreateLocations ||
                    account.rights.canDeleteLocations) && (
                    <ListRowActions>
                        {account.rights.canUpdateLocations && (
                            <Tooltip title={t('tooltip.location.edit')} placement="top">
                                <Edit
                                    onClick={() => {
                                        let modal;
                                        const closeModal = () => modal.close();
                                        locations.resource.form.data = { ...location };
                                        modal = general.addModal({
                                            type: 'window',
                                            width: 1100,
                                            title: t('modal.title.location.edit'),
                                            onMinimize: async modal => {
                                                locations.resource.form.data.name &&
                                                    (modal.title = `${t('modal.title.location.edit')}: ${
                                                        locations.resource.form.data.name
                                                    }`);
                                                return locations.resource.form.data;
                                            },
                                            onMaximize: modal => {
                                                locations.resource.form.data = { ...modal.data };
                                            },
                                            body: <LocationForm onCancel={closeModal} onConfirm={closeModal} />,
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                        {account.rights.canDeleteLocations && (
                            <Tooltip title={t('tooltip.location.delete')} placement="top">
                                <Delete
                                    onClick={() => {
                                        general.addModal({
                                            title: `${t('modal.title.location.delete')}: ${location.name}`,
                                            body: t('modal.body.location.delete'),
                                            buttonText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await locations.resource.delete(undefined, location.id);
                                                locations.resource.data = [];
                                                locations.resource.status = [];
                                                locations.resource.fetch();
                                            },
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                    </ListRowActions>
                )}
            </ListRow>
        </li>
    );
}

export default withTranslation()(LocationRow);
