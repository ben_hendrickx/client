import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewLocation(props) {
    const { locations } = useStore();

    useEffect(() => {
        return () => {
            locations.resource.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            store={locations.resource}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                locations.resource.data = [];
                locations.resource.status = [];
                locations.resource.fetch();
            }}
        />
    );
}

NewLocation.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewLocation);
