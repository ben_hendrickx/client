import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import LocationRow from './Components/LocationRow';
import LocationForm from './Forms/Location';
import { withTranslation } from 'react-i18next';

function LocationsOverview({ t }) {
    const { account, locations, general } = useStore();

    function rowRenderer({ key, index, style }) {
        const location = locations.resource.filtered[index];

        return <LocationRow key={key} style={style} location={location} />;
    }

    function onCreateLocation() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.location.new'),
            onMinimize: modal => {
                locations.resource.form.data.name &&
                    (modal.title = `${t('modal.title.location.new')}: ${locations.resource.form.data.name}`);
                return locations.resource.form.data;
            },
            onMaximize: modal => {
                locations.resource.form.data = { ...modal.data };
            },
            body: <LocationForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <ResourceOverview
            store={locations.resource}
            rowRenderer={rowRenderer}
            heightOffset={250}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateLocations && {
                    icon: 'add',
                    onClick: onCreateLocation,
                    tooltip: t('tooltip.location.create'),
                },
            ]}
        />
    );
}

export default withTranslation()(observer(LocationsOverview));
