import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import TypeRow from './Components/TypeRow';
import TypeForm from './Forms/Type';
import { withTranslation } from 'react-i18next';

function RepetitionTypesOverview({ t }) {
    const { account, types, general } = useStore();

    function rowRenderer({ key, index, style }) {
        const type = types.resource.filtered[index];

        return <TypeRow key={key} style={style} type={type} />;
    }

    function onCreateType() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.repetitionType.new'),
            onMinimize: modal => {
                types.resource.form.data.name &&
                    (modal.title = `${t('modal.title.repetitionType.new')}: ${types.resource.form.data.name}`);
                return types.resource.form.data;
            },
            onMaximize: modal => {
                types.resource.form.data = { ...modal.data };
            },
            body: <TypeForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <ResourceOverview
            store={types.resource}
            rowRenderer={rowRenderer}
            heightOffset={250}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateRepetitionTypes && {
                    icon: 'add',
                    onClick: onCreateType,
                    tooltip: t('tooltip.repetitionType.create'),
                },
            ]}
        />
    );
}

export default withTranslation()(observer(RepetitionTypesOverview));
