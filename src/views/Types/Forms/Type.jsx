import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewType(props) {
    const { types } = useStore();

    useEffect(() => {
        return () => {
            types.resource.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            store={types.resource}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                types.resource.data = [];
                types.resource.status = [];
                types.resource.fetch();
            }}
        />
    );
}

NewType.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewType);
