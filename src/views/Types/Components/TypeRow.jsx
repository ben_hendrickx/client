import React from 'react';
import ListRow from 'components/ListRow';
import { Delete, Edit } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { useStore } from '../../../mobx';
import TypeForm from '../Forms/Type';
import { withTranslation } from 'react-i18next';

function TypeRow({ key, style, type, t }) {
    const { name } = type;
    const { account, types, general } = useStore();

    return (
        <li key={key} style={style}>
            <ListRow key={key}>
                <span>{name}</span>
                {(account.rights.canUpdateRepetitionTypes ||
                    account.rights.canCreateRepetitionTypes ||
                    account.rights.canDeleteRepetitionTypes) && (
                    <ListRowActions>
                        {account.rights.canUpdateRepetitionTypes && (
                            <Tooltip title={t('tooltip.repetitionType.edit')} placement="top">
                                <Edit
                                    onClick={() => {
                                        let modal;
                                        const closeModal = () => modal.close();
                                        types.resource.form.data = { ...type };
                                        modal = general.addModal({
                                            type: 'window',
                                            width: 1100,
                                            title: t('modal.title.repetitionType.edit'),
                                            onMinimize: async modal => {
                                                types.resource.form.data.name &&
                                                    (modal.title = `${t('modal.title.repetitionType.edit')}: ${
                                                        types.resource.form.data.name
                                                    }`);
                                                return types.resource.form.data;
                                            },
                                            onMaximize: modal => {
                                                types.resource.form.data = { ...modal.data };
                                            },
                                            body: <TypeForm onCancel={closeModal} onConfirm={closeModal} />,
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                        {account.rights.canDeleteRepetitionTypes && (
                            <Tooltip title={t('tooltip.repetitionType.delete')} placement="top">
                                <Delete
                                    onClick={() => {
                                        general.addModal({
                                            title: `${t('modal.title.repetitionType.delete')}: ${type.name}`,
                                            body: t('modal.body.repetitionType.delete'),
                                            buttonText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await types.resource.delete(undefined, type.id);
                                                types.resource.data = [];
                                                types.resource.status = [];
                                                types.resource.fetch();
                                            },
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                    </ListRowActions>
                )}
            </ListRow>
        </li>
    );
}

export default withTranslation()(TypeRow);
