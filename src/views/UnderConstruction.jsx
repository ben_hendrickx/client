/* istanbul ignore file */
import Page from 'components/Page';
import React from 'react';

function NotFound() {
    return (
        <Page>
            <div style={{ padding: '70px 0 100px' }}>
                <h2 style={{ textAlign: 'center', fontSize: '2em', marginTop: 0, width: '100%' }}>
                    Deze pagina is volop in ontwikkeling
                    <br />
                    gelieve later nog eens terug te komen
                </h2>
            </div>
        </Page>
    );
}

export default NotFound;
