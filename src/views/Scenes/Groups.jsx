import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowBackIos, Group, Delete, ArtTrack } from '@material-ui/icons';

import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import groupsStyle from './groups.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Button from '../../components/CustomButtons/Button.jsx';
import Toolbar from '../../components/Toolbar/Toolbar.jsx';
import Loader from '../../components/Loader.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Tooltip from '@material-ui/core/Tooltip';
import { Admin } from 'components/Levels.jsx';
import { Success } from 'components/Notifications.jsx';

function SceneGroupsOverview(props) {
    const { classes } = props;
    const { projects, ui, scenes, groups, projectRoles: roles } = useStore();
    const { match, history } = useReactRouter();
    const [deleteModal, setDeleteModal] = useState(false);
    const [groupName, setGroupName] = useState('');
    const [groupId, setGroupId] = useState(null);

    const [deleteRoleModal, setDeleteRoleModal] = useState(false);
    const [roleName, setRoleName] = useState('');
    const [roleId, setRoleId] = useState(null);

    useEffect(() => {
        groups.resource.fetch(`/project/${projects.id}`);
        roles.resource.fetch(`/project/${projects.id}`);

        if (match && match.params && match.params.sceneId) {
            scenes.fetchScene(match.params.sceneId);
            scenes.fetchGroupsForScene(match.params.sceneId);
            scenes.fetchRolesForScene(match.params.sceneId);
        }

        return () => {
            scenes.groupId = null;
            scenes.roleId = null;
            scenes.success = null;
            groups.resource.reset();
            roles.resource.reset();
            scenes.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className={classes.container} style={{ flexGrow: 1 }}>
            {ui.isMobile && (
                <div className={classes.header}>
                    <ArrowBackIos onClick={() => history.goBack()} />
                    <span>{`${scenes.name}`}</span>
                </div>
            )}
            <Toolbar title={`${scenes.identifier} - ${scenes.name}`} />
            <Success message={scenes.success} onClose={() => (scenes.success = '')} />
            <Loader loading={scenes.loading}>
                <div className={classes.content}>
                    {!scenes.groups.length && !scenes.roles.length && (
                        <h2>Nog geen groepen/rollen in deze scène, voeg er een toe</h2>
                    )}
                    <GridContainer>
                        {scenes.sceneGroups.length > 0 && <h2>Groepen</h2>}
                        {scenes.sceneGroups.length > 0 && (
                            <GridItem xs={12} sm={12} md={12}>
                                <ul className={classes.groups}>
                                    {scenes.sceneGroups.map(group => (
                                        <li key={group.id}>
                                            <span>
                                                <Group /> {group.name}
                                            </span>
                                            <div className={classes.actions}>
                                                <Admin>
                                                    <Tooltip title="Verwijder groep uit scène" placement="top">
                                                        <Delete
                                                            onClick={() => {
                                                                setGroupName(group.name);
                                                                setGroupId(group.id);
                                                                setDeleteModal(true);
                                                            }}
                                                        />
                                                    </Tooltip>
                                                </Admin>
                                                {/* {account.level === 1 && (
                                            <Tooltip title="Bekijk inschrijvingen" placement="top">
                                                <Assignment onClick={() => openRegistrations(project)} />
                                            </Tooltip>
                                        )} */}
                                            </div>
                                        </li>
                                    ))}
                                </ul>
                            </GridItem>
                        )}
                        {scenes.roles.length > 0 && <h2>Rollen</h2>}
                        {scenes.roles.length > 0 && (
                            <GridItem xs={12} sm={12} md={12}>
                                <ul className={classes.groups}>
                                    {scenes.roles.map(role => (
                                        <li key={role.id}>
                                            <span>
                                                <ArtTrack /> {role.name}
                                            </span>
                                            <div className={classes.actions}>
                                                <Admin>
                                                    <Tooltip title="Verwijder rol uit scène" placement="top">
                                                        <Delete
                                                            onClick={() => {
                                                                setRoleName(role.name);
                                                                setRoleId(role.id);
                                                                setDeleteRoleModal(true);
                                                            }}
                                                        />
                                                    </Tooltip>
                                                </Admin>
                                                {/* {account.level === 1 && (
                                            <Tooltip title="Bekijk inschrijvingen" placement="top">
                                                <Assignment onClick={() => openRegistrations(project)} />
                                            </Tooltip>
                                        )} */}
                                            </div>
                                        </li>
                                    ))}
                                </ul>
                            </GridItem>
                        )}
                        {(groups.autoComplete.filter(g => {
                            return !scenes.sceneGroups.find(({ id }) => id === g.value);
                        }).length > 0 ||
                            roles.autoComplete.filter(g => {
                                return !scenes.roles.find(({ id }) => id === g.value);
                            }).length > 0) && (
                            <GridItem xs={12} sm={12} md={12}>
                                <div className={classes.card} style={{ display: 'flex' }}>
                                    {groups.autoComplete.filter(g => {
                                        return !scenes.sceneGroups.find(({ id }) => id === g.value);
                                    }).length > 0 && (
                                        <div
                                            style={{
                                                marginRight: 30,
                                                paddingRight: 30,
                                                flexGrow: 1,
                                                borderRight: '1px solid rgba(0,0,0,.1)',
                                            }}
                                        >
                                            <h2>Voeg groep toe aan scène</h2>
                                            <CustomInput
                                                type="select"
                                                labelText="Groep"
                                                formControlProps={{
                                                    className: classes.filter,
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    displayEmpty: false,
                                                    multiple: true,
                                                    useValue: true,
                                                    native: true,
                                                    value: scenes.groupId || [],
                                                    onChange: e => {
                                                        const selectedOptions = [...e.target.options]
                                                            .filter(({ selected }) => selected)
                                                            .map(({ value }) => value);
                                                        scenes.groupId = selectedOptions;
                                                    },
                                                    name: 'group',
                                                    inputProps: {
                                                        style: {
                                                            height:
                                                                groups.autoComplete.filter(g => {
                                                                    return !scenes.groups.resource.data.find(
                                                                        ({ id }) => id === g.value
                                                                    );
                                                                }).length * 20,
                                                            paddingTop: 25,
                                                        },
                                                        name: 'group',
                                                        id: 'group_FVD',
                                                    },
                                                    options: groups.autoComplete.filter(g => {
                                                        return !scenes.sceneGroups.find(({ id }) => id === g.value);
                                                    }),
                                                }}
                                            />
                                            <Button
                                                onClick={async () => {
                                                    if (scenes.groupId?.length) {
                                                        await scenes.saveGroups();
                                                        scenes.groupId = null;
                                                    }
                                                }}
                                                color={scenes.groupId?.length ? 'success' : 'disabled'}
                                                size="md"
                                            >
                                                <span style={{ marginTop: '1px', marginLeft: '5px' }}>
                                                    Voeg groep toe
                                                </span>
                                            </Button>
                                        </div>
                                    )}
                                    {roles.autoComplete.filter(g => {
                                        return !scenes.roles.find(({ id }) => id === g.value);
                                    }).length > 0 && (
                                        <div style={{ flexGrow: 1 }}>
                                            <h2>Voeg rol toe aan scène</h2>
                                            <CustomInput
                                                type="select"
                                                labelText="Rol"
                                                formControlProps={{
                                                    className: classes.filter,
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    displayEmpty: false,
                                                    multiple: true,
                                                    useValue: true,
                                                    native: true,
                                                    value: scenes.roleId || [],
                                                    onChange: e => {
                                                        const selectedOptions = [...e.target.options]
                                                            .filter(({ selected }) => selected)
                                                            .map(({ value }) => value);
                                                        scenes.roleId = selectedOptions;
                                                    },
                                                    name: 'role',
                                                    inputProps: {
                                                        style: {
                                                            height:
                                                                roles.autoComplete.filter(g => {
                                                                    return !scenes.roles.find(
                                                                        ({ id }) => id === g.value
                                                                    );
                                                                }).length * 20,
                                                            paddingTop: 25,
                                                        },
                                                        name: 'role',
                                                        id: 'role_FVD',
                                                    },
                                                    options: roles.autoComplete.filter(g => {
                                                        return !scenes.roles.find(({ id }) => id === g.value);
                                                    }),
                                                }}
                                            />
                                            <Button
                                                onClick={async () => {
                                                    if (scenes.roleId?.length) {
                                                        await scenes.saveRoles();
                                                        scenes.roleId = null;
                                                    }
                                                }}
                                                color={scenes.roleId?.length ? 'success' : 'disabled'}
                                                size="md"
                                            >
                                                <span style={{ marginTop: '1px', marginLeft: '5px' }}>
                                                    Voeg rol toe
                                                </span>
                                            </Button>
                                        </div>
                                    )}
                                </div>
                            </GridItem>
                        )}
                    </GridContainer>
                </div>
            </Loader>
            {deleteModal && (
                <div className={classes.overlay} onClick={() => setDeleteModal(false)}>
                    <div>
                        <h1>Verwijder groep {groupName} uit scène</h1>
                        <p>
                            Bent u zeker dat u de groep <b>{groupName}</b> van deze scène wilt verwijderen?
                        </p>
                        <Button
                            onClick={async () => {
                                await scenes.deleteGroupFromScene(groupId);
                                setDeleteModal(false);
                            }}
                            color="success"
                            size="md"
                        >
                            Verwijder groep uit scène
                        </Button>
                        <Button
                            onClick={() => {
                                setDeleteModal(false);
                            }}
                            color="default"
                            size="md"
                        >
                            Sluiten
                        </Button>
                    </div>
                </div>
            )}
            {deleteRoleModal && (
                <div className={classes.overlay} onClick={() => setDeleteRoleModal(false)}>
                    <div>
                        <h1>Verwijder rol {roleName} uit scène</h1>
                        <p>
                            Bent u zeker dat u de rol <b>{roleName}</b> van deze scène wilt verwijderen?
                        </p>
                        <Button
                            onClick={async () => {
                                await scenes.deleteRoleFromScene(roleId);
                                setDeleteRoleModal(false);
                            }}
                            color="success"
                            size="md"
                        >
                            Verwijder rol uit scène
                        </Button>
                        <Button
                            onClick={() => {
                                setDeleteRoleModal(false);
                            }}
                            color="default"
                            size="md"
                        >
                            Sluiten
                        </Button>
                    </div>
                </div>
            )}
        </div>
    );
}

SceneGroupsOverview.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(groupsStyle)(observer(SceneGroupsOverview));
