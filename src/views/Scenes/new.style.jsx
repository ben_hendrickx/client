import { primaryCardHeader, white } from '../../assets/jss/material-kit-react.jsx';

const newProjectStyle = {
    header: {
        ...primaryCardHeader,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    content: {
        margin: 10,
        '& > div': {
            background: white,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            padding: '10px 15px',
            marginBottom: 20,
        },
    },
};

export default newProjectStyle;
