import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowBackIos, Add, Edit, Group, RemoveRedEye, Delete } from '@material-ui/icons';
import projectsOverviewStyle from './overview.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Tooltip from '@material-ui/core/Tooltip';
import Toolbar from '../../components/Toolbar/Toolbar.jsx';
import Loader from '../../components/Loader.jsx';
import { Success } from 'components/Notifications.jsx';
import Scene from 'components/Scene.jsx';
import FilterBar from 'components/FilterBar/FilterBar.jsx';
import ListRowActions from 'components/ListRowActions.jsx';
import { withTranslation } from 'react-i18next';

const SceneRow = withTranslation()(
    withStyles(projectsOverviewStyle)(props => {
        const { classes, scene, t } = props;
        const isGroup = !!scene.scenes;
        const { account, general, scenes, projects } = useStore();
        const { history, match } = useReactRouter();

        return (
            <li>
                <div>
                    <Scene scene={props.scene} identifier groups={!isGroup} roles={!isGroup} description />
                    <ListRowActions>
                        {account.rights.canReadScenes && !isGroup && (
                            <Tooltip title={t('tooltip.scene.view')} placement="top">
                                <RemoveRedEye
                                    onClick={() => history.push(`/projecten/${match.params.id}/scenes/${scene.id}`)}
                                />
                            </Tooltip>
                        )}
                        {account.rights.canUpdateScenes && (
                            <Tooltip
                                title={isGroup ? t('tooltip.scene.editGroup') : t('tooltip.scene.edit')}
                                placement="top"
                            >
                                <Edit
                                    onClick={() =>
                                        history.push(
                                            `/projecten/${match.params.id}/scenes/${isGroup ? 'groep/' : ''}bewerk/${
                                                scene.id
                                            }`
                                        )
                                    }
                                />
                            </Tooltip>
                        )}
                        {account.rights.canUpdateSceneUsers && !isGroup && (
                            <Tooltip title={t('tooltip.scene.manageUsers')} placement="top">
                                <Group
                                    className="edit-groups-roles"
                                    onClick={() =>
                                        history.push(
                                            `/projecten/${match.params.id}/scenes/bewerk/rollen-groepen/${scene.id}`
                                        )
                                    }
                                />
                            </Tooltip>
                        )}
                        {account.rights.canDeleteScenes && (
                            <Tooltip
                                title={isGroup ? t('tooltip.scene.deleteGroup') : t('tooltip.scene.delete')}
                                placement="top"
                            >
                                <Delete
                                    onClick={() => {
                                        general.addModal({
                                            title: `Verwijder ${isGroup ? 'scène-groep' : 'scène'}: ${scene.name}`,
                                            body: `Bent u zeker dat u deze ${
                                                isGroup ? 'scène-groep' : 'scène'
                                            } wilt verwijderen`,
                                            buttonText: 'Verwijder',
                                            onConfirm: async () => {
                                                if (isGroup) {
                                                    await scenes.groups.resource.delete(
                                                        `/project/${projects.id}/scenes`,
                                                        scene.id
                                                    );
                                                    scenes.resource.success = scenes.groups.resource.success;
                                                } else {
                                                    await scenes.resource.delete(undefined, scene.id);
                                                }
                                                scenes.resource.data = [];
                                                scenes.resource.status = [];
                                                scenes.resource.fetch(`/project/${projects.id}`);
                                            },
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                    </ListRowActions>
                </div>
                {isGroup && (
                    <ul className={classes.projects} style={{ background: '#d8d5d5', border: '1px solid #b7b3b3' }}>
                        {scene.scenes.map(scene => (
                            <SceneRow scene={scene} />
                        ))}
                    </ul>
                )}
            </li>
        );
    })
);

function ProjectsOverview(props) {
    const { classes, t } = props;
    const { ui, account, scenes, projects } = useStore();
    const { history, match } = useReactRouter();

    useEffect(() => {
        scenes.resource.fetch(`/project/${projects.id}`);

        return () => {
            scenes.success = null;
            scenes.resource.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <div className={classes.container}>
                {ui.isMobile && (
                    <div className={classes.header}>
                        <ArrowBackIos onClick={() => history.replace('/')} />
                        <span>{t('resource.scènes')}</span>
                        {account.level > 0 && (
                            <Add onClick={() => history.push(`/projecten/${match.params.id}/scenes/nieuw`)} />
                        )}
                    </div>
                )}
                <Toolbar
                    title={t('resource.scènes')}
                    searchProps={{
                        value: scenes.filter,
                        onChange: e => (scenes.filter = e.target.value),
                        placeholder: `${t('form.labels.search')} ${t('resource.scène').toLowerCase()}`,
                    }}
                    actions={[
                        account.rights.canCreateScenes && {
                            icon: 'add',
                            route: `/projecten/${match.params.id}/scenes/nieuw`,
                            tooltip: t('tooltip.scene.create'),
                        },
                        account.rights.canCreateScenes && {
                            icon: 'addGroup',
                            route: `/projecten/${match.params.id}/scenes/groep/nieuw`,
                            tooltip: t('tooltip.scene.createGroup'),
                        },
                        account.rights.canExportScenes && {
                            icon: 'getApp',
                            href: '/api/reports/scenes.xlsx',
                            addition: 'projectId=' + projects.id,
                            tooltip: t('tooltip.report.download'),
                        },
                    ]}
                />
                <FilterBar total={scenes.resource.data.length} filtered={scenes.resource.filtered.length} />
                <Success message={scenes.resource.success} onClose={() => (scenes.resource.success = '')} />
                <Loader loading={scenes.loading}>
                    {scenes.resource.data.length === 0 && <h2>{t('empty.scenes')}</h2>}
                    {scenes.resource.data.length > 0 && scenes.filter && !scenes.resource.filtered.length === 0 && (
                        <h2>{t('empty.scenesWithFilter')}</h2>
                    )}
                    {scenes.resource.filtered.length > 0 && (
                        <div style={{ height: 'calc(100vh - 400px)', overflowY: 'auto' }}>
                            <ul className={classes.projects}>
                                {scenes.resource.filtered.map((scene, i) => (
                                    <SceneRow key={i} scene={scene} />
                                ))}
                            </ul>
                        </div>
                    )}
                </Loader>
            </div>
        </>
    );
}

ProjectsOverview.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(projectsOverviewStyle)(observer(ProjectsOverview)));
