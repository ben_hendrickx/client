import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import UserRow from '../../components/User/Row.jsx';
import Inputs from 'components/Inputs';
import Button from 'components/CustomButtons/Button.jsx';
import useReactRouter from 'use-react-router';

function Filters() {
    return (
        <>
            <Inputs.users.filters.sex />
            <Inputs.users.filters.minAge />
            <Inputs.users.filters.maxAge />
        </>
    );
}

function UsersOverview() {
    const { users, projects } = useStore();
    const { history, match } = useReactRouter();

    function rowRenderer({ key, index, style }) {
        const user = users.resource.filtered[index];
        return (
            <UserRow
                key={key}
                style={{ ...(style || {}), height: 'auto', width: '100%', margin: '10px 0px 10px 0px' }}
                onClick={() => {}}
                {...user}
                actions={[
                    {
                        developer: true,
                        tooltip: 'Delete user',
                        icon: 'delete',
                        callback: () => {},
                    },
                ]}
            />
        );
    }

    return (
        <>
            <ResourceOverview
                apiPrefix={`/project/${projects.id}/scene/${match.params.sceneId}`}
                store={users.resource}
                rowRenderer={rowRenderer}
                heightOffset={510}
                rowHeight={80}
                filters={<Filters />}
                sort={<Inputs.users.sort />}
            />
            <Button
                style={{ marginTop: -40 }}
                onClick={() => {
                    history.push(`/projecten/${projects.id}/scenes`);
                }}
                color="primary"
                size="md"
            >
                Ga terug
            </Button>
        </>
    );
}

export default observer(UsersOverview);
