import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowBackIos, Save } from '@material-ui/icons';

import newProjectStyle from './new.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Toolbar from '../../components/Toolbar/Toolbar.jsx';
import Loader from '../../components/Loader.jsx';
import { withTranslation } from 'react-i18next';
import { t } from 'i18next';

function NewProject(props) {
    const { classes } = props;
    const { scenes, ui } = useStore();
    const { history, match } = useReactRouter();

    useEffect(() => {
        if (match?.params?.id) {
            scenes.fetchGroups(match.params.id);
        }

        if (match?.params?.sceneId) {
            scenes.fetchScene(match.params.sceneId);
        }

        return () => {
            scenes.id = null;
            scenes.name = null;
            scenes.identifier = null;
            scenes.description = null;
            scenes.projecId = null;
            scenes.groupId = null;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div style={{ flexGrow: 1 }}>
            {ui.isMobile && (
                <div className={classes.header}>
                    <ArrowBackIos onClick={() => history.replace(`/projecten/${match.params.id}/scenes`)} />
                    <span>{scenes.id ? t('tooltip.scene.edit') : t('tooltip.scene.new')}</span>
                </div>
            )}
            <Loader loading={scenes.loading}>
                <Toolbar title={scenes.id ? t('tooltip.scene.edit') : t('tooltip.scene.new')} />
                <div className={classes.content}>
                    <div>
                        <CustomInput
                            labelText={t('form.labels.identifier')}
                            formControlProps={{
                                required: true,
                                fullWidth: true,
                            }}
                            inputProps={{
                                name: 'volgnummer',
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: scenes.identifier || '',
                                onChange: e => (scenes.identifier = e.target.value),
                            }}
                        />
                        <CustomInput
                            labelText={t('form.labels.scenes.name')}
                            formControlProps={{
                                required: true,
                                fullWidth: true,
                            }}
                            inputProps={{
                                name: 'scène-naam',
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: scenes.name || '',
                                onChange: e => (scenes.name = e.target.value),
                            }}
                        />
                        <CustomInput
                            labelText={t('form.labels.description')}
                            formControlProps={{
                                fullWidth: true,
                            }}
                            inputProps={{
                                name: 'omschrijving',
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: scenes.description || '',
                                onChange: e => (scenes.description = e.target.value),
                            }}
                        />
                        <CustomInput
                            type="select"
                            labelText={t('form.labels.scenes.linked')}
                            formControlProps={{
                                style: { minWidth: 250 },
                            }}
                            inputProps={{
                                name: 'gekoppeld',
                                useValue: true,
                                native: true,
                                value: scenes.groupId,
                                onChange: e => (scenes.groupId = e.target.value),
                                options: scenes.groupsAutocomplete,
                            }}
                        />
                    </div>
                    <Button
                        disabled={!scenes.name || !scenes.identifier}
                        onClick={async () => {
                            await scenes.save();
                            history.replace(`/projecten/${match.params.id}/scenes`);
                        }}
                        color="success"
                        size="md"
                    >
                        <Save style={{ marginRight: '5px' }} />
                        <span style={{ marginTop: '1px', marginLeft: '5px' }}>{t('form.button.save')}</span>
                    </Button>
                    <Button
                        onClick={() => {
                            history.replace(`/projecten/${match.params.id}/scenes`);
                        }}
                        style={{ marginLeft: '20px' }}
                        color="white"
                        size="md"
                    >
                        {t('form.button.cancel')}
                    </Button>
                </div>
            </Loader>
        </div>
    );
}

NewProject.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(newProjectStyle)(observer(NewProject)));
