import { white, primaryCardHeader, primaryColor, secondaryColor } from '../../assets/jss/material-kit-react.jsx';

const registrationsDetailStyle = {
    header: {
        ...primaryCardHeader,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    container: {
        flexGrow: 1,
        minWidth: '100%',
        '& h2': {
            textAlign: 'center',
            fontSize: '1.5em',
        },
    },
    groups: {
        listStyle: 'none',
        margin: '0',
        padding: '0',
        '& > li': {
            flexWrap: 'wrap',
            position: 'relative',
            display: 'flex',
            justifyContent: 'space-between',
            background: white,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            margin: '26px 0px',
            '& span.highlightOnHover:hover': {
                cursor: 'pointer',
                fontWeight: 500,
                '& svg': {
                    color: primaryColor,
                },
            },
            '& > span': {
                display: 'inline-flex',
                alignItems: 'center',
                minWidth: 220,
                minHeight: 54,
            },
            '& svg': {
                minWidth: 60,
                textAlign: 'center',
                marginRight: '10px',
                '@media (min-width: 960px)': {
                    minWidth: 'auto',
                    marginRight: '15px',
                },
            },
            '@media (min-width: 960px)': {
                padding: '0 0 0 15px !important',
                flexWrap: 'nowrap',
                justifyContent: 'space-between',
            },
        },
    },
    actions: {
        borderLeft: `1px solid ${primaryColor}`,
        padding: '15px 15px 15px 30px',
        display: 'inline-flex',
        alignItems: 'center',
        background: primaryColor,
        color: white,
        flexGrow: 1,
        justifyContent: 'space-evenly',
        '& span:hover': {
            cursor: 'pointer',
            color: secondaryColor,
        },
        '@media (min-width: 960px)': {
            flexGrow: 0,
        },
    },
    title: {
        '& > h1': {
            fontSize: '2em',
            margin: '0 10px',
        },
    },
    content: {
        margin: 10,
        '& > div > div > div ': {
            background: white,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            padding: '10px 15px',
            margin: '26px 0',
            '& > span': {
                marginTop: 10,
            },
            '& > span:first-child': {
                marginTop: 0,
            },
        },
    },
    label: {
        display: 'block',
        fontWeight: 400,
    },
    cardAction: {
        position: 'absolute',
        top: 0,
        right: 0,
        padding: 5,
        cursor: 'pointer',
        '&:hover': {
            color: primaryColor,
        },
    },
    card: {
        position: 'relative',
        '& img': {
            maxWidth: '100%',
        },
    },
    dropzone: {
        marginTop: 27,
        border: `2px dashed #888`,
        textAlign: 'center',
        color: '#888',
        borderRadius: '5px',
        fontSize: '0.9em',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& > div': {
            minHeight: '200px',
            width: '80%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    },
    overlay: {
        background: 'rgba(0,0,0,0.6)',
        borderRadius: 5,
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& div': {
            padding: '20px 30px',
            borderRadius: '5px',
            background: '#ffffff',
            boxShadow:
                '2px 1px 5px -1px rgba(0,0,0,0.8), 0px 0px 1px 0px rgba(0,0,0,0.74), 0px 3px 5px -2px rgba(0,0,0,0.72)',
            '& h1': {
                fontSize: '1.5rem',
            },
        },
    },
};

export default registrationsDetailStyle;
