import { primaryColor, secondaryColor, primaryCardHeader, white } from '../../assets/jss/material-kit-react.jsx';

const projectsOverviewStyle = {
    container: {
        flexGrow: 1,
        minWidth: '100%',
        '& h2': {
            textAlign: 'center',
            fontSize: '1.5em',
        },
    },
    header: {
        ...primaryCardHeader,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    actions: {
        borderLeft: `1px solid ${primaryColor}`,
        padding: '15px 15px 15px 30px',
        display: 'inline-flex',
        alignItems: 'center',
        background: primaryColor,
        color: white,
        flexGrow: 1,
        justifyContent: 'space-evenly',
        '& span:hover': {
            cursor: 'pointer',
            color: secondaryColor,
        },
        '@media (min-width: 960px)': {
            flexGrow: 0,
        },
    },
    projects: {
        listStyle: 'none',
        margin: '0',
        padding: '0',
        '& li': {
            position: 'relative',
            width: '100%',
            '& li': {
                maxWidth: 'calc(100% - 20px)',
            },
            '& > div': {
                flexWrap: 'wrap',
                padding: '0 0 0 15px',
                display: 'flex',
                justifyContent: 'space-between',
                background: white,
                boxShadow:
                    '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
                '& svg': {
                    minWidth: 60,
                    textAlign: 'center',
                    marginRight: '10px',
                    '@media (min-width: 960px)': {
                        minWidth: 'auto',
                        marginRight: '15px',
                    },
                },
            },
            margin: '10px 0px',
            '& span.highlightOnHover:hover': {
                cursor: 'pointer',
                fontWeight: 500,
                '& svg': {
                    color: primaryColor,
                },
            },
            '@media (min-width: 960px)': {
                flexWrap: 'nowrap',
                justifyContent: 'space-between',
            },
        },
    },
    overlay: {
        background: 'rgba(0,0,0,0.6)',
        borderRadius: 5,
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& div': {
            padding: '20px 30px',
            borderRadius: '5px',
            background: '#ffffff',
            boxShadow:
                '2px 1px 5px -1px rgba(0,0,0,0.8), 0px 0px 1px 0px rgba(0,0,0,0.74), 0px 3px 5px -2px rgba(0,0,0,0.72)',
            '& h1': {
                fontSize: '1.5rem',
            },
        },
    },
};

export default projectsOverviewStyle;
