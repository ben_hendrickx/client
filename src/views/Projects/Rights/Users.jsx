import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import useReactRouter from 'use-react-router';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Button from '../../../components/CustomButtons/Button.jsx';
import UserRow from '../../../components/User/Row.jsx';
import Page from 'components/Page.jsx';
import ResourceOverview from 'components/ResourceOverview.jsx';
import { Checkbox } from '@material-ui/core';
import Inputs from 'components/Inputs';

function RightsUsers() {
    const { users, projects } = useStore();
    const { match, history } = useReactRouter();
    const [linked, setLinked] = useState(0);

    useEffect(() => {
        async function fetchData() {
            await projects.accessRoles.fetchUsers(match.params.accessRoleId);
        }

        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        !linked && projects.accessRoles.users?.length && setLinked(1);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [projects.accessRoles.users]);

    const data = users.resource.filtered.filter(user => {
        if (linked === '') {
            return true;
        }

        if (linked) {
            return projects.accessRoles.users.includes(user.id);
        }

        return !projects.accessRoles.users.includes(user.id);
    });

    function rowRenderer({ key, index, style }) {
        const user = data[index];

        return (
            <UserRow
                watch={false}
                key={key}
                style={{ ...(style || {}), height: 'auto', width: '100%', margin: '10px 0px 20px 0px' }}
                onClick={() => {}}
                {...user}
            >
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <div style={{ width: 45, margin: '0 20px' }}>
                        <Checkbox
                            checked={projects.accessRoles.users.includes(user.id)}
                            onChange={e => {
                                if (e.target.checked) {
                                    projects.accessRoles.users.push(user.id);
                                } else {
                                    projects.accessRoles.users = projects.accessRoles.users.filter(
                                        userId => !(userId === user.id)
                                    );
                                }
                            }}
                        />
                    </div>
                </div>
            </UserRow>
        );
    }

    return (
        <Page title={`Koppel gebruikers aan toegangsrol`} style={{ height: 'calc(100% - 150px)' }}>
            <ResourceOverview
                noAutoSizer
                apiPrefix={`/project/${projects.id}`}
                store={users.resource}
                rowRenderer={rowRenderer}
                data={data}
                heightOffset={650}
                renderBefore={
                    <div style={{ display: 'flex', justifyContent: 'flex-end', marginRight: 15 }}>
                        <span style={{ display: 'block', width: 45, margin: '0 20px' }}>Gekoppeld</span>
                    </div>
                }
                isFiltering={linked}
                onClearFilters={() => {
                    setLinked('');
                }}
                filters={
                    <>
                        <Inputs.Input
                            label="Gekoppeld"
                            type="select"
                            value={linked}
                            onChange={v => setLinked(v)}
                            options={[{ label: 'Ja', value: 1 }, { label: 'Neen', value: 0 }]}
                        />
                    </>
                }
            />
            <Button
                onClick={async () => {
                    await projects.accessRoles.saveUsers(match.params.accessRoleId);
                    history.goBack();
                    projects.accessRoles.resource.success = `Medewerkers van toegangsrol werden succesvol gewijzigd`;
                }}
                color="success"
                size="md"
            >
                Sla op
            </Button>
        </Page>
    );
}

RightsUsers.propTypes = {
    classes: PropTypes.object,
};

export default observer(RightsUsers);
