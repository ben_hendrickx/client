import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import AccessRoleForm from '../Forms/AccessRole';
import ListRow from 'components/ListRow';
import { Tooltip } from '@material-ui/core';
import ListRowActions from 'components/ListRowActions';
import { Delete, Edit, PeopleOutline } from '@material-ui/icons';
import { useHistory } from 'react-router';

function ProjectsOverview() {
    const { projects, general } = useStore();
    const history = useHistory();

    function onCreateAccessRole() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: 'Nieuwe toegangsrol',
            onMinimize: modal => {
                projects.accessRoles.data.name &&
                    (modal.title = `Nieuwe toegangsrol: ${projects.accessRoles.data.name}`);
                return projects.accessRoles.data;
            },
            onMaximize: modal => {
                projects.accessRoles.data = { ...modal.data };
            },
            body: <AccessRoleForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <>
            <ResourceOverview
                apiPrefix={`/project/${projects.id}`}
                store={projects.accessRoles.resource}
                heightOffset={400}
                rowRenderer={({ key, index, style }) => {
                    const accessRole = projects.accessRoles.resource.filtered[index];
                    const { name } = accessRole;

                    return (
                        <li key={key} style={style}>
                            <ListRow key={key}>
                                <Tooltip placement="top" title={name}>
                                    <span style={{ width: 200 }}>
                                        <span
                                            style={{
                                                display: 'inline-block',
                                                width: 170,
                                                overflow: 'hidden',
                                                textOverflow: 'ellipsis',
                                                whiteSpace: 'nowrap',
                                            }}
                                        >
                                            {name}
                                        </span>
                                    </span>
                                </Tooltip>
                                <ListRowActions>
                                    <Tooltip title="Bewerk toegangsrol" placement="top">
                                        <Edit
                                            onClick={() => {
                                                let modal;
                                                const closeModal = () => modal.close();
                                                projects.accessRoles.data = { ...accessRole };
                                                modal = general.addModal({
                                                    type: 'window',
                                                    width: 1100,
                                                    title: 'Bewerk toegangsrol',
                                                    onMinimize: modal => {
                                                        projects.accessRoles.data.name &&
                                                            (modal.title = `Bewerk toegangsrol: ${projects.accessRoles.data.name}`);
                                                        return projects.accessRoles.data;
                                                    },
                                                    onMaximize: modal => {
                                                        projects.accessRoles.data = { ...modal.data };
                                                    },
                                                    body: (
                                                        <AccessRoleForm onCancel={closeModal} onConfirm={closeModal} />
                                                    ),
                                                });
                                            }}
                                        />
                                    </Tooltip>
                                    <Tooltip title="Beheer gebruikers" placement="top">
                                        <PeopleOutline
                                            onClick={() =>
                                                history.push(
                                                    `/projecten/${projects.id}/toegangsrol/${accessRole.id}/gebruikers`
                                                )
                                            }
                                        />
                                    </Tooltip>
                                    <Tooltip title="Verwijder toegangsrol" placement="top">
                                        <Delete
                                            onClick={() => {
                                                general.addModal({
                                                    title: `Verwijder toegangsrol: ${name}`,
                                                    body: `Bent u zeker dat u deze toegangsrol wilt verwijderen`,
                                                    buttonText: 'Verwijder',
                                                    onConfirm: async () => {
                                                        await projects.accessRoles.resource.delete(
                                                            `/project/${projects.id}`,
                                                            accessRole.id
                                                        );
                                                        projects.accessRoles.resource.data = [];
                                                        projects.accessRoles.resource.status = [];
                                                        projects.accessRoles.resource.fetch(`/project/${projects.id}`);
                                                    },
                                                });
                                            }}
                                        />
                                    </Tooltip>
                                </ListRowActions>
                            </ListRow>
                        </li>
                    );
                }}
                actions={[
                    //TODO: remove level property
                    {
                        icon: 'add',
                        tooltip: 'Maak toegangsrol aan',
                        onClick: onCreateAccessRole,
                    },
                ]}
            />
        </>
    );
}

export default observer(ProjectsOverview);
