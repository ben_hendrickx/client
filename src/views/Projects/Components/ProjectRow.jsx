import { Tooltip } from '@material-ui/core';
import ListRow from 'components/ListRow';
import React from 'react';
import { Delete, Edit, FilterNone, FolderOpen, Lock, LockOpen, NoteAdd, RemoveRedEye } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { useHistory } from 'react-router';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import moment from 'moment';
import { User } from 'components/Levels';
import Copy from '../Modals/Copy';
import ProjectForm from 'views/Projects/Forms/Project';
import DuplicateModal from '../Modals/Duplicate';
import { withTranslation } from 'react-i18next';

function Project(props) {
    const { general, projects } = useStore();
    const { style, project, t } = props;
    const history = useHistory();
    const { id, name, openFrom, closedFrom, closedOffStage, registered } = project;

    let endDate = closedFrom ? closedFrom : closedOffStage;
    closedOffStage && moment(closedFrom).isBefore(new Date(closedOffStage)) && (endDate = closedOffStage);
    const { account } = useStore();

    return (
        <li style={style}>
            <ListRow>
                <Tooltip placement="top" title={name}>
                    <span style={{ width: 200 }}>
                        <FolderOpen />{' '}
                        <span
                            style={{
                                display: 'inline-block',
                                width: 170,
                                overflow: 'hidden',
                                textOverflow: 'ellipsis',
                                whiteSpace: 'nowrap',
                            }}
                        >
                            {name}
                        </span>
                    </span>
                </Tooltip>
                {openFrom && (
                    <span>
                        <LockOpen /> {moment(openFrom).format('DD/MM/YYYY HH:mm')}
                    </span>
                )}
                {endDate && (
                    <span>
                        <Lock /> {moment(endDate).format('DD/MM/YYYY HH:mm')}
                    </span>
                )}
                <ListRowActions>
                    {account.rights.canReadProjects && (
                        <Tooltip title={t('tooltip.project.manage')} placement="top">
                            <RemoveRedEye
                                onClick={() => history.push(`/projecten/${id}/overzicht`)}
                                className="view-project"
                            />
                        </Tooltip>
                    )}
                    <User>
                        {!registered && (
                            <Tooltip title={'Schrijf je in'} placement="top">
                                {account.rights.canReadProjects ? (
                                    <NoteAdd
                                        onClick={async () => {
                                            await projects.fetchProject(project.id);
                                            history.push(`/projecten/inschrijven/${project.id}`);
                                        }}
                                    />
                                ) : (
                                    <span
                                        onClick={async () => {
                                            await projects.fetchProject(project.id);
                                            history.push(`/projecten/inschrijven/${project.id}`);
                                        }}
                                    >
                                        Inschrijven
                                    </span>
                                )}
                            </Tooltip>
                        )}
                    </User>
                    {(account.level === 0 ? !!registered : true) && (
                        <>
                            {account.rights.canUpdateProjects && (
                                <Tooltip title={t('tooltip.project.edit')} placement="top">
                                    <Edit
                                        className="edit-project"
                                        onClick={() => {
                                            let modal;
                                            const closeModal = () => modal.close();
                                            projects.formData = { ...project };
                                            modal = general.addModal({
                                                type: 'window',
                                                width: 1100,
                                                title: t('modal.title.project.edit'),
                                                onMinimize: async modal => {
                                                    projects.formData.name &&
                                                        (modal.title = `${t('modal.title.project.edit')}: ${
                                                            projects.formData.name
                                                        }`);
                                                    return projects.formData;
                                                },
                                                onMaximize: modal => {
                                                    projects.formData = { ...modal.data };
                                                },
                                                body: <ProjectForm onCancel={closeModal} onConfirm={closeModal} />,
                                            });
                                        }}
                                    />
                                </Tooltip>
                            )}
                            {account.rights.canCreateProjects && (
                                <Tooltip title={t('tooltip.project.copy')} placement="top">
                                    <FilterNone
                                        className="copy-project"
                                        onClick={() => {
                                            general.addModal({
                                                title: `${t('modal.title.project.copy')}: ${project.name}`,
                                                body: <DuplicateModal />,
                                                buttonText: t('form.button.duplicate'),
                                                onConfirm: async () => {
                                                    const modal = general.addModal({
                                                        title: `${t('modal.title.project.copy')}: ${project.name}`,
                                                        body: <Copy />,
                                                        width: 900,
                                                    });

                                                    await projects.copy.copy(project.id);
                                                    modal.close();
                                                    await projects.resource.fetch();
                                                    projects.resource.data = [];
                                                    projects.resource.status = [];
                                                    projects.resource.success = t('success.project.copy');
                                                },
                                            });
                                        }}
                                    />
                                </Tooltip>
                            )}
                            {account.rights.canDeleteProjects && (
                                <Tooltip title={t('tooltip.project.delete')} placement="top">
                                    <Delete
                                        className="delete-project"
                                        onClick={() => {
                                            general.addModal({
                                                title: t('modal.title.project.delete'),
                                                body: t('modal.body.project.delete'),
                                                buttonText: t('form.button.delete'),
                                                onConfirm: async () => {
                                                    await projects.resource.delete(undefined, id);
                                                    projects.resource.data = [];
                                                    projects.resource.status = [];
                                                    await projects.resource.fetch();
                                                },
                                            });
                                        }}
                                    />
                                </Tooltip>
                            )}
                        </>
                    )}
                </ListRowActions>
            </ListRow>
        </li>
    );
}

export default withTranslation()(observer(Project));
