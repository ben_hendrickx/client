import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewProject(props) {
    const { projects } = useStore();

    useEffect(() => {
        return () => projects.form.reset();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            store={projects}
            save={async () => await projects.resource.save(undefined, projects.formData)}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                projects.resource.data = [];
                projects.resource.status = [];
                projects.resource.fetch();
            }}
        />
    );
}

NewProject.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewProject);
