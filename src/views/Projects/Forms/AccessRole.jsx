import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewProject(props) {
    const { projects, accessRoles } = useStore();

    useEffect(() => {
        accessRoles.resource.fetch();
        return () => projects.accessRoles.form.reset();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            store={projects.accessRoles}
            save={() => projects.accessRoles.resource.save(`/project/${projects.id}`, projects.accessRoles.data)}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                projects.accessRoles.resource.data = [];
                projects.accessRoles.resource.status = [];
                projects.accessRoles.resource.fetch(`/project/${projects.id}`);
            }}
        />
    );
}

NewProject.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewProject);
