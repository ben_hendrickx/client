import React, { useState } from 'react';
import ImportRoleScheme from './ImportRoleScheme';
import ImportRoleUsersScheme from './ImportRoleUsersScheme';
import { Tab, Tabs } from '@material-ui/core';
import Button from 'components/CustomButtons/Button.jsx';
import { useStore } from '../../mobx';
import { withTranslation } from 'react-i18next';

function ImportExportView({ t }) {
    const { account, projects } = useStore();
    const [activeTab, setActiveTab] = useState(1);

    const { rights } = account;

    return (
        <>
            <Tabs value={activeTab} variant="fullWidth" onChange={(e, activeTab) => setActiveTab(activeTab)}>
                {(rights.canCreateRoles || rights.canCreateGroups || rights.canCreateScenes) && (
                    <Tab label={t('form.labels.project.importRoleScheme')} value={1} />
                )}
                {(rights.canUpdateRoleUsers || rights.canUpdateGroupUsers) && (
                    <Tab label={t('form.labels.project.importRoleUsers')} value={2} />
                )}
                {(rights.canReadRoles || rights.canReadGroups) && (
                    <Tab label={t('form.labels.project.exportRoleScheme')} value={3} />
                )}
            </Tabs>
            <div style={{ marginTop: 10 }}>
                {activeTab === 1 && <ImportRoleScheme />}
                {activeTab === 2 && <ImportRoleUsersScheme />}
                {activeTab === 3 && (
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                            justifyContent: 'center',
                            height: 'calc(100vh - 200px)',
                        }}
                    >
                        <h3>{t('form.labels.project.exportRoleScheme')}</h3>
                        <p>{t('form.labels.project.exportRoleSchemeText')}</p>
                        <Button
                            color="success"
                            onClick={async () => {
                                window.open(
                                    `${window.location.protocol}//${window.location.hostname}:${process.env
                                        .REACT_APP_PORT || 9998}/api/reports/role_scheme.xlsx?projectId=${
                                        projects.resource.item.id
                                    }&token=${account.token}`,
                                    '_blank'
                                );
                            }}
                            size="md"
                        >
                            {t('form.labels.project.exportRoleScheme')}
                        </Button>
                    </div>
                )}
            </div>
        </>
    );
}

export default withTranslation()(ImportExportView);
