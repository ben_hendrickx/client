import { primaryCardHeader } from '../../assets/jss/material-kit-react.jsx';

const newProjectStyle = {
    header: {
        ...primaryCardHeader,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    form: {
        margin: '0',
        '& h1': {
            fontSize: '2.5em',
        },
        '& h2': {
            color: '#444444',
            fontSize: '1.5em',
        },
        '& h3': {
            color: '#999',
            fontSize: '0.8em',
            lineHeight: '1em',
            paddingBottom: 17,
        },
    },
    imageOverlay: {
        background: 'rgba(0,0,0,0.6)',
        borderRadius: 5,
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& img': {
            cursor: 'pointer',
            maxWidth: '90vw',
            maxHeight: '90vh',
        },
    },
    card: {
        background: '#fff',
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: '10px 15px',
        margin: '26px 0',
        '& > span': {
            marginTop: 10,
        },
        '& > span:first-child': {
            marginTop: 0,
        },
    },
};

export default newProjectStyle;
