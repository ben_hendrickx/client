import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Widget from 'components/Widget.jsx';
import { ArtTrack, Assignment, CameraRoll, Group, Repeat } from '@material-ui/icons';
import Page from 'components/Page.jsx';
import Flex from 'components/Flex.jsx';
import { withTranslation } from 'react-i18next';

function Statistics({ t }) {
    const { account, overview, registrations, repetitions } = useStore();
    const { match } = useReactRouter();

    const { rights } = account;

    useEffect(() => {
        if (match?.params?.id) {
            overview.fetch(match.params.id);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.token]);

    return (
        <Page loading={overview.loading}>
            <Flex>
                <Widget
                    title={t('form.labels.project.statistics.numberOfRegistrations')}
                    number={overview.registrations}
                    link={
                        rights.canReadRegistrations
                            ? {
                                  href: `/projecten/${match.params.id}/inschrijvingen`,
                                  label: t('form.labels.project.statistics.viewRegistrations'),
                              }
                            : null
                    }
                    icon={<Assignment />}
                />
                <Widget
                    title={t('form.labels.project.statistics.numberOfOnStageRegistrations')}
                    number={overview.onStage}
                    link={
                        rights.canReadRegistrations
                            ? {
                                  href: `/projecten/${match.params.id}/inschrijvingen`,
                                  label: t('form.labels.project.statistics.viewOnStageRegistrations'),
                                  onClick: () => (registrations.resource.filters.onStage = 1),
                              }
                            : null
                    }
                    icon={<Assignment />}
                />
                <Widget
                    title={t('form.labels.project.statistics.numberOfOffStageRegistrations')}
                    number={overview.offStage}
                    link={
                        rights.canReadRegistrations
                            ? {
                                  href: `/projecten/${match.params.id}/inschrijvingen`,
                                  label: t('form.labels.project.statistics.viewOffStageRegistrations'),
                                  onClick: () => (registrations.resource.filters.onStage = 0),
                              }
                            : null
                    }
                    icon={<Assignment />}
                />
                <Widget
                    title={t('form.labels.project.statistics.numberOfRoles')}
                    number={overview.roles}
                    link={
                        rights.canReadRoles
                            ? {
                                  href: `/projecten/${match.params.id}/rollen`,
                                  label: t('form.labels.project.statistics.viewRoles'),
                              }
                            : null
                    }
                    icon={<ArtTrack />}
                />
                <Widget
                    title={t('form.labels.project.statistics.numberOfGroups')}
                    number={overview.groups}
                    link={
                        rights.canReadGroups
                            ? {
                                  href: `/projecten/${match.params.id}/groepen`,
                                  label: t('form.labels.project.statistics.viewGroups'),
                              }
                            : null
                    }
                    icon={<Group />}
                />
                <Widget
                    title={t('form.labels.project.statistics.numberOfScenes')}
                    number={overview.scenes}
                    link={
                        rights.canReadScenes
                            ? {
                                  href: `/projecten/${match.params.id}/scenes`,
                                  label: t('form.labels.project.statistics.viewScenes'),
                              }
                            : null
                    }
                    icon={<CameraRoll />}
                />
                <Widget
                    title={t('form.labels.project.statistics.numberOfRepetitions')}
                    number={overview.repetitions.total}
                    link={
                        rights.canReadScenes
                            ? {
                                  href: `/projecten/${match.params.id}/repetities`,
                                  label: t('form.labels.project.statistics.viewRepetitions'),
                                  onClick: () => delete repetitions.resource.filters.past,
                              }
                            : null
                    }
                    icon={<Repeat />}
                />
                <Widget
                    title={t('form.labels.project.statistics.numberOfRepetitionsPast')}
                    number={overview.repetitions.past}
                    link={
                        rights.canReadRepetitions
                            ? {
                                  href: `/projecten/${match.params.id}/repetities`,
                                  label: t('form.labels.project.statistics.viewRepetitionsPast'),
                                  onClick: () => (repetitions.resource.filters.past = 1),
                              }
                            : null
                    }
                    icon={<Repeat />}
                />
                <Widget
                    title={t('form.labels.project.statistics.numberOfRepetitionsFuture')}
                    number={overview.repetitions.future}
                    link={
                        rights.canReadRepetitions
                            ? {
                                  href: `/projecten/${match.params.id}/repetities`,
                                  label: t('form.labels.project.statistics.viewRepetitionsFuture'),
                                  onClick: () => (repetitions.resource.filters.past = 0),
                              }
                            : null
                    }
                    icon={<Repeat />}
                />
            </Flex>
        </Page>
    );
}

Statistics.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(observer(Statistics));
