import React, { useEffect } from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Inputs from 'components/Inputs';
import Row from './Components/ProjectRow';
import RegistrationsOverview from '../Registrations/Overview';
import ProjectForm from './Forms/Project';
import { withTranslation } from 'react-i18next';

function ProjectsOverview({ t }) {
    const { ui, account, projects, general, registrations } = useStore();

    useEffect(() => {
        if (account.level === 0) {
            delete projects.resource.filters.inactive;
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.level]);

    useEffect(() => {
        account.level === 0 && registrations.resource.fetch(`/user/${account.id}`);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.id]);

    function onCreateProject() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.project.new'),
            onMinimize: modal => {
                projects.resource.form.data.name &&
                    (modal.title = `${t('modal.title.project.new')}: ${projects.resource.form.data.name}`);
                return projects.resource.form.data;
            },
            onMaximize: modal => {
                projects.resource.form.data = { ...modal.data };
            },
            body: <ProjectForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <>
            <ResourceOverview
                style={account.level === 0 ? { maxHeight: 'calc(50vh - 50px)' } : {}}
                store={projects.resource}
                rowRenderer={({ key, index, style }) => (
                    <Row key={key} style={style} project={projects.resource.filtered[index]} />
                )}
                noAutoSizer={account.level === 0}
                heightOffset={
                    account.level === 0 ? `50vh - ${projects.resource.success ? 250 : 180}px` : ui.isMobile ? 310 : 250
                }
                baseHeight={account.level === 0 ? '100vh' : '100vh'}
                rowHeight={ui.isMobile ? 200 : 60}
                actions={[
                    //TODO: remove level property
                    account.rights.canCreateProjects && {
                        icon: 'add',
                        tooltip: t('tooltip.project.create'),
                        onClick: onCreateProject,
                    },
                    account.rights.canExportProjects && {
                        icon: 'getApp',
                        href: '/api/reports/projecten.xlsx',
                        tooltip: t('tooltip.report.download'),
                    },
                ]}
                filters={
                    account.level ? (
                        <>
                            <Inputs.projects.filters.inactive />
                        </>
                    ) : null
                }
            />
            {!ui.isMobile && account.level === 0 && account.id && (
                <RegistrationsOverview apiPrefix={`/user/${account.id}`} heightOffset={500} />
            )}
        </>
    );
}

export default withTranslation()(observer(ProjectsOverview));
