import { WarningOutlined } from '@material-ui/icons';
import React from 'react';
import { withTranslation } from 'react-i18next';

function DuplicateModal({ t }) {
    return (
        <>
            <b
                style={{
                    marginBottom: 10,
                    display: 'flex',
                    alignItems: 'center',
                    background: '#ffb547',
                    color: 'white',
                    padding: '5px 10px',
                    borderRadius: 2,
                }}
            >
                <WarningOutlined style={{ marginRight: 10 }} />
                {t('warning.project.inactive')}
            </b>
            {t('modal.body.project.copy')}
        </>
    );
}

export default withTranslation()(DuplicateModal);
