import { Success } from 'components/Notifications';
import ProgressBar from 'components/ProgressBar';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import { useStore } from '../../../mobx';

function CopyProject({ t }) {
    const { projects } = useStore();
    const { copy } = projects;

    useEffect(() => {
        projects.copy.listen();

        return () => {
            delete copy.success;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            {copy.success ? (
                <Success message={copy.success} />
            ) : (
                <>
                    <h2>{t('form.labels.patience')}</h2>
                    <ProgressBar progress={copy.progress} text={`${copy.text}` || t('form.labels.prepare')} />
                </>
            )}
        </>
    );
}

export default withTranslation()(observer(CopyProject));
