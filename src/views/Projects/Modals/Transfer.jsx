import Inputs from 'components/Inputs';
import React, { useEffect } from 'react';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function TransferModal({ t }) {
    const { projects } = useStore();

    useEffect(() => {
        const oldFilter = projects.resource.filters.inactive;
        delete projects.resource.filters.inactive;
        projects.resource.fetch();

        return () => {
            delete projects.copy.to;
            delete projects.copy.success;
            projects.resource.filters.inactive = oldFilter;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <Inputs.Input
                fullWidth
                label={t('form.labels.project.select')}
                type="select"
                value={projects.copy.to}
                onChange={v => (projects.copy.to = v)}
                options={projects.autoComplete}
            />
            <small style={{ display: 'block' }}>
                {t('modal.body.project.copyRegistrations', { name: projects.name })}
            </small>
            <Inputs.Input
                type="checkbox"
                label={t('form.labels.project.onStageRegistrations')}
                value={projects.copy.onStage}
                onChange={v => (projects.copy.onStage = v)}
            />
            {projects.copy.onStage && (
                <Inputs.Input
                    type="checkbox"
                    label={t('form.labels.project.asFigurant')}
                    value={projects.copy.appearing}
                    onChange={v => (projects.copy.appearing = v)}
                />
            )}
            <Inputs.Input
                type="checkbox"
                label={t('form.labels.project.offStageRegistrations')}
                value={projects.copy.offStage}
                onChange={v => (projects.copy.offStage = v)}
            />
        </>
    );
}

export default withTranslation()(observer(TransferModal));
