import React, { useEffect } from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import ListRow from 'components/ListRow';
import { observer } from 'mobx-react-lite';
import { ArtTrack, CameraRoll, Group, Person } from '@material-ui/icons';
import { Checkbox } from '@material-ui/core';
import Button from 'components/CustomButtons/Button';
import { withTranslation } from 'react-i18next';
import { t } from 'i18next';
import Inputs from 'components/Inputs';

const ObservedCheckBox = observer(({ data, t }) => {
    const { projectUsers } = useStore();

    const { sceneId, project_role_id, groupId, castId, userId } = data;
    const id = `${sceneId}|${project_role_id}|${groupId}|${castId}|${userId}`;

    return (
        <div style={{ padding: '6px 0' }}>
            <Checkbox
                checked={projectUsers.transfer.resource.checked.includes(id)}
                onChange={e => {
                    if (!projectUsers.transfer.resource.checked.includes(id)) {
                        projectUsers.transfer.resource.checked.push(id);
                    } else {
                        projectUsers.transfer.resource.checked.splice(
                            projectUsers.transfer.resource.checked.indexOf(id),
                            1
                        );
                    }
                }}
            />
        </div>
    );
});

function TransferRows(props) {
    const { projectId } = props;
    const { general, projectUsers, scenes, projectRoles, groups } = useStore();

    function rowRenderer({ key, index, style, checkable = true }) {
        const projectUser = projectUsers.transfer.resource.filtered[index];

        return (
            <li key={key} style={{ ...style, display: 'flex' }}>
                {checkable && <ObservedCheckBox data={projectUser} />}
                <ListRow style={{ flexGrow: 1 }}>
                    <span style={{ minWidth: 250 }}>
                        <CameraRoll />
                        {projectUser.sceneIdentifier} - {projectUser.sceneName}
                    </span>
                    {projectUser.roleName && (
                        <span style={{ flexGrow: 1 }}>
                            <ArtTrack />
                            {projectUser.roleIdentifier} - {projectUser.roleName} ({t('form.labels.cast')}:{' '}
                            {projectUser.castName})
                        </span>
                    )}
                    {projectUser.groupName && (
                        <span style={{ flexGrow: 1 }}>
                            <Group />
                            {projectUser.groupIdentifier} - {projectUser.groupName} ({t('form.labels.cast')}:{' '}
                            {projectUser.castName})
                        </span>
                    )}
                    <span style={{ minWidth: 250 }}>
                        <Person />
                        {projectUser.lastName} {projectUser.firstName}
                    </span>
                </ListRow>
            </li>
        );
    }

    useEffect(() => {
        scenes.resource.filters.project = projectId;
        projectRoles.resource.filters.project = projectId;
        groups.resource.filters.project = projectId;

        return () => {
            delete scenes.resource.filters.project;
        };
    }, []);

    return (
        <ResourceOverview
            apiPrefix={`/project/${projectId}`}
            store={projectUsers.transfer.resource}
            rowRenderer={rowRenderer}
            heightOffset={360}
            filters={
                <>
                    <Inputs.transferProjectUsers.filters.scene />
                    <Inputs.transferProjectUsers.filters.role />
                    <Inputs.transferProjectUsers.filters.group />
                    <Inputs.transferProjectUsers.filters.cast />
                </>
            }
            renderBefore={
                <div style={{ marginBottom: 10 }}>
                    <Button
                        onClick={async () => {
                            if (projectUsers.transfer.resource.checked.length) {
                                projectUsers.transfer.resource.checked = [];
                            } else {
                                projectUsers.transfer.resource.checked = projectUsers.transfer.resource.filtered.map(
                                    ({ sceneId, project_role_id, groupId, castId, userId }) =>
                                        `${sceneId}|${project_role_id}|${groupId}|${castId}|${userId}`
                                );
                            }
                        }}
                        color="primary"
                        size="md"
                    >
                        {t('form.button.selectEmployees')}
                    </Button>
                    <Button
                        color={!projectUsers.transfer.resource.checked.length ? 'disabled' : 'success'}
                        onClick={async () => {
                            if (projectUsers.transfer.resource.checked.length) {
                                general.addModal({
                                    title: t('modal.title.project.transfer'),
                                    body: t('modal.body.project.transfer', {
                                        count: projectUsers.transfer.resource.checked.length,
                                    }),
                                    buttonText: t('form.button.confirm'),
                                    onConfirm: async () => {
                                        await projectUsers.transfer.save(projectId);
                                        projectUsers.transfer.resource.data = [];
                                        projectUsers.transfer.resource.status = [];
                                        projectUsers.transfer.resource.fetch(`/project/${projectId}`);
                                        projectUsers.transfer.resource.success = t('success.project.transfer');
                                        projectUsers.transfer.resource.checked = [];
                                    },
                                });
                            }
                        }}
                        size="md"
                    >
                        <span style={{ marginTop: '1px', marginLeft: '5px' }}>
                            {t('form.button.transferSelection')}
                        </span>
                    </Button>
                </div>
            }
        />
    );
}

export default withTranslation()(observer(TransferRows));
