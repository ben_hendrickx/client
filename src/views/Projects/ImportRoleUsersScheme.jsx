import React from 'react';
import Button from 'components/CustomButtons/Button.jsx';
import CsvDropzone from 'components/Dropzone/CsvDropzone';
import { withStyles } from '@material-ui/styles';
import { useStore } from '../../mobx';
import { useState } from 'react';
import ListRow from 'components/ListRow';
import { Success } from 'components/Notifications';
import { Person } from '@material-ui/icons';
import { withTranslation } from 'react-i18next';

const style = {
    dropzone: {
        marginTop: 0,
        border: `2px dashed #888`,
        textAlign: 'center',
        color: '#888',
        borderRadius: '5px',
        fontSize: '0.9em',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& > div': {
            minHeight: '200px',
            width: '80%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    },
};

function ImportRoleScheme({ classes, t }) {
    const { account, projects } = useStore();
    const [items, setItems] = useState([]);
    const [success, setSuccess] = useState('');
    return (
        <>
            <Success message={success} onClose={() => setSuccess('')} />
            {items.length > 0 && (
                <div style={{ display: 'flex', flexDirection: 'column', maxHeight: 300, overflowY: 'auto' }}>
                    {items.map(i => (
                        <ListRow style={{ flexGrow: 1, background: '#eaeaea', paddingRight: 20 }}>
                            <div style={{ display: 'flex', alignItems: 'center', margin: '0 20px' }}>
                                <Person />{' '}
                                <span
                                    style={{
                                        width: 250,
                                        overflow: 'hidden',
                                        textOverflow: 'ellipsis',
                                        whiteSpace: 'nowrap',
                                    }}
                                >
                                    {i.lastName} {i.firstName}
                                </span>
                            </div>
                            <span
                                style={{
                                    width: 250,
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis',
                                    whiteSpace: 'nowrap',
                                }}
                            >
                                {i.identifier} ({t('form.labels.cast')}: {i.cast})
                            </span>
                        </ListRow>
                    ))}
                </div>
            )}
            {!items.length && (
                <CsvDropzone
                    onData={async data => {
                        setSuccess('');
                        data = data.split('\n').slice(1);

                        data = data.map(item => {
                            item = item.split(';');

                            return {
                                cast: item.shift(),
                                identifier: item.shift(),
                                firstName: item.shift(),
                                lastName: item.shift(),
                                email: item.shift(),
                            };
                        });

                        setItems(data);
                    }}
                    className={classes.dropzone}
                />
            )}
            <Button
                color="success"
                onClick={async () => {
                    window.open(
                        `${window.location.protocol}//${window.location.hostname}:${process.env.REACT_APP_PORT ||
                            9998}/api/templates/import_role_scheme_users.xlsx?token=${account.token}`,
                        '_blank'
                    );
                }}
                size="md"
            >
                {t('form.button.downloadTemplate')}
            </Button>
            <Button
                color="success"
                onClick={async () => {
                    await projects.importRoleSchemeUsers(items);
                    setSuccess(t('success.import', { count: items.length }));
                    setItems([]);
                }}
                size="md"
            >
                {t('form.button.import')}
            </Button>
        </>
    );
}

export default withTranslation()(withStyles(style)(ImportRoleScheme));
