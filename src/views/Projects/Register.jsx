import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';

import registerProjectStyle from './register.style.jsx';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { Danger } from 'components/Notifications.jsx';
import Page from 'components/Page.jsx';
import Form from 'components/Form.jsx';
import useReactRouter from 'use-react-router';
import { withTranslation } from 'react-i18next';

function RegisterProject(props) {
    const { onConfirm = () => {}, userId, t } = props;
    const { projects, account, shirtSizes, clothingSizes, roles, ui } = useStore();
    const [location, setLocation] = useState(null);
    const { match, history } = useReactRouter();

    useEffect(() => {
        async function fetchProject() {
            await projects.fetchProject(projects.id || match.params.id);
            const onStageEnabled =
                new Date(projects.resource.item.closedFrom).getTime() + 24 * 60 * 60 * 1000 >= Date.now();
            const offStageEnabled =
                new Date(projects.resource.item.closedOffStage).getTime() + 24 * 60 * 60 * 1000 >= Date.now();

            if (!offStageEnabled && !onStageEnabled) {
                setLocation('disabled');
            } else if (!onStageEnabled) {
                setLocation('off');
            } else if (!offStageEnabled) {
                setLocation('on');
            }
        }

        projects.fetchPartsOfTown();
        shirtSizes.fetch().then(async () => {
            await account.fetchSizes(userId);
            const { data } = projects.register;
            data.height = account.height;
            data.chest = account.chest;
            data.waist = account.waist;
            data.hipSize = account.hipSize;
            data.headSize = account.headSize;
            data.clothingSize = account.clothingSize;
            data.shirtSize = shirtSizes.autoComplete.find(({ label }) => label === account.tShirt)?.value;
        });
        clothingSizes.fetch();
        roles.fetch();

        if (projects.id || match.params.id) {
            fetchProject();
        }

        return () => {
            projects.register.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const project = projects.resource.item;

    if (location === 'disabled') {
        return null;
    }

    //TODO: use page component
    return (
        <Page title={t('page.title.registerForProject', { project: project.name })}>
            <Danger message={projects.register.error} onClose={() => delete projects.register.error} />
            <Form
                fullWidth={ui.isMobile}
                noCancel
                id={userId || account.id}
                store={projects.register}
                onConfirm={() => {
                    onConfirm();
                    if (account.level === 0) {
                        history.push('/inschrijvingen');
                    }
                }}
            />
        </Page>
    );
}

RegisterProject.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(registerProjectStyle)(observer(RegisterProject)));
