import React from 'react';
import Button from 'components/CustomButtons/Button.jsx';
import CsvDropzone from 'components/Dropzone/CsvDropzone';
import { withStyles } from '@material-ui/styles';
import { useStore } from '../../mobx';
import { useState } from 'react';
import ProjectUser from 'views/Retributions/Projects/Components/ProjectUser';
import ListRow from 'components/ListRow';
import { Success } from 'components/Notifications';
import spinner from '../../assets/img/spinner_button.gif';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

const style = {
    dropzone: {
        marginTop: 0,
        border: `2px dashed #888`,
        textAlign: 'center',
        color: '#888',
        borderRadius: '5px',
        fontSize: '0.9em',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& > div': {
            minHeight: '200px',
            width: '80%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    },
};

function ImportRoleScheme({ classes, t }) {
    const { account, projects } = useStore();
    const [items, setItems] = useState([]);
    const [success, setSuccess] = useState('');
    return (
        <>
            <Success message={success} onClose={() => setSuccess('')} />
            {items.length > 0 && (
                <div style={{ display: 'flex', flexDirection: 'column', maxHeight: 300, overflowY: 'auto' }}>
                    {items.map(i => {
                        const projectUser = {
                            sceneIdentifier: i.scene.identifier,
                            sceneName: i.scene.name,
                            sceneDescription: i.scene.description,
                            roleIdentifier: i.role?.identifier,
                            roleName: i.role?.name,
                            groupIdentifier: i.group?.identifier,
                            groupName: i.group?.name,
                        };
                        return (
                            <ListRow style={{ flexGrow: 1, background: '#eaeaea' }}>
                                <ProjectUser {...projectUser} />
                            </ListRow>
                        );
                    })}
                </div>
            )}
            {!items.length && (
                <CsvDropzone
                    onData={async data => {
                        setSuccess('');
                        data = data.split('\n').slice(1);

                        data = data
                            .filter(v => v)
                            .map(item => {
                                item = item.split(';');
                                const isRole = item[7] !== '';

                                if (isRole) {
                                    const speaking = item[8] !== '';

                                    return {
                                        scene: {
                                            identifier: item.shift(),
                                            name: item.shift(),
                                            description: item.shift(),
                                            group: item.shift(),
                                        },
                                        role: {
                                            identifier: item.shift(),
                                            name: item.shift(),
                                            description: item.shift(),
                                            speaking,
                                        },
                                    };
                                }

                                return {
                                    scene: {
                                        identifier: item.shift(),
                                        name: item.shift(),
                                        description: item.shift(),
                                        group: item.shift(),
                                    },
                                    group: {
                                        identifier: item.shift(),
                                        name: item.shift(),
                                        description: item.shift(),
                                    },
                                };
                            });

                        setItems(data);
                    }}
                    className={classes.dropzone}
                />
            )}
            <Button
                color="success"
                onClick={async () => {
                    window.open(
                        `${window.location.protocol}//${window.location.hostname}:${process.env.REACT_APP_PORT ||
                            9998}/api/templates/import_role_scheme.xlsx?token=${account.token}`,
                        '_blank'
                    );
                }}
                size="md"
            >
                Download template
            </Button>
            <Button
                color="success"
                onClick={async () => {
                    await projects.importRoleScheme(items);
                    setSuccess(t('success.import', { count: items.length }));
                    setItems([]);
                }}
                size="md"
            >
                {projects.loading ? <img alt="spinner" src={spinner} height={25} /> : t('form.button.import')}
            </Button>
        </>
    );
}

export default withTranslation()(withStyles(style)(observer(ImportRoleScheme)));
