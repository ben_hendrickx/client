import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import TabView from 'components/TabView.jsx';

import MessagesOverview from 'views/Messages/Overview.jsx';
import MessageReadOverview from 'views/Messages/ReadOverview.jsx';
import Overview from 'views/Projects/Statistics.jsx';
import Roles from 'views/Roles/Overview.jsx';
import Groups from 'views/Groups/Overview.jsx';
import Scenes from 'views/Scenes/Overview.jsx';
import Repetitions from 'views/Repetitions/Overview.jsx';
import NotRegistered from 'views/Registrations/NotRegistered.jsx';
import Registrations from 'views/Registrations/Overview.jsx';
import RoleUser from 'views/Roles/User.jsx';
import RegistrationDetail from 'views/Registrations/RegistrationDetail.jsx';
import Badges from 'views/Registrations/Badges.jsx';
import GroupDetail from 'views/Groups/Detail.jsx';
import GroupUsers from 'views/Groups/Users.jsx';
import NewScene from 'views/Scenes/New.jsx';
import NewSceneGroup from 'views/Scenes/NewGroup.jsx';
import Scene from 'views/Scenes/Detail.jsx';
import SceneGroups from 'views/Scenes/Groups.jsx';
import NewRepetition from 'views/Repetitions/New.jsx';
import PresenceOverview from 'views/Repetitions/Presence.jsx';
import RightsOverview from 'views/Projects/Rights/Overview.jsx';
import RightsUsers from 'views/Projects/Rights/Users.jsx';
import { Success } from 'components/Notifications';

import {
    AccountBalance,
    Edit,
    Assignment,
    ArtTrack,
    Group,
    CameraRoll,
    TransferWithinAStation,
    Repeat,
    LocalOfferOutlined,
    PanTool,
    ControlPointDuplicate,
    ImportExport,
    Forum,
} from '@material-ui/icons';
import Page from 'components/Page';
import Loader from 'components/Loader';
import New from './Forms/Project';
import Transfer from './Modals/Transfer';
import Copy from './Modals/Copy';
import TransferRows from './TransferRows';
import moment from 'moment';
import ImportExportView from './ImportExportView';
import Calendar from 'views/Calendar';
import { withTranslation } from 'react-i18next';

function Project({ t }) {
    const { account, general, projects, notifications } = useStore();
    const { history, match } = useReactRouter();

    const { rights } = account;

    useEffect(() => {
        if (match?.params?.id && !isNaN(parseInt(match.params.id))) {
            projects.fetchProject(match.params.id);

            if (!account.rights.canReadNotificationsGlobal) {
                notifications.listen_unread(parseInt(match.params.id));
            }
        }

        return () => {
            projects.resource.item = {};
            delete projects.id;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (isNaN(match.params.id) || !projects.id) {
        return <Loader loading={true} />;
    }

    function onTransferRegistrations() {
        let modal;
        modal = general.addModal({
            width: 600,
            title: t('modal.title.project.copyRegistrations'),
            body: <Transfer />,
            buttonText: t('form.button.copy'),
            onConfirm: async () => {
                const toProject = projects.resource.filtered.find(project => project.id === parseInt(projects.copy.to));
                general.addModal({
                    title: t('modal.title.project.copyRegistrations'),
                    body: <Copy />,
                    width: 900,
                    onClose: async () => {
                        delete projects.copy.success;
                        projects.resource.data = [];
                        projects.resource.status = [];
                        await projects.resource.fetch();
                    },
                });
                modal.close();
                await projects.copy.copyRegistrations(match.params.id, toProject.id);
            },
        });
    }

    function onTransferClothing() {
        let modal;
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.project.transfer'),
            body: <TransferRows projectId={projects.id} onConfirm={() => modal.close()} />,
        });
    }

    return (
        <Page title={`${t('page.title.manage')} ${projects.resource.item.name}`} showThreads>
            {projects.resource.success && (
                <Success message={projects.resource.success} onClose={() => (projects.resource.success = '')} />
            )}
            <TabView
                tabs={[
                    {
                        label: t('page.title.overview'),
                        icon: <AccountBalance />,
                        onClick: () => history.push(`/projecten/${projects.id}/overzicht`),
                    },
                    rights.canReadRegistrations && {
                        label: t('page.title.registrations'),
                        icon: <Assignment />,
                        onClick: () => history.push(`/projecten/${projects.id}/inschrijvingen`),
                    },
                    rights.canReadRoles && {
                        label: t('page.title.roles'),
                        icon: <ArtTrack />,
                        onClick: () => history.push(`/projecten/${projects.id}/rollen`),
                    },
                    rights.canReadGroups && {
                        label: t('page.title.groups'),
                        icon: <Group />,
                        onClick: () => history.push(`/projecten/${projects.id}/groepen`),
                    },
                    rights.canReadScenes && {
                        label: t('page.title.scenes'),
                        icon: <CameraRoll />,
                        onClick: () => history.push(`/projecten/${projects.id}/scenes`),
                    },
                    rights.canReadRepetitions && {
                        label: t('page.title.repetitions'),
                        icon: <Repeat />,
                        onClick: () => history.push(`/projecten/${projects.id}/repetities`),
                    },
                    rights.canReadMessages && {
                        label: t('page.title.messages'),
                        icon: <Forum />,
                        onClick: () => history.push(`/projecten/${projects.id}/berichten`),
                    },
                    rights.canManageProjectRights && {
                        label: t('page.title.accessRoles'),
                        icon: <PanTool />,
                        onClick: () => history.push(`/projecten/${projects.id}/toegangsrollen`),
                    },
                ]}
                actions={[
                    account.rights.canUpdateProjects && {
                        tooltip: t('tooltip.project.edit'),
                        icon: <Edit />,
                        onClick: () => {
                            let modal;
                            const closeModal = () => modal.close();
                            projects.formData = { ...projects.resource.item };
                            modal = general.addModal({
                                type: 'window',
                                width: 1100,
                                title: t('modal.title.project.edit'),
                                onMinimize: modal => {
                                    projects.formData.name &&
                                        (modal.title = `${t('modal.title.project.edit')}: ${projects.formData.name}`);
                                    return projects.formData;
                                },
                                onMaximize: modal => {
                                    projects.formData = { ...modal.data };
                                },
                                body: (
                                    <New
                                        onCancel={closeModal}
                                        onConfirm={async () => {
                                            await projects.fetchProject(match.params.id);
                                            closeModal();
                                        }}
                                    />
                                ),
                            });
                        },
                    },
                    rights.canTransferClothing &&
                        (!projects.resource.item.projectClosed ||
                            moment().isBefore(projects.resource.item.projectClosed)) && {
                            icon: <ControlPointDuplicate />,
                            tooltip: t('tooltip.project.transfer'),
                            onClick: onTransferClothing,
                        },
                    rights.canCopyRegistrations && {
                        icon: <TransferWithinAStation className="copy-registrations" />,
                        tooltip: t('tooltip.project.copyRegistrations'),
                        onClick: onTransferRegistrations,
                    },
                    //TODO: add right for creating badges
                    rights.canMakeBadges && {
                        icon: <LocalOfferOutlined />,
                        tooltip: t('tooltip.project.badges'),
                        onClick: () => history.push(`/projecten/${projects.id}/badges`),
                    },
                    (rights.canCreateRoles ||
                        rights.canCreateGroups ||
                        rights.canCreateScenes ||
                        rights.canUpdateRoleUsers ||
                        rights.canUpdateGroupUsers) && {
                        icon: <ImportExport />,
                        tooltip: t('tooltip.project.import'),
                        onClick: () => {
                            let outerModal;
                            outerModal = general.addModal({
                                type: 'window',
                                title: t('modal.title.project.import'),
                                body: (
                                    <ImportExportView
                                        onClose={() => {
                                            outerModal.close();
                                        }}
                                    />
                                ),
                            });
                        },
                    },
                ]}
                routes={[
                    rights.canReadProjects && { path: '/projecten/:id/overzicht', component: Overview },
                    rights.canCopyRegistrations && { path: '/projecten/:id/kopieer-deelnemers', component: Transfer },
                    rights.canReadRegistrations && { path: '/projecten/:id/inschrijvingen', component: Registrations },
                    //TODO: add right for creating badges
                    rights.canMakeBadges && { path: '/projecten/:id/badges', component: Badges },
                    rights.canReadRegistrations && {
                        path: '/projecten/:id/inschrijvingen/niet-ingeschreven',
                        component: NotRegistered,
                    },
                    rights.canReadRoles && { path: '/projecten/:id/rollen', component: Roles },
                    rights.canReadGroups && { path: '/projecten/:id/groepen', component: Groups },
                    rights.canReadScenes && { path: '/projecten/:id/scenes', component: Scenes },
                    rights.canReadRepetitions && { path: '/projecten/:id/repetities', component: Repetitions },
                    rights.canUpdateRoles && {
                        path: '/projecten/:id/rollen/bewerk/:roleId/gebruikers',
                        component: RoleUser,
                    },
                    rights.canReadRegistrations && {
                        path: '/projecten/:id/inschrijvingen/:registrationId',
                        component: RegistrationDetail,
                        exact: false,
                    },
                    rights.canReadGroups && { path: '/projecten/:id/groepen/:groupId', component: GroupDetail },
                    { path: '/projecten/:id/groepen/bewerk/:groupId/gebruikers', component: GroupUsers },
                    rights.canCreateScenes && { path: '/projecten/:id/scenes/nieuw', component: NewScene },
                    rights.canUpdateScenes && { path: '/projecten/:id/scenes/groep/nieuw', component: NewSceneGroup },
                    rights.canUpdateScenes && {
                        path: '/projecten/:id/scenes/groep/bewerk/:sceneId',
                        component: NewSceneGroup,
                    },
                    rights.canUpdateScenes && { path: '/projecten/:id/scenes/bewerk/:sceneId', component: NewScene },
                    rights.canReadScenes && { path: '/projecten/:id/scenes/:sceneId', component: Scene },
                    rights.canUpdateSceneUsers && {
                        path: '/projecten/:id/scenes/bewerk/rollen-groepen/:sceneId',
                        component: SceneGroups,
                    },
                    rights.canCreateRepetitions && {
                        path: '/projecten/:id/repetities/nieuw',
                        component: NewRepetition,
                    },
                    rights.canUpdateRepetitions && {
                        path: '/projecten/:id/repetities/:repetitionId/bewerk',
                        component: NewRepetition,
                    },
                    rights.canReadRepetitions && {
                        path: '/projecten/:id/repetities/:repetitionId/aanwezigheid',
                        component: PresenceOverview,
                    },
                    rights.canManageProjectRights && {
                        path: '/projecten/:id/toegangsrollen',
                        component: RightsOverview,
                    },
                    rights.canManageProjectRights && {
                        path: '/projecten/:id/toegangsrol/:accessRoleId/gebruikers',
                        component: RightsUsers,
                    },
                    rights.canSeeCalendar && {
                        path: '/projecten/:id/kalender',
                        component: Calendar,
                    },
                    rights.canReadMessages && { path: '/projecten/:id/berichten', component: MessagesOverview },
                    rights.canReadMessagesRead && {
                        path: '/projecten/:id/berichten/:messageId/gelezen',
                        component: MessageReadOverview,
                    },
                ]}
            />
        </Page>
    );
}

Project.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(observer(Project));
