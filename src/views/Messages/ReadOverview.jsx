import React from 'react';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import ResourceOverview from 'components/ResourceOverview.jsx';
import { withTranslation } from 'react-i18next';
import { useParams } from 'react-router';
import ListRow from 'components/ListRow';
import moment from 'moment';
import { Check, OpenInNew } from '@material-ui/icons';
import { useEffect } from 'react';
import Loader from 'components/Loader';

function ReadOverview({ t }) {
    const { messageId } = useParams();
    const { users, projects, messages } = useStore();

    function rowRenderer({ key, index, style }) {
        const user = users.resource.filtered[index];
        return (
            <ListRow key={key}>
                <span style={{ width: '33%' }}>
                    {user.lastName} {user.firstName}
                </span>
                <span style={{ width: '33%' }}>
                    <OpenInNew /> {moment(user.openedDt).format('DD/MM/YYYY HH:mm')}
                </span>
                <span style={{ width: '33%' }}>
                    <Check />{' '}
                    {user.readDt ? moment(user.readDt).format('DD/MM/YYYY HH:mm') : <b>Niet gemarkeerd als gelezen</b>}
                </span>
            </ListRow>
        );
    }

    useEffect(() => {
        messages.getMessage(messageId);
    }, [messageId]);

    if (!messages.resource.item) {
        return <Loader />;
    }

    return (
        <>
            <ResourceOverview
                backend
                title={`Leesrapporten van bericht: ${messages.resource.item.title}`}
                apiPrefix={`/projects/${projects.id}/messages/${messageId}`}
                store={users.resource}
                rowRenderer={rowRenderer}
                heightOffset={300}
            />
        </>
    );
}

export default withTranslation()(observer(ReadOverview));
