import React from 'react';
import parse from 'html-react-parser';
import { useParams } from 'react-router';
import { useEffect } from 'react';
import { apiUrl, useStore } from '../../mobx';
import Loader from 'components/Loader';
import { observer } from 'mobx-react-lite';
import { CloudDownload, Visibility } from '@material-ui/icons';
import { withTranslation } from 'react-i18next';
import Button from '../../components/CustomButtons/Button.jsx';
import { NotificationManager } from 'react-notifications';

function Message({ t }) {
    const { account, messages } = useStore();
    const { id } = useParams();

    useEffect(() => {
        messages.getMessage(id);
        messages.openedByUser(account.id, id);

        return () => {
            delete messages.resource.item;
        };
    }, []);

    if (!messages.resource.item) {
        return <Loader />;
    }

    const { item: message } = messages.resource;

    return (
        <div className="message">
            <div>
                <h1>{message.title}</h1>
                <div className="card" style={{ maxHeight: 'calc(100vh - 200px)', overflowY: 'auto' }}>
                    <div>{parse(message.description || '')}</div>
                </div>
                {!message.read && (
                    <Button
                        onClick={async () => {
                            await messages.markRead(account.id, message.id);
                            NotificationManager.success('Bericht werd gemarkeerd als gelezen');
                            await messages.getMessage(message.id);
                        }}
                        color="primary"
                        size="sm"
                    >
                        <Visibility /> {t('form.button.notifications.markRead')}
                    </Button>
                )}
            </div>
            <div>
                {message.files?.length > 0 && (
                    <>
                        <h1>Bestanden</h1>
                        {message.files.map(f => (
                            <div className="card">
                                <div className="flex">
                                    <span>{f.fileUrl}</span>
                                    <CloudDownload
                                        style={{ cursor: 'pointer', marginRight: 20 }}
                                        onClick={() => {
                                            window.open(`${apiUrl}/api/download?file=${f.fileUrl}`);
                                        }}
                                    />
                                </div>
                            </div>
                        ))}
                    </>
                )}
            </div>
        </div>
    );
}

export default withTranslation()(observer(Message));
