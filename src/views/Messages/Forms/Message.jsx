import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewMessage(props) {
    const { messages, projects } = useStore();

    useEffect(() => {
        return () => {
            messages.resource.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            apiPrefix={`/projects/${projects.id}`}
            store={messages.resource}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                messages.resource.data = [];
                messages.resource.status = [];
                messages.resource.fetch(`/projects/${projects.id}`);
            }}
        />
    );
}

NewMessage.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewMessage);
