import React from 'react';
import { DateRange, Delete, Message, Notifications, PlaylistAddCheck, RemoveRedEye } from '@material-ui/icons';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { Checkbox, Tooltip } from '@material-ui/core';
import ResourceOverview from 'components/ResourceOverview.jsx';
import ListRow from 'components/ListRow.jsx';
import { withTranslation } from 'react-i18next';
import { useCallback } from 'react';
import MessageForm from './Forms/Message';
import ListRowActions from 'components/ListRowActions';
import moment from 'moment';
import Inputs from 'components/Inputs';
import { useHistory } from 'react-router';

const ObservedCheckBox = observer(({ id }) => {
    const { notifications } = useStore();

    return (
        <div style={{ padding: '6px 0' }}>
            <Checkbox
                checked={notifications.resource.checked.includes(id)}
                onChange={e => {
                    if (!notifications.resource.checked.includes(id)) {
                        notifications.resource.checked.push(id);
                    } else {
                        notifications.resource.checked.splice(notifications.resource.checked.indexOf(id), 1);
                    }
                }}
            />
        </div>
    );
});

function MessagesOverview({ t }) {
    const history = useHistory();
    const { account, general, messages, projects } = useStore();

    function rowRenderer({ key, style, index, checkable }) {
        const message = messages.resource.filtered[index];

        return (
            <li key={key} style={{ ...style, display: 'flex' }}>
                {checkable && <ObservedCheckBox id={message.id} />}
                <ListRow style={{ flexGrow: 1 }}>
                    <span style={{ width: 400 }}>
                        <Message /> {message.title}
                    </span>
                    <span style={{ width: 400 }}>
                        <DateRange /> {moment(message.creationDt).format('DD/MM/YYYY HH:mm')}
                    </span>
                </ListRow>
                <ListRowActions>
                    {account.rights.canReadMessages && (
                        <Tooltip title={t('tooltip.message.read')} placement="top">
                            <RemoveRedEye
                                onClick={() => {
                                    history.push(`/berichten/${message.id}`);
                                }}
                            />
                        </Tooltip>
                    )}
                    {account.rights.canReadMessagesRead && (
                        <Tooltip title={t('tooltip.message.reports')} placement="top">
                            <PlaylistAddCheck
                                onClick={() => {
                                    history.push(`/projecten/${projects.id}/berichten/${message.id}/gelezen`);
                                }}
                            />
                        </Tooltip>
                    )}
                    {account.rights.canDeleteMessages && (
                        <Tooltip title={t('tooltip.message.delete')} placement="top">
                            <Delete
                                onClick={() => {
                                    general.addModal({
                                        title: `Bericht verwijderen`,
                                        body: 'Bent u zeker dat u dit bericht wilt verwijderen?',
                                        buttonText: 'Verwijder',
                                        onConfirm: async () => {
                                            await messages.resource.delete(undefined, message.id);
                                            await messages.resource.fetch(`/projects/${projects.id}`);
                                        },
                                    });
                                }}
                            />
                        </Tooltip>
                    )}
                </ListRowActions>
            </li>
        );
    }

    const onCreateMessage = useCallback(() => {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.message.new'),
            onMinimize: modal => {
                messages.resource.form.data.name &&
                    (modal.title = `${t('modal.title.message.new')}: ${messages.resource.form.data.name}`);
                return messages.resource.form.data;
            },
            onMaximize: modal => {
                messages.resource.form.data = { ...modal.data };
            },
            body: <MessageForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }, []);

    return (
        <>
            <ResourceOverview
                checkable
                backend
                apiPrefix={window.location.pathname.includes('projecten') ? `/projects/${projects.id}` : ''}
                store={messages.resource}
                rowRenderer={rowRenderer}
                heightOffset={!window.location.pathname.includes('projecten') ? 270 : 430}
                actions={[
                    //TODO: check rights and remove level property
                    account.rights.canCreateMessages && {
                        icon: 'add',
                        onClick: onCreateMessage,
                        tooltip: t('tooltip.message.create'),
                    },
                ]}
                filters={
                    !window.location.pathname.includes('projecten') ? (
                        <>
                            <Inputs.messages.filters.project />
                            {/* <Inputs.messages.filters.read /> */}
                        </>
                    ) : null
                }
            />
        </>
    );
}

export default withTranslation()(observer(MessagesOverview));
