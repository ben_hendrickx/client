import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewCast(props) {
    const { casts } = useStore();

    useEffect(() => {
        return () => {
            casts.resource.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            store={casts.resource}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                casts.resource.data = [];
                casts.resource.status = [];
                casts.resource.fetch();
            }}
        />
    );
}

NewCast.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewCast);
