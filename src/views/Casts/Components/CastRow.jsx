import React from 'react';
import ListRow from 'components/ListRow';
import { Delete, Edit } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { useStore } from '../../../mobx';
import CastForm from '../Forms/Cast';
import { withTranslation } from 'react-i18next';

function Cast(props) {
    const { cast, style, key, t } = props;
    const { name } = cast;
    const { account, casts, general } = useStore();

    return (
        <li key={key} style={style}>
            <ListRow key={key}>
                <span>{name}</span>
                {(account.rights.canUpdateCasts || account.rights.canCreateCasts || account.rights.canDeleteCasts) && (
                    <ListRowActions>
                        {account.rights.canUpdateCasts && (
                            <Tooltip title={t('tooltip.cast.edit')} placement="top">
                                <Edit
                                    onClick={() => {
                                        let modal;
                                        const closeModal = () => modal.close();
                                        casts.resource.form.data = { ...cast };
                                        modal = general.addModal({
                                            type: 'window',
                                            width: 1100,
                                            title: t('modal.title.cast.edit'),
                                            onMinimize: async modal => {
                                                casts.resource.form.data.name &&
                                                    (modal.title = `${t('modal.title.cast.edit')}: ${
                                                        casts.resource.form.data.name
                                                    }`);
                                                return casts.resource.form.data;
                                            },
                                            onMaximize: modal => {
                                                casts.resource.form.data = { ...modal.data };
                                            },
                                            body: <CastForm onCancel={closeModal} onConfirm={closeModal} />,
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                        {account.rights.canDeleteCasts && (
                            <Tooltip title={t('modal.title.cast.delete')} placement="top">
                                <Delete
                                    onClick={() => {
                                        general.addModal({
                                            title: `${t('modal.title.cast.delete')}: ${cast.name}`,
                                            body: t('modal.body.cast.delete'),
                                            buttonText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await casts.resource.delete(undefined, cast.id);
                                                casts.resource.data = [];
                                                casts.resource.status = [];
                                                casts.resource.fetch();
                                            },
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                    </ListRowActions>
                )}
            </ListRow>
        </li>
    );
}

export default withTranslation()(Cast);
