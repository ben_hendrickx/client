import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Cast from './Components/CastRow';
import CastForm from './Forms/Cast';
import { withTranslation } from 'react-i18next';

function CastsOverview({ t }) {
    const { ui, account, casts, general } = useStore();

    function rowRenderer({ key, index, style }) {
        const cast = casts.resource.filtered[index];

        return <Cast cast={cast} style={style} key={key} />;
    }

    function onCreateCast() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.cast.new'),
            onMinimize: modal => {
                casts.resource.form.data.name &&
                    (modal.title = `${t('modal.title.cast.new')}: ${casts.resource.form.data.name}`);
                return casts.resource.form.data;
            },
            onMaximize: modal => {
                casts.resource.form.data = { ...modal.data };
            },
            body: <CastForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <ResourceOverview
            store={casts.resource}
            rowRenderer={rowRenderer}
            rowHeight={ui.isMobile ? 100 : 60}
            heightOffset={250}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateCasts && {
                    icon: 'add',
                    onClick: onCreateCast,
                    tooltip: t('tooltip.cast.create'),
                },
            ]}
        />
    );
}

export default withTranslation()(observer(CastsOverview));
