import React from 'react';
import { withStyles } from '@material-ui/styles';
import Role from 'components/Role';
import { Checkbox } from '@material-ui/core';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';

const sceneRoleSelectorStyle = {
    row: {
        display: 'flex',
        justifyContent: 'space-between',
        textAlign: 'center',
        padding: '0 10px',
        '& h2': {
            fontSize: '1em !important',
            margin: '0 !important',
            textAlign: 'justify',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            alignItems: 'center',
            display: 'flex',
            '& svg': {
                marginRight: 10,
            },
        },
        '& > div': {
            borderRight: '1px solid #ddd',
            textAlign: 'center',
            flexGrow: 1,
        },
        '& > div:first-child': {
            width: 200,
        },
        '& > div:last-child': {
            border: 0,
        },
    },
    container: {
        background: 'rgb(250, 250, 250)',
        borderBottom: '2px solid rgb(221, 221, 221)',
        borderTop: '2px solid rgb(221, 221, 221)',
    },
};

function SceneRoleSelector(props) {
    const { classes, open, roles = [] } = props;
    const { selector } = useStore();

    const { checkedSceneRoles = [], casts = [], checkedScenes = [] } = selector;

    // useEffect(() => {
    //     setRoles(props.roles);
    // }, [roles, props.roles]);

    const Row = props => (
        <div style={props.style || {}} className={classes.row}>
            {props.children}
        </div>
    );
    const Column = props => (
        <div style={props.style || {}} className={classes.column}>
            {props.children}
        </div>
    );

    if (!open) {
        return null;
    }
    return (
        <div className={classes.container}>
            {roles.length > 0 && (
                <Row style={{ background: '#ddd' }}>
                    <h2>
                        <b>Rollen</b>
                    </h2>
                </Row>
            )}
            {roles.length > 0 &&
                roles.map((role, roleIndex) => {
                    const sceneChecked =
                        checkedScenes.filter(checkedScene => checkedScene.sceneId === props.sceneId).length ===
                        casts.length;

                    const partiallyChecked = sceneChecked || checkedSceneRoles.some(checkedSceneRole => checkedSceneRole.sceneId === props.sceneId && checkedSceneRole.roleId === role.id);

                    return (
                        <Row style={partiallyChecked ? { background: 'rgba(247, 189, 69, .3)' } : {}}>
                            <Column style={{ display: 'flex', marginLeft: 20, textAlign: 'left' }}>
                                <Role role={role} identifier icon={false} />
                                <Checkbox
                                    className={`role-${roleIndex}`}
                                    checked={
                                        sceneChecked ||
                                        checkedSceneRoles.filter(
                                            checkedSceneRole =>
                                                checkedSceneRole.sceneId === props.sceneId &&
                                                checkedSceneRole.roleId === role.id
                                        ).length === casts.length
                                    }
                                    onChange={e => {
                                        selector.calculate({
                                            checked: e.target.checked,
                                            sceneId: props.sceneId,
                                            roleId: role.id,
                                        });
                                    }}
                                />
                            </Column>
                            {role.casts.map((cast, castIndex) => {
                                const castChecked = checkedScenes.some(
                                    checkedScene =>
                                        checkedScene.sceneId === props.sceneId && checkedScene.castId === cast.id
                                );

                                return (
                                    <div
                                        style={{ maxWidth: 150, minWidth: 150 }}
                                        key={`role-${roleIndex}-cast-${castIndex}`}
                                    >
                                        <Column>
                                            <Checkbox
                                                className={`role-${roleIndex}-cast-${castIndex}`}
                                                checked={
                                                    castChecked ||
                                                    checkedSceneRoles.some(
                                                        checkedSceneRole =>
                                                            checkedSceneRole.castId === cast.id &&
                                                            checkedSceneRole.roleId === role.id &&
                                                            checkedSceneRole.sceneId === props.sceneId
                                                    )
                                                }
                                                onChange={e => {
                                                    selector.calculate({
                                                        checked: e.target.checked,
                                                        sceneId: props.sceneId,
                                                        roleId: role.id,
                                                        castId: cast.id,
                                                    });
                                                }}
                                            />
                                        </Column>
                                    </div>
                                );
                            })}
                            <div
                                style={{
                                    maxWidth: 40,
                                    paddingTop: 10,
                                }}
                            ></div>
                        </Row>
                    );
                })}
        </div>
    );
}

export default withStyles(sceneRoleSelectorStyle)(observer(SceneRoleSelector));
