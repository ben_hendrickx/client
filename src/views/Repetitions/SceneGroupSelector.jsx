import React from 'react';
import { withStyles } from '@material-ui/styles';
import { Checkbox } from '@material-ui/core';
import Group from 'components/Group';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';

const sceneGroupSelectorStyle = {
    row: {
        display: 'flex',
        justifyContent: 'space-between',
        textAlign: 'center',
        padding: '0 10px',
        '& h2': {
            fontSize: '1em !important',
            margin: '0 !important',
            textAlign: 'justify',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            alignItems: 'center',
            display: 'flex',
            '& svg': {
                marginRight: 10,
            },
        },
        '& > div': {
            borderRight: '1px solid #ddd',
            textAlign: 'center',
            flexGrow: 1,
        },
        '& > div:first-child': {
            width: 200,
        },
        '& > div:last-child': {
            border: 0,
        },
    },
    container: {
        background: 'rgb(250, 250, 250)',
        borderBottom: '2px solid rgb(221, 221, 221)',
        borderTop: '2px solid rgb(221, 221, 221)',
    },
};

function SceneGroupSelector(props) {
    const { classes, open, groups = [] } = props;
    const { selector } = useStore();

    const { checkedSceneGroups = [], casts = [], checkedScenes = [] } = selector;

    const Row = props => (
        <div key={props.key} style={props.style || {}} className={classes.row}>
            {props.children}
        </div>
    );
    const Column = props => (
        <div style={props.style || {}} className={classes.column}>
            {props.children}
        </div>
    );

    if (!open) {
        return null;
    }
    return (
        <div className={classes.container}>
            {groups.length > 0 && (
                <Row style={{ background: '#ddd' }}>
                    <h2>
                        <b>Groepen</b>
                    </h2>
                </Row>
            )}
            {groups.length > 0 &&
                groups.map((group, groupIndex) => {
                    const sceneChecked =
                        checkedScenes.filter(checkedScene => checkedScene.sceneId === props.sceneId).length ===
                        casts.length;

                    const partiallyChecked = sceneChecked || checkedSceneGroups.some(checkedSceneGroup => checkedSceneGroup.sceneId === props.sceneId && checkedSceneGroup.groupId === group.id);

                    return (
                        <Row key={`group-${groupIndex}`} style={partiallyChecked ? { background: 'rgba(247, 189, 69, .3)' } : {}}>
                            <Column style={{ display: 'flex', paddingLeft: 20, textAlign: 'left' }}>
                                <Group group={group} identifier icon={false} />
                                <Checkbox
                                    className={`group-${groupIndex}`}
                                    checked={
                                        sceneChecked ||
                                        checkedSceneGroups.filter(
                                            checkedSceneGroup =>
                                                checkedSceneGroup.sceneId === props.sceneId &&
                                                checkedSceneGroup.groupId === group.id
                                        ).length === casts.length
                                    }
                                    onChange={e => {
                                        selector.calculate({
                                            checked: e.target.checked,
                                            sceneId: props.sceneId,
                                            groupId: group.id,
                                        });
                                    }}
                                />
                            </Column>
                            {group.casts.map((cast, castIndex) => {
                                const castChecked = checkedScenes.some(
                                    checkedScene =>
                                        checkedScene.sceneId === props.sceneId && checkedScene.castId === cast.id
                                );
                                return (
                                    <div
                                        style={{ maxWidth: 150, minWidth: 150 }}
                                        key={`group-${groupIndex}-cast-${castIndex}`}
                                    >
                                        <Column>
                                            <Checkbox
                                            className={`group-${groupIndex}-cast-${castIndex}`}
                                                checked={
                                                    castChecked ||
                                                    checkedSceneGroups.some(
                                                        checkedSceneGroup =>
                                                            checkedSceneGroup.castId === cast.id &&
                                                            checkedSceneGroup.groupId === group.id &&
                                                            checkedSceneGroup.sceneId === props.sceneId
                                                    )
                                                }
                                                onChange={e => {
                                                    selector.calculate({
                                                        checked: e.target.checked,
                                                        sceneId: props.sceneId,
                                                        groupId: group.id,
                                                        castId: cast.id,
                                                    });
                                                }}
                                            />
                                        </Column>
                                    </div>
                                );
                            })}
                            <div
                                style={{
                                    maxWidth: 40,
                                    paddingTop: 10,
                                }}
                            ></div>
                        </Row>
                    );
                })}
        </div>
    );
}

export default withStyles(sceneGroupSelectorStyle)(observer(SceneGroupSelector));
