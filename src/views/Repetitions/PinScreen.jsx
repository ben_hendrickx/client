import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { withStyles } from '@material-ui/styles';
import { useStore } from '../../mobx';
import useReactRouter from 'use-react-router';
import CustomInput from 'components/CustomInput/CustomInput';
import Button from '../../components/CustomButtons/Button.jsx';

const pinScreenStyle = {
    pinscreen: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        '& main': {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            background: '#fff',
            borderRadius: 5,
            padding: 60,
            maxWidth: 550,
        },
    },
};

function PinScreen(props) {
    const { classes } = props;
    const { repetitions } = useStore();
    const { match, history } = useReactRouter();

    useEffect(() => {
        if (match && match.params && match.params.repetitionId) {
            repetitions.fetchRepetition(match.params.repetitionId);
        }

        function handler(e) {
            if (e.code && e.code.toLowerCase().includes('enter')) {
                repetitions.enterPin(
                    match.params.repetitionId,
                    undefined,
                    history.location.search.replace('?repetitionIds=', '').split(',')
                );
            }
        }
        window.addEventListener('keyup', handler);

        return () => {
            window.removeEventListener('keyup', handler);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (repetitions.success) {
            setTimeout(() => {
                repetitions.success = null;
                repetitions.pin = '';
            }, parseInt(process.env.REACT_APP_PIN_DURATION));
        }

        if (repetitions.error) {
            setTimeout(() => {
                repetitions.error = null;
                repetitions.pin = '';
            }, parseInt(process.env.REACT_APP_PIN_DURATION));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [repetitions.success, repetitions.error]);

    if (repetitions.success) {
        return (
            <div className={classes.pinscreen}>
                <main>
                    <p style={{ fontSize: '2.3em' }}>{repetitions.success}</p>
                </main>
            </div>
        );
    }

    if (repetitions.error) {
        return (
            <div className={classes.pinscreen}>
                <main>
                    <p style={{ fontSize: '2.3em', lineHeight: 1.5 }}>{repetitions.error}</p>
                </main>
            </div>
        );
    }

    return (
        <div className={classes.pinscreen}>
            <main>
                <p style={{ fontSize: '2.3em', fontWeight: 500 }}>Geef uw pincode in:</p>
                <p>
                    Uw unieke pincode bestaat uit 5 cijfers.
                    <br />
                    Deze kan je steeds terugvinden op uw profiel op fanvandimpna.be
                    <br />
                    Uw regie-assistent(e) kan deze ook ten allen tijde raadplegen voor u
                </p>
                <div style={{ textAlign: 'center' }}>
                    <CustomInput
                        labelText="Pincode"
                        id="cast"
                        formControlProps={{
                            required: true,
                            fullWidth: true,
                        }}
                        inputProps={{
                            inputProps: { style: { fontSize: '5em' } },
                            autoFocus: true,
                            color: 'primary',
                            type: 'text',
                            autoComplete: 'off',
                            value: repetitions.pin,
                            onChange: e => (repetitions.pin = e.target.value),
                        }}
                    />
                </div>
                <Button
                    onClick={async () => {
                        await repetitions.enterPin(
                            match.params.repetitionId,
                            undefined,
                            history.location.search.replace('?repetitionIds=', '').split(',')
                        );
                    }}
                    color="primary"
                    size="md"
                >
                    Bevestig pincode
                </Button>
            </main>
        </div>
    );
}

export default withStyles(pinScreenStyle)(observer(PinScreen));
