import React, { useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowDropUp, ArrowDropDown } from '@material-ui/icons';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { primaryCardHeader } from '../../assets/jss/material-kit-react.jsx';
import Checkbox from '@material-ui/core/Checkbox';
import Role from 'components/Role';
import { withTranslation } from 'react-i18next';

const userSelectorStyle = {
    selector: {
        marginBottom: 30,
        '& > h2': {
            ...primaryCardHeader,
            padding: '10px 20px',
            marginBottom: 0,
            position: 'relative',
        },
    },
    toggle: {
        position: 'absolute',
        right: 20,
        top: 10,
        padding: '5px 10px',
        borderRadius: '50%',
        height: 40,
        '&:hover': {
            background: 'rgba(0,0,0,.1)',
            cursor: 'pointer',
        },
    },
    row: {
        display: 'flex',
        justifyContent: 'space-between',
        textAlign: 'center',
        padding: '0 10px',
        '& h2': {
            fontSize: '1em !important',
            margin: '0 !important',
            textAlign: 'justify',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            alignItems: 'center',
            display: 'flex',
            '& svg': {
                marginRight: 10,
            },
        },
        '& > div': {
            borderRight: '1px solid #ddd',
            textAlign: 'center',
            flexGrow: 1,
        },
        '& > div:first-child': {
            width: 200,
        },
        '& > div:last-child': {
            border: 0,
        },
    },
    column: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
};

function RolesSelector(props) {
    const { classes, t } = props;
    const { selector } = useStore();
    const [rolesOpen, setRolesOpen] = useState(false);

    const View = props => {
        return (
            <div className={classes.selector}>
                <h2>
                    <span>{props.title}</span>
                    <span className={classes.toggle} onClick={() => props.onToggle(!props.open)}>
                        {props.open ? <ArrowDropUp /> : <ArrowDropDown className="open" />}
                    </span>
                </h2>
                {props.open && <div className={classes.content}>{props.children}</div>}
            </div>
        );
    };

    const Row = props => (
        <div className={classes.row} style={props.style || {}}>
            {props.children}
        </div>
    );
    const Column = props => (
        <div style={props.style || {}} className={classes.column}>
            {props.children}
        </div>
    );

    const { roles = [], checkedRoles = [], casts = [] } = selector;

    const rolesSelected = checkedRoles.reduce((acc, checkedRole) => {
        !acc.includes(checkedRole.roleId) && acc.push(checkedRole.roleId);
        return acc;
    }, []);

    return (
        <>
            <View
                title={`${t('resource.rollen')} (${rolesSelected.length} ${t('form.labels.selected').toLowerCase()})`}
                open={rolesOpen}
                onToggle={open => setRolesOpen(open)}
            >
                <div style={{ border: '1px solid #ddd', borderTop: 0 }}>
                    <div className={classes.row} style={{ background: '#ddd' }}>
                        <div style={{ textAlign: 'left' }}>{t('form.labels.roles.name')}</div>
                        {casts.map((cast, i) => (
                            <Column
                                style={{ maxWidth: 150, minWidth: 150, display: 'inline-block' }}
                                key={`role-cast-${i}`}
                            >
                                {cast.name}
                            </Column>
                        ))}
                    </div>
                    {roles.map((role, roleIndex) => {
                        const checked = checkedRoles.filter(checkedRole => checkedRole.roleId === role.id).length;
                        return (
                            <Row
                                key={`role-${roleIndex}`}
                                style={{ background: checked ? 'rgba(247, 189, 69, .3)' : 'white' }}
                            >
                                <Column style={{ paddingLeft: 20 }}>
                                    <Role role={role} identifier icon={false} description />
                                    <Checkbox
                                        className={`role-${roleIndex}`}
                                        checked={checked === casts.length}
                                        onChange={e => {
                                            const otherRoles = checkedRoles.filter(
                                                checkedRole => checkedRole.roleId !== role.id
                                            );
                                            if (e.target.checked) {
                                                role.casts.forEach(cast =>
                                                    otherRoles.push({ roleId: role.id, castId: cast.id })
                                                );
                                            }
                                            selector.checkedRoles = otherRoles;
                                        }}
                                    />
                                </Column>
                                {role.casts.map((cast, castIndex) => (
                                    <div
                                        style={{ maxWidth: 150, minWidth: 150 }}
                                        key={`role-${roleIndex}-cast-${castIndex}`}
                                    >
                                        <Column style={{ display: 'inline-block' }}>
                                            <Checkbox
                                                checked={checkedRoles.some(
                                                    checkedRole =>
                                                        checkedRole.roleId === role.id && checkedRole.castId === cast.id
                                                )}
                                                onChange={e => {
                                                    if (e.target.checked) {
                                                        checkedRoles.push({ roleId: role.id, castId: cast.id });
                                                    } else {
                                                        selector.checkedRoles = checkedRoles.filter(
                                                            checkedRole =>
                                                                checkedRole.castId !== cast.id ||
                                                                checkedRole.roleId !== role.id
                                                        );
                                                    }
                                                }}
                                            />
                                        </Column>
                                    </div>
                                ))}
                            </Row>
                        );
                    })}
                </div>
            </View>
        </>
    );
}

RolesSelector.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withTranslation()(withStyles(userSelectorStyle)(observer(RolesSelector)));
