import React, { useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowDropUp, ArrowDropDown } from '@material-ui/icons';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { primaryCardHeader } from '../../assets/jss/material-kit-react.jsx';
import Checkbox from '@material-ui/core/Checkbox';
import { withTranslation } from 'react-i18next';

const userSelectorStyle = {
    selector: {
        marginBottom: 30,
        '& > h2': {
            ...primaryCardHeader,
            padding: '10px 20px',
            marginBottom: 0,
            position: 'relative',
        },
    },
    toggle: {
        position: 'absolute',
        right: 20,
        top: 10,
        padding: '5px 10px',
        borderRadius: '50%',
        height: 40,
        '&:hover': {
            background: 'rgba(0,0,0,.1)',
            cursor: 'pointer',
        },
    },
    row: {
        display: 'flex',
        justifyContent: 'space-between',
        textAlign: 'center',
        padding: '0 10px',
        '& h2': {
            fontSize: '1em !important',
            margin: '0 !important',
            textAlign: 'justify',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            alignItems: 'center',
            display: 'flex',
            '& svg': {
                marginRight: 10,
            },
        },
        '& > div': {
            borderRight: '1px solid #ddd',
            textAlign: 'center',
            flexGrow: 1,
        },
        '& > div:first-child': {
            width: 200,
        },
        '& > div:last-child': {
            border: 0,
        },
    },
    column: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
};

function UserSelector(props) {
    const { classes, t } = props;
    const { selector } = useStore();
    const [usersOpen, setUsersOpen] = useState(false);

    const View = props => {
        return (
            <div className={classes.selector}>
                <h2>
                    <span>{props.title}</span>
                    <span className={classes.toggle} onClick={() => props.onToggle(!props.open)}>
                        {props.open ? <ArrowDropUp /> : <ArrowDropDown className="open" />}
                    </span>
                </h2>
                {props.open && <div className={classes.content}>{props.children}</div>}
            </div>
        );
    };

    const Row = props => (
        <div className={classes.row} style={props.style || {}}>
            {props.children}
        </div>
    );
    const Column = props => (
        <div style={props.style || {}} className={classes.column}>
            {props.children}
        </div>
    );

    const { users = [], checkedUsers = [] } = selector;

    return (
        <>
            <View
                title={`${t('resource.medewerkers')} (${checkedUsers.length} ${t(
                    'form.labels.selected'
                ).toLowerCase()})`}
                open={usersOpen}
                onToggle={open => setUsersOpen(open)}
            >
                <div style={{ border: '1px solid #ddd', borderTop: 0 }}>
                    <div className={classes.row} style={{ background: '#ddd' }}>
                        <div style={{ textAlign: 'left' }}>
                            {t('form.labels.name')} {t('resource.medewerker').toLowerCase()}
                        </div>
                    </div>
                    {users.map((user, i) => (
                        <Row
                            key={`user-${i}`}
                            style={checkedUsers.includes(user.id) ? { background: 'rgba(247, 189, 69, 0.3)' } : {}}
                        >
                            <Column>
                                {user.lastName} {user.firstName}
                            </Column>
                            <Column style={{ maxWidth: 35 }}>
                                <Checkbox
                                    className={`user-${i}`}
                                    checked={checkedUsers.includes(user.id)}
                                    onChange={e => {
                                        if (e.target.checked) {
                                            checkedUsers.push(user.id);
                                        } else {
                                            const index = checkedUsers.indexOf(user.id);
                                            !!(index > -1) && checkedUsers.splice(index, 1);
                                        }
                                    }}
                                />
                            </Column>
                        </Row>
                    ))}
                </div>
            </View>
        </>
    );
}

UserSelector.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withTranslation()(withStyles(userSelectorStyle)(observer(UserSelector)));
