import React, { useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowDropUp, ArrowDropDown } from '@material-ui/icons';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { primaryCardHeader } from '../../assets/jss/material-kit-react.jsx';
import Checkbox from '@material-ui/core/Checkbox';
import Scene from 'components/Scene';
import SceneRoleSelector from './SceneRoleSelector';
import SceneGroupSelector from './SceneGroupSelector';
import { withTranslation } from 'react-i18next';

const userSelectorStyle = {
    selector: {
        marginBottom: 30,
        '& > h2': {
            ...primaryCardHeader,
            padding: '10px 20px',
            marginBottom: 0,
            position: 'relative',
        },
    },
    toggle: {
        position: 'absolute',
        right: 20,
        top: 10,
        padding: '5px 10px',
        borderRadius: '50%',
        height: 40,
        '&:hover': {
            background: 'rgba(0,0,0,.1)',
            cursor: 'pointer',
        },
    },
    row: {
        display: 'flex',
        justifyContent: 'space-between',
        textAlign: 'center',
        padding: '0 10px',
        '& h2': {
            fontSize: '1em !important',
            margin: '0 !important',
            textAlign: 'justify',
            textOverflow: 'ellipsis',
            overflow: 'hidden',
            whiteSpace: 'nowrap',
            alignItems: 'center',
            display: 'flex',
            '& svg': {
                marginRight: 10,
            },
        },
        '& > div': {
            borderRight: '1px solid #ddd',
            textAlign: 'center',
            flexGrow: 1,
        },
        '& > div:first-child': {
            width: 200,
        },
        '& > div:last-child': {
            border: 0,
        },
    },
    column: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
};

function SceneSelector(props) {
    const { classes, t } = props;
    const { selector } = useStore();
    const [scenesOpen, setScenesOpen] = useState(false);

    const View = props => {
        return (
            <div className={classes.selector}>
                <h2>
                    <span>{props.title}</span>
                    <span className={classes.toggle} onClick={() => props.onToggle(!props.open)}>
                        {props.open ? <ArrowDropUp /> : <ArrowDropDown className="open" />}
                    </span>
                </h2>
                {props.open && <div className={classes.content}>{props.children}</div>}
            </div>
        );
    };

    const Row = props => (
        <div key={props.key} style={props.style || {}} className={classes.row}>
            {props.children}
        </div>
    );
    const Column = props => (
        <div style={props.style || {}} className={classes.column}>
            {props.children}
        </div>
    );

    const { scenes = [], checkedScenes = [], casts = [], checkedSceneRoles = [], checkedSceneGroups = [] } = selector;

    return (
        <>
            <View
                title={`${t('resource.scènes')} (${t('resource.scènes')}: ${selector.uniqueCheckedScenes} | ${t(
                    'resource.groepen'
                )}: ${selector.uniqueCheckedSceneGroups} | ${t('resource.rollen')}: ${
                    selector.uniqueCheckedSceneRoles
                } | ${t('resource.casts')}: ${selector.uniqueCheckedCasts})`}
                open={scenesOpen}
                onToggle={open => setScenesOpen(open)}
            >
                <div style={{ border: '1px solid #ddd', borderTop: 0 }}>
                    <div className={classes.row} style={{ background: '#ddd' }}>
                        <div style={{ textAlign: 'left' }}>Scène-naam</div>
                        {casts.map((cast, i) => (
                            <div style={{ maxWidth: 150, minWidth: 150 }} key={`group-cast-${i}`}>
                                {cast.name}
                            </div>
                        ))}
                        <div
                            style={{
                                maxWidth: 40,
                                paddingTop: 10,
                            }}
                        ></div>
                    </div>
                    {scenes.map((scene, sceneIndex) => {
                        const sceneGroups = selector.getGroupsForScene(scene.id);
                        const sceneRoles = selector.getProjectRolesForScene(scene.id);

                        const partiallyChecked = [...checkedSceneRoles, ...checkedSceneGroups, ...checkedScenes].some(
                            checkedScene => checkedScene.sceneId === scene.id
                        );

                        return (
                            <>
                                <Row
                                    key={`scene-${sceneIndex}`}
                                    style={partiallyChecked ? { background: 'rgba(247, 189, 69, .3)' } : {}}
                                >
                                    <Column>
                                        <Scene scene={scene} identifier icon={false} />
                                        <Checkbox
                                            className={`scene-${sceneIndex}`}
                                            checked={
                                                checkedScenes.filter(checkedScene => checkedScene.sceneId === scene.id)
                                                    .length === casts.length
                                            }
                                            onChange={e => {
                                                selector.calculate({
                                                    checked: e.target.checked,
                                                    sceneId: scene.id,
                                                });
                                            }}
                                        />
                                    </Column>
                                    {scene.casts.map((cast, castIndex) => (
                                        <div
                                            style={{ maxWidth: 150, minWidth: 150 }}
                                            key={`scene-${sceneIndex}-cast-${castIndex}`}
                                        >
                                            <Checkbox
                                                checked={checkedScenes.some(
                                                    checkedScene =>
                                                        checkedScene.sceneId === scene.id &&
                                                        checkedScene.castId === cast.id
                                                )}
                                                onChange={e => {
                                                    selector.calculate({
                                                        checked: e.target.checked,
                                                        sceneId: scene.id,
                                                        castId: cast.id,
                                                    });
                                                }}
                                            />
                                        </div>
                                    ))}
                                    <div
                                        onClick={() => {
                                            scene.open = !scene.open;
                                        }}
                                        style={{
                                            maxWidth: 40,
                                            paddingTop: 10,
                                            fontSize: 30,
                                            cursor: 'pointer',
                                        }}
                                    >
                                        {(sceneGroups.length > 0 || sceneRoles.length > 0) &&
                                            (scene.open ? (
                                                <ArrowDropUp className="toggle" />
                                            ) : (
                                                <ArrowDropDown className="toggle" />
                                            ))}
                                    </div>
                                </Row>
                                <SceneGroupSelector groups={sceneGroups} sceneId={scene.id} open={!!scene.open} />
                                <SceneRoleSelector roles={sceneRoles} sceneId={scene.id} open={!!scene.open} />
                            </>
                        );
                    })}
                </div>
            </View>
        </>
    );
}

SceneSelector.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withTranslation()(withStyles(userSelectorStyle)(observer(SceneSelector)));
