import React, { useEffect } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import myRolesStyle from './my.style.jsx';

import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';

import { FolderOpen, GetApp, PictureAsPdf } from '@material-ui/icons';
import { useHistory } from 'react-router';

import Page from 'components/Page.jsx';
import Loader from '../../components/Loader.jsx';
import TabView from 'components/TabView.jsx';
import Repetition from 'components/Repetitions/Repetition.jsx';
import { Success } from 'components/Notifications.jsx';

function Repetitions(props) {
    const { classes } = props;
    const { repetitions } = useStore();

    return (
        <>
            <Loader loading={repetitions.loading}>
                {repetitions.data && repetitions.filteredData.length === 0 && (
                    <h2>Deze gebruiker neemt nog geen deel aan repetities</h2>
                )}
                {repetitions.data && repetitions.filteredData.length > 0 && <h2>Repetities</h2>}
                <ul className={classes.projects}>
                    {repetitions.filteredData.map(repetition => (
                        <Repetition
                            canCancel={true}
                            repetition={repetition}
                            markPresent={bool => repetitions.markPresent(repetition.id, bool)}
                        />
                    ))}
                </ul>
            </Loader>
        </>
    );
}

function MyRoles(props) {
    const { repetitions, account, projects } = useStore();
    const history = useHistory();

    function fetchRepetitions() {
        console.log(account.id);
        repetitions.fetch(undefined, account.id).then(() => {
            const projects = repetitions.data
                .reduce((acc, repetition) => {
                    if (!acc.includes(repetition.project)) {
                        acc.push(repetition.project);
                    }
                    return acc;
                }, [])
                .sort((a, b) => a.id - b.id);

            if (
                projects.length &&
                !projects.some(({ name }) => history.location.pathname.includes(name.toLowerCase()))
            ) {
                history.replace(`/repetities/${projects[0].name.toLowerCase()}`);
            }
        });
    }

    useEffect(() => {
        account.id && fetchRepetitions();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.token]);

    useEffect(() => {
        console.log(history);
        if (history.action === 'POP' || history.action === 'PUSH') {
            account.id && fetchRepetitions();
        }

        return () => {
            repetitions.success = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [history.location]);

    const tabs = repetitions.filteredData.reduce((acc, role) => {
        if (!acc.find(project => project.label === role.project.name)) {
            acc.push({
                label: role.project.name,
                icon: <FolderOpen />,
                onClick: () => {
                    projects.id = role.project.id;
                    history.push(`/repetities/${role.project.name.toLowerCase()}`);
                },
            });
        }
        return acc;
    }, []);

    return (
        <Page title={`${account.firstName} ${account.lastName} - Mijn repetities`}>
            <Success message={repetitions.success} onClose={() => (repetitions.success = '')} />
            <Loader loading={repetitions.loading}>
                <TabView
                    tabs={tabs}
                    routes={[{ path: '/repetities/:name', component: withStyles(myRolesStyle)(observer(Repetitions)) }]}
                    actions={[
                        {
                            icon: <GetApp />,
                            href: `/api/repetitions/ical`,
                            tooltip: 'Download naar kalender',
                        },
                        {
                            icon: <PictureAsPdf />,
                            href: `/api/repetitions/pdf`,
                            tooltip: 'Download als PDF',
                        },
                    ]}
                />
                {repetitions.data && repetitions.filteredData.length === 0 && (
                    <h2>Je wordt in de nabije toekomst niet verwacht op een repetitie</h2>
                )}
            </Loader>
        </Page>
    );
}

export default withStyles(myRolesStyle)(observer(MyRoles));
