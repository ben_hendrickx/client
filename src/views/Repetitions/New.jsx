import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowBackIos, Save } from '@material-ui/icons';

import newProjectStyle from './new.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Toolbar from '../../components/Toolbar/Toolbar.jsx';
import Loader from '../../components/Loader.jsx';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import UserSelector from 'views/Repetitions/UserSelector.jsx';
import RolesSelector from 'views/Repetitions/RolesSelector.jsx';
import GroupSelector from 'views/Repetitions/GroupSelector.jsx';
import SceneSelector from './SceneSelector.jsx';
import { withTranslation } from 'react-i18next';
import moment from 'moment/moment.js';

function NewProject(props) {
    const { classes, t } = props;
    const [done, setDone] = useState(false);
    const { repetitions, ui, selector } = useStore();
    const { history, match } = useReactRouter();

    useEffect(() => {
        async function fetchData() {
            if (match?.params?.id) {
                await selector.fetchCasts(match.params.id);
                await Promise.all([
                    selector.fetchScenes(match.params.id),
                    selector.fetchSceneRoles(),
                    selector.fetchSceneGroups(),
                    selector.fetchGroups(match.params.id),
                    selector.fetchRoles(match.params.id),
                    selector.fetchUsers(match.params.id),
                    repetitions.fetchLocations(),
                    repetitions.fetchTypes(),
                ]);
            }

            if (match?.params?.id) {
                !match?.params?.repetitionId &&
                    (await repetitions.fetchSelectorData(match.params.id).then(() => (repetitions.loading = false)));
            }

            if (match?.params?.repetitionId) {
                await repetitions.fetchSelectorData(match.params.id, match.params.repetitionId).then(() =>
                    repetitions.fetchRepetition(match.params.repetitionId).then(repetition => {
                        repetition.allChecked && selector.calculate({ checked: true });
                    })
                );
            }

            setDone(true);
        }

        fetchData();

        return () => {
            repetitions.id = null;
            repetitions.name = '';
            repetitions.description = '';
            repetitions.identifier = '';
            repetitions.date = null;
            repetitions.start = '';
            repetitions.end = '';
            repetitions.register = false;
            repetitions.locations.value = '';
            repetitions.types.value = '';
            repetitions.selectorData = '';
            repetitions.allChecked = false;
            repetitions.castChecked = false;
            selector.checkedScenes = [];
            selector.checkedSceneRoles = [];
            selector.checkedSceneGroups = [];
            selector.checkedRoles = [];
            selector.checkedGroups = [];
            selector.checkedUsers = [];
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (!done) {
        return <Loader loading={true}></Loader>;
    }

    return (
        <div style={{ flexGrow: 1 }}>
            {ui.isMobile && (
                <div className={classes.header}>
                    <ArrowBackIos onClick={() => history.replace(`/projecten/${match.params.id}/repetities`)} />
                    <span>{repetitions.id ? t('tooltip.repetition.edit') : t('tooltip.repetition.new')}</span>
                </div>
            )}
            <Loader loading={!!match.params?.repetitionId && repetitions.loading}>
                <Toolbar title={repetitions.id ? t('tooltip.repetition.edit') : t('tooltip.repetition.new')} />
                <div className={classes.content}>
                    <div>
                        <CustomInput
                            type="select"
                            labelText={t('form.labels.repetition.type')}
                            formControlProps={{
                                className: classes.filter,
                                fullWidth: true,
                            }}
                            inputProps={{
                                useValue: true,
                                native: true,
                                value: repetitions.types.value,
                                onChange: e => (repetitions.types.value = e.target.value),
                                name: 'type',
                                options: repetitions.types.autoComplete,
                            }}
                        />
                        <CustomInput
                            labelText={t('form.labels.identifier')}
                            formControlProps={{
                                fullWidth: true,
                                error: repetitions.identifier?.length > 10,
                            }}
                            inputProps={{
                                name: 'volgnummer',
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: repetitions.identifier || '',
                                onChange: e => (repetitions.identifier = e.target.value),
                            }}
                        />
                        <CustomInput
                            labelText={t('form.labels.name')}
                            formControlProps={{
                                fullWidth: true,
                                error: repetitions.name?.length > 70,
                            }}
                            inputProps={{
                                name: 'naam',
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: repetitions.name,
                                onChange: e => (repetitions.name = e.target.value),
                            }}
                        />
                        <CustomInput
                            labelText={t('form.labels.description')}
                            formControlProps={{
                                fullWidth: true,
                                error: repetitions.description?.length > 255,
                            }}
                            inputProps={{
                                name: 'omschrijving',
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: repetitions.description,
                                onChange: e => (repetitions.description = e.target.value),
                            }}
                        />
                        <CustomInput
                            type="date"
                            labelText={t('form.labels.repetition.date')}
                            formControlProps={{
                                error: repetitions.date && !repetitions.date instanceof Date,
                                required: true,
                                fullWidth: true,
                            }}
                            inputProps={{
                                placeholder: 'dd/mm/jjjj',
                                name: 'datum_van_repetitie',
                                value: repetitions.date || null,
                                onChange: date => (repetitions.date = date),
                            }}
                        />
                        <CustomInput
                            type="time"
                            labelText={t('form.labels.repetition.startTime')}
                            formControlProps={{
                                fullWidth: true,
                            }}
                            inputProps={{
                                name: 'start-uur',
                                placeholder: 'uu:mm',
                                color: 'primary',
                                autoComplete: 'off',
                                value: repetitions.start || moment().startOf('day'),
                                onChange: d => {
                                    repetitions.start = d;
                                },
                            }}
                        />
                        <CustomInput
                            type="time"
                            labelText={t('form.labels.repetition.endTime')}
                            formControlProps={{
                                fullWidth: true,
                            }}
                            inputProps={{
                                name: 'eind-uur',
                                placeholder: 'uu:mm',
                                color: 'primary',
                                autoComplete: 'off',
                                value: repetitions.end || moment().startOf('day'),
                                onChange: d => (repetitions.end = d),
                            }}
                        />
                        <CustomInput
                            type="select"
                            labelText={t('form.labels.repetition.location')}
                            formControlProps={{
                                className: classes.filter,
                                fullWidth: true,
                            }}
                            inputProps={{
                                name: 'locatie',
                                useValue: true,
                                native: true,
                                value: repetitions.locations.value,
                                onChange: e => (repetitions.locations.value = e.target.value),
                                options: repetitions.locations.autoComplete,
                            }}
                        />
                        <div>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={!!repetitions.register}
                                        onChange={e => {
                                            repetitions.register = e.target.checked;
                                        }}
                                    />
                                }
                                label={t('form.labels.repetition.presence')}
                            />
                        </div>
                        <div>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={!!repetitions.notifications}
                                        onChange={e => {
                                            repetitions.notifications = e.target.checked;
                                        }}
                                    />
                                }
                                label={t('form.labels.repetition.notifications')}
                            />
                        </div>
                    </div>
                    <h2>{t('resource.deelnemers')}</h2>
                    <div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                            <div style={{ flexGrow: 0, border: 0 }}>
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <h2 style={{ margin: 0, fontSize: '1.2em' }}>
                                        {t('form.labels.repetition.everyone')}
                                    </h2>
                                    <Checkbox
                                        checked={!!selector.allChecked}
                                        onChange={e => {
                                            selector.calculate({ checked: e.target.checked });
                                        }}
                                    />
                                </div>
                            </div>
                            {selector.casts.map((cast, i) => (
                                <div style={{ flexGrow: 0, border: 0 }} key={`cast-${i}`}>
                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                        <h2 style={{ margin: 0, fontSize: '1.2em' }}>
                                            {t('form.labels.cast')} {cast.name}
                                        </h2>
                                        <Checkbox
                                            checked={
                                                selector.checkedScenes.filter(
                                                    checkedScene => checkedScene.castId === cast.id
                                                ).length === selector.scenes.length
                                            }
                                            onChange={e => {
                                                selector.calculate({
                                                    checked: e.target.checked,
                                                    castId: cast.id,
                                                });
                                            }}
                                        />
                                    </div>
                                </div>
                            ))}
                        </div>
                        <SceneSelector />
                        <GroupSelector />
                        <RolesSelector />
                        <UserSelector />
                    </div>
                    <Button
                        disabled={
                            !repetitions.identifier ||
                            repetitions.identifier?.length > 10 ||
                            !repetitions.name ||
                            repetitions.name?.length > 70 ||
                            !repetitions.date ||
                            repetitions.description?.length > 255 ||
                            !repetitions.start ||
                            !repetitions.end ||
                            !repetitions.locations.value ||
                            !repetitions.types.value
                        }
                        onClick={async () => {
                            await repetitions.save();
                            repetitions.name = '';
                            history.replace(`/projecten/${match.params.id}/repetities`);
                        }}
                        color="success"
                        size="md"
                    >
                        <Save style={{ marginRight: '5px' }} />
                        <span style={{ marginTop: '1px', marginLeft: '5px' }}>{t('form.button.save')}</span>
                    </Button>
                    <Button
                        onClick={() => {
                            repetitions.name = '';
                            history.replace(`/projecten/${match.params.id}/repetities`);
                        }}
                        style={{ marginLeft: '20px' }}
                        color="white"
                        size="md"
                    >
                        {t('form.button.cancel')}
                    </Button>
                </div>
            </Loader>
        </div>
    );
}

NewProject.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(newProjectStyle)(observer(NewProject)));
