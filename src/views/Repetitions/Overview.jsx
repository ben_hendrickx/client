import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import withStyles from '@material-ui/core/styles/withStyles';
import {
    Repeat,
    DateRange,
    Edit,
    FilterNone,
    Delete,
    VisibilityOff,
    Visibility,
    PlaylistAddCheck,
    LockOpen,
} from '@material-ui/icons';

import repetitionsOverviewStyle from './overview.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import Tooltip from '@material-ui/core/Tooltip';

import Loader from '../../components/Loader.jsx';
import Button from '../../components/CustomButtons/Button.jsx';

import Checkbox from '@material-ui/core/Checkbox';
import Inputs from 'components/Inputs/index.jsx';
import ResourceOverview from 'components/ResourceOverview.jsx';
import ListRow from 'components/ListRow.jsx';
import ListRowActions from 'components/ListRowActions.jsx';
import { withTranslation } from 'react-i18next';

const Placeholder = ({ width = 50 }) => <div style={{ height: 15, width, background: 'rgba(0,0,0,.1)' }}></div>;

const ObservedCheckBox = observer(({ id }) => {
    const { repetitions } = useStore();

    return (
        <div style={{ padding: '6px 0' }}>
            <Checkbox
                checked={repetitions.pinned.includes(id)}
                onChange={e => {
                    if (!repetitions.pinned.includes(id)) {
                        repetitions.pinned.push(id);
                    } else {
                        repetitions.pinned.splice(repetitions.pinned.indexOf(id), 1);
                    }
                }}
            />
        </div>
    );
});

function RepetitionsOverview(props) {
    const { classes, t } = props;
    const { projects, repetitions, account, general } = useStore();
    const { history, match } = useReactRouter();
    const [checkedRepetitions, setCheckedRepetitions] = useState([]);

    useEffect(() => {
        if (projects.id) {
            //repetitions.resource.fetch(`/project/${projects.id}`);
        }
        return () => {
            repetitions.resource.success = null;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.token, repetitions.resource.filters.past]);

    useEffect(() => {
        return () => {
            repetitions.resource.reset();
            repetitions.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function rowRenderer({ key, index, style }) {
        const repetition = repetitions.resource.data[index] || {};

        return (
            <div
                key={key}
                style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', ...(style || {}) }}
            >
                {(account.rights.canOpenCloseRepetitions || account.rights.canDeleteRepetitions) && (
                    <div style={{ padding: '0px 0' }}>
                        <Checkbox
                            checked={checkedRepetitions.includes(repetition.id)}
                            onChange={e => {
                                if (!checkedRepetitions.includes(repetition.id)) {
                                    setCheckedRepetitions([...checkedRepetitions, repetition.id]);
                                } else {
                                    const newUsers = [...checkedRepetitions];
                                    newUsers.splice(checkedRepetitions.indexOf(repetition.id), 1);
                                    setCheckedRepetitions([...newUsers]);
                                }
                            }}
                        />
                    </div>
                )}
                <li key={repetition.id} style={{ flexGrow: 1 }}>
                    <ListRow>
                        <span>
                            <div style={{ borderRight: '1px solid rgba(0,0,0,.1)', marginRight: 20 }}>
                                {repetition.open ? (
                                    <Visibility style={{ paddingTop: 3, marginTop: 4, color: 'green' }} />
                                ) : (
                                    <VisibilityOff style={{ paddingTop: 3, marginTop: 4, color: 'tomato' }} />
                                )}
                            </div>
                            <Repeat /> {repetition.name || <Placeholder width={60} />}
                        </span>
                        <div style={{ display: 'flex' }}>
                            {repetition.startDt ? (
                                <span style={{ width: 250 }}>
                                    <span style={{ display: 'flex', alignItems: 'center' }}>
                                        <DateRange /> {moment(repetition.startDt).format('DD/MM/YYYY HH:mm')} -{' '}
                                        {moment(repetition.endDt).format('HH:mm')}
                                    </span>
                                </span>
                            ) : (
                                <span>
                                    <DateRange /> <Placeholder width={180} />
                                </span>
                            )}
                            {/* <span
                            style={{
                                display: 'flex',
                                alignItems: 'center',
                                marginRight: 20,
                                width: 100,
                            }}
                        >
                            <People /> {repetition.allChecked ? 'Iedereen' : (repetition.users || <Placeholder width={40} />)}
                        </span> */}

                            {(account.rights.canPresentRepetitions ||
                                account.rights.canPinRepetitions ||
                                account.rights.canUpdateRepetitions ||
                                account.rights.canCreateRepetitions ||
                                account.rights.canDeleteRepetitions) && (
                                <ListRowActions>
                                    {account.rights.canPresentRepetitions && (
                                        <Tooltip title={t('tooltip.repetition.presence')} placement="top">
                                            <PlaylistAddCheck
                                                onClick={() =>
                                                    history.push(
                                                        `/projecten/${match.params.id}/repetities/${repetition.id}/aanwezigheid`
                                                    )
                                                }
                                            />
                                        </Tooltip>
                                    )}
                                    {account.rights.canPinRepetitions &&
                                        !!repetition.register &&
                                        new Date(repetition.startDt).getTime() <
                                            new Date().getTime() + 90 * 60 * 1000 &&
                                        new Date(repetition.startDt).getTime() + 15 * 60 * 1000 >
                                            new Date().getTime() && (
                                            <Tooltip title={t('tooltip.repetition.unlock')} placement="top">
                                                <LockOpen
                                                    onClick={async () => {
                                                        let modal;
                                                        await repetitions.fetchSameLocation(repetition.id);
                                                        modal = general.addModal({
                                                            title: t('modal.title.repetition.unlock'),
                                                            body: (
                                                                <>
                                                                    <p>
                                                                        {t('modal.body.repetition.unlock1')}
                                                                        {repetitions.pinning?.filter(
                                                                            r => r.id !== repetition.id
                                                                        ).length > 0 && (
                                                                            <>
                                                                                <br />
                                                                                {t('modal.body.repetition.unlock2')}
                                                                            </>
                                                                        )}
                                                                    </p>
                                                                    <Loader loading={repetitions.loadingPinning}>
                                                                        {repetitions.pinning
                                                                            ?.filter(r => r.id !== repetition.id)
                                                                            ?.map(repetition => (
                                                                                <div className={classes.row}>
                                                                                    <ObservedCheckBox
                                                                                        id={repetition.id}
                                                                                    />
                                                                                    <div
                                                                                        key={`repetition-${repetition.id}`}
                                                                                        className={classes.pin}
                                                                                    >
                                                                                        <span>
                                                                                            {repetition.identifier} -{' '}
                                                                                            {repetition.name}
                                                                                        </span>
                                                                                        <span>
                                                                                            <DateRange />{' '}
                                                                                            {moment(
                                                                                                repetition.startDt
                                                                                            ).format(
                                                                                                'DD/MM/YYYY HH:mm'
                                                                                            )}{' '}
                                                                                            -{' '}
                                                                                            {moment(
                                                                                                repetition.endDt
                                                                                            ).format('HH:mm')}
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            ))}
                                                                    </Loader>
                                                                </>
                                                            ),
                                                            buttonText: t('form.button.repetition.unlock'),
                                                            onConfirm: async () => {
                                                                modal.close();
                                                                unlockRepetition(repetition.id);
                                                            },
                                                        });
                                                    }}
                                                />
                                            </Tooltip>
                                        )}
                                    {account.rights.canUpdateRepetitions && (
                                        <Tooltip title={t('tooltip.repetition.edit')} placement="top">
                                            <Edit
                                                onClick={() =>
                                                    history.push(
                                                        `/projecten/${match.params.id}/repetities/${repetition.id}/bewerk`
                                                    )
                                                }
                                            />
                                        </Tooltip>
                                    )}
                                    {account.rights.canCreateRepetitions && (
                                        <Tooltip title={t('tooltip.repetition.copy')} placement="top">
                                            <FilterNone
                                                onClick={() => {
                                                    let modal;
                                                    modal = general.addModal({
                                                        title: t('modal.title.repetition.copy'),
                                                        body: (
                                                            <>
                                                                <p>{t('modal.body.repetition.copy')}</p>
                                                            </>
                                                        ),
                                                        buttonText: t('form.button.duplicate'),
                                                        onConfirm: async () => {
                                                            await repetitions.copy(repetition.id);
                                                            repetitions.resource.success = t('success.repetition.copy');
                                                            repetitions.resource.data = [];
                                                            repetitions.resource.status = [];
                                                            await repetitions.resource.fetch(`/project/${projects.id}`);
                                                            modal.close();
                                                        },
                                                    });
                                                }}
                                            />
                                        </Tooltip>
                                    )}
                                    {account.rights.canDeleteRepetitions && (
                                        <Tooltip title={t('tooltip.repetition.delete')} placement="top">
                                            <Delete
                                                onClick={() => {
                                                    let modal;
                                                    modal = general.addModal({
                                                        title: t('modal.title.repetition.delete'),
                                                        body: (
                                                            <>
                                                                <p>{t('modal.body.repetition.delete')}</p>
                                                            </>
                                                        ),
                                                        buttonText: t('form.button.delete'),
                                                        onConfirm: async () => {
                                                            await repetitions.delete(repetition.id);
                                                            repetitions.resource.success = t(
                                                                'success.repetition.delete'
                                                            );
                                                            repetitions.resource.data = [];
                                                            repetitions.resource.status = [];
                                                            await repetitions.resource.fetch(`/project/${projects.id}`);
                                                            modal.close();
                                                        },
                                                    });
                                                }}
                                            />
                                        </Tooltip>
                                    )}
                                </ListRowActions>
                            )}
                        </div>
                    </ListRow>
                </li>
            </div>
        );
    }

    function unlockRepetition(id) {
        window.open(`/pinscherm/${id}?repetitionIds=${repetitions.pinned.join(',')}`);
        repetitions.pinned = [];
    }

    return (
        <>
            <ResourceOverview
                apiPrefix={`/project/${projects.id}`}
                backend
                store={repetitions.resource}
                rowRenderer={rowRenderer}
                rowHeight={60}
                heightOffset={450}
                actions={[
                    account.rights.canCreateRepetitions && {
                        icon: 'add',
                        route: `/projecten/${match.params.id}/repetities/nieuw`,
                        tooltip: t('tooltip.repetition.create'),
                    },
                    account.rights.canExportRepetitions && {
                        icon: 'getApp',
                        href: '/api/reports/repetitions.xlsx',
                        addition: `projectId=${projects.id}`,
                        tooltip: t('tooltip.report.download'),
                    },
                    account.rights.canExportRepetitions && {
                        icon: 'getApp',
                        href: `/api/reports/repetitions_complete.xlsx`,
                        addition: `projectId=${projects.id}`,
                        tooltip: t('tooltip.repetition.completeReport'),
                    },
                    account.rights.canExportRepetitions && {
                        icon: 'getApp',
                        href: `/api/reports/presence_complete.xlsx`,
                        addition: `projectId=${projects.id}`,
                        tooltip: t('tooltip.repetition.completePresenceReport'),
                    },
                ]}
                filters={<Inputs.repetitions.filters.past />}
                renderBefore={
                    <div>
                        {account.rights.canOpenCloseRepetitions && (
                            <Button
                                style={{ marginRight: 20 }}
                                disabled={!checkedRepetitions.length}
                                onClick={() => {
                                    let modal;
                                    modal = general.addModal({
                                        title: t('modal.title.repetition.open'),
                                        body: (
                                            <>
                                                <p>{t('modal.body.repetition.open')}</p>
                                            </>
                                        ),
                                        buttonText: t('form.button.repetition.open'),
                                        onConfirm: async () => {
                                            await repetitions.open(checkedRepetitions);
                                            repetitions.resource.data = [];
                                            repetitions.resource.status = [];
                                            await repetitions.resource.fetch(`/project/${projects.id}`);
                                            setCheckedRepetitions([]);
                                            repetitions.resource.success = t('success.repetition.open');
                                            modal.close();
                                        },
                                    });
                                }}
                                color="success"
                                size="md"
                            >
                                {t('form.button.repetition.openSelected')}
                            </Button>
                        )}
                        {account.rights.canOpenCloseRepetitions && (
                            <Button
                                disabled={!checkedRepetitions.length}
                                onClick={() => {
                                    let modal;
                                    modal = general.addModal({
                                        title: t('modal.title.repetition.close'),
                                        body: (
                                            <>
                                                <p>{t('modal.body.repetition.close')}</p>
                                            </>
                                        ),
                                        buttonText: t('form.button.repetition.close'),
                                        onConfirm: async () => {
                                            await repetitions.close(checkedRepetitions);
                                            repetitions.resource.data = [];
                                            repetitions.resource.status = [];
                                            await repetitions.resource.fetch(`/project/${projects.id}`);
                                            setCheckedRepetitions([]);
                                            repetitions.resource.success = t('success.repetition.close');
                                            modal.close();
                                        },
                                    });
                                }}
                                color="danger"
                                size="md"
                            >
                                {t('form.button.repetition.closeSelected')}
                            </Button>
                        )}
                        {account.rights.canDeleteRepetitions && (
                            <Button
                                disabled={!checkedRepetitions.length}
                                onClick={() => {
                                    let modal;
                                    modal = general.addModal({
                                        title: t('modal.title.repetition.deleteMultiple'),
                                        body: (
                                            <>
                                                <p>{t('modal.body.repetition.deleteMultiple')}</p>
                                            </>
                                        ),
                                        buttonText: t('form.button.delete'),
                                        onConfirm: async () => {
                                            await repetitions.delete(checkedRepetitions);
                                            repetitions.resource.success = t('success.repetition.deleteMultiple');
                                            repetitions.resource.data = [];
                                            repetitions.resource.status = [];
                                            await repetitions.resource.fetch(`/project/${projects.id}`);
                                            setCheckedRepetitions([]);
                                            modal.close();
                                        },
                                    });
                                }}
                                color="danger"
                                size="md"
                            >
                                {t('form.button.repetition.deleteSelected')}
                            </Button>
                        )}
                        {(account.rights.canOpenCloseRepetitions || account.rights.canDeleteRepetitions) && (
                            <Button
                                style={{ float: 'right' }}
                                onClick={async () => {
                                    if (checkedRepetitions.length) {
                                        setCheckedRepetitions([]);
                                    } else {
                                        setCheckedRepetitions(repetitions.resource.filtered.map(({ id }) => id));
                                    }
                                }}
                                color="primary"
                                size="md"
                            >
                                {t('form.button.selectRepetitions')}
                            </Button>
                        )}
                    </div>
                }
            />
        </>
    );
}

RepetitionsOverview.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(repetitionsOverviewStyle)(observer(RepetitionsOverview)));
