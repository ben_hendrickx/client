import React, { useEffect } from 'react';

import { useStore } from '../../mobx';
import { withStyles } from '@material-ui/styles';
import { observer } from 'mobx-react-lite';
import useReactRouter from 'use-react-router';
import { primaryColor, white, secondaryColor } from '../../assets/jss/material-kit-react.jsx';
import {
    CheckCircle,
    Cancel,
    FilterList,
    ClearAll,
    Phone,
    Help,
    Error,
    FiberPin,
    Check,
    GpsFixed,
    DateRange,
} from '@material-ui/icons';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import { Tooltip } from '@material-ui/core';
import CustomInput from 'components/CustomInput/CustomInput';
import moment from 'moment';

const presenceOverviewStyle = {
    container: {
        flexGrow: 1,
        minWidth: '100%',
        '& h2': {
            textAlign: 'center',
            fontSize: '1.5em',
        },
    },
    actions: {
        borderLeft: `1px solid ${primaryColor}`,
        padding: '15px 15px 15px 30px',
        display: 'inline-flex',
        alignItems: 'center',
        background: primaryColor,
        color: white,
        flexGrow: 1,
        justifyContent: 'space-evenly',
        '& span:hover': {
            cursor: 'pointer',
            color: secondaryColor,
        },
        '@media (min-width: 960px)': {
            flexGrow: 0,
        },
    },
    rows: {
        listStyle: 'none',
        margin: '0',
        padding: '0',
        '& > li': {
            position: 'relative',
            width: '100%',
            '& li': {
                width: 'calc(100% - 20px)',
            },
            '& > div': {
                flexWrap: 'wrap',
                padding: '0 0 0 15px',
                display: 'flex',
                justifyContent: 'space-between',
                background: white,
                boxShadow:
                    '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
                '& span': {
                    display: 'inline-flex',
                    alignItems: 'center',
                    minWidth: 180,
                    minHeight: 54,
                },
                '& svg': {
                    minWidth: 60,
                    textAlign: 'center',
                    marginRight: '10px',
                    '@media (min-width: 960px)': {
                        minWidth: 'auto',
                        marginRight: '15px',
                    },
                },
            },
            margin: '26px 10px',
            '& span.highlightOnHover:hover': {
                cursor: 'pointer',
                fontWeight: 500,
                '& svg': {
                    color: primaryColor,
                },
            },
            '@media (min-width: 960px)': {
                flexWrap: 'nowrap',
                justifyContent: 'space-between',
            },
        },
    },
    elements: {
        '& > div': {
            display: 'flex',
            justifyContent: 'space-between',
        },
        '@media (min-width: 960px)': {
            display: 'flex',
            flexGrow: 1,
            marginLeft: 20,
            alignItems: 'center',
            justifyContent: 'flex-end',
            '& > svg': {
                marginRight: 20,
            },
        },
    },
    filter: {
        minWidth: '45%',
        '@media (min-width: 960px)': {
            minWidth: 100,
            marginTop: -9,
            marginRight: 20,
        },
        '@media (min-width: 1440px)': {
            minWidth: 110,
        },
    },
    filters: {
        background: '#fff',
        boxShadow:
            '0px 1px 2px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: 10,
        '@media (min-width: 960px)': {
            margin: '0 10px',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            padding: '0 20px',
        },
    },
    results: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        '& > div': {
            display: 'flex',
            alignItems: 'center',
            '& > svg': {
                marginRight: 20,
            },
            '& > span': {
                minWidth: 150,
            },
        },
    },
};

const Present = () => (
    <Tooltip title="Aanwezig" placement="top">
        <CheckCircle style={{ color: 'green' }} />
    </Tooltip>
);

const NotPresent = () => (
    <Tooltip title="Gewettigd afwezig" placement="top">
        <Cancel style={{ color: 'tomato' }} />
    </Tooltip>
);

const ShouldHaveBeenPresent = () => (
    <Tooltip title="Ongewettigd afwezig" placement="top">
        <Error style={{ color: 'orange' }} />
    </Tooltip>
);

const ProbablyPresent = () => (
    <Tooltip title="Waarschijnlijk aanwezig" placement="top">
        <Help style={{ color: 'skyblue' }} />
    </Tooltip>
);

function Scenes(props) {
    if (!props.scenes.length) {
        return null;
    }
    return (
        <>
            <Toolbar title={`Te repeteren scènes`} />
            <div>
                {props.scenes
                    .filter(scene => scene)
                    .map(scene => (
                        <div
                            style={{
                                marginLeft: 10,
                                marginRight: 10,
                                background: '#fff',
                                display: 'flex',
                                padding: '10px 20px',
                                marginBottom: 10,
                                boxShadow:
                                    '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
                            }}
                        >
                            <span style={{ marginRight: 30 }}>{scene.identifier}</span>
                            <span>{scene.name}</span>
                        </div>
                    ))}
            </div>
        </>
    );
}

function PresenceOverview(props) {
    const { classes } = props;
    const { ui, repetitions, locations } = useStore();
    const { match } = useReactRouter();

    useEffect(() => {
        let subscription;

        if (match?.params?.repetitionId) {
            repetitions.fetchRepetition(match.params.repetitionId).then(repetition => {
                locations.fetchLocation(repetition.locationId);
            });
            repetitions.fetchPresence(match.params.repetitionId);
            repetitions.fetchScenes(match.params.repetitionId);
            repetitions.listen(match.params.repetitionId);
        }

        return () => {
            locations.name = null;
            repetitions.presence = [];
            repetitions.scenes = [];
            subscription && subscription();
            repetitions.id = null;
            repetitions.name = '';
            repetitions.description = '';
            repetitions.identifier = '';
            repetitions.date = null;
            repetitions.start = '';
            repetitions.end = '';
            repetitions.register = false;
            repetitions.locations.value = '';
            repetitions.types.value = '';
            repetitions.selectorData = '';
            repetitions.allChecked = false;
            repetitions.castChecked = false;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const repetitionNotYetStarted = moment(repetitions.startDt).subtract(30, 'minutes').isSameOrAfter(moment());

    return (
        <>
            <div className={classes.container}>
                <Scenes scenes={repetitions.scenes || []} />
                <Toolbar
                    title={`Aanwezigheid op repetitie ${repetitions.name}`}
                    searchProps={{
                        value: repetitions.userFilter,
                        onChange: e => (repetitions.userFilter = e.target.value),
                        placeholder: 'Zoek medewerker',
                    }}
                    actions={[
                        {
                            icon: 'getApp',
                            href: '/api/reports/repetition.xlsx',
                            addition: 'repetitionId=' + match.params.repetitionId,
                            tooltip: 'Download rapport',
                        },
                    ]}
                />
                {locations.name && (
                    <div
                        className={classes.filters}
                        style={{
                            display: 'flex',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            padding: '10px 20px',
                            marginBottom: 10,
                        }}
                    >
                        <GpsFixed style={{ marginRight: 10 }} />
                        <span>{locations.name}</span>
                        <span
                            style={{
                                marginLeft: 30,
                                display: 'flex',
                                justifyContent: 'flex-start',
                                alignItems: 'center',
                            }}
                        >
                            <DateRange style={{ marginRight: 10 }} />{' '}
                            {moment(repetitions.startDt).format('DD/MM/YYYY HH:mm')} -{' '}
                            {moment(repetitions.endDt).format('HH:mm')}
                        </span>
                    </div>
                )}
                <div className={classes.filters}>
                    <div className={classes.results}>
                        <div>
                            <FilterList />
                            <span>
                                {repetitions.presenceFiltered ? repetitions.presenceFiltered.length : 0}
                                {repetitions.presenceFiltered.length === repetitions.presence.length
                                    ? ''
                                    : `/${repetitions.presence.length}`}{' '}
                                resultaten
                            </span>
                        </div>
                        <div style={{ marginRight: 20 }} className="present">
                            <Present />{' '}
                            {new Date(repetitions.startDt).getTime() + 15 * 60 * 1000 <= Date.now() &&
                            !repetitions.register
                                ? repetitions.presence.filter(({ present, markedPresent }) => present || markedPresent)
                                      .length
                                : repetitions.presence.filter(({ present }) => present).length}
                        </div>
                        <div style={{ marginRight: 20 }}>
                            <NotPresent />{' '}
                            {
                                repetitions.presence.filter(({ present, markedPresent }) => !present && !markedPresent)
                                    .length
                            }
                        </div>
                        <div style={{ marginRight: 20 }}>
                            <ShouldHaveBeenPresent />{' '}
                            {repetitions.register &&
                            new Date(repetitions.startDt).getTime() + 15 * 60 * 1000 <= Date.now()
                                ? repetitions.presence.filter(({ present, markedPresent }) => !present && markedPresent)
                                      .length
                                : 0}
                        </div>
                        <div style={{ marginRight: 20 }}>
                            <ProbablyPresent />
                            {new Date(repetitions.startDt).getTime() + 15 * 60 * 1000 > Date.now()
                                ? repetitions.presence.filter(({ present, markedPresent }) => !present && markedPresent)
                                      .length
                                : 0}
                        </div>
                        {ui.isMobile && (
                            <Tooltip
                                id="instagram-twitter"
                                title="Verwijder filters"
                                placement={window.innerWidth > 959 ? 'top' : 'left'}
                                classes={{ tooltip: classes.tooltip }}
                            >
                                <ClearAll
                                    htmlColor={primaryColor}
                                    fontSize="large"
                                    onClick={() => {
                                        repetitions.userFilter = '';
                                        repetitions.presenceFilter = '';
                                        repetitions.roleFilter = '';
                                    }}
                                />
                            </Tooltip>
                        )}
                    </div>
                    <div className={classes.elements}>
                        <div>
                            <CustomInput
                                type="select"
                                labelText="Individuele rol"
                                formControlProps={{
                                    className: classes.filter,
                                }}
                                inputProps={{
                                    native: true,
                                    useValue: true,
                                    value: repetitions.roleFilter,
                                    onChange: e => {
                                        repetitions.roleFilter = e.target.value;
                                    },
                                    name: 'role',
                                    inputProps: {
                                        name: 'role',
                                        id: 'role_FVD',
                                    },
                                    options: [{ label: 'Ja', value: 1 }, { label: 'Neen', value: 0 }],
                                }}
                            />
                        </div>
                        <div>
                            <CustomInput
                                type="select"
                                labelText="Aanwezigheid"
                                formControlProps={{
                                    className: classes.filter,
                                }}
                                inputProps={{
                                    native: true,
                                    useValue: true,
                                    value: repetitions.presenceFilter,
                                    onChange: e => {
                                        repetitions.presenceFilter = e.target.value;
                                    },
                                    name: 'present',
                                    inputProps: {
                                        name: 'present',
                                        id: 'present_FVD',
                                    },
                                    options: [
                                        { label: 'Aanwezig', value: 1 },
                                        { label: 'Gewettigd afwezig', value: 0 },
                                        { label: 'Ongewettigd afwezig', value: 2 },
                                        { label: 'Waarschijnlijk aanwezig', value: 3 },
                                    ],
                                }}
                            />
                        </div>
                        {!ui.isMobile && (
                            <Tooltip
                                id="instagram-twitter"
                                title="Verwijder filters"
                                placement={window.innerWidth > 959 ? 'top' : 'left'}
                                classes={{ tooltip: classes.tooltip }}
                            >
                                <ClearAll
                                    htmlColor={primaryColor}
                                    fontSize="large"
                                    onClick={() => {
                                        repetitions.userFilter = '';
                                        repetitions.presenceFilter = '';
                                        repetitions.roleFilter = '';
                                    }}
                                />
                            </Tooltip>
                        )}
                    </div>
                </div>
                <ul className={classes.rows}>
                    {(repetitions.presenceFiltered || []).map(
                        ({ id, firstName, lastName, markedPresent, present, cellPhone, phoneNumber, settings }) => (
                            <li key={id}>
                                <div>
                                    <span>
                                        {lastName} {firstName}
                                    </span>
                                    <span>
                                        <FiberPin /> <span className="pin">{settings.pin}</span>
                                    </span>
                                    <span>
                                        {cellPhone || phoneNumber ? (
                                            <>
                                                <Phone /> {cellPhone || phoneNumber}
                                            </>
                                        ) : (
                                            ''
                                        )}
                                    </span>
                                    <span>
                                        {present ||
                                        (!repetitions.register &&
                                            new Date(repetitions.startDt).getTime() + 15 * 60 * 1000 < Date.now() &&
                                            markedPresent) ? (
                                            <Present />
                                        ) : !markedPresent ? (
                                            <NotPresent />
                                        ) : new Date(repetitions.startDt).getTime() + 15 * 60 * 1000 < Date.now() ? (
                                            <ShouldHaveBeenPresent />
                                        ) : (
                                            <ProbablyPresent />
                                        )}
                                    </span>
                                    <div className={classes.actions}>
                                    {repetitionNotYetStarted && markedPresent && <Tooltip
                                            id="instagram-twitter"
                                            title="Markeer als afwezig"
                                            placement={window.innerWidth > 959 ? 'top' : 'left'}
                                            classes={{ tooltip: classes.tooltip }}
                                        >
                                            <Cancel
                                                onClick={() => {
                                                    repetitions.markPresent(match.params.repetitionId, false, id);
                                                    const presence = repetitions.presence.find(p => p.id === id);
                                                    presence && (presence.markedPresent = false);
                                                }}
                                            />
                                        </Tooltip>}
                                        {repetitionNotYetStarted && !markedPresent && <Tooltip
                                            id="instagram-twitter"
                                            title="Markeer als aanwezig"
                                            placement={window.innerWidth > 959 ? 'top' : 'left'}
                                            classes={{ tooltip: classes.tooltip }}
                                        >
                                            <Check
                                                onClick={() => {
                                                    repetitions.markPresent(match.params.repetitionId, true, id);
                                                    const presence = repetitions.presence.find(p => p.id === id);
                                                    presence && (presence.markedPresent = true);
                                                }}
                                            />
                                        </Tooltip>}
                                        {!repetitionNotYetStarted && !present && (
                                            <Tooltip
                                                id="instagram-twitter"
                                                title="Markeer als aanwezig"
                                                placement={window.innerWidth > 959 ? 'top' : 'left'}
                                                classes={{ tooltip: classes.tooltip }}
                                            >
                                                <Check
                                                    onClick={() => {
                                                        repetitions.enterPin(match.params.repetitionId, settings.pin);
                                                    }}
                                                />
                                            </Tooltip>
                                        )}
                                        {!repetitionNotYetStarted && markedPresent === true && !present && (
                                            <Tooltip
                                                id="instagram-twitter"
                                                title="Markeer als wettig afwezig"
                                                placement={window.innerWidth > 959 ? 'top' : 'left'}
                                                classes={{ tooltip: classes.tooltip }}
                                            >
                                                <Cancel
                                                    onClick={() => {
                                                        repetitions.markPresent(match.params.repetitionId, false, id);
                                                        const presence = repetitions.presence.find(p => p.id === id);
                                                        presence && (presence.markedPresent = false);
                                                    }}
                                                />
                                            </Tooltip>
                                        )}
                                        {!repetitionNotYetStarted && present && (
                                            <Tooltip
                                                id="instagram-twitter"
                                                title="Markeer als niet aanwezig"
                                                placement={window.innerWidth > 959 ? 'top' : 'left'}
                                                classes={{ tooltip: classes.tooltip }}
                                            >
                                                <Cancel
                                                    onClick={() => {
                                                        repetitions.enterPin(
                                                            match.params.repetitionId,
                                                            settings.pin,
                                                            [],
                                                            false
                                                        );
                                                    }}
                                                />
                                            </Tooltip>
                                        )}
                                    </div>
                                </div>
                            </li>
                        )
                    )}
                </ul>
            </div>
        </>
    );
}

export default withStyles(presenceOverviewStyle)(observer(PresenceOverview));
