import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Button from '../../components/CustomButtons/Button.jsx';
import UserRow from '../../components/User/Row.jsx';
import Page from 'components/Page.jsx';
import ResourceOverview from 'components/ResourceOverview.jsx';
import { Checkbox } from '@material-ui/core';
import Inputs from 'components/Inputs';

function SceneGroupsOverview() {
    const { projectRoles: roles, users, casts, projects } = useStore();
    const { match, history } = useReactRouter();
    const [linked, setLinked] = useState(0);
    const [cast, setCast] = useState('');
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        async function fetchData() {
            await Promise.all([
                roles.fetchRole(match.params.roleId),
                casts.resource.fetch(),
                roles.fetchUsers(match.params.roleId).then(linked => setLinked(linked)),
            ]);
            setLoading(false);
        }

        fetchData();

        return () => {
            roles.reset();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const data = users.resource.filtered.filter(user => {
        if (linked === '') {
            return true;
        }

        if (linked) {
            return casts.checked.some(({ userId, castId }) => userId === user.id && (!cast || castId === cast));
        }

        return !casts.checked.some(({ userId }) => userId === user.id);
    });

    function rowRenderer({ key, index, style }) {
        const user = data[index];

        return (
            <UserRow
                watch={false}
                key={key}
                style={{ ...(style || {}), height: 'auto', width: '100%', margin: '10px 0px 20px 0px' }}
                onClick={() => {}}
                {...user}
            >
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    {casts.resource.filtered.map(cast => (
                        <div style={{ width: 45, margin: '0 20px' }}>
                            <Checkbox
                                checked={casts.checked.some(
                                    ({ userId, castId }) => userId === user.id && castId === cast.id
                                )}
                                onChange={e => {
                                    if (e.target.checked) {
                                        casts.checked.push({ userId: user.id, castId: cast.id });
                                    } else {
                                        casts.checked = casts.checked.filter(
                                            ({ userId, castId }) => !(userId === user.id && castId === cast.id)
                                        );
                                    }
                                }}
                            />
                        </div>
                    ))}
                </div>
            </UserRow>
        );
    }

    return (
        <Page title={`Koppel gebruikers aan rol: ${roles.identifier} - ${roles.name}`} loading={casts.loading} style={{ height: 'calc(100% - 150px)' }}>
            <ResourceOverview
                loading={loading}
                noAutoSizer
                apiPrefix={`/project/${projects.id}/onstage`}
                store={users.resource}
                data={data}
                rowRenderer={rowRenderer}
                heightOffset={650}
                renderBefore={
                    <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                        {casts.resource.filtered.map(cast => (
                            <span style={{ display: 'block', width: 45, margin: '0 20px', textAlign: 'center' }}>
                                {cast.name}
                            </span>
                        ))}
                    </div>
                }
                isFiltering={linked}
                onClearFilters={() => {
                    setLinked('');
                    setCast('');
                }}
                filters={
                    <>
                        <Inputs.users.filters.sex />
                        <Inputs.users.filters.minAge />
                        <Inputs.users.filters.maxAge />
                        <Inputs.Input
                            label="Gekoppeld"
                            type="select"
                            value={linked}
                            onChange={v => setLinked(v)}
                            options={[{ label: 'Ja', value: 1 }, { label: 'Neen', value: 0 }]}
                        />
                        {!!linked && (
                            <Inputs.Input
                                label="Cast"
                                type="select"
                                value={cast}
                                onChange={v => setCast(v)}
                                options={casts.resource.filtered.map(cast => ({ label: cast.name, value: cast.id }))}
                            />
                        )}
                    </>
                }
                sort={<Inputs.users.sort />}
            />
            <Button
                onClick={async () => {
                    await roles.saveUsers();
                    history.goBack();
                    roles.resource.success = `Medewerkers van rol '${roles.name}' werden succesvol gewijzigd`;
                }}
                color="success"
                size="md"
            >
                Sla op
            </Button>
        </Page>
    );
}

SceneGroupsOverview.propTypes = {
    classes: PropTypes.object,
};

export default observer(SceneGroupsOverview);
