import React from 'react';
import ListRow from 'components/ListRow';
import { Delete, Edit, PersonAdd } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import Role from 'components/Role';
import { useHistory } from 'react-router';
import { useStore } from '../../../mobx';
import RoleForm from '../Forms/Role';
import { withTranslation } from 'react-i18next';

function RoleRow({ style, role, t }) {
    const history = useHistory();
    const { projects, projectRoles, general, account } = useStore();

    return (
        <li style={style}>
            <ListRow>
                <Role role={role} identifier roles type description users />
                {(account.rights.canReadRoles || account.rights.canUpdateRoles || account.rights.canDeleteRoles) && (
                    <ListRowActions>
                        {account.rights.canUpdateRoles && (
                            <Tooltip title={t('tooltip.role.edit')} placement="top">
                                <Edit
                                    onClick={() => {
                                        let modal;
                                        const closeModal = () => modal.close();
                                        const { id, identifier, name, description, speaking } = role;
                                        projectRoles.resource.form.data = {
                                            id,
                                            identifier,
                                            name,
                                            description,
                                            speaking,
                                        };
                                        modal = general.addModal({
                                            type: 'window',
                                            width: 1100,
                                            title: t('modal.title.role.edit'),
                                            onMinimize: async modal => {
                                                projectRoles.resource.form.data.name &&
                                                    (modal.title = `${t('modal.title.role.edit')}: ${
                                                        projectRoles.resource.form.data.name
                                                    }`);
                                                return projectRoles.resource.form.data;
                                            },
                                            onMaximize: modal => {
                                                projectRoles.resource.form.data = { ...modal.data };
                                            },
                                            body: <RoleForm onCancel={closeModal} onConfirm={closeModal} />,
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                        {account.rights.canUpdateRoleUsers && (
                            <Tooltip title={t('tooltip.role.manageUsers')} placement="top">
                                <PersonAdd
                                    className="edit-users"
                                    onClick={() =>
                                        history.push(`/projecten/${projects.id}/rollen/bewerk/${role.id}/gebruikers`)
                                    }
                                />
                            </Tooltip>
                        )}
                        {account.rights.canDeleteRoles && (
                            <Tooltip title={t('tooltip.role.delete')} placement="top">
                                <Delete
                                    onClick={() => {
                                        general.addModal({
                                            title: `${t('modal.title.role.delete')}: ${role.name}`,
                                            body: t('modal.body.role.delete'),
                                            buttonText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await projectRoles.resource.delete(undefined, role.id);
                                                projectRoles.resource.data = [];
                                                projectRoles.resource.status = [];
                                                projectRoles.resource.fetch(`/project/${projects.id}`);
                                            },
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                    </ListRowActions>
                )}
            </ListRow>
        </li>
    );
}

export default withTranslation()(RoleRow);
