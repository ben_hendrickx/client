import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Inputs from 'components/Inputs';
import RoleRow from './Components/RoleRow';
import RoleForm from './Forms/Role';
import { withTranslation } from 'react-i18next';

function RolesOverview({ t }) {
    const { projectRoles, projects, account, general } = useStore();

    function rowRenderer({ key, index, style }) {
        const role = projectRoles.resource.filtered[index];

        return <RoleRow key={key} style={style} role={role} />;
    }

    function onCreateRole() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.role.new'),
            onMinimize: modal => {
                projectRoles.resource.form.data.name &&
                    (modal.title = `${t('modal.title.role.new')}: ${projectRoles.resource.form.data.name}`);
                return projectRoles.resource.form.data;
            },
            onMaximize: modal => {
                projectRoles.resource.form.data = { ...modal.data };
            },
            body: <RoleForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <ResourceOverview
            apiPrefix={`/project/${projects.id}`}
            store={projectRoles.resource}
            rowRenderer={rowRenderer}
            heightOffset={400}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateRoles && {
                    icon: 'add',
                    onClick: onCreateRole,
                    tooltip: t('tooltip.role.create'),
                },
                account.rights.canExportRoles && {
                    icon: 'getApp',
                    href: '/api/reports/roles.xlsx',
                    addition: 'projectId=' + projects.id,
                    tooltip: t('tooltip.report.download'),
                },
            ]}
            filters={<Inputs.roles.filters.scene />}
            sort={<Inputs.roles.sort />}
        />
    );
}

export default withTranslation()(observer(RolesOverview));
