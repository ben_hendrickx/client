import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowBackIos, Save } from '@material-ui/icons';

import newProjectStyle from './new.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Toolbar from '../../components/Toolbar/Toolbar.jsx';
import Loader from '../../components/Loader.jsx';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

function NewProject(props) {
    const { classes } = props;
    const { projectRoles: roles, ui } = useStore();
    const { history, match } = useReactRouter();

    useEffect(() => {
        if (match?.params?.roleId) {
            roles.fetchRole(match.params.roleId);
        }

        return () => {
            roles.id = null;
            roles.name = null;
            roles.description = null;
            roles.success = null;
            roles.identifier = null;
            roles.speaking = null;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div style={{ flexGrow: 1 }}>
            {ui.isMobile && (
                <div className={classes.header}>
                    <ArrowBackIos onClick={() => history.replace(`/projecten/${match.params.id}/rollen`)} />
                    <span>{roles.id ? 'Bewerk' : 'Nieuwe'} rol</span>
                </div>
            )}
            <Loader loading={roles.loading}>
                <Toolbar title={`${roles.id ? 'Bewerk' : 'Nieuwe'} rol`} />
                <div className={classes.content}>
                    <div style={{ padding: '20px' }}>
                        <CustomInput
                            labelText="Volgnummer"
                            id="role"
                            formControlProps={{
                                required: true,
                                fullWidth: true,
                            }}
                            inputProps={{
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: roles.identifier,
                                onChange: e => (roles.identifier = e.target.value),
                            }}
                        />
                        <CustomInput
                            labelText="Rol-naam"
                            id="role"
                            formControlProps={{
                                required: true,
                                fullWidth: true,
                            }}
                            inputProps={{
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: roles.name,
                                onChange: e => (roles.name = e.target.value),
                            }}
                        />
                        <CustomInput
                            labelText="Omschrijving"
                            id="role"
                            formControlProps={{
                                fullWidth: true,
                            }}
                            inputProps={{
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: roles.description,
                                onChange: e => (roles.description = e.target.value),
                            }}
                        />
                        <div>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={roles.speaking}
                                        onChange={e => {
                                            roles.speaking = e.target.checked;
                                        }}
                                    />
                                }
                                label="Dit is een sprekende rol"
                            />
                        </div>
                    </div>
                    <Button
                        disabled={!roles.name || !roles.identifier}
                        onClick={async () => {
                            await roles.save();
                            history.replace(`/projecten/${match.params.id}/rollen`);
                        }}
                        color="success"
                        size="md"
                    >
                        <Save style={{ marginRight: '5px' }} />
                        <span style={{ marginTop: '1px', marginLeft: '5px' }}>Sla op</span>
                    </Button>
                    <Button
                        onClick={() => {
                            history.replace(`/projecten/${match.params.id}/rollen`);
                        }}
                        style={{ marginLeft: '20px' }}
                        color="white"
                        size="md"
                    >
                        Annuleer
                    </Button>
                </div>
            </Loader>
        </div>
    );
}

NewProject.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(newProjectStyle)(observer(NewProject));
