import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewRole(props) {
    const { projects, projectRoles } = useStore();

    useEffect(() => {
        return () => {
            projectRoles.resource.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const apiPrefix = `/project/${projects.id}`;

    return (
        <Form
            apiPrefix={apiPrefix}
            store={projectRoles.resource}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                projectRoles.resource.data = [];
                projectRoles.resource.status = [];
                projectRoles.resource.fetch(apiPrefix);
            }}
        />
    );
}

NewRole.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewRole);
