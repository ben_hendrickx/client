import React, { useEffect, useState } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';

import myRolesStyle from './my.style.jsx';

import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';

import { ArtTrack, FolderOpen, CameraRoll, Group } from '@material-ui/icons';
import { useHistory } from 'react-router';

import Loader from '../../components/Loader.jsx';
import TabView from 'components/TabView.jsx';

import Page from 'components/Page.jsx';
import moment from 'moment';
import Button from 'components/CustomButtons/Button.jsx';

function Roles(props) {
    const { classes, data = [], type } = props;

    return data.length > 0 ? (
        <ul className={classes.projects}>
            {data
                .filter(role => (type === 'role' ? role.speaking !== undefined : role.speaking === undefined))
                .map(role => (
                    <li key={role.id}>
                        {role.sceneIdentifier && (
                            <span>
                                <CameraRoll />
                                <span style={{ minWidth: 120 }}>
                                    {role.sceneIdentifier} - {role.sceneName}
                                </span>
                            </span>
                        )}
                        {role.scene && (
                            <span>
                                <CameraRoll />
                                <span style={{ minWidth: 120 }}>
                                    {role.scene.identifier} - {role.scene.name}
                                </span>
                            </span>
                        )}
                        <span>
                            {type === 'role' && <ArtTrack />}
                            {type === 'group' && <Group />}
                            <span style={{ paddingRight: 20 }}>
                                {role.identifier} - {role.name} (Cast: {role.cast.name})
                            </span>
                        </span>
                    </li>
                ))}
        </ul>
    ) : null;
}

function GroupsAndRoles() {
    const RolesView = withStyles(myRolesStyle)(Roles);
    const { groups, projectRoles: roles } = useStore();

    return (
        <>
            <div class="page">
                {roles.filteredData.length > 0 && <h2>Individuele rollen</h2>}
                <RolesView type="role" data={roles.filteredData} />
            </div>
            <div class="page">
                {groups.filteredData.length > 0 && <h2>Groepsrollen</h2>}
                <RolesView type="group" data={groups.filteredData} />
            </div>
        </>
    );
}

function MyRoles(props) {
    const { projectRoles: roles, groups, account, projects, registrations } = useStore();
    const history = useHistory();
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        setTimeout(() => {
            Promise.all([
                groups.fetch(undefined, account.id, false, true),
                roles.fetch(undefined, account.id, false, true),
                projects.resource.fetch(),
                registrations.resource.fetch(`/user/${account.id}`),
            ]).then(() => {
                const projects = [...roles.data, ...groups.data]
                    .reduce((acc, role) => {
                        if (!acc.includes(role.project)) {
                            acc.push(role.project);
                        }
                        return acc;
                    }, [])
                    .sort((a, b) => a.id - b.id);

                if (projects.length) {
                    history.replace(`/rollen/${projects[0].name.toLowerCase()}`);
                }
                setLoading(false);
            });
        }, 500);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.token]);

    const tabs = [...roles.filteredData, ...groups.filteredData].reduce((acc, role) => {
        if (!acc.find(project => project.label === role.project.name)) {
            acc.push({
                label: role.project.name,
                icon: <FolderOpen />,
                onClick: () => {
                    projects.id = role.project.id;
                    history.replace(`/rollen/${role.project.name.toLowerCase()}`);
                },
            });
        }
        return acc;
    }, []);

    const hasOpenProjects = !!projects.resource.data.length;
    const hasOpenProjectsWithRolesOpen = registrations.resource.data.some(
        r => r?.project && moment(r.project.rolesOpen).isAfter(moment())
    );
    const hasRegistrations = !!registrations.resource.data.length;
    const hasRoles = !!roles.filteredData.length || !!groups.filteredData.length;
    let message;

    if (hasRegistrations) {
        if (hasOpenProjectsWithRolesOpen) {
            message = 'Het rollenschema staat momenteel niet open, kom later nog eens een kijkje nemen';
        } else if (!hasRoles) {
            message = 'Je bent momenteel niet aan een rol toegewezen';
        }
    } else {
        if (hasOpenProjects) {
            message = 'Je bent niet ingeschreven in een project, gelieve je eerst in te schrijven';
        } else {
            message = 'Er zijn momenteel geen lopende projecten, kom later nog eens een kijkje nemen';
        }
    }

    return (
        <Page title={`${account.firstName} ${account.lastName} - Mijn rollen`} loading={loading}>
            <TabView tabs={tabs} routes={[{ path: '/rollen/:name', component: GroupsAndRoles }]} />
            <Loader loading={roles.loading}>
                <h2>{message}</h2>
                {!hasRegistrations && hasOpenProjects && (
                    <Button color="primary" onClick={() => history.push('/projecten')}>
                        Bekijk projecten
                    </Button>
                )}
            </Loader>
        </Page>
    );
}

export default withStyles(myRolesStyle)(observer(MyRoles));
