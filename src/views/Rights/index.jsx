import React from 'react';
import { useStore } from '../../mobx';
import ResourceOverview from 'components/ResourceOverview';
import ListRow from 'components/ListRow';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { Delete, Edit, FilterNone, InfoOutlined } from '@material-ui/icons';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function Rights({ t }) {
    const history = useHistory();
    const { accessRoles, general, account } = useStore();

    function rowRenderer({ key, index, style }) {
        const accessRole = accessRoles.resource.filtered[index];

        const { id, name } = accessRole;

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span>{name}</span>
                    {(account.rights.canReadAccessRoles ||
                        account.rights.canUpdateAccessRoles ||
                        account.rights.canCreateAccessRoles ||
                        account.rights.canDeleteAccessRoles) && (
                        <ListRowActions>
                            {account.rights.canUpdateAccessRoles && (
                                <Tooltip title={t('tooltip.accessRole.viewUsers')} placement="top">
                                    <InfoOutlined onClick={() => history.push(`/toegangsrollen/${id}/gebruikers`)} />
                                </Tooltip>
                            )}
                            {account.rights.canUpdateAccessRoles && (
                                <Tooltip title={t('tooltip.accessRole.edit')} placement="top">
                                    <Edit onClick={() => history.push(`/toegangsrollen/${id}/bewerk`)} />
                                </Tooltip>
                            )}
                            {account.rights.canCreateAccessRoles && (
                                <Tooltip title={t('tooltip.accessRole.copy')} placement="top">
                                    <FilterNone
                                        onClick={() => {
                                            general.addModal({
                                                title: t('modal.title.accessRole.copy'),
                                                body: t('modal.body.accessRole.copy'),
                                                buttonText: t('form.button.duplicate'),
                                                onConfirm: async () => {
                                                    await accessRoles.copy(id);
                                                    setTimeout(async () => {
                                                        accessRoles.resource.success =
                                                            'Toegangsrol succesvol gedupliceerd ';
                                                        accessRoles.resource.data = [];
                                                        accessRoles.resource.status = [];
                                                        await accessRoles.resource.fetch();
                                                    }, 1000);
                                                },
                                            });
                                        }}
                                    />
                                </Tooltip>
                            )}
                            {account.rights.canDeleteAccessRoles && (
                                <Tooltip title={t('tooltip.accessRole.delete')} placement="top">
                                    <Delete
                                        onClick={() => {
                                            general.addModal({
                                                title: t('modal.title.accessRole.delete'),
                                                body: t('modal.body.accessRole.delete'),
                                                buttonText: t('form.button.delete'),
                                                onConfirm: async () => {
                                                    await accessRoles.resource.delete(undefined, id);
                                                    accessRoles.resource.data = [];
                                                    accessRoles.resource.status = [];
                                                    await accessRoles.resource.fetch();
                                                },
                                            });
                                        }}
                                    />
                                </Tooltip>
                            )}
                        </ListRowActions>
                    )}
                </ListRow>
            </li>
        );
    }

    return (
        <>
            <ResourceOverview
                store={accessRoles.resource}
                rowRenderer={rowRenderer}
                heightOffset={250}
                actions={[
                    //TODO: check rights and remove level property
                    account.rights.canCreateAccessRoles && {
                        icon: 'add',
                        route: '/toegangsrollen/nieuw',
                        tooltip: t('tooltip.accessRole.create'),
                    },
                ]}
            />
        </>
    );
}

export default withTranslation()(observer(Rights));
