import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Page from 'components/Page.jsx';
import Form from 'components/Form';
import { Checkbox } from '@material-ui/core';
import useReactRouter from 'use-react-router';
import { withTranslation } from 'react-i18next';

function NewRight({ t }) {
    const { account, accessRoles } = useStore();
    const { match } = useReactRouter();

    useEffect(() => {
        async function fetchData() {
            await accessRoles.fetchRights();

            if (match.params?.id) {
                await accessRoles.fetchAccessRole(match.params.id);
            }
        }

        fetchData();

        return () => {
            account.rights.fetch();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page
            title={`${match.params?.id ? t('modal.title.accessRole.edit') : t('modal.title.accessRole.new')}`}
            loading={accessRoles.loading}
        >
            <Form
                store={accessRoles.resource}
                fullWidth
                onSave={() => {
                    accessRoles.resource.form.data.rights = [...accessRoles.rights];
                }}
            >
                {accessRoles.rights.length > 0 && (
                    <div className="rights">
                        <div className="rights__header">
                            <div>
                                <span style={{ minWidth: 200 }}>{t('form.labels.name')}</span>
                            </div>
                            <div>
                                <span>{t('actions.read')}</span>
                                <span>{t('actions.create')}</span>
                                <span>{t('actions.update')}</span>
                                <span>{t('actions.delete')}</span>
                                <span>{t('actions.access')}</span>
                            </div>
                        </div>
                        <div style={{ maxHeight: 'calc(100vh - 520px)', overflowY: 'auto' }}>
                            {accessRoles.rights.map((right, i) => (
                                <div key={`right_${i}`}>
                                    <div className="rights__right">
                                        <div>
                                            <span>{t(`right.${right.description}`)}</span>
                                        </div>
                                        <div>
                                            <span>
                                                <Checkbox
                                                    className={`right-${i}-read`}
                                                    checked={
                                                        !!right.read ||
                                                        right.subRights.some(({ access }) => !!access) ||
                                                        !!right.create ||
                                                        !!right.update ||
                                                        !!right.delete
                                                    }
                                                    onChange={e => {
                                                        if (!e.target.checked) {
                                                            right.subRights = right.subRights.map(subRight => ({
                                                                ...subRight,
                                                                access: false,
                                                            }));
                                                        }
                                                        right.read =
                                                            e.target.checked ||
                                                            right.subRights.some(({ access }) => !!access) ||
                                                            !!right.create ||
                                                            !!right.update ||
                                                            !!right.delete;
                                                    }}
                                                />
                                            </span>
                                            <span>
                                                <Checkbox
                                                    className={`right-${i}-create`}
                                                    checked={!!right.create}
                                                    onChange={e => {
                                                        e.target.checked && (right.read = true);
                                                        right.create = e.target.checked;
                                                    }}
                                                />
                                            </span>
                                            <span>
                                                <Checkbox
                                                    className={`right-${i}-update`}
                                                    checked={!!right.update}
                                                    onChange={e => {
                                                        e.target.checked && (right.read = true);
                                                        right.update = e.target.checked;
                                                    }}
                                                />
                                            </span>
                                            <span>
                                                <Checkbox
                                                    className={`right-${i}-delete`}
                                                    checked={!!right.delete}
                                                    onChange={e => {
                                                        e.target.checked && (right.read = true);
                                                        right.delete = e.target.checked;
                                                    }}
                                                />
                                            </span>
                                            <span></span>
                                        </div>
                                    </div>
                                    {right.subRights.map((subRight, j) => (
                                        <div
                                            className="rights__right"
                                            style={{ marginLeft: 50 }}
                                            key={`rights_${i}_subright_${j}`}
                                        >
                                            <div>
                                                <span>{t(`subright.${subRight.description}`)}</span>
                                            </div>
                                            <div>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span></span>
                                                <span>
                                                    <Checkbox
                                                        className={`right-${i}-subright-${j}`}
                                                        checked={!!subRight.access}
                                                        onChange={e => {
                                                            e.target.checked && (right.read = true);
                                                            subRight.access = e.target.checked;
                                                        }}
                                                    />
                                                </span>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>
                    </div>
                )}
            </Form>
        </Page>
    );
}

NewRight.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(observer(NewRight));
