import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import UserRow from '../../components/User/Row.jsx';
import Inputs from 'components/Inputs';
import useReactRouter from 'use-react-router';

function Filters() {
    return (
        <>
            <Inputs.users.filters.sex />
            <Inputs.users.filters.minAge />
            <Inputs.users.filters.maxAge />
        </>
    );
}

function UsersOverview() {
    const { users } = useStore();
    const { match } = useReactRouter();

    function rowRenderer({ key, index, style }) {
        const user = users.resource.filtered[index];
        return (
            <UserRow
                key={key}
                style={{ ...(style || {}), height: 'auto', width: '100%', margin: '10px 0px 10px 0px' }}
                onClick={() => {}}
                {...user}
            />
        );
    }

    return (
        <>
            <ResourceOverview
                apiPrefix={`/access_role/${match.params.id}`}
                store={users.resource}
                rowRenderer={rowRenderer}
                heightOffset={250}
                rowHeight={80}
                filters={<Filters />}
                sort={<Inputs.users.sort />}
            />
        </>
    );
}

export default observer(UsersOverview);
