/* istanbul ignore file */
import Page from 'components/Page';
import React from 'react';
import { withTranslation } from 'react-i18next';

function NotFound({ t }) {
    return (
        <Page>
            <div style={{ padding: '50px 0 100px' }}>
                <h1 style={{ textAlign: 'center', fontSize: '10em', marginBottom: 0 }}>404</h1>
                <h2 style={{ textAlign: 'center', fontSize: '2em', marginTop: 0, width: '100%' }}>
                    {t('errors.notFound')}
                </h2>
            </div>
        </Page>
    );
}

export default withTranslation()(NotFound);
