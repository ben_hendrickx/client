/* istanbul ignore file */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowBackIos, Delete, Add, Search, Close, ArrowDownward, ArrowUpward } from '@material-ui/icons';

import projectsOverviewStyle from '../../ClothingManagement/overview.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';

import Toolbar from '../../../components/Toolbar/Toolbar.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from '../../../components/CustomButtons/Button.jsx';
import ArticleOverview from '../../ClothingManagement/Articles/Overview.jsx';
import Inputs from 'components/Inputs';
import Address from 'components/Inputs/Address.jsx';
import ArticleRow from '../../ClothingManagement/Articles/Components/ArticleRow';
import Label from 'components/Label/Label.jsx';
import { NotificationManager } from 'react-notifications';

import moment from 'moment';
import { withTranslation } from 'react-i18next';

function LabelWithValue({ label, value, show = true }) {
    if (!show) {
        return null;
    }

    return (
        <div style={{ margin: '10px 0' }}>
            <Label value={`${label}:`} />
            {value || 'Niet ingevuld'}
        </div>
    );
}

function ProjectsOverview(props) {
    const { classes, t } = props;
    const { ui, clothingManagement, general, articles } = useStore();
    const { history, match } = useReactRouter();
    const [searchArticle, setSearchArticle] = useState(false);

    const { orders } = clothingManagement;
    const { resource } = orders;
    const {
        form: { data },
    } = resource;
    const {
        internal,
        type,
        name,
        socialSecurity,
        companyName,
        uniqueNumber,
        isFor,
        bankAccount,
        cellPhone,
        email,
        fromDt,
        toDt,
        periods,
        info,
        streetName,
        houseNumber,
        zipCode,
        cityName,
        items,
    } = data;

    useEffect(() => {
        if (match?.params?.id) {
            clothingManagement.getOrder(match.params.id);
        } else {
            clothingManagement.orders.resource.form.data.type = 2;
            clothingManagement.orders.resource.form.data.items = [];
        }

        setTimeout(() => {
            articles.resource.fetch();
        }, 500);

        function handler(e) {
            if (e.code && e.code.toLowerCase().includes('enter')) {
                clothingManagement
                    .addArticle(clothingManagement.articleId)
                    .catch(() => NotificationManager.error('Artikel bestaat niet'));

                clothingManagement.articleId = '';
            }
        }
        window.addEventListener('keyup', handler);

        return () => {
            window.removeEventListener('keyup', handler);
            clothingManagement.orders.resource.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        fromDt && periods && clothingManagement.getPrices();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [items?.length, fromDt, internal, periods]);

    useEffect(() => {
        if (clothingManagement.articleId) {
            articles.resource.offset = 0;
            articles.resource.batchSize = 1;
            articles.resource.filters.text = clothingManagement.articleId;
            articles.resource.fetchAndReturn().then(({ data: [item] }) => {
                clothingManagement.description = item?.description;
            });
        } else {
            delete clothingManagement.description;
        }
    }, [clothingManagement.articleId]);

    return (
        <>
            {searchArticle && (
                <div className={classes.overlay}>
                    <div style={{ position: 'relative', width: '80%', height: '80%', margin: 30 }}>
                        <div
                            style={{
                                position: 'absolute',
                                right: 0,
                                top: 0,
                                padding: 10,
                                cursor: 'pointer',
                            }}
                            onClick={() => setSearchArticle(false)}
                        >
                            <Close />
                        </div>
                        <ArticleOverview
                            offset={700}
                            onArticleClick={articleId => {
                                clothingManagement.addArticle(articleId);
                                setSearchArticle(false);
                            }}
                        />
                    </div>
                </div>
            )}
            <div className={classes.container}>
                {ui.isMobile && (
                    <div className={classes.header}>
                        <ArrowBackIos onClick={() => history.replace('/')} />
                        <span>{t('page.title.newOrder')}</span>
                    </div>
                )}
                <Toolbar title={t('page.title.newOrder')} actions={[]} />
                <div className={classes.content}>
                    <div style={{ display: 'flex', padding: '20px 30px' }}>
                        <div style={{ width: '29%', marginRight: '4%' }}>
                            <h1 style={{ fontSize: '1.6em', width: '100%' }}>{t('form.sections.order.renter')}</h1>
                            <Inputs.orders.type />
                            {process.env.REACT_APP_IS_FVD === 'true' && (
                                <Inputs.Input
                                    fullWidth
                                    label="Afkomstig van Geel"
                                    type="checkbox"
                                    value={internal}
                                    onChange={v => (data.internal = v)}
                                />
                            )}
                            {type === 1 && (
                                <>
                                    <Inputs.Input
                                        required
                                        fullWidth
                                        label={t('form.labels.name')}
                                        type="text"
                                        value={name}
                                        onChange={v => (data.name = v)}
                                    />
                                    <Inputs.Input
                                        fullWidth
                                        label={t('form.labels.socialSecurity')}
                                        type="text"
                                        value={socialSecurity}
                                        onChange={v => (data.socialSecurity = v)}
                                    />
                                </>
                            )}
                            {type === 2 && (
                                <>
                                    <Inputs.Input
                                        required
                                        fullWidth
                                        label={t('form.labels.order.associationName')}
                                        type="text"
                                        value={companyName}
                                        onChange={v => (data.companyName = v)}
                                    />
                                    <Inputs.Input
                                        required
                                        fullWidth
                                        label={t('form.labels.order.responsibleName')}
                                        type="text"
                                        value={name}
                                        onChange={v => (data.name = v)}
                                    />
                                    <Inputs.Input
                                        required
                                        fullWidth
                                        label={t('form.labels.order.companyNumber')}
                                        type="text"
                                        value={uniqueNumber}
                                        onChange={v => (data.uniqueNumber = v)}
                                    />
                                    <Inputs.Input
                                        fullWidth
                                        label={t('form.labels.order.clothingFor')}
                                        type="text"
                                        value={isFor}
                                        onChange={v => (data.isFor = v)}
                                    />
                                </>
                            )}
                            {type === 3 && (
                                <>
                                    <Inputs.Input
                                        required
                                        fullWidth
                                        label={t('form.labels.order.companyName')}
                                        type="text"
                                        value={companyName}
                                        onChange={v => (data.companyName = v)}
                                    />
                                    <Inputs.Input
                                        required
                                        fullWidth
                                        label={t('form.labels.order.responsibleName')}
                                        type="text"
                                        value={name}
                                        onChange={v => (data.name = v)}
                                    />
                                    <Inputs.Input
                                        required
                                        fullWidth
                                        label={t('form.labels.order.vatNumber')}
                                        type="text"
                                        value={uniqueNumber}
                                        onChange={v => (data.uniqueNumber = v)}
                                    />
                                    <Inputs.Input
                                        fullWidth
                                        label={t('form.labels.order.clothingFor')}
                                        type="text"
                                        value={isFor}
                                        onChange={v => (data.isFor = v)}
                                    />
                                </>
                            )}
                            <Inputs.Input
                                fullWidth
                                label={t('form.labels.order.bankAccount')}
                                type="text"
                                value={bankAccount}
                                onChange={v => (data.bankAccount = v)}
                            />
                        </div>
                        <div style={{ width: '29%', marginRight: '4%' }}>
                            <h1 style={{ fontSize: '1.6em', width: '100%' }}>{t('form.labels.contactDetails')}</h1>
                            <Inputs.Input
                                required
                                fullWidth
                                label={t('form.labels.cellPhone')}
                                type="text"
                                value={cellPhone}
                                onChange={v => (data.cellPhone = v)}
                            />
                            <Inputs.Input
                                required
                                fullWidth
                                label={t('form.labels.email')}
                                type="text"
                                value={email}
                                onChange={v => (data.email = v)}
                            />
                            <Address store={orders.resource.form.data} />
                        </div>
                        <div style={{ width: '33%' }}>
                            <h1 style={{ fontSize: '1.6em', width: '100%' }}>{t('form.sections.order.info')}</h1>
                            {match.params?.id && fromDt && (
                                <LabelWithValue
                                    label={t('form.labels.order.from')}
                                    value={moment(fromDt).format('DD/MM/YYYY')}
                                />
                            )}
                            {!match.params.id && (
                                <CustomInput
                                    type="date"
                                    labelText={`${t('form.labels.order.from')} *`}
                                    formControlProps={{
                                        error: !!fromDt && !fromDt?.isValid(),
                                        required: true,
                                        fullWidth: true,
                                    }}
                                    inputProps={{
                                        id: 'dob_FVD',
                                        maxDate: new Date(Date.now() + 100 * 365 * 24 * 60 * 60 * 1000),
                                        maxDateMessage:
                                            'Gelieve een datum te selecteren die maximum 100 jaar in de toekomst is',
                                        inputProps: {
                                            id: 'dob_FVD',
                                        },
                                        value: fromDt || null,
                                        onChange: date => (data.fromDt = date),
                                    }}
                                />
                            )}
                            <CustomInput
                                type="date"
                                labelText={`${t('form.labels.order.to')} *`}
                                formControlProps={{
                                    error: !!toDt && !toDt.isValid(),
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    id: 'dob_FVD',
                                    maxDate: new Date(Date.now() + 100 * 365 * 24 * 60 * 60 * 1000),
                                    maxDateMessage:
                                        'Gelieve een datum te selecteren die maximum 100 jaar in de toekomst is',
                                    inputProps: {
                                        id: 'dob_FVD',
                                    },
                                    value: toDt || null,
                                    onChange: date => (data.toDt = date),
                                }}
                            />
                            <CustomInput
                                type="number"
                                labelText={t('form.labels.order.periods')}
                                formControlProps={{
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    id: 'dob_FVD',
                                    inputProps: {
                                        id: 'dob_FVD',
                                    },
                                    value: periods || '',
                                    onChange: e => (data.periods = e.target.value),
                                }}
                            />
                            <CustomInput
                                labelText={t('form.labels.order.information')}
                                id="talents_FVD"
                                formControlProps={{
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    rows: 2,
                                    multiline: true,
                                    color: 'primary',
                                    type: 'text',
                                    autoComplete: 'off',
                                    value: info,
                                    onChange: e => (data.info = e.target.value),
                                }}
                            />
                        </div>
                    </div>
                </div>
                <div className={classes.content}>
                    <div>
                        <div className={classes.clothes}>
                            <h1>{t('resource.artikels')}</h1>
                            <div className={classes.addedClothes}>
                                {!items?.length && (
                                    <h1 style={{ textAlign: 'center', marginTop: 150, fontSize: '1.3em' }}>
                                        {t('empty.order.articles')}
                                    </h1>
                                )}
                                <ul className={classes.list} style={{ overflowY: 'auto', maxHeight: 400 }}>
                                    {items?.map(item => (
                                        <>
                                            <ArticleRow
                                                edit={false}
                                                article={item}
                                                actions={[
                                                    <span onClick={() => clothingManagement.removeItem(item.id)}>
                                                        <Delete />
                                                    </span>,
                                                    <>
                                                        {!item.showExtra && (
                                                            <span onClick={() => (item.showExtra = true)}>
                                                                <ArrowDownward />
                                                            </span>
                                                        )}
                                                        {item.showExtra && (
                                                            <span onClick={() => (item.showExtra = false)}>
                                                                <ArrowUpward />
                                                            </span>
                                                        )}
                                                    </>,
                                                ]}
                                            />
                                            {item.showExtra && (
                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        padding: '5px 20px',
                                                        background: 'white',
                                                        margin: '-30px 15px 15px',
                                                        boxShadow:
                                                            '0px 1px 3px 0px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 2px 1px -1px rgb(0 0 0 / 12%)',
                                                    }}
                                                >
                                                    {item.tags?.includes('accessoire') && (
                                                        <Inputs.Input
                                                            style={{ minWidth: 200, marginRight: 30 }}
                                                            label={t('form.labels.amount')}
                                                            type="number"
                                                            value={item.amount}
                                                            onChange={v => {
                                                                item.amount = v;
                                                                clothingManagement.getPrices();
                                                            }}
                                                        />
                                                    )}
                                                    <Inputs.Input
                                                        style={{ flexGrow: 1 }}
                                                        label={t('form.labels.remarks')}
                                                        type="text"
                                                        value={item.remarks}
                                                        onChange={v => (item.remarks = v)}
                                                    />
                                                </div>
                                            )}
                                        </>
                                    ))}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div style={{ display: 'block' }}>
                        <div style={{ marginLeft: 20 }}>
                            <h1 style={{ fontSize: '1.6em' }}>{t('form.sections.order.addArticle')}</h1>
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                <div>
                                    <div style={{ display: 'flex', alignItems: 'center' }}>
                                        <CustomInput
                                            labelText={t('form.labels.order.articleNumber')}
                                            formControlProps={{
                                                required: true,
                                                fullWidth: false,
                                            }}
                                            inputProps={{
                                                id: 'art_FVD',
                                                inputProps: {
                                                    id: 'art_FVD',
                                                },
                                                value: clothingManagement.articleId,
                                                onChange: e => (clothingManagement.articleId = e.target.value),
                                            }}
                                        />
                                        <Button
                                            disabled={
                                                !clothingManagement.articleId ||
                                                isNaN(parseInt(clothingManagement.articleId)) ||
                                                !parseInt(clothingManagement.articleId)
                                            }
                                            onClick={() => {
                                                clothingManagement
                                                    .addArticle(clothingManagement.articleId)
                                                    .catch(() => NotificationManager.error('Artikel bestaat niet'));

                                                clothingManagement.articleId = '';
                                            }}
                                            color="primary"
                                            size="sm"
                                            style={{
                                                outline: 'none',
                                                boxShadow: 'none',
                                                padding: '10px 5px 10px 10px',
                                                borderRadius: 0,
                                            }}
                                        >
                                            <Add />
                                        </Button>
                                    </div>
                                    {clothingManagement.description && (
                                        <small
                                            style={{
                                                display: 'block',
                                                marginTop: -17,
                                                fontSize: '15px',
                                                background: '#fdc300',
                                                padding: '5px 10px',
                                                color: 'white',
                                                borderRadius: '0 0 3px 3px',
                                            }}
                                        >
                                            <b>{clothingManagement.description}</b>
                                        </small>
                                    )}
                                </div>
                                <span>
                                    <b>{t('form.labels.or')}</b>
                                </span>
                                <Button
                                    color="primary"
                                    size="md"
                                    onClick={() => {
                                        general.addModal({
                                            type: 'window',
                                            width: 1100,
                                            body: (
                                                <ArticleOverview
                                                    offset={250}
                                                    onArticleClick={articleId => {
                                                        clothingManagement.addArticle(articleId);
                                                        NotificationManager.success(
                                                            'Artikel werd toegevoegd aan bestelling'
                                                        );
                                                    }}
                                                />
                                            ),
                                        });
                                    }}
                                >
                                    <Search style={{ marginRight: 5 }} /> {t('form.button.searchArticle')}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={classes.content}>
                    <div className={classes.clothes} style={{ minHeight: 'auto' }}>
                        <h1>{t('form.sections.order.prices')}</h1>
                        {(!data.fromDt || !data.periods) && <h2>{t('empty.order.dates')}</h2>}
                        {data.fromDt && data.periods && (
                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <div className={classes.prices}>
                                    <h2>
                                        {t('form.labels.priceExcl')}:<span>€ {(data.price || 0).toFixed(2)}</span>
                                    </h2>
                                    <h2>
                                        {t('form.labels.vat')}:<span>€ {((data.price || 0) * 0.21).toFixed(2)}</span>
                                    </h2>
                                    <h2>
                                        {t('form.labels.priceExcl')}:
                                        <span>€ {((data.price || 0) * 1.21).toFixed(2)}</span>
                                    </h2>
                                </div>
                                <div className={classes.prices}>
                                    <h2>
                                        {t('form.labels.replacementPriceExcl')}:
                                        <span>€ {(data.replacementPrice || 0).toFixed(2)}</span>
                                    </h2>
                                    <h2>
                                        {t('form.labels.replacementVat')}:
                                        <span>€ {((data.replacementPrice || 0) * 0.21).toFixed(2)}</span>
                                    </h2>
                                    <h2>
                                        {t('form.labels.replacementPriceIncl')}:
                                        <span>€ {((data.replacementPrice || 0) * 1.21).toFixed(2)}</span>
                                    </h2>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
                <div style={{ paddingLeft: 10 }}>
                    <Button
                        onClick={() =>
                            general.addModal({
                                title: t('form.button.rentArticles'),
                                body: t('modal.body.order.rent'),
                                onConfirm: async () => {
                                    const rentalId = match?.params?.id
                                        ? await clothingManagement.updateRental(match.params.id)
                                        : await clothingManagement.createRental();
                                    history.replace(
                                        '/retributiebeheer/bestellingen/detail/' + (rentalId || match.params?.id)
                                    );
                                },
                            })
                        }
                        color="success"
                        size="md"
                        disabled={
                            !name ||
                            !cellPhone ||
                            !email ||
                            !fromDt ||
                            !toDt ||
                            !periods ||
                            !streetName ||
                            !houseNumber ||
                            !zipCode ||
                            !cityName ||
                            !items?.length ||
                            (type === 3 && uniqueNumber.length < 9)
                        }
                    >
                        {t('form.button.rentArticles')}
                    </Button>
                    <Button style={{ marginLeft: 10 }} onClick={() => history.goBack()} color="primary" size="md">
                        {t('form.button.goBack')}
                    </Button>
                </div>
            </div>
        </>
    );
}

ProjectsOverview.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(projectsOverviewStyle)(observer(ProjectsOverview)));
