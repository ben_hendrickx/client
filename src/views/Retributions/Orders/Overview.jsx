/* istanbul ignore file */
import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Inputs from 'components/Inputs';
import OrderRow from './Components/OrderRow';
import { Checkbox } from '@material-ui/core';
import Button from 'components/CustomButtons/Button.jsx';
import { withTranslation } from 'react-i18next';

function Filters() {
    return (
        <>
            <Inputs.orders.filters.return />
            <Inputs.orders.filters.billed />
            <Inputs.orders.filters.returned />
        </>
    );
}

const ObservedCheckBox = observer(({ id }) => {
    const { clothingManagement } = useStore();

    return (
        <div style={{ padding: '6px 0' }}>
            <Checkbox
                checked={clothingManagement.orders.checked.includes(id)}
                onChange={e => {
                    if (!clothingManagement.orders.checked.includes(id)) {
                        clothingManagement.orders.checked.push(id);
                    } else {
                        clothingManagement.orders.checked.splice(clothingManagement.orders.checked.indexOf(id), 1);
                    }
                }}
            />
        </div>
    );
});

function OrdersOverview({ t }) {
    const { account, clothingManagement } = useStore();

    function rowRenderer({ key, index, style }) {
        const order = clothingManagement.orders.resource.filtered[index];

        return (
            <li style={{ ...style, display: 'flex' }} key={key}>
                {!order.billed && <ObservedCheckBox id={order.id} />}
                {!!order.billed && <div style={{ minWidth: 42 }}></div>}
                <OrderRow order={order} />
            </li>
        );
    }

    return (
        <ResourceOverview
            store={clothingManagement.orders.resource}
            rowRenderer={rowRenderer}
            heightOffset={430}
            renderBefore={
                <Button
                    onClick={async () => {
                        await Promise.all(
                            clothingManagement.orders.checked.map(id =>
                                clothingManagement.updateRental(id, { billed: true })
                            )
                        );
                        clothingManagement.orders.resource.data = [];
                        clothingManagement.orders.resource.status = [];
                        await clothingManagement.orders.resource.fetch();
                        clothingManagement.orders.checked = [];
                    }}
                    color="success"
                    size="sm"
                    disabled={!clothingManagement.orders.checked?.length}
                >
                    {t('form.button.markBilled')}
                </Button>
            }
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateOrders && {
                    icon: 'add',
                    route: '/retributiebeheer/bestellingen/nieuw',
                    tooltip: t('tooltip.order.create'),
                },
                {
                    icon: 'getApp',
                    href: '/api/reports/orders.xlsx',
                    tooltip: t('tooltip.report.download'),
                },
            ]}
            filters={<Filters />}
        />
    );
}

export default withTranslation()(observer(OrdersOverview));
