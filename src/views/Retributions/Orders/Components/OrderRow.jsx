/* istanbul ignore file */
import React from 'react';
import { useHistory } from 'react-router';
import ListRow from 'components/ListRow';
import { CheckCircle, Edit, KeyboardReturn, Payment, PictureAsPdf, RemoveRedEye } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { apiUrl, useStore } from '../../../../mobx';
import moment from 'moment';
import { withTranslation } from 'react-i18next';

function OrderRow({ key, style, order, t }) {
    const { account } = useStore();
    const history = useHistory();

    return (
        <li key={key} style={{ ...style, flexGrow: 1 }}>
            <ListRow key={key}>
                <div style={{ flexGrow: 1, display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                    <span style={{ width: 30 }}>{order.id}</span>
                    <span style={{ width: 300 }}>{order.name}</span>
                    <span style={{ width: 300 }}>
                        <KeyboardReturn style={{ marginRight: 10 }} />{' '}
                        {moment(order.toDt)
                            .add(14, 'd')
                            .format('DD-MM-YYYY')}
                    </span>
                    <span style={{ width: 100 }}>
                        <Payment style={{ marginRight: 10 }} /> € {(order.price * 1.21).toFixed(2)}
                    </span>
                    <span style={{ width: 100 }}>
                        <b style={{ marginRight: 10, fontSize: 13 }}>VW:</b> €{' '}
                        {(order.replacementPrice * 1.21).toFixed(2)}
                    </span>
                    <span style={{ width: 30 }}>{!!order.billed && <CheckCircle style={{ color: 'green' }} />}</span>
                </div>
                {account.rights.canReadOrders && (
                    <ListRowActions>
                        <Tooltip title={t('tooltip.order.view')} placement="top">
                            <RemoveRedEye
                                onClick={() => history.push('/retributiebeheer/bestellingen/detail/' + order.id)}
                            />
                        </Tooltip>
                        {!order.billed && (
                            <Tooltip title={t('tooltip.order.edit')} placement="top">
                                <Edit
                                    onClick={() => history.push('/retributiebeheer/bestellingen/bewerk/' + order.id)}
                                />
                            </Tooltip>
                        )}
                        <Tooltip title={t('tooltip.order.pdf')} placement="top">
                            <PictureAsPdf onClick={() => window.open(`${apiUrl}/api/order/${order.id}/pdf`)} />
                        </Tooltip>
                    </ListRowActions>
                )}
            </ListRow>
        </li>
    );
}

export default withTranslation()(OrderRow);
