/* istanbul ignore file */
import Inputs from 'components/Inputs';
import { useStore } from '../../../../mobx';
import { observer } from 'mobx-react-lite';
import React from 'react';

function ReceiveModal({ amount, onChange, text }) {
    const { clothingManagement } = useStore();

    return (
        <div>
            <p>
                {(text || 'Er zijn {amount} stuks uitgeleend, hoeveel ga je er nu in ontvangst nemen?').replace(
                    '{amount}',
                    amount
                )}
            </p>
            <Inputs.Input
                validations={[{ type: 'number', min: text ? 0 : 1, max: amount }]}
                type="number"
                value={clothingManagement.amount}
                onChange={v => {
                    clothingManagement.amount = v;
                    onChange();
                }}
            />
        </div>
    );
}

export default observer(ReceiveModal);
