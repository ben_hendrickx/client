/* istanbul ignore file */
/* eslint-disable eqeqeq */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { ArrowBackIos, CallMade, CallReceived, ClearAll, PictureAsPdf, Search } from '@material-ui/icons';

import projectsOverviewStyle from '../../ClothingManagement/overview.style.jsx';
import useReactRouter from 'use-react-router';
import { apiUrl, useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';

import Toolbar from 'components/Toolbar/Toolbar.jsx';
import moment from 'moment';
import Button from 'components/CustomButtons/Button.jsx';
import Loader from 'components/Loader.jsx';
import Label from 'components/Label/Label.jsx';
import ArticleRow from '../../ClothingManagement/Articles/Components/ArticleRow.jsx';
import { Checkbox, InputBase, Tooltip } from '@material-ui/core';
import ReceiveModal from './Modals/Receive.jsx';
import { Warning } from 'components/Notifications.jsx';
import Inputs from 'components/Inputs/index.jsx';
import { primaryColor } from 'assets/jss/material-kit-react.jsx';

function LabelWithValue({ label, value, show = true }) {
    if (!show) {
        return null;
    }

    return (
        <div style={{ margin: '10px 0' }}>
            <Label value={`${label}:`} />
            {value || 'Niet ingevuld'}
        </div>
    );
}

function pad(value, length) {
    if ((value || '').toString().length >= length) {
        return value;
    }

    return pad('0' + value, length);
}

function ProjectsOverview(props) {
    const { classes } = props;
    const { ui, clothingManagement, general } = useStore();
    const { history, match } = useReactRouter();

    function filterItems(item) {
        let isValid = true;

        if (clothingManagement.orderArticleFilter) {
            const values = [
                item.clothing.name,
                item.size.size,
                item.pattern.name,
                item.fabric.type,
                item.color.name,
                pad(item.id, 6),
                item.tags,
            ].join('');
            const pieces = clothingManagement.orderArticleFilter.split(' ').map(v => v.trim());
            isValid = pieces.every(piece => values.includes(piece));
        }

        if (isValid && [0, 1].includes(clothingManagement.returnedFilter)) {
            switch (clothingManagement.returnedFilter) {
                case 0:
                    isValid = item.amount - item.returned !== 0;
                    break;
                case 1:
                    isValid = item.amount - item.returned === 0;
                    break;
                default:
                    break;
            }
        }

        return isValid;
    }

    useEffect(() => {
        if (match && match.params && match.params.id) {
            clothingManagement.getOrder(match.params.id);
            clothingManagement.listen_single_order(match.params.id);
        }

        return () => {
            clothingManagement.orders.resource.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [match.params.id]);

    const { orders } = clothingManagement;

    const order = orders.resource.form.data;

    const typeLabel = order.type == 1 ? 'Particulier' : order.type == 2 ? 'Vereniging' : 'Bedrijf';

    return (
        <>
            <div className={classes.container}>
                {ui.isMobile && (
                    <div className={classes.header}>
                        <ArrowBackIos onClick={() => history.replace('/')} />
                        <span>Bestelling - {match.params.id}</span>
                    </div>
                )}
                <Toolbar title={`Bestelling nummer ${match.params.id}`} actions={[]} />
                <Loader loading={clothingManagement.loading}>
                    <div className={classes.content}>
                        <div className={classes.print}>
                            <PictureAsPdf onClick={() => window.open(`${apiUrl}/api/order/${match.params.id}/pdf`)} />
                        </div>
                        <div className={classes.details}>
                            <div style={{ display: 'flex', padding: '20px 30px' }}>
                                <div style={{ width: '29%', marginRight: '4%' }}>
                                    <h1 style={{ fontSize: '1.6em', width: '100%' }}>Info huurder</h1>
                                    <LabelWithValue label="Type huurder" value={typeLabel} />
                                    <LabelWithValue label="Afkomstig van Geel" value={order.internal ? 'Ja' : 'Neen'} />
                                    <LabelWithValue label="Naam" value={order.name} show={order.type == 1} />
                                    <LabelWithValue
                                        label="Naam vereniging"
                                        value={order.companyName}
                                        show={order.type == 2}
                                    />
                                    <LabelWithValue
                                        label="Naam bedrijf"
                                        value={order.companyName}
                                        show={order.type == 3}
                                    />
                                    <LabelWithValue
                                        label="Rijksregisternummer"
                                        value={order.socialSecurity}
                                        show={order.type == 1}
                                    />
                                    <LabelWithValue
                                        label="Naam verantwoordelijke"
                                        value={order.name}
                                        show={[2, 3].includes(order.type)}
                                    />
                                    <LabelWithValue
                                        label="Ondernemingsnummer"
                                        value={order.uniqueNumber}
                                        show={order.type == 2}
                                    />
                                    <LabelWithValue
                                        label="BTW-nummer"
                                        value={order.uniqueNumber}
                                        show={order.type == 3}
                                    />
                                    <LabelWithValue
                                        label="Kledij ten behoeve van"
                                        value={order.isFor}
                                        show={[2, 3].includes(order.type)}
                                    />
                                    <LabelWithValue
                                        label="BTW-nummer"
                                        value={order.uniqueNumber}
                                        show={order.type == 3}
                                    />
                                    <LabelWithValue label="Rekening-nummer" value={order.bankAccount} />
                                </div>
                                <div style={{ width: '29%', marginRight: '4%' }}>
                                    <h1 style={{ fontSize: '1.6em', width: '100%' }}>Contactgegevens</h1>
                                    <LabelWithValue label="G.S.M.-nummer" value={order.cellPhone} />
                                    <LabelWithValue label="Email" value={order.email} />
                                    <LabelWithValue
                                        label="Adres"
                                        value={`${order.streetName} ${order.houseNumber}, ${order.zipCode} ${order.cityName}`}
                                    />
                                </div>
                                <div style={{ width: '33%' }}>
                                    <h1 style={{ fontSize: '1.6em', width: '100%' }}>Bestellingsgegevens</h1>
                                    <LabelWithValue label="Vanaf" value={moment(order.fromDt).format('DD/MM/YYYY')} />
                                    <LabelWithValue
                                        label="Laatste speeldag"
                                        value={moment(order.toDt).format('DD/MM/YYYY')}
                                    />
                                    <LabelWithValue label="Periodes" value={order.periods} />
                                    <LabelWithValue label="Bijkomende informatie" value={order.info} />
                                </div>
                            </div>
                        </div>
                        <div className={classes.clothes}>
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                <h1>Kledingstukken</h1>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                    <Inputs.Input
                                        style={{ minWidth: 120, marginRight: 10, marginTop: -7 }}
                                        label="Teruggebracht"
                                        type="select"
                                        options={[{ label: 'Ja', value: 1 }, { label: 'Neen', value: 0 }]}
                                        value={clothingManagement.returnedFilter}
                                        onChange={e => (clothingManagement.returnedFilter = e)}
                                    />
                                    <div
                                        style={{
                                            maxWidth: 250,
                                            display: 'flex',
                                            alignItems: 'center',
                                            borderBottom: '1px solid rgba(0,0,0,.15)',
                                            marginRight: 10,
                                        }}
                                    >
                                        <InputBase
                                            placeholder="Zoek kledingstuk"
                                            inputProps={{ 'aria-label': 'Zoek kledingstuk' }}
                                            value={clothingManagement.orderArticleFilter}
                                            onChange={e => (clothingManagement.orderArticleFilter = e.target.value)}
                                        />
                                        <Search />
                                    </div>
                                    <div
                                        style={{
                                            marginLeft: 10,
                                            borderLeft: '1px solid rgba(0,0,0,.1)',
                                            paddingLeft: 10,
                                            marginTop: 8,
                                        }}
                                    >
                                        <Tooltip
                                            title="Verwijder filters"
                                            placement={window.innerWidth > 959 ? 'top' : 'left'}
                                        >
                                            <ClearAll
                                                htmlColor={primaryColor}
                                                fontSize="large"
                                                onClick={() => {
                                                    delete clothingManagement.returnedFilter;
                                                    clothingManagement.orderArticleFilter = '';
                                                }}
                                            />
                                        </Tooltip>
                                    </div>
                                </div>
                            </div>
                            <div className={classes.addedClothes}>
                                {!order.items?.filter(filterItems).length && (
                                    <h1 style={{ textAlign: 'center', marginTop: 150, fontSize: '1.3em' }}>
                                        Geen kledingstukken in deze bestelling
                                    </h1>
                                )}
                                <ul className={classes.list}>
                                    {order.items?.filter(filterItems).map(item => {
                                        const actions = [];

                                        if (item.returned !== item.amount) {
                                            actions.push(
                                                <Tooltip title="Artikel ontvangen" placement="top">
                                                    <CallReceived
                                                        onClick={async () => {
                                                            if (item.amount > 1) {
                                                                let modal;
                                                                clothingManagement.amount = item.amount - item.returned;
                                                                modal = general.addModal({
                                                                    title: 'Ontvangen van kledingstuk',
                                                                    width: 540,
                                                                    body: (
                                                                        <ReceiveModal
                                                                            amount={item.amount - item.returned}
                                                                            onChange={() => {
                                                                                modal.setDisabled(
                                                                                    clothingManagement.amount <= 0 ||
                                                                                        clothingManagement.amount >
                                                                                            item.amount - item.returned
                                                                                );
                                                                            }}
                                                                        />
                                                                    ),
                                                                    buttonText: 'Ontvang',
                                                                    onConfirm: async () => {
                                                                        await clothingManagement.receiveItem(
                                                                            match.params.id,
                                                                            item.id,
                                                                            true,
                                                                            item.tags?.includes('accessoire')
                                                                        );
                                                                        clothingManagement.amount = 1;
                                                                        setTimeout(
                                                                            () => clothingManagement.getPrices(),
                                                                            500
                                                                        );
                                                                    },
                                                                });
                                                                return;
                                                            }
                                                            await clothingManagement.receiveItem(
                                                                match.params.id,
                                                                item.id,
                                                                true,
                                                                item.tags?.includes('accessoire')
                                                            );
                                                            setTimeout(() => clothingManagement.getPrices(), 500);
                                                        }}
                                                    />
                                                </Tooltip>
                                            );
                                        }

                                        if (item.returned > 0) {
                                            actions.push(
                                                <Tooltip title="Artikel corrigeren" placement="top">
                                                    <CallMade
                                                        onClick={async () => {
                                                            if (item.amount > 1) {
                                                                let modal;
                                                                clothingManagement.amount = item.returned;
                                                                modal = general.addModal({
                                                                    title: 'Corrigeren van kledingstuk',
                                                                    width: 540,
                                                                    body: (
                                                                        <ReceiveModal
                                                                            text="Er zijn {amount} stuks teruggebracht, hoeveel zouden het er moeten zijn?"
                                                                            amount={item.returned}
                                                                            onChange={() => {
                                                                                modal.setDisabled(
                                                                                    clothingManagement.amount < 0 ||
                                                                                        clothingManagement.amount >
                                                                                            item.returned
                                                                                );
                                                                            }}
                                                                        />
                                                                    ),
                                                                    buttonText: 'Corrigeer',
                                                                    onConfirm: async () => {
                                                                        await clothingManagement.receiveItem(
                                                                            match.params.id,
                                                                            item.id,
                                                                            false,
                                                                            item.tags?.includes('accessoire')
                                                                        );
                                                                        setTimeout(
                                                                            () => clothingManagement.getPrices(),
                                                                            500
                                                                        );
                                                                        clothingManagement.amount = 1;
                                                                    },
                                                                });
                                                                return;
                                                            }
                                                            await clothingManagement.receiveItem(
                                                                match.params.id,
                                                                item.id,
                                                                false,
                                                                item.tags?.includes('accessoire')
                                                            );
                                                            setTimeout(() => clothingManagement.getPrices(), 500);
                                                        }}
                                                    />
                                                </Tooltip>
                                            );
                                        }

                                        return (
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    justifyContent: 'space-between',
                                                }}
                                            >
                                                <Checkbox
                                                    style={{ marginTop: -10 }}
                                                    checked={clothingManagement.selectedArticles.includes(item.id)}
                                                    onChange={e => {
                                                        if (e.target.checked) {
                                                            clothingManagement.selectedArticles.push(item.id);
                                                        } else {
                                                            clothingManagement.selectedArticles = clothingManagement.selectedArticles.filter(
                                                                id => id !== item.id
                                                            );
                                                        }
                                                    }}
                                                />
                                                <ArticleRow
                                                    style={{ margin: 0, flexGrow: 1, marginLeft: 10 }}
                                                    key={`article-${item.id}`}
                                                    article={item}
                                                    edit={false}
                                                    actions={actions}
                                                    returned={item.returned === item.amount}
                                                    amount
                                                />
                                            </div>
                                        );
                                    })}
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className={classes.content}>
                        <div className={classes.clothes} style={{ minHeight: 'auto' }}>
                            <h1>Prijzen</h1>
                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                <div className={classes.prices}>
                                    <h2>
                                        Prijs excl. B.T.W.:<span>€ {order.price}</span>
                                    </h2>
                                    <h2>
                                        Prijs B.T.W.:<span>€ {(order.price * 0.21).toFixed(2)}</span>
                                    </h2>
                                    <h2>
                                        Prijs incl. B.T.W.:<span>€ {(order.price * 1.21).toFixed(2)}</span>
                                    </h2>
                                </div>
                                <div className={classes.prices}>
                                    <h2>
                                        VW excl. B.T.W.:<span>€ {order.replacementPrice}</span>
                                    </h2>
                                    <h2>
                                        VW B.T.W.:<span>€ {(order.replacementPrice * 0.21).toFixed(2)}</span>
                                    </h2>
                                    <h2>
                                        VW incl. B.T.W.:<span>€ {(order.replacementPrice * 1.21).toFixed(2)}</span>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </Loader>
                <div style={{ paddingLeft: 10 }}>
                    {order.billed !== undefined && !order.billed && (
                        <Button
                            style={{ marginLeft: 10 }}
                            onClick={async () => {
                                await clothingManagement.updateRental(match.params.id, { billed: true });
                                await clothingManagement.getOrder(match.params.id);
                            }}
                            color="success"
                            size="md"
                        >
                            Markeer als gefactureerd
                        </Button>
                    )}
                    <Button
                        style={{ marginLeft: 10 }}
                        onClick={async () => {
                            general.addModal({
                                title: 'Ontvangen van kledingstukken',
                                width: 750,
                                body: (
                                    <>
                                        <Warning message="Let op! Als er meerdere stuks van een artikel zijn uitgeleend zullen deze allemaal in ontvangst worden genomen" />
                                        Bent u zeker dat u al deze kledingstukken in ontvangst wil nemen?
                                        <br />
                                        <br />
                                    </>
                                ),
                                buttonText: 'Ontvang kledingstukken',
                                onConfirm: async () => {
                                    await clothingManagement.receiveSelectedItems(match.params.id);
                                    clothingManagement.selectedArticles = [];
                                    setTimeout(() => clothingManagement.getPrices(), 500);
                                },
                            });
                        }}
                        color="success"
                        size="md"
                    >
                        Ontvang geselecteerde artikels
                    </Button>
                    <Button style={{ marginLeft: 10 }} onClick={() => history.goBack()} color="primary" size="md">
                        Ga terug
                    </Button>
                </div>
            </div>
        </>
    );
}

ProjectsOverview.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(projectsOverviewStyle)(observer(ProjectsOverview));
