/* istanbul ignore file */
import React, { useState, useEffect } from 'react';

import { withStyles } from '@material-ui/styles';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../../../mobx';

import Form from 'components/Form.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import { Danger } from 'components/Notifications';
import { Accessibility, Add, HowToReg, Search } from '@material-ui/icons';
import ResourceOverview from 'components/ResourceOverview';
import ArticleRow from 'views/ClothingManagement/Articles/Components/ArticleRow';
import ProjectUser from 'views/Retributions/Projects/Components/ProjectUser';
import { Checkbox, Tooltip } from '@material-ui/core';
import ListRow from 'components/ListRow';
import { Project } from 'components/Labels/Project';
import { Scene } from 'components/Labels/Scene';
import { Role } from 'components/Labels/Role';
import { Group } from 'components/Labels/Group';
import { primaryColor } from 'assets/jss/material-kit-react';
import ArticleOverview from 'views/ClothingManagement/Articles/Overview.jsx';
import { NotificationManager } from 'react-notifications';
import { withTranslation } from 'react-i18next';

const styles = {
    info: {
        background: '#fff',
        boxShadow:
            '0px 1px 2px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: 10,
        '@media (min-width: 960px)': {
            margin: '0 10px 0 0',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            padding: '0 20px',
        },
    },
    actions: {
        '& svg': {
            '&:hover': {
                cursor: 'pointer',
                color: primaryColor,
            },
        },
    },
};

const ScenesModal = ({ scenes }) => {
    const { projectUsers } = useStore();

    return (
        <>
            <p>Wenst u de wijzigingen ook door te voeren in volgende scènes</p>
            <ul style={{ listStyle: 'none', margin: 0, padding: 0, maxHeight: 400, overflowY: 'scroll' }}>
                {scenes.map(otherScene => (
                    <li style={{ display: 'flex', alignItems: 'center' }}>
                        <ObservedCheckBox uuid={otherScene.uuid} />
                        <ListRow>
                            <ProjectUser {...otherScene} />
                        </ListRow>
                    </li>
                ))}
            </ul>
            <Button
                color="primary"
                onClick={() => {
                    if (projectUsers.selectedRows?.length) {
                        projectUsers.selectedRows = [];
                    } else {
                        projectUsers.selectedRows = scenes.map(({ uuid }) => uuid);
                    }
                }}
            >
                (De-)selecteer alle
            </Button>
        </>
    );
};

const ObservedCheckBox = observer(scene => {
    const { projectUsers } = useStore();

    return (
        <div style={{ padding: '6px 0' }}>
            <Checkbox
                checked={projectUsers.selectedRows.includes(scene.uuid)}
                onChange={e => {
                    if (!projectUsers.selectedRows.includes(scene.uuid)) {
                        projectUsers.selectedRows.push(scene.uuid);
                    } else {
                        projectUsers.selectedRows.splice(projectUsers.selectedRows.indexOf(scene.uuid), 1);
                    }
                }}
            />
        </div>
    );
});

function UserArticles(props) {
    const { classes, projectUser, t } = props;
    const [error, setError] = useState(false);
    const { general, projectUsers, articles } = useStore();

    const formData = projectUsers.articles.data;
    const isDisabled = !formData.articleId && !formData.statusId;

    function rowRenderer({ key, index, style }) {
        const article = projectUsers.articles.resource.data[index] || {
            clothing: {},
            color: {},
            pattern: {},
            fabric: {},
            rack: {},
            clothingSize: {},
        };

        return (
            <ArticleRow
                key={key}
                style={style}
                article={article}
                edit={false}
                onDelete={async uuid => {
                    const scenes = await projectUsers.articles.getOtherScenes(projectUser, uuid);

                    if (!scenes.length) {
                        general.addModal({
                            title: `Artikel verwijderen`,
                            body: 'Bent u zeker dat u dit artikel wilt verwijderen?',
                            buttonText: 'Verwijder',
                            onConfirm: async () => {
                                await projectUsers.articles.delete(projectUser, uuid, async () => {
                                    projectUsers.articles.resource.data = [];
                                    projectUsers.articles.resource.status = [];
                                    await projectUsers.articles.resource.fetch(`/project_user/${projectUser.uuid}`);
                                    NotificationManager.success('Artikel werd verwijderd');
                                });
                            },
                        });
                        return;
                    }

                    general.addModal({
                        title: 'Verwijderen uit andere scènes',
                        width: 1100,
                        body: <ScenesModal scenes={scenes} />,
                        onConfirm: async () => {
                            projectUsers.articles.data.other = projectUsers.selectedRows;
                            await projectUsers.articles.delete(projectUser, uuid, () => {
                                projectUsers.selectedRows = [];
                                projectUsers.articles.resource.data = [];
                                projectUsers.articles.resource.status = [];
                                projectUsers.articles.resource.fetch(`/project_user/${projectUser.uuid}`);
                                NotificationManager.success('Artikel werd verwijderd');
                            });
                        },
                    });
                }}
            />
        );
    }

    async function addArticle(ignoreDisabled = false) {
        if (!ignoreDisabled && isDisabled) {
            setError('Gelieve een artikel-nummer of status in te vullen');
        } else {
            setError(false);
            const scenes = await projectUsers.articles.getOtherScenes(projectUser);

            if (scenes.length) {
                general.addModal({
                    title: 'Doorvoeren in andere scènes',
                    width: 1100,
                    body: <ScenesModal scenes={scenes} />,
                    onConfirm: async () => {
                        projectUsers.articles.data.other = projectUsers.selectedRows;
                        await projectUsers.articles.add(projectUser, () => {
                            projectUsers.selectedRows = [];
                            projectUsers.articles.resource.data = [];
                            projectUsers.articles.resource.status = [];
                            projectUsers.articles.resource.fetch(`/project_user/${projectUser.uuid}`);
                            NotificationManager.success('Artikel werd toegevoegd');
                        });
                    },
                });
            } else {
                await projectUsers.articles.add(projectUser, () => {
                    projectUsers.articles.resource.data = [];
                    projectUsers.articles.resource.status = [];
                    projectUsers.articles.resource.fetch(`/project_user/${projectUser.uuid}`);
                    NotificationManager.success('Artikel werd toegevoegd');
                });
            }
        }
    }

    useEffect(() => {
        if (projectUsers.articles.data.articleId) {
            articles.resource.offset = 0;
            articles.resource.batchSize = 1;
            articles.resource.filters.text = projectUsers.articles.data.articleId;
            articles.resource.fetchAndReturn().then(({ data: [item] }) => {
                projectUsers.articles.form.data.description = item?.description;
            });
        } else {
            delete projectUsers.articles.form.data.description;
        }
    }, [projectUsers.articles.data.articleId]);

    useEffect(() => {
        function handler(e) {
            if (e.code && e.code.toLowerCase().includes('enter')) {
                addArticle();
            }
        }
        window.addEventListener('keyup', handler);

        return () => {
            window.removeEventListener('keyup', handler);
        };
    }, [isDisabled]);

    useEffect(() => {
        return () => {
            projectUsers.articles.form.reset();
        };
    }, []);

    return (
        <>
            {error && <Danger message={error} onClose={() => setError(false)} />}
            <div
                className={classes.info}
                style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}
            >
                <div
                    style={{
                        display: 'flex',
                        justifyContent: 'flex-start',
                        alignItems: 'center',
                        paddingTop: 10,
                        marginBottom: 10,
                        gap: 30,
                    }}
                >
                    <Project name={projectUser.projectName} />
                    <Scene identifier={projectUser.sceneIdentifier} name={projectUser.sceneName} />
                    {projectUser.roleIdentifier && (
                        <Role identifier={projectUser.roleIdentifier} name={projectUser.roleName} />
                    )}
                    {projectUser.groupIdentifier && (
                        <Group identifier={projectUser.groupIdentifier} name={projectUser.groupName} />
                    )}
                    <span style={{ marginLeft: -20 }}>(Cast: {projectUser.castName})</span>
                </div>
                <div className={classes.actions} style={{ display: 'flex', gap: 10 }}>
                    <Tooltip title="Moet komen passen" placement="top">
                        <Accessibility
                            style={projectUser.status === 1 ? { color: primaryColor } : {}}
                            onClick={() => {
                                projectUsers.fit(projectUser.uuid);
                                projectUser.status = 1;
                                projectUsers.resource.fetch();
                            }}
                        />
                    </Tooltip>
                    <Tooltip title="Kleding volledig klaar" placement="top">
                        <HowToReg
                            style={projectUser.status === 2 ? { color: primaryColor } : {}}
                            onClick={() => {
                                projectUsers.ready(projectUser.uuid);
                                projectUser.status = 2;
                                projectUsers.resource.fetch();
                            }}
                        />
                    </Tooltip>
                </div>
            </div>
            <Form
                noCancel
                store={projectUsers.articles}
                confirmButton={
                    <>
                        <Button color={isDisabled ? 'disabled' : 'success'} onClick={addArticle} size="md">
                            <Add style={{ marginRight: '5px' }} />
                            <span style={{ marginTop: '1px', marginLeft: '5px' }}>Voeg toe</span>
                        </Button>
                        <Button
                            color="primary"
                            size="md"
                            onClick={() => {
                                general.addModal({
                                    type: 'window',
                                    width: 1100,
                                    body: (
                                        <ArticleOverview
                                            offset={250}
                                            onArticleClick={async articleId => {
                                                projectUsers.articles.data.articleId = articleId;
                                                addArticle(true);
                                            }}
                                        />
                                    ),
                                });
                            }}
                        >
                            <Search style={{ marginRight: 5 }} /> {t('form.button.searchArticle')}
                        </Button>
                    </>
                }
            />
            <ResourceOverview
                backend
                apiPrefix={`/project_user/${projectUser.uuid}`}
                store={projectUsers.articles.resource}
                rowRenderer={rowRenderer}
                heightOffset={470}
            />
        </>
    );
}

export default withTranslation()(withStyles(styles)(observer(UserArticles)));
