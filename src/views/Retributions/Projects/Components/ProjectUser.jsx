/* istanbul ignore file */
import React from 'react';
import { ArtTrack, CameraRoll, FolderOpen, Group, Person } from '@material-ui/icons';
import { Tooltip } from '@material-ui/core';

function ProjectUser(props) {
    return (
        <>
            {props.projectName && (
                <Tooltip interactive placement="top" title={`${props.projectName}`}>
                    <span style={{ width: 200 }}>
                        <FolderOpen />
                        <span
                            style={{ width: 165, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
                        >
                            {props.projectName}
                        </span>
                    </span>
                </Tooltip>
            )}
            {props.sceneName && (
                <Tooltip interactive placement="top" title={`${props.sceneIdentifier} - ${props.sceneName}`}>
                    <span style={{ width: 250 }}>
                        <CameraRoll />
                        <span
                            style={{ width: 215, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
                        >
                            {props.sceneIdentifier} - {props.sceneName}
                        </span>
                    </span>
                </Tooltip>
            )}
            {props.roleName && (
                <Tooltip
                    interactive
                    placement="top"
                    title={`${props.roleIdentifier} - ${props.roleName} (Cast: ${props.castName})`}
                >
                    <span style={{ width: 250 }}>
                        <ArtTrack />
                        <span
                            style={{ width: 215, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
                        >
                            {props.roleIdentifier} - {props.roleName}{' '}
                            {props.castName ? `(Cast: ${props.castName})` : ''}
                        </span>
                    </span>
                </Tooltip>
            )}
            {props.groupName && (
                <Tooltip
                    interactive
                    placement="top"
                    title={`${props.groupIdentifier} - ${props.groupName} (Cast: ${props.castName})`}
                >
                    <span style={{ width: 250 }}>
                        <Group />
                        <span
                            style={{ width: 215, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
                        >
                            {props.groupIdentifier} - {props.groupName}{' '}
                            {props.castName ? `(Cast: ${props.castName})` : ''}
                        </span>
                    </span>
                </Tooltip>
            )}
            {props.firstName && (
                <Tooltip
                    interactive
                    placement="top"
                    title={
                        <>
                            <h1 style={{ fontWeight: 'bold', fontSize: '1.6em', margin: '10px 0 5px' }}>
                                {props.lastName} {props.firstName}
                            </h1>
                            <h1 style={{ fontSize: '1.6em', margin: '10px 0 5px' }}>Kledijgegevens</h1>
                            <div style={{ fontSize: '1.2em', lineHeight: '1.3em' }}>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span>Lichaamslengte:</span>
                                    <span>{props.height || 'Niet ingevuld'}</span>
                                </div>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span>Borstomtrek:</span>
                                    <span>{props.chest || 'Niet ingevuld'}</span>
                                </div>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span>Taille:</span>
                                    <span>{props.waist || 'Niet ingevuld'}</span>
                                </div>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span>Heupomtrek:</span>
                                    <span>{props.hipSize || 'Niet ingevuld'}</span>
                                </div>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span>Hoofdomtrek:</span>
                                    <span>{props.headSize || 'Niet ingevuld'}</span>
                                </div>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span>Kledingmaat:</span>
                                    <span>{props.clothingSize || 'Niet ingevuld'}</span>
                                </div>
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span>T-shirtmaat:</span>
                                    <span>{props.shirtSize || 'Niet ingevuld'}</span>
                                </div>
                            </div>
                            <h1 style={{ fontSize: '1.6em', margin: '10px 0 5px' }}>Contactgegevens</h1>
                            <div style={{ fontSize: '1.2em', lineHeight: '1.3em' }}>
                                {props.phoneNumber && (
                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                        <span>Telefoonnummer:</span>
                                        <span>{props.phoneNumber}</span>
                                    </div>
                                )}
                                {props.cellPhone && (
                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                        <span>G.S.M.-nummer:</span>
                                        <span>{props.cellPhone}</span>
                                    </div>
                                )}
                                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span style={{ marginRight: 15 }}>E-mailadres:</span>
                                    <span>{props.email}</span>
                                </div>
                                {props.secondaryEmail && (
                                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                        <span style={{ marginRight: 15 }}>Tweede e-mailadres:</span>
                                        <span>{props.secondaryEmail}</span>
                                    </div>
                                )}
                            </div>
                        </>
                    }
                >
                    <span style={{ width: 150 }}>
                        <Person />
                        <span
                            style={{ width: 115, overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}
                        >
                            {props.lastName} {props.firstName}
                        </span>
                    </span>
                </Tooltip>
            )}
        </>
    );
}

export default ProjectUser;
