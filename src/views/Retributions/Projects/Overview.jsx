/* istanbul ignore file */
import React, { useEffect } from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import ListRow from 'components/ListRow';
import { observer } from 'mobx-react-lite';
import { Accessibility, Delete, Edit, HowToReg, List } from '@material-ui/icons';
import Inputs from 'components/Inputs';
import { Checkbox, Tooltip } from '@material-ui/core';
import ListRowActions from 'components/ListRowActions';
import Button from 'components/CustomButtons/Button.jsx';
import UserArticlesForm from './Forms/UserArticles.jsx';
import ProjectUser from './Components/ProjectUser';
import { withTranslation } from 'react-i18next';

const ObservedCheckBox = observer(({ id }) => {
    const { projectUsers } = useStore();

    return (
        <div style={{ padding: '6px 0' }}>
            <Checkbox
                checked={projectUsers.resource.checked.includes(id)}
                onChange={e => {
                    if (!projectUsers.resource.checked.includes(id)) {
                        projectUsers.resource.checked.push(id);
                    } else {
                        projectUsers.resource.checked.splice(projectUsers.resource.checked.indexOf(id), 1);
                    }
                }}
            />
        </div>
    );
});

function ProjectUsersOverview({ t }) {
    const { projectUsers, general } = useStore();

    useEffect(() => {
        projectUsers.resource.checked = [];
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        projectUsers.resource.filters.project,
        projectUsers.resource.filters.scene,
        projectUsers.resource.filters.role,
        projectUsers.resource.filters.group,
        projectUsers.resource.filters.cast,
        projectUsers.resource.sort,
        projectUsers.resource.text,
    ]);

    function rowRenderer({ key, index, style, checkable = true }) {
        const projectUser = projectUsers.resource.data[index];
        const background = projectUser?.status === 1 ? '#ffe076' : projectUser?.status === 2 ? '#afffaf' : '#ffffff';

        return (
            <li key={key} style={{ ...style, display: 'flex' }}>
                {checkable && <ObservedCheckBox id={projectUser?.uuid} />}
                <ListRow style={{ flexGrow: 1, background }}>
                    <ProjectUser {...projectUser} />
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <Tooltip placement="top" title="Aantal toegewezen kledingstukken">
                            <span>
                                <List />
                                <span style={{ width: 24, margin: 0 }}>{projectUser?.count || 0}</span>
                            </span>
                        </Tooltip>
                        {!projectUser?.status && <span style={{ width: 24, margin: 0 }}></span>}
                        {projectUser?.status === 1 && (
                            <Tooltip placement="top" title="Moet komen passen">
                                <Accessibility />
                            </Tooltip>
                        )}
                        {projectUser?.status === 2 && (
                            <Tooltip placement="top" title="Kleding klaar voor rol">
                                <HowToReg />
                            </Tooltip>
                        )}
                        <ListRowActions style={{ marginLeft: 15 }}>
                            <Tooltip title="Bewerk deelnemer" placement="top">
                                <Edit
                                    className="edit"
                                    onClick={() => {
                                        general.addModal({
                                            type: 'window',
                                            width: 1100,
                                            title: `Kleding voor ${projectUser.lastName} ${projectUser.firstName}`,
                                            body: <UserArticlesForm projectUser={projectUser} />,
                                        });
                                    }}
                                />
                            </Tooltip>
                            <Tooltip title="Verwijder deelnemer" placement="top">
                                <Delete
                                    onClick={() => {
                                        general.addModal({
                                            title: `Verwijder deelnemer`,
                                            body: `Bent u zeker dat u deze deelnemer wilt verwijderen`,
                                            buttonText: 'Verwijder',
                                            onConfirm: async () => {
                                                await projectUsers.resource.delete(undefined, projectUser.uuid);
                                                projectUsers.resource.data = [];
                                                projectUsers.resource.status = [];
                                                await projectUsers.resource.fetch();
                                            },
                                        });
                                    }}
                                />
                            </Tooltip>
                        </ListRowActions>
                    </div>
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            checkable
            export
            backend
            store={projectUsers.resource}
            rowRenderer={rowRenderer}
            heightOffset={450}
            actions={[
                //TODO: add rights
                {
                    icon: 'getApp',
                    href: '/api/reports/project_users_with_articles.xlsx',
                    tooltip: 'Download uitgebreid rapport',
                },
            ]}
            filters={
                <>
                    <Inputs.projectUsers.filters.project />
                    <Inputs.projectUsers.filters.scene />
                    <Inputs.projectUsers.filters.role />
                    <Inputs.projectUsers.filters.group />
                    <Inputs.projectUsers.filters.cast />
                </>
            }
            sort={<Inputs.projectUsers.sort />}
            renderBefore={
                projectUsers.resource.filtered.length > 0 && (
                    <div style={{ marginBottom: 10 }}>
                        <Button
                            disabled={!projectUsers.resource.checked.length}
                            onClick={async () => {
                                let body = 'Bent u zeker dat u deze deelnemer wilt verwijderen?';
                                if (projectUsers.resource.checked.length > 1) {
                                    body = 'Bent u zeker dat u deze deelnemers wilt verwijderen?';
                                }
                                general.addModal({
                                    title: 'Verwijder deelnemers',
                                    body,
                                    buttonText: 'Verwijder',
                                    onConfirm: async () => {
                                        await projectUsers.resource.delete(undefined, projectUsers.resource.checked);
                                        projectUsers.resource.data = [];
                                        projectUsers.resource.status = [];
                                        projectUsers.resource.fetch();
                                    },
                                });
                            }}
                            color="danger"
                            size="sm"
                        >
                            <Delete /> Verwijder geselecteerde deelnemers
                        </Button>
                    </div>
                )
            }
        />
    );
}

export default withTranslation()(observer(ProjectUsersOverview));
