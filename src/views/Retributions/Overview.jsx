/* istanbul ignore file */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Category, ShoppingBasket, FolderOpen } from '@material-ui/icons';

import { useHistory } from 'react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import TabView from 'components/TabView.jsx';

import NewOrder from 'views/Retributions/Orders/New.jsx';
import OrdersOverview from 'views/Retributions/Orders/Overview.jsx';
import OrderDetail from 'views/Retributions/Orders/Detail.jsx';
import CategoryOverview from 'views/Retributions/Categories/Overview.jsx';
import ClothingProjectsOverview from 'views/Retributions/Projects/Overview.jsx';
import NewCategory from 'views/Retributions/Categories/New.jsx';
import { Success } from 'components/Notifications.jsx';
import Page from 'components/Page.jsx';
import { withTranslation } from 'react-i18next';

function ProjectsOverview({ t }) {
    const { account, clothingManagement } = useStore();
    const history = useHistory();

    const { retributions = {} } = clothingManagement;

    useEffect(() => {
        return () => {
            clothingManagement.success = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page title={t('page.title.renting')} loading={retributions.loading}>
            <Success message={retributions.success} onClose={() => (retributions.success = '')} />
            <TabView
                tabs={[
                    account.rights.canReadOrders && {
                        label: t('resource.bestellingen'),
                        icon: <ShoppingBasket />,
                        onClick: () => history.push(`/retributiebeheer/bestellingen`),
                    },
                    account.rights.canReadCategories && {
                        label: t('resource.categorieën'),
                        icon: <Category />,
                        onClick: () => history.push(`/retributiebeheer/categorieen`),
                    },
                    {
                        label: t('page.title.clothingProjects'),
                        as: 'projecten',
                        icon: <FolderOpen />,
                        onClick: () => history.push(`/retributiebeheer/projecten`),
                    },
                ]}
                actions={[]}
                routes={[
                    { path: '/retributiebeheer/categorieen/nieuw', component: NewCategory },
                    { path: '/retributiebeheer/categorieen/:id/bewerk', component: NewCategory },
                    { path: '/retributiebeheer/categorieen', component: CategoryOverview },
                    { path: '/retributiebeheer/bestellingen', component: OrdersOverview },
                    { path: '/retributiebeheer/bestellingen/nieuw', component: NewOrder },
                    { path: '/retributiebeheer/bestellingen/bewerk/:id', component: NewOrder },
                    { path: '/retributiebeheer/bestellingen/detail/:id', component: OrderDetail },
                    { path: '/retributiebeheer/projecten', component: ClothingProjectsOverview },
                ]}
            />
        </Page>
    );
}

ProjectsOverview.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(observer(ProjectsOverview));
