/* istanbul ignore file */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { Save } from '@material-ui/icons';

import newProjectStyle from '../../ClothingManagement/overview.style.jsx';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';

import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Page from 'components/Page.jsx';

import useReactRouter from 'use-react-router';
import FilterBar from 'components/FilterBar/FilterBar.jsx';
import { AutoSizer, List } from 'react-virtualized';
import ListWrapper from 'components/ListWrapper.jsx';
import ListRow from 'components/ListRow.jsx';
import moment from 'moment';
import Inputs from 'components/Inputs/index.jsx';
import { Success } from 'components/Notifications.jsx';
import { withTranslation } from 'react-i18next';

const PriceForm = withTranslation()(
    observer(function PriceForm({ t, dates = false, onConfirm }) {
        const { clothingManagement } = useStore();
        const { categories } = clothingManagement;

        return (
            <>
                {dates && (
                    <>
                        <div>
                            <Inputs.Input
                                required
                                fullWidth
                                label={t('form.labels.validFrom')}
                                type="date"
                                value={categories.fromDt}
                                onChange={v => (categories.fromDt = v)}
                            />
                        </div>
                        <div>
                            <Inputs.Input
                                fullWidth
                                label={t('form.labels.validUntil')}
                                type="date"
                                value={categories.toDt}
                                onChange={v => (categories.toDt = v)}
                            />
                        </div>
                    </>
                )}
                {process.env.REACT_APP_IS_FVD === 'true' && (
                    <div>
                        <Inputs.Input
                            required
                            fullWidth
                            label="Prijs/maand Geelse vereniging"
                            type="number"
                            value={categories.internalPrice}
                            onChange={v => (categories.internalPrice = v)}
                        />
                    </div>
                )}
                {process.env.REACT_APP_IS_FVD === 'true' && (
                    <div>
                        <Inputs.Input
                            required
                            fullWidth
                            label="Prijs/maand niet-Geelse vereniging"
                            type="number"
                            value={categories.externalPrice}
                            onChange={v => (categories.externalPrice = v)}
                        />
                    </div>
                )}
                {process.env.REACT_APP_IS_FVD === 'false' && (
                    <div>
                        <Inputs.Input
                            required
                            fullWidth
                            label={t('form.labels.price')}
                            type="number"
                            value={categories.externalPrice}
                            onChange={v => (categories.externalPrice = v)}
                        />
                    </div>
                )}
                <div>
                    <Inputs.Input
                        required
                        fullWidth
                        label={t('form.labels.replacementPrice')}
                        type="number"
                        value={categories.replacementPrice}
                        onChange={v => (categories.replacementPrice = v)}
                    />
                </div>
                {dates && (
                    <Button
                        disabled={
                            !categories.fromDt ||
                            !categories.internalPrice ||
                            !categories.externalPrice ||
                            !categories.replacementPrice
                        }
                        onClick={onConfirm}
                        color="success"
                        size="md"
                    >
                        {t('form.button.save')}
                    </Button>
                )}
            </>
        );
    })
);

function NewCategory(props) {
    const { classes, t } = props;
    const { history, match } = useReactRouter();
    const { clothingManagement, general } = useStore();

    const { categories } = clothingManagement;

    useEffect(() => {
        if (match?.params?.id) {
            categories.fetchCategory(match.params.id);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <Page
                title={match?.params?.id ? t('modal.title.category.edit') : t('modal.title.category.new')}
                loading={categories.loading}
            >
                <div className={classes.content}>
                    <div style={{ display: 'block' }}>
                        <div>
                            <CustomInput
                                labelText={t('form.labels.name')}
                                id="cast"
                                formControlProps={{
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    type: 'text',
                                    autoComplete: 'off',
                                    value: categories.name,
                                    onChange: e => (categories.name = e.target.value),
                                }}
                            />
                        </div>
                        {!match?.params?.id && <PriceForm />}
                        <Button
                            disabled={
                                match?.params?.id
                                    ? !categories.name
                                    : !categories.name ||
                                      !categories.internalPrice ||
                                      !categories.externalPrice ||
                                      !categories.replacementPrice
                            }
                            onClick={async () => {
                                !match?.params?.id ? await categories.create() : await categories.save(match.params.id);
                                history.replace(`/retributiebeheer/categorieen`);
                            }}
                            color="success"
                            size="md"
                        >
                            <Save style={{ marginRight: '5px' }} />
                            <span style={{ marginTop: '1px', marginLeft: '5px' }}>{t('form.button.save')}</span>
                        </Button>
                        <Button
                            onClick={() => {
                                history.replace(`/retributiebeheer/categorieen`);
                            }}
                            style={{ marginLeft: '20px' }}
                            color="white"
                            size="md"
                        >
                            {t('form.button.cancel')}
                        </Button>
                    </div>
                </div>
                {match?.params?.id && (
                    <Page
                        title={t('form.sections.order.prices')}
                        toolbar={{
                            actions: [
                                {
                                    icon: 'add',
                                    tooltip: t('tooltip.category.price.create'),
                                    onClick: () => {
                                        let modal;
                                        modal = general.addModal({
                                            title: t('modal.title.category.price.create'),
                                            body: (
                                                <PriceForm
                                                    dates
                                                    onConfirm={async () => {
                                                        await categories.createPrice(match.params?.id);
                                                        categories.fetchCategory(match.params?.id);
                                                        modal.close();
                                                    }}
                                                />
                                            ),
                                            buttonText: t('form.button.save'),
                                        });
                                    },
                                },
                            ],
                        }}
                    >
                        <Success message={categories.success} onClose={() => (categories.success = '')} />
                        <FilterBar total={categories.prices?.length} filtered={categories.filteredPrices?.length}>
                            <Inputs.Input
                                label={t('form.labels.active')}
                                style={{ marginRight: 20 }}
                                type="select"
                                value={categories.priceFilters.active}
                                onChange={v => (categories.priceFilters.active = v)}
                                options={[
                                    {
                                        label: t('form.labels.yes'),
                                        value: 1,
                                    },
                                    {
                                        label: t('form.labels.no'),
                                        value: 0,
                                    },
                                ]}
                            />
                        </FilterBar>
                        {categories.filteredPrices?.length === 0 && (
                            <h2 style={{ textAlign: 'center', marginTop: 40, paddingBottom: 40 }}>
                                {t('empty.prices')}
                            </h2>
                        )}
                        {categories.filteredPrices?.length > 0 && (
                            <AutoSizer>
                                {({ height, width }) => (
                                    <ListWrapper>
                                        <List
                                            width={width}
                                            height={categories.filteredPrices?.length * 64}
                                            rowCount={categories.filteredPrices?.length}
                                            rowHeight={64}
                                            rowRenderer={({ key, style, index }) => {
                                                const price = categories.filteredPrices[index];

                                                if (!price) {
                                                    return null;
                                                }

                                                return (
                                                    <li key={key} style={style}>
                                                        <ListRow key={key}>
                                                            <span style={{ fontSize: 14 }}>
                                                                <b style={{ marginRight: 10 }}>Geldig vanaf:</b>{' '}
                                                                {moment(price.startDt).format('DD/MM/YYYY')}
                                                            </span>
                                                            <span style={{ fontSize: 14 }}>
                                                                <b style={{ marginRight: 10 }}>Geldig tot:</b>{' '}
                                                                {price.endDt
                                                                    ? moment(price.endDt).format('DD/MM/YYYY')
                                                                    : 'N.V.T.'}
                                                            </span>
                                                            {process.env.REACT_APP_IS_FVD === 'true' && (
                                                                <span style={{ fontSize: 14 }}>
                                                                    <b style={{ marginRight: 10 }}>Binnen Geel:</b> €{' '}
                                                                    {price.internalPrice}
                                                                </span>
                                                            )}
                                                            {process.env.REACT_APP_IS_FVD === 'true' && (
                                                                <span style={{ fontSize: 14 }}>
                                                                    <b style={{ marginRight: 10 }}>Buiten Geel:</b> €{' '}
                                                                    {price.externalPrice}
                                                                </span>
                                                            )}
                                                            {process.env.REACT_APP_IS_FVD === 'false' && (
                                                                <span style={{ fontSize: 14 }}>
                                                                    <b style={{ marginRight: 10 }}>
                                                                        {t('form.labels.price')}:
                                                                    </b>{' '}
                                                                    € {price.externalPrice}
                                                                </span>
                                                            )}
                                                            <span style={{ fontSize: 14 }}>
                                                                <b style={{ marginRight: 10 }}>
                                                                    {t('form.labels.replacementPrice')}:
                                                                </b>{' '}
                                                                € {price.replacementPrice}
                                                            </span>
                                                        </ListRow>
                                                    </li>
                                                );
                                            }}
                                        />
                                    </ListWrapper>
                                )}
                            </AutoSizer>
                        )}
                    </Page>
                )}
            </Page>
        </>
    );
}

NewCategory.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(newProjectStyle)(observer(NewCategory)));
