/* istanbul ignore file */
import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import ListRow from 'components/ListRow';
import { Edit } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function CategoryOverview({ t }) {
    const { account, clothingManagement } = useStore();
    const history = useHistory();

    function rowRenderer({ key, index, style }) {
        const category = clothingManagement.categories.resource.filtered[index];
        const currentPrice = clothingManagement.categories.currentPrice(category.prices);

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span style={{ minWidth: 150 }}>{category.name}</span>
                    {process.env.REACT_APP_IS_FVD === 'true' && (
                        <span style={{ fontSize: 14, minWidth: 200 }}>
                            <b style={{ marginRight: 10 }}>Binnen Geel:</b> € {currentPrice?.internalPrice}
                        </span>
                    )}
                    {process.env.REACT_APP_IS_FVD === 'true' && (
                        <span style={{ fontSize: 14, minWidth: 200 }}>
                            <b style={{ marginRight: 10 }}>Buiten Geel:</b> € {currentPrice?.externalPrice}
                        </span>
                    )}
                    {process.env.REACT_APP_IS_FVD === 'false' && (
                        <span style={{ fontSize: 14, minWidth: 200 }}>
                            <b style={{ marginRight: 10 }}>{t('form.labels.price')}:</b> € {currentPrice?.externalPrice}
                        </span>
                    )}
                    <span style={{ fontSize: 14, minWidth: 200 }}>
                        <b style={{ marginRight: 10 }}>{t('form.labels.replacementPrice')}:</b> €{' '}
                        {currentPrice?.replacementPrice}
                    </span>
                    {account.rights.canUpdateCategories && (
                        <ListRowActions>
                            <Tooltip title={t('tooltip.category.edit')} placement="top">
                                <Edit
                                    onClick={() => {
                                        history.replace(`/retributiebeheer/categorieen/${category.id}/bewerk`);
                                    }}
                                />
                            </Tooltip>
                        </ListRowActions>
                    )}
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            export
            store={clothingManagement.categories.resource}
            rowRenderer={rowRenderer}
            heightOffset={400}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateCategories && {
                    icon: 'add',
                    route: '/retributiebeheer/categorieen/nieuw',
                    tooltip: t('tooltip.category.create'),
                },
            ]}
        />
    );
}

export default withTranslation()(observer(CategoryOverview));
