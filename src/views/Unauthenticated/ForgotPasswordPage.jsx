import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import InputAdornment from '@material-ui/core/InputAdornment';
import Email from '@material-ui/icons/Email';
import GridItem from 'components/Grid/GridItem.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import BaseLayout from 'components/BaseLayout.jsx';
import Inputs from 'components/Inputs/index.jsx';

import { Link } from 'react-router-dom';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { Success, Danger } from 'components/Notifications.jsx';

import forgotPasswordPageStyle from './home.style.jsx';
import { withTranslation } from 'react-i18next';
function ForgotPasswordPage(props) {
    const { ui, account } = useStore();
    const { classes, t } = props;

    useEffect(() => {
        account.resetNotifications();

        return () => {
            account.resetNotifications();
            account.set({
                email: '',
            });
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <BaseLayout>
            {!ui.isMobile && (
                <GridItem xs={12} sm={12} md={6}>
                    <h1 className={classes.title}>{t('page.title.forgotPassword')}</h1>
                    <h4 className={classes.subtitle}>{t('page.subtitle.forgotPassword')}</h4>
                </GridItem>
            )}
            <GridItem xs={false} sm={false} md={1}></GridItem>
            <GridItem xs={12} sm={12} md={4}>
                <Success message={account.success} onClose={() => (account.success = '')} />
                <Danger message={account.error} onClose={() => (account.error = '')} />
                <form className={classes.form}>
                    <CardBody>
                        <Inputs.Input
                            fullWidth
                            required
                            type="text"
                            label={t('form.labels.email')}
                            autoComplete="username"
                            value={account.email}
                            onChange={e => (account.email = e)}
                            endAdornment={
                                <InputAdornment position="end">
                                    <Email className={classes.inputIconsColor} />
                                </InputAdornment>
                            }
                            validations={[{ type: 'email' }]}
                        />
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                        <Button
                            onClick={() => account.forgotPassword()}
                            color="primary"
                            size="md"
                            disabled={
                                !account.email ||
                                (account.email &&
                                    (account.email.length < 6 ||
                                        !account.email.includes('@') ||
                                        !account.email.includes('.')))
                            }
                        >
                            {t('form.button.confirmEmail')}
                        </Button>
                    </CardFooter>
                    <CardFooter className={classes.cardFooter}>
                        <Link className={classes.link} to="/">
                            <span data-hover={t('form.button.signIn')}>{t('form.button.signIn')}</span>
                        </Link>
                    </CardFooter>
                    <CardFooter className={classes.cardFooter}>
                        <Link className={classes.link} to="/account-aanmaken">
                            <span data-hover={t('form.button.register')}>{t('form.button.register')}</span>
                        </Link>
                    </CardFooter>
                </form>
            </GridItem>
        </BaseLayout>
    );
}

ForgotPasswordPage.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(forgotPasswordPageStyle)(observer(ForgotPasswordPage)));
