import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import GridItem from 'components/Grid/GridItem.jsx';
import InputAdornment from '@material-ui/core/InputAdornment';
import Email from '@material-ui/icons/Email';
import LockOutlined from '@material-ui/icons/LockOutlined';
import Button from 'components/CustomButtons/Button.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import Inputs from 'components/Inputs/index.jsx';
import BaseLayout from 'components/BaseLayout.jsx';
import parse from 'html-react-parser';

import { Link } from 'react-router-dom';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { Success, Danger } from 'components/Notifications.jsx';
import { Warning } from '@material-ui/icons';

import homeStyle from './home.style.jsx';
import { withTranslation } from 'react-i18next';

function Home(props) {
    const { classes, t } = props;
    const { ui, account } = useStore();

    useEffect(() => {
        ui.fetch();

        return () => {
            account.resetNotifications();
            account.set({
                email: '',
                password: '',
            });
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    console.log(ui.subtitle);

    return (
        <BaseLayout>
            {!ui.isMobile && (
                <GridItem xs={12} sm={12} md={7}>
                    <h1 className={classes.title}>{ui.title}</h1>
                    <h4 className={classes.subtitle}>
                        {typeof ui.subtitle === 'string'
                            ? parse((ui.subtitle || '').replace(/\n/g, '<br />'))
                            : ui.subtitle}
                    </h4>
                </GridItem>
            )}
            <GridItem xs={12} sm={12} md={4}>
                <Warning message={account.warn} onClose={() => (account.warn = '')} />
                <Danger message={account.error} onClose={() => (account.error = '')} />
                <Success message={account.success} onClose={() => (account.success = '')} />
                <form className={classes.form}>
                    <CardBody>
                        <Inputs.Input
                            id="email"
                            fullWidth
                            required
                            type="text"
                            label={t('form.labels.email')}
                            autoComplete="username"
                            value={account.email}
                            onChange={e => (account.email = e)}
                            endAdornment={
                                <InputAdornment position="end">
                                    <Email className={classes.inputIconsColor} />
                                </InputAdornment>
                            }
                        />
                        <Inputs.Input
                            id="password"
                            fullWidth
                            required
                            type="password"
                            label={t('form.labels.password')}
                            autoComplete="current-password"
                            value={account.password}
                            onChange={e => (account.password = e)}
                            endAdornment={
                                <InputAdornment position="end">
                                    <LockOutlined className={classes.inputIconsColor} />
                                </InputAdornment>
                            }
                        />
                        <Inputs.Input
                            fullWidth
                            type="checkbox"
                            label={t('form.labels.rememberMe')}
                            value={account.remember}
                            onChange={e => (account.remember = e)}
                        />
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                        <Button id="signInButton" onClick={() => account.login()} color="primary" size="md">
                            {t('form.button.signIn')}
                        </Button>
                    </CardFooter>
                    <CardFooter className={classes.cardFooter}>
                        <Link className={classes.link} to="/wachtwoord-vergeten">
                            <span data-hover={t('form.button.forgotPassword')}>{t('form.button.forgotPassword')}</span>
                        </Link>
                    </CardFooter>
                    <CardFooter className={classes.cardFooter}>
                        <Link className={classes.link} to="/account-aanmaken">
                            <span data-hover={t('form.button.register')}>{t('form.button.register')}</span>
                        </Link>
                    </CardFooter>
                </form>
            </GridItem>
        </BaseLayout>
    );
}

Home.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(homeStyle)(observer(Home)));
