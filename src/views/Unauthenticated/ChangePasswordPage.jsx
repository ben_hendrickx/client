/* istanbul ignore file */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import InputAdornment from '@material-ui/core/InputAdornment';
import GridItem from 'components/Grid/GridItem.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import queryString from 'query-string';
import BaseLayout from 'components/BaseLayout.jsx';
import Inputs from 'components/Inputs/index.jsx';

import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { useHistory } from 'react-router';
import { LockOutlined } from '@material-ui/icons';
import { Success, Danger } from 'components/Notifications.jsx';

import changePasswordPageStyle from './home.style.jsx';
import { withTranslation } from 'react-i18next';

function ChangePasswordPage(props) {
    const { ui, account } = useStore();
    const { classes, t } = props;
    const history = useHistory();

    useEffect(() => {
        account.resetNotifications();
        // TODO: check if this can be removed
        // if (
        //     !window.location.search ||
        //     !window.location.search.includes('email') ||
        //     !window.location.search.includes('token')
        // ) {
        //     history.replace('/');
        //     return;
        // }

        return () => {
            account.set({
                password: '',
                confirmPassword: '',
            });
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const { email, token } = queryString.parse(window.location.search);

    return (
        <BaseLayout>
            {!ui.isMobile && (
                <GridItem xs={12} sm={12} md={6}>
                    <h1 className={classes.title}>{t('page.title.changePassword')}</h1>
                    <h4 className={classes.subtitle}>{t('page.subtitle.changePassword')}</h4>
                </GridItem>
            )}
            <GridItem xs={false} sm={false} md={1}></GridItem>
            <GridItem xs={12} sm={12} md={4}>
                <Danger message={account.error} onClose={() => (account.error = '')} />
                <Success message={account.success} onClose={() => (account.success = '')} />
                <form className={classes.form}>
                    <CardBody>
                        <Inputs.Input
                            fullWidth
                            required
                            type="password"
                            label={t('form.labels.password')}
                            autoComplete="new-password"
                            value={account.password}
                            onChange={e => (account.password = e)}
                            endAdornment={
                                <InputAdornment position="end">
                                    <LockOutlined className={classes.inputIconsColor} />
                                </InputAdornment>
                            }
                        />
                        <Inputs.Input
                            fullWidth
                            required
                            type="password"
                            label={t('form.labels.confirmPassword')}
                            autoComplete="new-password"
                            value={account.confirmPassword}
                            onChange={e => (account.confirmPassword = e)}
                            endAdornment={
                                <InputAdornment position="end">
                                    <LockOutlined className={classes.inputIconsColor} />
                                </InputAdornment>
                            }
                        />
                    </CardBody>
                    <CardFooter className={classes.cardFooter}>
                        <Button
                            onClick={() => account.setPassword(email, token).then(() => history.replace('/'))}
                            color="primary"
                            size="md"
                            disabled={
                                !account.password ||
                                !account.confirmPassword ||
                                account.password !== account.confirmPassword ||
                                account.password.length < 6
                            }
                        >
                            {t('form.button.changePassword')}
                        </Button>
                    </CardFooter>
                </form>
            </GridItem>
        </BaseLayout>
    );
}

ChangePasswordPage.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(changePasswordPageStyle)(observer(ChangePasswordPage)));
