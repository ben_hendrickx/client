import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import CardBody from 'components/Card/CardBody.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import ImageDropzone from 'components/Dropzone/ImageDropzone.jsx';
import Address from 'components/Inputs/Address.jsx';

import signUpPageStyle from './home.style.jsx';
import spinner from '../../assets/img/spinner.gif';

import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import BaseLayout from 'components/BaseLayout.jsx';
import Inputs from 'components/Inputs/index.jsx';
import { Trans, withTranslation } from 'react-i18next';

function SignUpPage(props) {
    const { classes, t } = props;
    const { account, countries, shirtSizes } = useStore();
    const history = useHistory();

    useEffect(() => {
        account.resetNotifications();
        countries.fetch();
        shirtSizes.fetch();

        return () => {
            account.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (account.success) {
            history.push('/');
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.success]);

    const notAllFilled =
        !account.password ||
        !account.confirmPassword ||
        !account.privacy ||
        !account.firstName ||
        !account.lastName ||
        !account.dateOfBirth ||
        account.sex === undefined ||
        !account.countryOfBirth ||
        !account.origin ||
        !account.mail ||
        !account.confirmMail ||
        !account.tShirt ||
        !account.streetName ||
        !account.houseNumber ||
        !account.cityName ||
        !account.zipCode ||
        (!account.phone && !account.cell);
    const disabled = !!account.success || !!account.error || notAllFilled;

    function validate(byClick = false) {
        if (account.firstName && account.firstName.length < 2) {
            return 'Voornaam is ongeldig';
        }

        if (account.lastName && account.lastName.length < 2) {
            return 'Achternaam is ongeldig';
        }

        if (account.dateOfBirth && !account.dateOfBirth.isValid()) {
            return 'Geboortedatum is geen geldige datum';
        }

        if (account.origin && account.origin.length < 2) {
            return 'Nationaliteit is ongeldig';
        }

        if (
            account.phone &&
            (account.phone.length < 9 ||
                !/^((\+|00)\d{1,3}\s?|0)(\d\s?\d{3}|\d{2}\s?\d{2})(\s?\d{2}){2}$/.test(
                    account.phone.replace(/(\/|\.)/g, '')
                ))
        ) {
            return 'Telefoonnummer is ongeldig';
        }

        if (
            account.cell &&
            (account.cell.length < 9 ||
                !/^((\+|00)\d{1,3}\s?|0)\d(60|[6789]\d)(\s?\d{2}){3}$/.test(account.cell.replace(/(\/|\.)/g, '')))
        ) {
            return 'G.S.M.-nummer is ongeldig';
        }

        if (account.mail) {
            if (account.mail.length < 6 || !account.mail.includes('@') || !account.mail.includes('.')) {
                return 'E-mailadres is geen geldig mailadres';
            }
            if (account.confirmMail && account.mail !== account.confirmMail) {
                return 'E-mailadressen komen niet overeen';
            }
        }

        if (account.confirmMail) {
            if (
                account.confirmMail.length < 6 ||
                !account.confirmMail.includes('@') ||
                !account.confirmMail.includes('.')
            ) {
                return 'E-mailadres (bevestiging) is geen geldig mailadres';
            }
            if (account.mail && account.mail !== account.confirmMail) {
                return 'E-mailadressen komen niet overeen';
            }
        }

        if (account.secondaryMail) {
            if (
                account.secondaryMail.length < 6 ||
                !account.secondaryMail.includes('@') ||
                !account.secondaryMail.includes('.')
            ) {
                return 'Tweede e-mailadres is geen geldig mailadres';
            }
        }

        if (account.password) {
            if (account.password.length < 6) {
                return 'Wachtwoord moet bestaan uit minimum 6 karakters';
            }
            if (account.confirmPassword && account.password !== account.confirmPassword) {
                return 'Wachtwoorden komen niet overeen';
            }
        }

        if (account.confirmPassword) {
            if (account.confirmPassword.length < 6) {
                return 'Wachtwoord (bevestiging) moet bestaan uit minimum 6 karakters';
            }
            if (account.password && account.password !== account.confirmPassword) {
                return 'Wachtwoorden komen niet overeen';
            }
        }

        if (
            account.mail ||
            account.confirmMail ||
            account.secondaryMail ||
            account.password ||
            account.confirmPassword
        ) {
            if (!account.phone && !account.cell) {
                return 'Telefoonnummer of G.S.M.-nummer moet ingevuld zijn.';
            }
        }

        if (account.password || account.confirmPassword) {
            if (!account.streetName && !account.houseNumber && !account.zipCode && !account.cityName) {
                return 'Adres moet ingevuld zijn';
            }

            if (!account.streetName || !account.houseNumber || !account.zipCode || !account.cityName) {
                return 'Adres is niet volledig';
            }
        }

        if (byClick && notAllFilled) {
            let response = 'Gelieve de ontbrekende velden in te vullen (';
            const missing = [];
            !account.firstName && missing.push('Voornaam');
            !account.lastName && missing.push('Achternaam');
            !account.dateOfBirth && missing.push('Geboortedatum');
            account.sex === undefined && missing.push('Geslacht');
            !account.origin && missing.push('Nationaliteit');
            !account.countryOfBirth && missing.push('Geboorteland');
            !account.phone && !account.cell && missing.push('Telefoonnummer/G.S.M.-nummer');
            !account.mail && missing.push('Mailadres');
            !account.confirmMail && missing.push('Mailadres (bevestiging)');
            !account.streetName &&
                !account.houseNumber &&
                !account.zipCode &&
                !account.cityName &&
                missing.push('Adres');
            !account.tShirt && missing.push('T-shirt maat');
            !account.password && missing.push('Wachtwoord');
            !account.confirmPassword && missing.push('Wachtwoord (bevestiging)');
            !account.privacy && missing.push('Privacy-verklaring');
            return response + missing.join(', ') + ')';
        }

        return null;
    }

    return (
        <BaseLayout>
            <h1 className={classes.title}>{t('page.title.register')}</h1>
            {account.error && (
                <div
                    style={{
                        padding: '10px 15px',
                        background: '#f44336',
                        color: '#fff',
                    }}
                >
                    {account.error}
                </div>
            )}
            <form className={classes.form} autoComplete="off">
                <CardBody>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={5}>
                            <h1>{t('form.labels.profilePicture')}</h1>
                            <ImageDropzone
                                className={classes.dropzone}
                                store={account}
                                onData={data => (account.image = data)}
                            />
                            <h2>{t('form.hint.profilePicture')}</h2>
                            <h1>{t('form.labels.personalData')}</h1>
                            <Inputs.Input
                                fullWidth
                                required
                                type="text"
                                label={t('form.labels.firstName')}
                                autoComplete="given-name"
                                value={account.firstName}
                                onChange={e => (account.firstName = e)}
                            />
                            <Inputs.Input
                                fullWidth
                                required
                                type="text"
                                label={t('form.labels.lastName')}
                                autoComplete="family-name"
                                value={account.lastName}
                                onChange={e => (account.lastName = e)}
                            />
                            <Inputs.Input
                                fullWidth
                                required
                                type="date"
                                label={t('form.labels.dateOfBirth')}
                                value={account.dateOfBirth}
                                onChange={e => (account.dateOfBirth = e)}
                            />{' '}
                            {/* TODO: add required * automatically to label */}
                            <Inputs.Input
                                fullWidth
                                required
                                type="select"
                                label={t('form.labels.sex')}
                                value={account.sex}
                                onChange={e => (account.sex = e)}
                                options={[
                                    { label: t('form.options.male'), value: 0 },
                                    { label: t('form.options.female'), value: 1 },
                                    { label: t('form.options.genderX'), value: 2 },
                                ]}
                            />
                            <Inputs.Input
                                fullWidth
                                required
                                type="text"
                                label={t('form.labels.origin')}
                                value={account.origin}
                                onChange={e => (account.origin = e)}
                            />
                            <Inputs.Input
                                fullWidth
                                required
                                type="select"
                                label={t('form.labels.countryOfBirth')}
                                autoComplete="country"
                                value={account.countryOfBirth}
                                onChange={e => (account.countryOfBirth = e)}
                                options={countries.autoComplete}
                            />
                        </GridItem>
                        <GridItem xs={false} sm={false} md={1}></GridItem>
                        <GridItem xs={12} sm={12} md={6}>
                            <h1>{t('form.labels.contactDetails')}</h1>
                            <Inputs.Input
                                fullWidth
                                type="text"
                                label={t('form.labels.phoneNumber')}
                                autoComplete="tel"
                                value={account.phone}
                                onChange={e => (account.phone = e)}
                            />
                            <Inputs.Input
                                fullWidth
                                type="text"
                                label={t('form.labels.cellPhone')}
                                autoComplete="tel"
                                value={account.cell}
                                onChange={e => (account.cell = e)}
                                placeholder="04xx/xx.xx.xx"
                            />
                            <Inputs.Input
                                fullWidth
                                required
                                type="text"
                                label={t('form.labels.email')}
                                autoComplete="email"
                                value={account.mail}
                                onChange={e => (account.mail = e)}
                            />
                            <Inputs.Input
                                fullWidth
                                required
                                type="text"
                                label={t('form.labels.confirmEmail')}
                                autoComplete="email"
                                value={account.confirmMail}
                                onChange={e => (account.confirmMail = e)}
                            />
                            <Inputs.Input
                                fullWidth
                                type="text"
                                label={t('form.labels.secondaryEmail')}
                                value={account.secondaryMail}
                                onChange={e => (account.secondaryMail = e)}
                            />
                            <h1>{t('form.labels.addressInfo')}</h1>
                            <Address store={account} />
                            <h1>{t('form.labels.otherInfo')}</h1>
                            <Inputs.Input
                                fullWidth
                                required
                                type="select"
                                label={t('form.labels.shirtSize')}
                                autoComplete="country"
                                value={account.tShirt}
                                onChange={e => (account.tShirt = e)}
                                options={shirtSizes.autoComplete}
                            />
                            <Inputs.Input
                                fullWidth
                                required
                                type="password"
                                label={t('form.labels.password')}
                                autoComplete="new-password"
                                value={account.password}
                                onChange={e => (account.password = e)}
                            />
                            <Inputs.Input
                                fullWidth
                                required
                                type="password"
                                label={t('form.labels.confirmPassword')}
                                autoComplete="new-password"
                                value={account.confirmPassword}
                                onChange={e => (account.confirmPassword = e)}
                            />
                        </GridItem>
                    </GridContainer>
                    <CardFooter className={classes.divider}>
                        {/* TODO: make url dynamic and translate */}
                        <Inputs.Input
                            fullWidth
                            required
                            type="checkbox"
                            label={
                                <p key="privacy">
                                    Ik bevestig{' '}
                                    <a target="_blank" rel="noopener noreferrer" href={process.env.REACT_APP_PRIVACY}>
                                        de privacyverklaring van {process.env.REACT_APP_CLIENT} m.b.t. bescherming van
                                        de persoonlijke gegevens
                                    </a>{' '}
                                    gelezen te hebben
                                </p>
                            }
                            value={account.privacy}
                            onChange={e => (account.privacy = e)}
                        />
                    </CardFooter>
                    <CardFooter className={classes.cardFooter}>
                        <Inputs.Input
                            fullWidth
                            type="checkbox"
                            label={
                                <p>
                                    <Trans i18nKey="privacy">
                                        Hierbij verklaar ik dat foto’s en video’s waarop ik duidelijk in beeld sta door
                                        het Lokaal Bestuur Geel gebruikt mogen worden:
                                        <br />
                                        - in publicaties, magazines of brochures van Lokaal Bestuur Geel
                                        <br />
                                        - in de stedelijke rubriek in Deze Week en Rondom
                                        <br />
                                        - op de websites van Lokaal Bestuur Geel, op sociale media van Lokaal Bestuur
                                        Geel (Facebook, Twitter, Instagram…), in de digitale nieuwsbrieven van Lokaal
                                        Bestuur Geel
                                        <br />
                                        <br />
                                        Indien u nog geen 16 jaar bent, moet een ouder of voogd toestemming geven voor
                                        het gebruiken van foto- en beeldmateriaal. Dit kan door een e-mail te sturen
                                        naar {process.env.REACT_APP_EMAIL}. Gelieve de naam van het kind duidelijk te
                                        vermelden in de mail. Wenst u niet langer dat uw foto- en beeldmateriaal wordt
                                        gebruikt, laat dit dan weten via {process.env.REACT_APP_EMAIL} of aan de balie
                                        van Toerisme Geel.
                                    </Trans>
                                </p>
                            }
                            value={account.pictures}
                            onChange={e => (account.pictures = e)}
                            style={{ alignItems: 'start' }}
                        />
                    </CardFooter>
                    <CardFooter className={classes.cardFooter}>
                        <Button
                            onClick={async () => {
                                if (disabled) {
                                    account.error = validate(true);
                                    return;
                                }
                                !account.loading && (await account.signup());
                            }}
                            color={`${disabled || account.loading ? 'disabled' : 'primary'}`}
                            size="md"
                        >
                            {account.loading ? (
                                <img alt="spinner" src={spinner} height={25} />
                            ) : (
                                t('form.button.signIn')
                            )}
                        </Button>
                    </CardFooter>
                </CardBody>
            </form>
        </BaseLayout>
    );
}

SignUpPage.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(signUpPageStyle)(observer(SignUpPage)));
