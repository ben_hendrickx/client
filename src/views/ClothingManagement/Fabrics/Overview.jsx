/* istanbul ignore file */
import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import ListRow from 'components/ListRow';
import { Delete, Edit } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function FabricsOverview({ t }) {
    const { account, clothingManagement, general } = useStore();
    const history = useHistory();

    function rowRenderer({ key, index, style }) {
        const fabric = clothingManagement.fabrics.resource.filtered[index];

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span>{fabric.type}</span>
                    {account.rights.canUpdateFabrics && (
                        <ListRowActions>
                            <Tooltip title={t('tooltip.fabric.edit')} placement="top">
                                <Edit onClick={() => history.push(`/kledijbeheer/stoffen/${fabric.id}`)} />
                            </Tooltip>
                            <Tooltip title={t('tooltip.fabric.delete')} placement="top">
                                <Delete
                                    onClick={() =>
                                        general.addModal({
                                            title: t('modal.title.fabric.delete'),
                                            body: t('modal.body.fabric.delete'),
                                            confirmText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await clothingManagement.fabrics.resource.delete(undefined, fabric.id);
                                                clothingManagement.fabrics.resource.data = [];
                                                clothingManagement.fabrics.resource.status = [];
                                                await clothingManagement.fabrics.resource.fetch();
                                            },
                                        })
                                    }
                                />
                            </Tooltip>
                        </ListRowActions>
                    )}
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            store={clothingManagement.fabrics.resource}
            rowRenderer={rowRenderer}
            heightOffset={450}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateFabrics && {
                    icon: 'add',
                    route: '/kledijbeheer/stoffen/nieuw',
                    tooltip: t('tooltip.fabric.create'),
                },
                account.rights.canExportFabrics && {
                    icon: 'getApp',
                    tooltip: t('tooltip.report.download'),
                    href: '/api/reports/fabrics.xlsx',
                },
            ]}
        />
    );
}

export default withTranslation()(observer(FabricsOverview));
