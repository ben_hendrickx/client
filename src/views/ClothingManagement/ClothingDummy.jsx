/* istanbul ignore file */
import React, { useState } from 'react';
import { observer } from 'mobx-react-lite';
import { withStyles } from '@material-ui/styles';
import { useStore } from '../../mobx';
import { useHistory } from 'react-router';
import Button from 'components/CustomButtons/Button.jsx';
import { primaryColor } from 'assets/jss/material-kit-react';

const pinScreenStyle = {
    pinscreen: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        '& > main': {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            background: '#fff',
            borderRadius: 5,
            padding: '40px 80px 30px',
            maxWidth: 850,
        },
    },
    success: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        borderRadius: 5,
        maxWidth: 550,
        padding: '0px 25px',
        background: '#54ab25',
        color: '#fff',
        marginBottom: 50,
        width: '100%',
        '& p': {
            margin: 0,
            padding: '10px 0',
            fontSize: '1.05em',
        },
    },
    error: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        borderRadius: 5,
        maxWidth: 550,
        padding: '0px 25px',
        background: '#f44336',
        color: '#fff',
        marginBottom: 50,
        width: '100%',
        '& p': {
            margin: 0,
            padding: '10px 0',
            fontSize: '1.05em',
        },
    },
    list: {
        listStyle: 'none',
        margin: 0,
        padding: 0,
        '& > li': {
            '&:hover': {
                background: primaryColor,
                color: 'white',
                borderColor: primaryColor,
                cursor: 'pointer',
            },
            padding: '10px 20px',
            marginBottom: 10,
            borderRadius: 3,
            border: '1px solid rgba(0,0,0,0.3)',
        },
    },
};

function PinScreen(props) {
    const { classes } = props;
    const { clothingManagement } = useStore();
    const [type, setType] = useState(null);
    const history = useHistory();

    let notification = null;

    if (clothingManagement.success) {
        notification = (
            <div className={classes.success}>
                <p>{clothingManagement.success}</p>
            </div>
        );
    }

    if (clothingManagement.error) {
        notification = (
            <div className={classes.error}>
                <main>
                    <p>{clothingManagement.error}</p>
                </main>
            </div>
        );
    }

    return (
        <div className={classes.pinscreen}>
            {notification}
            {!type && (
                <main>
                    <p style={{ fontSize: '2.2em', fontWeight: 500, marginBottom: 40 }}>Wat wil je beheren?</p>
                    <ul className={classes.list}>
                        <li onClick={() => setType('clothing')}>Een kledingstuk</li>
                        <li onClick={() => setType('category')}>Een kleding-categorie</li>
                        <li onClick={() => setType('order')}>Een bestelling</li>
                    </ul>
                </main>
            )}
            {type === 'clothing' && (
                <main>
                    <p style={{ fontSize: '2.2em', fontWeight: 500, marginBottom: 40 }}>Wat wil je doen?</p>
                    <ul className={classes.list}>
                        <li onClick={() => history.push('/kledijbeheer/kleding/nieuw')}>Een kledingstuk toevoegen</li>
                        <li onClick={() => history.push('/kledijbeheer/terugname')}>Een kledingstuk ontvangen</li>
                    </ul>
                    <Button onClick={() => setType(null)} color="danger" size="md">
                        Vorige
                    </Button>
                </main>
            )}
            {type === 'category' && (
                <main>
                    <p style={{ fontSize: '2.2em', fontWeight: 500, marginBottom: 40 }}>Wat wil je doen?</p>
                    <ul className={classes.list}>
                        <li onClick={() => history.push('/retributiebeheer/categorieen/nieuw')}>
                            Een categorie toevoegen
                        </li>
                    </ul>
                    <Button onClick={() => setType(null)} color="danger" size="md">
                        Vorige
                    </Button>
                </main>
            )}
            {type === 'order' && (
                <main>
                    <p style={{ fontSize: '2.2em', fontWeight: 500, marginBottom: 40 }}>Wat wil je doen?</p>
                    <ul className={classes.list}>
                        <li onClick={() => history.push('/retributiebeheer/bestellingen/nieuw')}>
                            Een bestelling plaatsen
                        </li>
                    </ul>
                    <Button onClick={() => setType(null)} color="danger" size="md">
                        Vorige
                    </Button>
                </main>
            )}
        </div>
    );
}

export default withStyles(pinScreenStyle)(observer(PinScreen));
