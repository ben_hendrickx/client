/* istanbul ignore file */
import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import ListRow from 'components/ListRow';
import { Delete, Edit } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function SizesOverview({ t }) {
    const { account, clothingManagement, general } = useStore();
    const history = useHistory();

    function rowRenderer({ key, index, style }) {
        const size = clothingManagement.sizes.resource.filtered[index];

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span>{size.size}</span>
                    {account.rights.canUpdateClothingSizes && (
                        <ListRowActions>
                            <Tooltip title={t('tooltip.clothingSize.edit')} placement="top">
                                <Edit onClick={() => history.push(`/kledijbeheer/kledij-maat/${size.id}`)} />
                            </Tooltip>
                            <Tooltip title={t('tooltip.clothingSize.delete')} placement="top">
                                <Delete
                                    onClick={() =>
                                        general.addModal({
                                            title: t('modal.title.clothingSize.delete'),
                                            body: t('modal.body.clothingSize.delete'),
                                            confirmText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await clothingManagement.sizes.resource.delete(undefined, size.id);
                                                clothingManagement.sizes.resource.data = [];
                                                clothingManagement.sizes.resource.status = [];
                                                await clothingManagement.sizes.resource.fetch();
                                            },
                                        })
                                    }
                                />
                            </Tooltip>
                        </ListRowActions>
                    )}
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            store={clothingManagement.sizes.resource}
            rowRenderer={rowRenderer}
            heightOffset={450}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateClothingSizes && {
                    icon: 'add',
                    route: '/kledijbeheer/kledij-maat/nieuw',
                    tooltip: t('tooltip.clothingSize.create'),
                },
                account.rights.canExportClothingSizes && {
                    icon: 'getApp',
                    tooltip: t('tooltip.report.download'),
                    href: '/api/reports/clothing_sizes.xlsx',
                },
            ]}
        />
    );
}

export default withTranslation()(observer(SizesOverview));
