/* istanbul ignore file */
import { withStyles } from '@material-ui/core';
import Button from 'components/CustomButtons/Button.jsx';
import Page from 'components/Page';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import CustomInput from 'components/CustomInput/CustomInput';
import useReactRouter from 'use-react-router';
import { Save } from '@material-ui/icons';
import { withTranslation } from 'react-i18next';

const newSizeStyle = {
    content: {
        position: 'relative',
        display: 'flex',
        background: '#ffffff',
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: '10px 15px',
        marginBottom: 20,
    },
};

function NewSize(props) {
    const { classes, t } = props;
    const { clothingManagement } = useStore();
    const { match, history } = useReactRouter();

    useEffect(() => {
        if (match?.params?.id) {
            clothingManagement.getSingleClothingSize(match.params.id);
        }

        return () => {
            clothingManagement.size = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <Page
            title={match?.params?.id ? t('modal.title.clothingSize.edit') : t('modal.title.clothingSize.new')}
            loading={clothingManagement.loading}
        >
            <div className={classes.content}>
                <CustomInput
                    labelText={t('form.labels.clothing.size')}
                    formControlProps={{
                        required: true,
                        fullWidth: true,
                    }}
                    inputProps={{
                        color: 'primary',
                        type: 'text',
                        autoComplete: 'off',
                        value: clothingManagement.size,
                        onChange: e => (clothingManagement.size = e.target.value),
                    }}
                />
            </div>
            <Button
                disabled={!clothingManagement.size}
                onClick={async () => {
                    if (match?.params?.id) {
                        await clothingManagement.updateSize(match?.params?.id);
                    } else {
                        await clothingManagement.createSize();
                    }
                    history.replace(`/kledijbeheer/kledij-maten`);
                }}
                color="success"
                size="md"
            >
                <Save style={{ marginRight: '5px' }} />
                <span style={{ marginTop: '1px', marginLeft: '5px' }}>{t('form.button.save')}</span>
            </Button>
            <Button
                onClick={() => {
                    history.replace(`/kledijbeheer/kledij-maten`);
                }}
                style={{ marginLeft: '20px' }}
                color="white"
                size="md"
            >
                {t('form.button.cancel')}
            </Button>
        </Page>
    );
}

export default withTranslation()(withStyles(newSizeStyle)(observer(NewSize)));
