/* istanbul ignore file */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';

import newProjectStyle from '../overview.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';

import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Page from 'components/Page.jsx';
import { Save } from '@material-ui/icons';
import { withTranslation } from 'react-i18next';

function NewProject(props) {
    const { classes, t } = props;
    const { clothingManagement } = useStore();
    const { match, history } = useReactRouter();

    useEffect(() => {
        if (match?.params?.id) {
            clothingManagement.getSinglePattern(match.params.id);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page
            title={match?.params?.id ? t('modal.title.pattern.edit') : t('modal.title.pattern.new')}
            loading={clothingManagement.loading}
        >
            <div className={classes.content}>
                <div style={{ display: 'block' }}>
                    <div>
                        <CustomInput
                            labelText={t('form.labels.name')}
                            id="cast"
                            formControlProps={{
                                required: true,
                                fullWidth: true,
                            }}
                            inputProps={{
                                color: 'primary',
                                type: 'text',
                                autoComplete: 'off',
                                value: clothingManagement.patternName,
                                onChange: e => (clothingManagement.patternName = e.target.value),
                            }}
                        />
                    </div>
                    <Button
                        disabled={!clothingManagement.patternName}
                        onClick={async () => {
                            if (match?.params?.id) {
                                await clothingManagement.updatePattern(match.params.id);
                            } else {
                                await clothingManagement.createPattern();
                            }
                            history.replace(`/kledijbeheer/motieven`);
                        }}
                        color="success"
                        size="md"
                    >
                        <Save style={{ marginRight: '5px' }} />
                        <span style={{ marginTop: '1px', marginLeft: '5px' }}>{t('form.button.save')}</span>
                    </Button>
                    <Button
                        onClick={() => {
                            history.replace(`/kledijbeheer/motieven`);
                        }}
                        style={{ marginLeft: '20px' }}
                        color="white"
                        size="md"
                    >
                        {t('form.button.cancel')}
                    </Button>
                </div>
            </div>
        </Page>
    );
}

NewProject.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(newProjectStyle)(observer(NewProject)));
