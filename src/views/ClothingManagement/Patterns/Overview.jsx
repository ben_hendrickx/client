/* istanbul ignore file */
import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import ListRow from 'components/ListRow';
import { Delete, Edit } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function PatternOverview({ t }) {
    const { account, clothingManagement, general } = useStore();
    const history = useHistory();

    function rowRenderer({ key, index, style }) {
        const pattern = clothingManagement.patterns.resource.filtered[index];

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span>{pattern.name}</span>
                    {account.rights.canUpdatePatterns && (
                        <ListRowActions>
                            <Tooltip title={t('tooltip.pattern.edit')} placement="top">
                                <Edit onClick={() => history.push(`/kledijbeheer/motieven/${pattern.id}`)} />
                            </Tooltip>
                            <Tooltip title={t('tooltip.pattern.delete')} placement="top">
                                <Delete
                                    onClick={() =>
                                        general.addModal({
                                            title: t('modal.title.pattern.delete'),
                                            body: t('modal.body.pattern.delete'),
                                            confirmText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await clothingManagement.patterns.resource.delete(
                                                    undefined,
                                                    pattern.id
                                                );
                                                clothingManagement.patterns.resource.data = [];
                                                clothingManagement.patterns.resource.status = [];
                                                await clothingManagement.patterns.resource.fetch();
                                            },
                                        })
                                    }
                                />
                            </Tooltip>
                        </ListRowActions>
                    )}
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            store={clothingManagement.patterns.resource}
            rowRenderer={rowRenderer}
            heightOffset={450}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreatePatterns && {
                    icon: 'add',
                    route: '/kledijbeheer/motieven/nieuw',
                    tooltip: t('tooltip.pattern.create'),
                },
                account.rights.canExportPatterns && {
                    icon: 'getApp',
                    tooltip: t('tooltip.report.download'),
                    href: '/api/reports/patterns.xlsx',
                },
            ]}
        />
    );
}

export default withTranslation()(observer(PatternOverview));
