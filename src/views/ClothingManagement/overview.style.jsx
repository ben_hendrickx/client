import { primaryColor, secondaryColor, primaryCardHeader, white } from '../../assets/jss/material-kit-react.jsx';

const projectsOverviewStyle = {
    container: {
        flexGrow: 1,
        minWidth: '100%',
        '& h2': {
            textAlign: 'center',
            fontSize: '1.5em',
        },
    },
    header: {
        ...primaryCardHeader,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    clothes: {
        '& h1': {
            fontSize: '1.6em',
            paddingLeft: 10,
        },
        flexGrow: 1,
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        minHeight: 400,
    },
    addedClothes: {
        flexGrow: 1,
        background: '#eee',
        marginLeft: 10,
        marginBottom: 20,
        padding: '0px 10px',
    },
    list: {
        listStyle: 'none',
        margin: '0',
        padding: '30px 0',
        '& li': {
            margin: '0px 0px 26px',
            flexWrap: 'wrap',
            position: 'relative',
            justifyContent: 'space-between',
            '& span.highlightOnHover:hover': {
                cursor: 'pointer',
                fontWeight: 500,
                '& svg': {
                    color: primaryColor,
                },
            },
            '& > span': {
                display: 'inline-flex',
                alignItems: 'center',
                minWidth: 220,
                minHeight: 54,
            },
            '& svg': {
                minWidth: 45,
                textAlign: 'center',
                marginRight: '10px',
                '@media (min-width: 960px)': {
                    minWidth: 'auto',
                    marginRight: '15px',
                },
            },
            '@media (min-width: 960px)': {
                flexWrap: 'nowrap',
                justifyContent: 'space-between',
                '& svg': {
                    minWidth: 60,
                },
            },
        },
    },
    actions: {
        borderLeft: `1px solid ${primaryColor}`,
        padding: '15px 15px 15px 30px',
        display: 'inline-flex',
        alignItems: 'center',
        background: primaryColor,
        color: white,
        flexGrow: 1,
        justifyContent: 'space-evenly',
        '& span': {
            display: 'flex',
        },
        '& span:hover': {
            cursor: 'pointer',
            color: secondaryColor,
        },
        '@media (min-width: 960px)': {
            flexGrow: 0,
        },
    },
    content: {
        position: 'relative',
        '& > div': {
            background: white,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            padding: '10px 15px',
            marginBottom: 20,
        },
    },
    print: {
        position: 'absolute',
        right: 10,
        top: -44,
        borderRadius: '3px 3px 0 0',
        boxShadow: 'none !important',
        '&:hover': {
            cursor: 'pointer',
            background: primaryColor,
            color: white,
        },
    },
    details: {
        '& h1': {
            fontSize: '1.6em',
            paddingRight: 30,
        },
        width: '100%',
        '& > div': {
            minHeight: 320,
        },
    },
    filter: {
        minWidth: '45%',
        '@media (min-width: 960px)': {
            minWidth: 100,
            marginTop: -9,
        },
        '@media (min-width: 1440px)': {
            minWidth: 110,
        },
    },
    globalActions: {
        paddingLeft: 10,
        display: 'flex',
        alignItems: 'center',
        borderTop: '1px solid #ddd',
        '& button': {
            transition: 'all .2s ease-in-out',
            maxHeight: 30,
            border: 'none',
            marginBottom: -3,
            color: white,
            background: primaryColor,
            paddingTop: 3,
            '&:hover': {
                filter: 'brightness(110%)',
                cursor: 'pointer',
            },
        },
    },
    disabled: {
        background: 'rgba(0,0,0, .3) !important',
        cursor: 'not-allowed !important',
    },
    overlay: {
        background: 'rgba(0,0,0,0.6)',
        borderRadius: 5,
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& > div': {
            padding: '20px 30px',
            borderRadius: '5px',
            background: '#eee',
            boxShadow:
                '2px 1px 5px -1px rgba(0,0,0,0.8), 0px 0px 1px 0px rgba(0,0,0,0.74), 0px 3px 5px -2px rgba(0,0,0,0.72)',
            '& h1': {
                fontSize: '1.5rem',
            },
        },
    },
    hover: {
        '&:hover': {
            cursor: 'pointer',
            filter: 'brightness(90%)',
        },
    },
    prices: {
        paddingLeft: 10,
        paddingRight: 10,
        flexGrow: 1,
        '& h2': {
            margin: 0,
            fontSize: '20px',
            textAlign: 'left',
            width: '50%',
            minWidth: 300,
            '& span': {
                fontWeight: 'bold',
                float: 'right',
            },
        },
    },
};

export default projectsOverviewStyle;
