/* istanbul ignore file */
import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import { useParams } from 'react-router';
import { observer } from 'mobx-react-lite';
import ArticleRow from '../Articles/Components/ArticleRow';
import Inputs from 'components/Inputs';
import { useEffect } from 'react';

function RackOverview() {
    const { articles, clothingManagement } = useStore();
    const { id } = useParams();

    useEffect(() => {
        id && clothingManagement.getRack(id);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function rowRenderer({ key, index, style }) {
        const article = articles.resource.data[index] || {
            clothing: {},
            color: {},
            pattern: {},
            fabric: {},
            rack: {},
            clothingSize: {},
        };

        return <ArticleRow key={key} style={style} article={article} edit={false} />;
    }

    return (
        <ResourceOverview
            title={`Artikels voor rek: ${clothingManagement.rackDescription}`}
            backend
            apiPrefix={`/racks/${id}`}
            store={articles.resource}
            rowRenderer={rowRenderer}
            heightOffset={450}
            filters={
                <>
                    <Inputs.articles.filters.size />
                    <Inputs.articles.filters.fabric />
                    <Inputs.articles.filters.pattern />
                    <Inputs.articles.filters.color />
                    <Inputs.articles.filters.rack />
                </>
            }
        />
    );
}

export default observer(RackOverview);
