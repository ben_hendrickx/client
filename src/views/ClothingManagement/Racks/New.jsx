/* istanbul ignore file */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { Save } from '@material-ui/icons';

import newProjectStyle from '../overview.style.jsx';
import useReactRouter from 'use-react-router';
import { useHistory } from 'react-router';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';

import CustomInput from 'components/CustomInput/CustomInput.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Page from 'components/Page.jsx';
import 'react-tagsinput/react-tagsinput.css';
import { withTranslation } from 'react-i18next';

function NewRack(props) {
    const { classes, t } = props;
    const { clothingManagement } = useStore();
    const history = useHistory();
    const { match } = useReactRouter();

    useEffect(() => {
        if (match && match.params && match.params.id) {
            clothingManagement.getRack(match.params.id);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [match.params.id]);

    useEffect(() => {
        return () => {
            delete clothingManagement.rackId;
            delete clothingManagement.rackDescription;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page
            title={`${
                match && match.params && match.params.id
                    ? `${t('modal.title.rack.edit')}: ${match.params.id}`
                    : t('modal.title.rack.new')
            }`}
            loading={clothingManagement.loading}
        >
            <div className={classes.content}>
                <div style={{ display: 'block' }}>
                    <CustomInput
                        labelText={t('form.labels.clothing.rackNumber')}
                        id="cast"
                        type="number"
                        formControlProps={{
                            required: true,
                            fullWidth: true,
                        }}
                        inputProps={{
                            useValue: true,
                            native: true,
                            color: 'primary',
                            type: 'number',
                            autoComplete: 'off',
                            value: clothingManagement.rackId,
                            onChange: e => (clothingManagement.rackId = e.target.value),
                        }}
                    />
                    <CustomInput
                        labelText={t('form.labels.clothing.rackDescription')}
                        id="cast"
                        type="text"
                        formControlProps={{
                            required: true,
                            fullWidth: true,
                        }}
                        inputProps={{
                            useValue: true,
                            native: true,
                            color: 'primary',
                            type: 'text',
                            autoComplete: 'off',
                            value: clothingManagement.rackDescription,
                            onChange: e => (clothingManagement.rackDescription = e.target.value),
                        }}
                    />
                </div>
                <Button
                    disabled={!clothingManagement.rackId || !clothingManagement.rackDescription}
                    onClick={async () => {
                        const id = match && match.params ? match.params.id : '';
                        await clothingManagement.saveRack(id);
                        history.replace(`/kledijbeheer/rekken`);
                    }}
                    color="success"
                    size="md"
                >
                    <Save style={{ marginRight: '5px' }} />
                    <span style={{ marginTop: '1px', marginLeft: '5px' }}>{t('form.button.save')}</span>
                </Button>
                <Button
                    onClick={() => {
                        history.replace(`/kledijbeheer/rekken`);
                    }}
                    style={{ marginLeft: '20px' }}
                    color="white"
                    size="md"
                >
                    {t('form.button.cancel')}
                </Button>
            </div>
        </Page>
    );
}

NewRack.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(newProjectStyle)(observer(NewRack)));
