/* istanbul ignore file */
import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import ListRow from 'components/ListRow';
import { Delete, Edit, RemoveRedEye } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function RackOverview({ t }) {
    const { account, clothingManagement, general } = useStore();
    const history = useHistory();

    function rowRenderer({ key, index, style }) {
        const rack = clothingManagement.racks.resource.filtered[index];

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span style={{ width: 100 }}>{rack.id}</span>
                    <span style={{ flexGrow: 1 }}>{rack.description}</span>
                    {account.rights.canUpdateRacks && (
                        <ListRowActions>
                            <Tooltip title={t('tooltip.rack.view')} placement="top">
                                <RemoveRedEye
                                    onClick={() => history.push(`/kledijbeheer/rekken/${rack.id}/artikels`)}
                                />
                            </Tooltip>
                            <Tooltip title={t('tooltip.rack.edit')} placement="top">
                                <Edit onClick={() => history.push(`/kledijbeheer/rekken/${rack.id}`)} />
                            </Tooltip>
                            <Tooltip title={t('tooltip.rack.delete')} placement="top">
                                <Delete
                                    onClick={() =>
                                        general.addModal({
                                            title: t('modal.title.rack.delete'),
                                            body: t('modal.body.rack.delete'),
                                            confirmText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await clothingManagement.racks.resource.delete(undefined, rack.id);
                                                clothingManagement.racks.resource.data = [];
                                                clothingManagement.racks.resource.status = [];
                                                await clothingManagement.racks.resource.fetch();
                                            },
                                        })
                                    }
                                />
                            </Tooltip>
                        </ListRowActions>
                    )}
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            store={clothingManagement.racks.resource}
            rowRenderer={rowRenderer}
            heightOffset={450}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateRacks && {
                    icon: 'add',
                    route: '/kledijbeheer/rekken/nieuw',
                    tooltip: t('tooltip.rack.create'),
                },
                account.rights.canExportRacks && {
                    icon: 'getApp',
                    tooltip: t('tooltip.report.download'),
                    href: '/api/reports/racks.xlsx',
                },
            ]}
        />
    );
}

export default withTranslation()(observer(RackOverview));
