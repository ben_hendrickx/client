/* istanbul ignore file */
import React from 'react';
import { observer } from 'mobx-react-lite';
import { withStyles } from '@material-ui/styles';
import { useStore } from '../../mobx';
import CustomInput from 'components/CustomInput/CustomInput';
import Button from 'components/CustomButtons/Button.jsx';

const pinScreenStyle = {
    pinscreen: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        '& > main': {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            background: '#fff',
            borderRadius: 5,
            padding: 60,
            maxWidth: 550,
        },
    },
    success: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        borderRadius: 5,
        maxWidth: 550,
        padding: '0px 25px',
        background: '#54ab25',
        color: '#fff',
        marginBottom: 50,
        width: '100%',
        '& p': {
            margin: 0,
            padding: '10px 0',
            fontSize: '1.05em',
        },
    },
    error: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        borderRadius: 5,
        maxWidth: 550,
        padding: '0px 25px',
        background: '#f44336',
        color: '#fff',
        marginBottom: 50,
        width: '100%',
        '& p': {
            margin: 0,
            padding: '10px 0',
            fontSize: '1.05em',
        },
    },
};

function PinScreen(props) {
    const { classes } = props;
    const { account, clothingManagement, general } = useStore();

    let notification = null;

    if (clothingManagement.success) {
        notification = (
            <div className={classes.success}>
                <p>{clothingManagement.success}</p>
            </div>
        );
    }

    if (clothingManagement.error) {
        notification = (
            <div className={classes.error}>
                <main>
                    <p>{clothingManagement.error}</p>
                </main>
            </div>
        );
    }

    const buttonsDisabled = !clothingManagement.artNumber || clothingManagement.artNumber.length < 6;

    return (
        <div className={classes.pinscreen}>
            {notification}
            <main>
                <p style={{ fontSize: '2.2em', fontWeight: 500, marginBottom: 20 }}>Geef artikel-nummer in:</p>
                <p>
                    Een artikel-nummer bestaat uit 6 cijfers.
                    <br />
                    Dit nummer vind je terug op het etiket van het betreffende kledingstuk.
                </p>
                <div style={{ textAlign: 'center' }}>
                    <CustomInput
                        labelText="Artikel-nummer"
                        id="cast"
                        formControlProps={{
                            required: true,
                            fullWidth: true,
                        }}
                        inputProps={{
                            inputProps: { style: { fontSize: '4em' } },
                            autoFocus: true,
                            color: 'primary',
                            type: 'text',
                            autoComplete: 'off',
                            value: clothingManagement.artNumber,
                            onChange: e => (clothingManagement.artNumber = e.target.value),
                        }}
                    />
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    {account.rights.canUpdateArticles && (
                        <>
                            <Button
                                onClick={() => {
                                    general.addModal({
                                        title: 'Rek-nummer',
                                        body: (
                                            <>
                                                <>
                                                    <span>Geef het rek-nummer in waar je het artikel gaat hangen</span>
                                                </>
                                                <CustomInput
                                                    labelText="Rek-nummer"
                                                    id="cast"
                                                    formControlProps={{
                                                        required: true,
                                                        fullWidth: true,
                                                    }}
                                                    inputProps={{
                                                        autoFocus: true,
                                                        color: 'primary',
                                                        type: 'text',
                                                        autoComplete: 'off',
                                                        value: clothingManagement.rack,
                                                        onChange: e => (clothingManagement.rack = e.target.value),
                                                    }}
                                                />
                                            </>
                                        ),
                                        onConfirm: () => {
                                            clothingManagement.return();
                                        },
                                    });
                                }}
                                color="success"
                                size="lg"
                                disabled={buttonsDisabled}
                            >
                                Terug in rek
                            </Button>
                            <Button
                                onClick={() => clothingManagement.washing()}
                                color="primary"
                                size="sm"
                                disabled={buttonsDisabled}
                            >
                                Naar wasserij
                            </Button>
                        </>
                    )}
                    {account.rights.canDeleteArticles && (
                        <Button
                            onClick={() => clothingManagement.remove()}
                            color="danger"
                            size="sm"
                            disabled={buttonsDisabled}
                        >
                            In vuilbak
                        </Button>
                    )}
                </div>
            </main>
        </div>
    );
}

export default withStyles(pinScreenStyle)(observer(PinScreen));
