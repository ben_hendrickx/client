/* istanbul ignore file */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
    Accessibility,
    KeyboardReturn,
    ImportExport,
    ColorLens,
    DragIndicator,
    Eco,
    AmpStories,
} from '@material-ui/icons';
import { useHistory } from 'react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import TabView from 'components/TabView.jsx';
import ArticleOverview from 'views/ClothingManagement/Articles/Overview.jsx';
import RackOverview from 'views/ClothingManagement/Racks/Overview.jsx';
import ColorOverview from 'views/ClothingManagement/Colors/Overview.jsx';
import FabricsOverview from 'views/ClothingManagement/Fabrics/Overview.jsx';
import PatternOverview from 'views/ClothingManagement/Patterns/Overview.jsx';
import SizesOverview from 'views/ClothingManagement/Sizes/Overview.jsx';
import NewSize from 'views/ClothingManagement/Sizes/New.jsx';
import NewFabric from 'views/ClothingManagement/Fabrics/New.jsx';
import NewRack from 'views/ClothingManagement/Racks/New.jsx';
import RackDetail from 'views/ClothingManagement/Racks/Detail.jsx';
import NewPattern from 'views/ClothingManagement/Patterns/New.jsx';
import NewColor from 'views/ClothingManagement/Colors/New.jsx';
import Import from 'views/ClothingManagement/Import.jsx';
import { Success } from 'components/Notifications.jsx';
import Page from 'components/Page.jsx';
import { withTranslation } from 'react-i18next';

function ProjectsOverview({ t }) {
    const { account, clothingManagement } = useStore();
    const history = useHistory();

    useEffect(() => {
        return () => {
            clothingManagement.success = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page title={t('page.title.clothingManagement')}>
            <Success message={clothingManagement.success} onClose={() => (clothingManagement.success = '')} />
            <TabView
                tabs={[
                    account.rights.canReadArticles && {
                        label: t('resource.artikels'),
                        icon: <Accessibility />,
                        onClick: () => history.push(`/kledijbeheer/artikels`),
                    },
                    account.rights.canReadClothingSizes && {
                        label: t('resource.kledij-maten'),
                        icon: <Accessibility />,
                        onClick: () => history.push(`/kledijbeheer/kledij-maten`),
                    },
                    account.rights.canReadFabrics && {
                        label: t('resource.stoffen'),
                        icon: <Eco />,
                        onClick: () => history.push(`/kledijbeheer/stoffen`),
                    },
                    account.rights.canReadPatterns && {
                        label: t('resource.motieven'),
                        icon: <DragIndicator />,
                        onClick: () => history.push(`/kledijbeheer/motieven`),
                    },
                    account.rights.canReadColors && {
                        label: t('resource.kleuren'),
                        icon: <ColorLens />,
                        onClick: () => history.push(`/kledijbeheer/kleuren`),
                    },
                    account.rights.canReadRacks && {
                        label: t('resource.rekken'),
                        icon: <AmpStories />,
                        onClick: () => history.push(`/kledijbeheer/rekken`),
                    },
                ]}
                actions={[
                    process.env.REACT_APP_IS_FVD === 'true' &&
                        account.rights.canCreateArticles && {
                            tooltip: 'Importeren van database',
                            icon: <ImportExport />,
                            onClick: () => history.push('/kledijbeheer/import'),
                        },
                    process.env.REACT_APP_IS_FVD === 'true' &&
                        account.rights.canUpdateArticles && {
                            tooltip: 'Ontvangen van kledingstuk',
                            icon: <KeyboardReturn />,
                            onClick: () => window.open('/kledijbeheer/terugname'),
                        },
                    // {
                    //     tooltip: 'Help',
                    //     icon: <Help />,
                    //     onClick: () => history.push('/kledijbeheer/help'),
                    // },
                ]}
                routes={[
                    { path: '/', component: ArticleOverview },
                    { path: '/kledijbeheer/artikels', component: ArticleOverview },
                    { path: '/kledijbeheer/stoffen', component: FabricsOverview },
                    { path: '/kledijbeheer/motieven', component: PatternOverview },
                    { path: '/kledijbeheer/kleuren', component: ColorOverview },
                    { path: '/kledijbeheer/rekken', component: RackOverview },
                    { path: '/kledijbeheer/stoffen/nieuw', component: NewFabric },
                    { path: '/kledijbeheer/stoffen/:id', component: NewFabric },
                    { path: '/kledijbeheer/motieven/nieuw', component: NewPattern },
                    { path: '/kledijbeheer/motieven/:id', component: NewPattern },
                    { path: '/kledijbeheer/kleuren/nieuw', component: NewColor },
                    { path: '/kledijbeheer/kleuren/:id', component: NewColor },
                    { path: '/kledijbeheer/rekken/nieuw', component: NewRack },
                    { path: '/kledijbeheer/rekken/:id', component: NewRack },
                    { path: '/kledijbeheer/rekken/:id/artikels', component: RackDetail },
                    { path: '/kledijbeheer/import', component: Import },
                    { path: '/kledijbeheer/kledij-maten', component: SizesOverview },
                    { path: '/kledijbeheer/kledij-maat/nieuw', component: NewSize },
                    { path: '/kledijbeheer/kledij-maat/:id', component: NewSize },
                ]}
            />
        </Page>
    );
}

ProjectsOverview.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(observer(ProjectsOverview));
