/* istanbul ignore file */
import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import ListRow from 'components/ListRow';
import { Delete, Edit } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import { useHistory } from 'react-router';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function ColorOverview({ t }) {
    const { account, clothingManagement, general } = useStore();
    const history = useHistory();

    function rowRenderer({ key, index, style }) {
        const color = clothingManagement.colors.resource.filtered[index];

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span>{color.name}</span>
                    {account.rights.canUpdateColors && (
                        <ListRowActions>
                            <Tooltip title={t('tooltip.color.edit')} placement="top">
                                <Edit onClick={() => history.push(`/kledijbeheer/kleuren/${color.id}`)} />
                            </Tooltip>
                            <Tooltip title={t('tooltip.color.delete')} placement="top">
                                <Delete
                                    onClick={() =>
                                        general.addModal({
                                            title: t('modal.title.color.delete'),
                                            body: t('modal.body.color.delete'),
                                            confirmText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await clothingManagement.colors.resource.delete(undefined, color.id);
                                                clothingManagement.colors.resource.data = [];
                                                clothingManagement.colors.resource.status = [];
                                                await clothingManagement.colors.resource.fetch();
                                            },
                                        })
                                    }
                                />
                            </Tooltip>
                        </ListRowActions>
                    )}
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            store={clothingManagement.colors.resource}
            rowRenderer={rowRenderer}
            heightOffset={450}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateColors && {
                    icon: 'add',
                    route: '/kledijbeheer/kleuren/nieuw',
                    tooltip: t('tooltip.color.create'),
                },
                account.rights.canExportColors && {
                    icon: 'getApp',
                    tooltip: t('tooltip.report.download'),
                    href: '/api/reports/colors.xlsx',
                },
            ]}
        />
    );
}

export default withTranslation()(observer(ColorOverview));
