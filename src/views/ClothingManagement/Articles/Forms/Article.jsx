/* istanbul ignore file */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewArticle(props) {
    const { articles } = useStore();

    useEffect(() => {
        articles.getSuggestions();

        return () => {
            articles.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Form
            store={articles}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                articles.resource.data = [];
                articles.resource.status = [];
                articles.resource.fetch();
            }}
        />
    );
}

NewArticle.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewArticle);
