/* istanbul ignore file */
import { Tooltip } from '@material-ui/core';
import ListRow from 'components/ListRow';
import React from 'react';
import { Add, CheckCircle, Delete, Edit, Subject } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { useStore } from '../../../../mobx';
import { observer } from 'mobx-react-lite';
import ArticleForm from '../Forms/Article';
import { withTranslation } from 'react-i18next';

function pad(value, length) {
    if ((value || '').toString().length >= length) {
        return value;
    }

    return pad('0' + value, length);
}

const Placeholder = ({ width = 50 }) => <div style={{ height: 15, width, background: 'rgba(0,0,0,.1)' }}></div>;

function ArticleRow(props) {
    const { style, article, onArticleClick, onDelete, actions, edit = true, returned, amount, t } = props;
    const {
        id,
        active,
        clothingSizeId,
        size,
        fabricId,
        fabric,
        patternId,
        pattern,
        colorId,
        color,
        categoryId,
        category,
        tags,
        rackId,
        rack,
        washing,
        description,
        rackDescription,
    } = article;
    const { account, articles, general } = useStore();

    const Status = () => {
        let label;
        if (active === undefined) {
            return <Placeholder width={200} />;
        }

        if (!!active) {
            if (parseInt(rackId) > 0) {
                label = `${t('form.labels.clothing.rack')}: ${rackId} - ${rackDescription || rack.description}`;
            }
            if (!!washing) {
                label = <b>{t('form.labels.clothing.washing')}</b>;
            }

            if (!washing && rackId === 0) {
                label = <b>{t('form.labels.clothing.rented')}</b>;
            }
        } else {
            label = <b>{t('form.labels.clothing.inactive')}</b>;
        }

        return <span style={{ width: 300 }}>{label}</span>;
    };

    if (article.status) {
        return (
            <li style={style}>
                <ListRow>
                    <div
                        style={{ display: 'flex', flexGrow: 1, justifyContent: 'space-between', alignItems: 'center' }}
                    >
                        <span style={{ width: 150 }}>{t('form.labels.clothing.noArticleNumber')}</span>
                        <span style={{ width: 400 }}>
                            {
                                [
                                    { label: t('form.labels.clothing.status.toCut'), value: 1 },
                                    { label: t('form.labels.clothing.status.cutting'), value: 2 },
                                    { label: t('form.labels.clothing.status.cut'), value: 3 },
                                    { label: t('form.labels.clothing.status.fixing'), value: 4 },
                                    { label: t('form.labels.clothing.status.toAdjust'), value: 5 },
                                    { label: t('form.labels.clothing.status.ownClothing'), value: 5 },
                                ].find(({ value }) => value === article.status)?.label
                            }
                        </span>
                        {!!article.remarks && (
                            <span style={{ minWidth: 40 }}>
                                <Tooltip title={article.remarks} placement="top">
                                    <Subject />
                                </Tooltip>
                            </span>
                        )}
                    </div>
                    <ListRowActions>
                        <Tooltip title={t('tooltip.article.delete')} placement="top">
                            <Delete onClick={() => onDelete(article.uuid)} />
                        </Tooltip>
                    </ListRowActions>
                </ListRow>
            </li>
        );
    }

    return (
        <li style={style}>
            <ListRow>
                <div style={{ display: 'flex', flexGrow: 1, justifyContent: 'space-between', alignItems: 'center' }}>
                    <span style={{ width: 80 }}>{id ? pad(id, 6) : <Placeholder width={70} />}</span>
                    {description ? (
                        <Tooltip
                            title={`${t('form.labels.clothing.category')}: ${category?.name || 'Geen categorie'} | ${t(
                                'form.labels.clothing.clothingSize'
                            )}: ${size?.size} | ${t('form.labels.clothing.fabric')}: ${fabric?.type ||
                                'Geen stof'} |  ${t('form.labels.clothing.pattern')}: ${pattern?.name ||
                                'Geen motief'} | ${t('form.labels.clothing.color')}: ${color?.name || 'Geen kleur'}`}
                            placement="top"
                        >
                            <span style={{ width: 300 }}>
                                {description}
                                {tags?.includes('accessoire') &&
                                    !!amount &&
                                    ` (${article.returned}/${article.amount} stuks)`}
                            </span>
                        </Tooltip>
                    ) : (
                        <Placeholder width={150} />
                    )}
                    {article.tags !== undefined ? (
                        article.tags ? (
                            <Tooltip title={`${tags.split('|').join(', ')}`} placement="top">
                                <span style={{ width: 80 }}>{tags?.split('|').length} tags</span>
                            </Tooltip>
                        ) : (
                            <span style={{ width: 80 }}>0 tags</span>
                        )
                    ) : (
                        <Placeholder width={70} />
                    )}
                    <Status />
                    {!!returned && <CheckCircle style={{ color: 'green' }} />}
                    {!returned && <span style={{ width: 20 }}></span>}
                </div>
                {!!article.remarks && (
                    <span style={{ minWidth: 40 }}>
                        <Tooltip title={article.remarks} placement="top">
                            <Subject />
                        </Tooltip>
                    </span>
                )}
                {((account.rights.canUpdateArticles && edit) || onArticleClick || onDelete || !!actions?.length) && (
                    <ListRowActions>
                        {account.rights.canUpdateArticles && edit && (
                            <Tooltip title={t('tooltip.article.edit')} placement="top">
                                <Edit
                                    onClick={
                                        id
                                            ? () => {
                                                  let modal;
                                                  const closeModal = () => modal.close();
                                                  articles.data = {
                                                      id,
                                                      active,
                                                      categoryId,
                                                      clothingSizeId,
                                                      patternId,
                                                      fabricId,
                                                      colorId,
                                                      rackId,
                                                      tags: tags?.split('|'),
                                                      washing,
                                                      description,
                                                  };
                                                  modal = general.addModal({
                                                      type: 'window',
                                                      width: 1100,
                                                      title: `${t('modal.title.article.edit')}: ${pad(
                                                          articles.data.id,
                                                          6
                                                      )}`,
                                                      onMinimize: async modal => {
                                                          articles.data.id &&
                                                              (modal.title = `${t('modal.title.article.edit')}: ${pad(
                                                                  articles.data.id,
                                                                  6
                                                              )}`);
                                                          return articles.data;
                                                      },
                                                      onMaximize: modal => {
                                                          articles.data = { ...modal.data };
                                                      },
                                                      body: (
                                                          <ArticleForm onCancel={closeModal} onConfirm={closeModal} />
                                                      ),
                                                  });
                                              }
                                            : () => {}
                                    }
                                />
                            </Tooltip>
                        )}
                        {onArticleClick && (
                            <Tooltip title={t('tooltip.article.add')} placement="top">
                                <Add onClick={() => id && onArticleClick(id)} />
                            </Tooltip>
                        )}
                        {onDelete && (
                            <Tooltip title={t('tooltip.article.delete')} placement="top">
                                <Delete onClick={() => id && onDelete(article.uuid)} />
                            </Tooltip>
                        )}
                        {!!actions?.length ? actions : null}
                    </ListRowActions>
                )}
            </ListRow>
        </li>
    );
}

export default withTranslation()(observer(ArticleRow));
