/* istanbul ignore file */
import React, { useEffect } from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import ArticleRow from './Components/ArticleRow';
import ArticleForm from './Forms/Article';
import Inputs from 'components/Inputs';
import { withTranslation } from 'react-i18next';

function ArticlesOverview({ offset, onArticleClick, t }) {
    const { account, articles, general, clothingManagement } = useStore();

    useEffect(() => {
        clothingManagement.categories.resource.fetch();
        clothingManagement.sizes.resource.fetch();
        clothingManagement.colors.resource.fetch();
        clothingManagement.patterns.resource.fetch();
        clothingManagement.fabrics.resource.fetch();
        clothingManagement.racks.resource.fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function rowRenderer({ key, index, style }) {
        const article = articles.resource.data[index] || {
            clothing: {},
            color: {},
            pattern: {},
            fabric: {},
            rack: {},
            clothingSize: {},
        };

        return <ArticleRow key={key} style={style} article={article} onArticleClick={onArticleClick} />;
    }

    function onCreateArticle() {
        let modal;
        const closeModal = () => modal.close();
        articles.data.active = true;
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.article.new'),
            onMinimize: modal => {
                articles.data.categoryId &&
                    (modal.title = `${t('modal.title.article.new')}: ${
                        clothingManagement.categories?.resource.data.find(({ id }) => id === articles.data.categoryId)
                            ?.name
                    }`);
                return articles.data;
            },
            onMaximize: modal => {
                articles.data = { ...modal.data };
            },
            body: <ArticleForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <ResourceOverview
            backend
            checkable={!!onArticleClick}
            store={articles.resource}
            rowRenderer={rowRenderer}
            heightOffset={offset || 450}
            filters={
                <>
                    <Inputs.articles.filters.accessoire />
                    <Inputs.articles.filters.size />
                    <Inputs.articles.filters.fabric />
                    <Inputs.articles.filters.pattern />
                    <Inputs.articles.filters.color />
                    <Inputs.articles.filters.rack />
                </>
            }
            export={account.rights.canExportArticles}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateArticles && {
                    icon: 'add',
                    onClick: onCreateArticle,
                    tooltip: t('tooltip.article.create'),
                },
            ]}
        />
    );
}

export default withTranslation()(observer(ArticlesOverview));
