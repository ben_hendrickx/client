/* istanbul ignore file */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';

import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Page from 'components/Page.jsx';
import CsvDropzone from 'components/Dropzone/CsvDropzone';
import Card from 'components/Card/Card';
import Button from 'components/CustomButtons/Button.jsx';
import { Success } from 'components/Notifications';
import ProgressBar from 'components/ProgressBar';

const style = {
    title: {
        textAlign: 'left !important',
        marginTop: 0,
    },
    dropzone: {
        marginTop: 0,
        border: `2px dashed #888`,
        textAlign: 'center',
        color: '#888',
        borderRadius: '5px',
        fontSize: '0.9em',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& > div': {
            minHeight: '200px',
            width: '80%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    },
    card: {
        margin: 0,
        background: '#fff',
        boxShadow:
            'rgba(0, 0, 0, 0.2) 0px 1px 3px 0px, rgba(0, 0, 0, 0.14) 0px 1px 1px 0px, rgba(0, 0, 0, 0.12) 0px 2px 1px -1px',
        padding: '10px 15px',
        borderRadius: 0,
    },
    button: {
        width: 200,
    },
};

function Import(props) {
    const { classes } = props;
    const { clothingManagement } = useStore();
    const [items, setItems] = useState(null);
    const [success, setSuccess] = useState(null);

    useEffect(() => {
        clothingManagement.listen();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page title="Import">
            <Success message={success} onClose={() => setSuccess('')} />
            {!success && items && clothingManagement.progress > 0 && (
                <Card className={classes.card}>
                    <h2 className={classes.title}>Even geduld aub</h2>
                    <ProgressBar
                        progress={clothingManagement.progress}
                        text={`${clothingManagement.text}` || 'Voorbereiden'}
                    />
                </Card>
            )}
            {!items && !clothingManagement.importing && (
                <CsvDropzone
                    onData={data => {
                        data = data
                            .split('\n')
                            .slice(1)
                            .map(row => {
                                const [
                                    artId,
                                    description,
                                    tags,
                                    clothingSize,
                                    fabric,
                                    pattern,
                                    color,
                                    rackId,
                                    rackDescription,
                                    categorie,
                                    internalPrice,
                                    externalPrice,
                                    replacementPrice,
                                ] = row.split(';');

                                return {
                                    artId,
                                    description,
                                    tags,
                                    clothingSize,
                                    fabric,
                                    pattern,
                                    color,
                                    rackId,
                                    rackDescription,
                                    categorie,
                                    internalPrice,
                                    externalPrice,
                                    replacementPrice,
                                };
                            });

                        setSuccess('');
                        setItems(data);
                    }}
                    className={classes.dropzone}
                />
            )}

            {items && !clothingManagement.importing && (
                <Card className={classes.card}>
                    <h2 className={classes.title}>Bestand geladen</h2>
                    <p>Ben je zeker dat je {items.length} items wil importeren?</p>
                    <Button
                        className={classes.button}
                        onClick={async () => {
                            await clothingManagement.import(items);
                            setItems(null);
                            setSuccess(`${items.length} artikels werden succesvol geïmporteerd`);
                        }}
                        color="success"
                        size="md"
                    >
                        Importeer {items.length} artikels
                    </Button>
                </Card>
            )}
        </Page>
    );
}

Import.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(style)(observer(Import));
