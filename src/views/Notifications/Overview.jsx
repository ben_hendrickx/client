import React, { useEffect } from 'react';
import { Notifications, Schedule, RemoveRedEye, Delete, Person, VisibilityOff, Visibility } from '@material-ui/icons';
import { useHistory, useParams } from 'react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Tooltip from '@material-ui/core/Tooltip';
import { Checkbox } from '@material-ui/core';
import ResourceOverview from 'components/ResourceOverview.jsx';
import ListRow from 'components/ListRow.jsx';
import ListRowActions from 'components/ListRowActions.jsx';
import Inputs from 'components/Inputs';
import Button from '../../components/CustomButtons/Button.jsx';
import moment from 'moment';
import Notification from './Notification';
import { withTranslation } from 'react-i18next';
import Loader from 'components/Loader';

const ObservedCheckBox = observer(({ id }) => {
    const { notifications } = useStore();

    return (
        <div style={{ padding: '6px 0' }}>
            <Checkbox
                checked={notifications.resource.checked.includes(id)}
                onChange={e => {
                    if (!notifications.resource.checked.includes(id)) {
                        notifications.resource.checked.push(id);
                    } else {
                        notifications.resource.checked.splice(notifications.resource.checked.indexOf(id), 1);
                    }
                }}
            />
        </div>
    );
});

function NotificationsOverview({ t }) {
    const { notifications, account, general, projects } = useStore();
    const history = useHistory();
    const { id } = useParams();

    useEffect(() => {
        if (id) {
            notifications.resource.filters.project = id;
        }

        return () => {
            id && delete projects.id;
            delete notifications.resource.filters.project;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    function rowRenderer({ key, style, index, checkable = true }) {
        const notification = notifications.resource.filtered[index];

        return (
            <li key={key} style={{ ...style, display: 'flex' }}>
                {checkable && <ObservedCheckBox id={notification.id} />}
                <ListRow style={{ flexGrow: 1 }}>
                    <span style={{ width: 400 }}>
                        <Notifications /> {notification.label}
                    </span>
                    <span
                        style={{ width: 200 }}
                        onClick={() => history.push(`/medewerker/${notification.user.id}/overzicht`)}
                    >
                        <Person /> {notification.user?.name}
                    </span>
                    <span style={{ width: 200 }}>
                        <Schedule /> {moment(notification.creationDt).format('DD/MM/YYYY HH:mm:ss')}
                    </span>
                    <ListRowActions>
                        <Tooltip title={t('tooltip.notifications.view')} placement="top">
                            <RemoveRedEye
                                onClick={() => {
                                    notifications.markRead(notification.id);
                                    general.addModal({
                                        title: notification?.label,
                                        body: <Notification notification={notification} />,
                                    });
                                }}
                            />
                        </Tooltip>
                        <Tooltip title={t('tooltip.notifications.delete')} placement="top">
                            <Delete
                                onClick={() => {
                                    general.addModal({
                                        title: t('modal.title.notifications.delete'),
                                        body: t('modal.body.notifications.delete'),
                                        buttonText: t('form.button.delete'),
                                        onConfirm: async () => {
                                            await notifications.resource.delete(`/user/${account.id}`, notification.id);
                                        },
                                    });
                                }}
                            />
                        </Tooltip>
                    </ListRowActions>
                </ListRow>
            </li>
        );
    }

    if (id && !notifications.resource.filters.project) {
        return <Loader />;
    }

    return (
        <>
            <ResourceOverview
                checkable
                apiPrefix={`/user/${account.id}`}
                store={notifications.resource}
                rowRenderer={rowRenderer}
                heightOffset={300}
                filters={
                    <>
                        <Inputs.notifications.filters.category />
                        <Inputs.notifications.filters.read />
                    </>
                }
                renderBefore={
                    notifications.resource.filtered.length > 0 && (
                        <div style={{ marginBottom: 10 }}>
                            <Button
                                onClick={async () => {
                                    if (notifications.resource.checked.length) {
                                        notifications.resource.checked = [];
                                    } else {
                                        notifications.resource.checked = notifications.resource.filtered.map(
                                            ({ id }) => id
                                        );
                                    }
                                }}
                                color="primary"
                                size="sm"
                            >
                                {t('form.button.notifications.selectAll')}
                            </Button>
                            <div style={{ display: 'inline' }}>
                                <Button
                                    disabled={!notifications.resource.checked.length}
                                    onClick={async () => {
                                        let body = t('modal.body.notifications.markRead');
                                        if (notifications.resource.checked.length > 1) {
                                            body = t('modal.body.notifications.markReadMany');
                                        }
                                        general.addModal({
                                            title: t('modal.title.notifications.markRead'),
                                            body,
                                            buttonText: t('form.button.confirm'),
                                            onConfirm: async () => {
                                                await notifications.markRead(notifications.resource.checked);
                                                notifications.resource.checked = [];
                                                setTimeout(() => {
                                                    notifications.resource.data = [];
                                                    notifications.resource.status = [];
                                                    notifications.resource.fetch(`/user/${account.id}`);
                                                }, 100);
                                            },
                                        });
                                    }}
                                    color="danger"
                                    size="sm"
                                    style={{ float: 'right' }}
                                >
                                    <Visibility /> {t('form.button.notifications.markRead')}
                                </Button>
                                <Button
                                    disabled={!notifications.resource.checked.length}
                                    onClick={async () => {
                                        let body = t('modal.body.notifications.markUnread');
                                        if (notifications.resource.checked.length > 1) {
                                            body = t('modal.body.notifications.markUnreadMany');
                                        }
                                        general.addModal({
                                            title: t('modal.title.notifications.markUnread'),
                                            body,
                                            buttonText: t('form.button.confirm'),
                                            onConfirm: async () => {
                                                await notifications.markUnread(notifications.resource.checked);
                                                notifications.resource.checked = [];
                                                setTimeout(() => {
                                                    notifications.resource.data = [];
                                                    notifications.resource.status = [];
                                                    notifications.resource.fetch(`/user/${account.id}`);
                                                }, 100);
                                            },
                                        });
                                    }}
                                    color="danger"
                                    size="sm"
                                    style={{ float: 'right' }}
                                >
                                    <VisibilityOff /> {t('form.button.notifications.markUnread')}
                                </Button>
                                <Button
                                    disabled={!notifications.resource.checked.length}
                                    onClick={async () => {
                                        let body = t('modal.body.notifications.delete');
                                        if (notifications.resource.checked.length > 1) {
                                            body = t('modal.body.notifications.deleteMany');
                                        }
                                        general.addModal({
                                            title: t('modal.title.notifications.delete'),
                                            body,
                                            buttonText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await notifications.resource.delete(
                                                    `/user/${account.id}`,
                                                    notifications.resource.checked
                                                );
                                                notifications.resource.checked = [];
                                                notifications.resource.data = [];
                                                notifications.resource.status = [];
                                                await notifications.resource.fetch(`/user/${account.id}`);
                                            },
                                        });
                                    }}
                                    color="danger"
                                    size="sm"
                                    style={{ float: 'right' }}
                                >
                                    <Delete /> {t('form.button.notifications.delete')}
                                </Button>
                            </div>
                        </div>
                    )
                }
            />
        </>
    );
}

export default withTranslation()(observer(NotificationsOverview));
