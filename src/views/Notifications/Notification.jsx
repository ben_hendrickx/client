import { Person, Schedule } from '@material-ui/icons';
import React from 'react';
import moment from 'moment';
import parse from 'html-react-parser';
import { useHistory } from 'react-router';

function Notification(props) {
    const { notification } = props;
    const history = useHistory();
    
    return <>
        <small
            style={{
                display: 'flex',
                alignItems: 'center',
                borderBottom: '1px solid rgba(0,0,0,0.1)',
                borderTop: '1px solid rgba(0,0,0,0.1)',
                marginBottom: 20,
                padding: 10,
            }}
        >
            <Schedule style={{ marginRight: 10 }} />{' '}
            {moment(notification?.creationDt).format('DD/MM/YYYY HH:mm:ss')}
            <Person style={{ marginLeft: 20, marginRight: 10 }} />
            <span
                onClick={() =>
                    history.push(
                        `/medewerker/${notification?.user?.id}/overzicht`
                    )
                }
            >
                {notification?.user?.name}
            </span>
        </small>
        <p style={{ padding: 10 }}>{parse(notification?.notification || '')}</p>
    </>;
};

export default Notification;