import Form from 'components/Form';
import Page from 'components/Page';
import { useStore } from '../mobx';
import React, { useEffect } from 'react';
import { Success } from 'components/Notifications';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function Settings({ t }) {
    const { settings } = useStore();

    useEffect(() => {
        settings.fetch();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page title={t('page.title.settings')}>
            <Success message={settings.success} onClose={() => (settings.success = '')} />
            <Form
                store={settings}
                noCancel
                onConfirm={async () => {
                    await settings.save();
                    await settings.fetch();
                }}
            />
        </Page>
    );
}

export default withTranslation()(observer(Settings));
