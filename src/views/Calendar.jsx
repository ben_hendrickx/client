import FullCalendar from '@fullcalendar/react';
import React, { useEffect, useState } from 'react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction';
import Page from '../components/Page';
import { useRef } from 'react';
import useReactRouter from 'use-react-router';
import moment from 'moment';
import { useStore } from '../mobx';
import { withTranslation } from 'react-i18next';
import i18n from '../i18n';

const style = {
    background: '#fff',
    boxShadow: '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
    padding: 30,
    marginBottom: 30,
};

function Calendar({ t }) {
    const calendar = useRef();
    const [events, setEvents] = useState([]);
    const { match, history } = useReactRouter();
    const { account, ui, repetitions } = useStore();

    useEffect(() => {
        async function getData() {
            const all = await repetitions.getAll(match?.params?.id);
            setEvents(all.data);
        }

        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page title={t('page.title.calendar')}>
            <div style={{ ...style, ...(ui.isMobile ? { marginTop: -30, marginBottom: 0 } : {}) }}>
                <FullCalendar
                    ref={calendar}
                    buttonText={{
                        today: t('calendar.today'),
                        month: t('calendar.month'),
                        week: t('calendar.week'),
                        day: t('calendar.day'),
                        list: t('calendar.list'),
                    }}
                    dateClick={function(info) {
                        calendar.current.getApi().changeView('timeGridDay', info.date);
                    }}
                    eventClick={function(info) {
                        account.rights.canPresentRepetitions && history.push(`/${info.event.id}/aanwezigheid`);
                    }}
                    allDaySlot={false}
                    height={ui.isMobile ? 'calc(100vh - 115px)' : 'calc(100vh - 225px)'}
                    locale={i18n.language === 'fr' ? 'fr-FR' : i18n.language === 'en' ? 'en-US' : 'nl-BE'}
                    themeSystem="bootstrap"
                    plugins={[dayGridPlugin, timeGridPlugin, listPlugin, interactionPlugin]}
                    initialView={ui.isMobile ? 'timeGridDay' : 'dayGridMonth'}
                    schedulerLicenseKey="CC-Attribution-NonCommercial-NoDerivatives"
                    weekends={true}
                    events={events.map(event => ({
                        id: `projecten/${event.projectId}/repetities/${event.id}`,
                        title: event.identifier,
                        start: event.startDt,
                        end: event.endDt,
                    }))}
                    headerToolbar={
                        ui.isMobile
                            ? { left: 'prev', center: 'title', right: 'next' }
                            : {
                                  left: 'prev,next today',
                                  center: 'title',
                                  right: 'dayGridMonth,dayGridWeek,timeGridDay,listWeek',
                              }
                    }
                    time
                    eventTimeFormat={{
                        hour: '2-digit',
                        minute: '2-digit',
                        meridiem: false,
                    }}
                    slotMinTime="07:00"
                    slotMaxTime="23:00"
                    editable={true}
                    eventDrop={async info => {
                        if (moment(info.oldEvent._instance.range.end).isBefore(moment())) {
                            return info.revert();
                        }
                        if (moment().isAfter(info.event.start)) {
                            return info.revert();
                        }

                        // const { id } = info.event;
                        // const oldEvent = events.find(event => event.id === parseInt(id));
                        // const values = { ...oldEvent, startsAt: moment(info.event.start), endsAt: moment(info.event.end)};
                        //await update({id, values, userId: oldEvent.user.id });
                    }}
                />
            </div>
        </Page>
    );
}

export default withTranslation()(Calendar);
