import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';

import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { Switch, Route } from 'react-router-dom';
import { LoggedIn } from 'components/Routes';
import { useHistory } from 'react-router';

import Overview from 'views/Dashboard/Overview.jsx';
import Projects from 'views/Projects/Overview.jsx';
import Navigation from 'components/Navigation.jsx';
import TopBar from 'components/TopBar.jsx';
import PinScreen from 'views/Repetitions/PinScreen.jsx';
import ReturnClothing from 'views/ClothingManagement/Return.jsx';
import ClothingDummy from 'views/ClothingManagement/ClothingDummy.jsx';
import ClothingOverview from 'views/ClothingManagement/Overview';
import ImagePreview from 'components/ImagePreview';

const dashboardStyle = {
    container: {
        display: 'flex',
        flexGrow: 1,
    },
    content: {
        padding: '0px 30px',
        flexGrow: 1,
        display: 'flex',
        flexWrap: 'wrap',
        '& > div': {
            width: '100%',
        },
        '@media print': {
            padding: 0,
        },
    },
};

function Dashboard(props) {
    const { classes } = props;
    const { account, ui, repetitions } = useStore();
    const history = useHistory();

    const pathNameLc = history.location.pathname.toLowerCase();

    if (pathNameLc.includes('pinscherm') || pathNameLc.includes('terugname') || pathNameLc.includes('help')) {
        return (
            <div>
                <div
                    className={classes.container}
                    style={
                        repetitions.success
                            ? { background: 'lightgreen' }
                            : repetitions.error
                            ? { background: 'tomato' }
                            : {}
                    }
                >
                    <div className={classes.content}>
                        <Route exact path="/pinscherm/:repetitionId" component={PinScreen} />
                        <Route exact path="/kledijbeheer/terugname" component={ReturnClothing} />
                        <Route exact path="/kledijbeheer/help" component={ClothingDummy} />
                    </div>
                </div>
            </div>
        );
    }

    let component = Projects;

    switch (account.level) {
        case 1:
        case 2:
            component = Overview;
            break;
        case 3:
            component = ClothingOverview;
            break;
        default:
            break;
    }

    return (
        <div>
            <TopBar />
            <div className={classes.container}>
                <Navigation />
                {!ui.isMobile && (
                    <><div className={classes.content}>
                        <Switch>
                            <Route exact path="/" component={component} />
                            <LoggedIn />
                        </Switch>
                    </div>
                    {ui.imagePreview && <ImagePreview />}</>
                )}
            </div>
        </div>
    );
}

Dashboard.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(dashboardStyle)(observer(Dashboard));
