import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import { Accessibility, FolderOpen, ShoppingBasket, SupervisedUserCircle, Wifi } from '@material-ui/icons';

import Widget from 'components/Widget.jsx';
import Flex from 'components/Flex.jsx';
import Page from 'components/Page.jsx';
import { withTranslation } from 'react-i18next';

function Overview({ t }) {
    const { account, overview } = useStore();
    const { rights } = account;

    const { canReadProjects, canReadUsers, canReadArticles, canReadOrders } = rights;

    useEffect(() => {
        overview.fetch();
        return () => {};
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.token]);

    return (
        <Page title={t('page.title.overview')} loading={overview.loading}>
            <Flex>
                <Widget
                    title={t('page.overview.numberOfProjects')}
                    number={overview.projects}
                    link={canReadProjects ? { href: '/projecten', label: t('page.overview.button.projects') } : null}
                    icon={<FolderOpen />}
                />
                <Widget
                    title={t('page.overview.numberOfEmployees')}
                    number={overview.employees}
                    link={canReadUsers ? { href: '/medewerkers', label: t('page.overview.button.employees') } : null}
                    icon={<SupervisedUserCircle />}
                />
                <Widget
                    title={t('page.overview.numberOnline')}
                    number={overview.online}
                    link={
                        canReadUsers ? { href: '/medewerkers/online', label: t('page.overview.button.online') } : null
                    }
                    icon={<Wifi />}
                />
            </Flex>
            <Flex>
                <Widget
                    title={t('page.overview.numberOfArticles')}
                    number={overview.articles}
                    link={
                        canReadArticles
                            ? { href: '/kledijbeheer/artikels', label: t('page.overview.button.articles') }
                            : null
                    }
                    icon={<Accessibility />}
                />
                <Widget
                    title={t('page.overview.numberOfOrders')}
                    number={overview.rentals}
                    link={
                        canReadOrders
                            ? { href: '/retributiebeheer/bestellingen', label: t('page.overview.button.orders') }
                            : null
                    }
                    icon={<ShoppingBasket />}
                />
            </Flex>
        </Page>
    );
}

Overview.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(observer(Overview));
