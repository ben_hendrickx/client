const overviewStyle = {
    section: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
    },
};

export default overviewStyle;
