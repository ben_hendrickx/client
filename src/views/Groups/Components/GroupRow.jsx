import Group from 'components/Group';
import React from 'react';
import ListRow from 'components/ListRow';
import { Delete, Edit, FilterNone, InfoOutlined, PeopleOutline } from '@material-ui/icons';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import GroupForm from '../Forms/Group';
import { useStore } from '../../../mobx';
import { useHistory } from 'react-router';
import { withTranslation } from 'react-i18next';

function GroupRow({ key, style, group, t }) {
    const { groups, projects, general, account } = useStore();
    const history = useHistory();
    const tooltip = t('tooltip.group.manageUsers');

    return (
        <li key={key} style={style}>
            <ListRow key={key}>
                <Group group={group} identifier users description />
                {(account.rights.canReadGroups || account.rights.canUpdateGroups || account.rights.canDeleteGroups) && (
                    <ListRowActions>
                        <Tooltip title={t('tooltip.group.view')} placement="top">
                            <InfoOutlined
                                onClick={() => history.push(`/projecten/${projects.id}/groepen/${group.id}`)}
                            />
                        </Tooltip>
                        {account.rights.canUpdateGroups && (
                            <Tooltip title={t('tooltip.group.edit')} placement="top">
                                <Edit
                                    onClick={() => {
                                        let modal;
                                        const closeModal = () => modal.close();
                                        const { id, identifier, name, description } = group;
                                        groups.resource.form.data = {
                                            id,
                                            identifier,
                                            name,
                                            description,
                                        };
                                        modal = general.addModal({
                                            type: 'window',
                                            width: 1100,
                                            title: t('modal.title.group.edit'),
                                            onMinimize: async modal => {
                                                groups.resource.form.data.name &&
                                                    (modal.title = `${t('modal.title.group.edit')}: ${
                                                        groups.resource.form.data.name
                                                    }`);
                                                return groups.resource.form.data;
                                            },
                                            onMaximize: modal => {
                                                groups.resource.form.data = { ...modal.data };
                                            },
                                            body: <GroupForm onCancel={closeModal} onConfirm={closeModal} />,
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                        {account.rights.canUpdateGroupUsers && (
                            <Tooltip title={tooltip} placement="top">
                                <PeopleOutline
                                    className="edit-users"
                                    onClick={() =>
                                        history.push(`/projecten/${projects.id}/groepen/bewerk/${group.id}/gebruikers`)
                                    }
                                />
                            </Tooltip>
                        )}
                        {account.rights.canCreateGroups && (
                            <Tooltip title={t('tooltip.group.copy')} placement="top">
                                <FilterNone
                                    onClick={() => {
                                        general.addModal({
                                            title: t('modal.title.group.copy'),
                                            body: t('modal.body.group.copy'),
                                            buttonText: t('form.button.duplicate'),
                                            onConfirm: async () => {
                                                await groups.copy(group.id);
                                                groups.resource.data = [];
                                                groups.resource.status = [];
                                                await groups.resource.fetch(`/project/${projects.id}`);
                                            },
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                        {account.rights.canDeleteGroups && (
                            <Tooltip title={t('tooltip.group.delete')} placement="top">
                                <Delete
                                    onClick={() => {
                                        general.addModal({
                                            title: t('modal.title.group.delete'),
                                            body: t('modal.body.group.delete'),
                                            buttonText: t('form.button.delete'),
                                            onConfirm: async () => {
                                                await groups.resource.delete(`/project/${projects.id}`, group.id);
                                                groups.resource.data = [];
                                                groups.resource.status = [];
                                                await groups.resource.fetch(`/project/${projects.id}`);
                                            },
                                        });
                                    }}
                                />
                            </Tooltip>
                        )}
                    </ListRowActions>
                )}
            </ListRow>
        </li>
    );
}

export default withTranslation()(GroupRow);
