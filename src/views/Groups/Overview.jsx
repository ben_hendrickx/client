import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Inputs from 'components/Inputs';
import GroupForm from './Forms/Group';
import GroupRow from './Components/GroupRow';
import { withTranslation } from 'react-i18next';

function GroupsOverview({ t }) {
    const { groups, projects, general, account } = useStore();

    function rowRenderer({ key, index, style }) {
        const group = groups.resource.filtered[index];
        return <GroupRow key={key} style={style} group={group} />;
    }

    function onCreateGroup() {
        let modal;
        const closeModal = () => modal.close();
        modal = general.addModal({
            type: 'window',
            width: 1100,
            title: t('modal.title.group.new'),
            onMinimize: modal => {
                groups.resource.form.data.name &&
                    (modal.title = `${t('modal.title.group.new')}: ${groups.resource.form.data.name}`);
                return groups.resource.form.data;
            },
            onMaximize: modal => {
                groups.resource.form.data = { ...modal.data };
            },
            body: <GroupForm onCancel={closeModal} onConfirm={closeModal} />,
        });
    }

    return (
        <ResourceOverview
            apiPrefix={`/project/${projects.id}`}
            store={groups.resource}
            rowRenderer={rowRenderer}
            heightOffset={400}
            actions={[
                //TODO: check rights and remove level property
                account.rights.canCreateGroups && {
                    icon: 'add',
                    onClick: onCreateGroup,
                    tooltip: t('tooltip.group.create'),
                },
                account.rights.canExportGroups && {
                    icon: 'getApp',
                    href: '/api/reports/groups.xlsx',
                    addition: 'projectId=' + projects.id,
                    tooltip: t('tooltip.report.download'),
                },
            ]}
            filters={<Inputs.groups.filters.scene />}
            sort={<Inputs.groups.sort />}
        />
    );
}

export default withTranslation()(observer(GroupsOverview));
