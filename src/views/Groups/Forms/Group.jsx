import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useStore } from '../../../mobx';
import { observer } from 'mobx-react-lite';
import Form from 'components/Form';

function NewGroup(props) {
    const { projects, groups } = useStore();

    useEffect(() => {
        return () => {
            groups.resource.form.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const apiPrefix = `/project/${projects.id}`;

    return (
        <Form
            apiPrefix={apiPrefix}
            store={groups.resource}
            noCancel
            onConfirm={() => {
                props.onConfirm();
                groups.resource.data = [];
                groups.resource.status = [];
                groups.resource.fetch(apiPrefix);
            }}
        />
    );
}

NewGroup.propTypes = {
    classes: PropTypes.object,
};

export default observer(NewGroup);
