import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import ListRow from 'components/ListRow';
import { observer } from 'mobx-react-lite';
import Group from 'components/Group';
import Inputs from 'components/Inputs';

function GroupsOverview() {
    const { groups, users } = useStore();

    function rowRenderer({ key, index, style }) {
        const group = groups.resource.filtered[index];

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <Group group={group} identifier type description project scene />

                    {/* TODO: support this <ListRowActions>
                        <Tooltip title="Haal gebruiker uit deze groep" placement="top">
                            <Delete onClick={() => {}} />
                        </Tooltip>
                    </ListRowActions> */}
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            apiPrefix={`/user/${users.selectedUser.id}`}
            store={groups.resource}
            rowRenderer={rowRenderer}
            heightOffset={400}
            filters={
                <>
                    <Inputs.groups.filters.project />
                    {groups.resource.filters.project !== undefined && (
                        <Inputs.groups.filters.scene projectId={groups.resource.filters.project} />
                    )}
                </>
            }
        />
    );
}

export default observer(GroupsOverview);
