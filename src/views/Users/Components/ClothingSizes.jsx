import { useStore } from '../../../mobx';
import React, { useEffect, useState } from 'react';
import profileStyles from '../profile.style.jsx';
import Inputs from 'components/Inputs';
import { withStyles } from '@material-ui/styles';
import { observer } from 'mobx-react-lite';
import EditableCard from 'components/EditableCard';
import { withTranslation } from 'react-i18next';

const notPresent = 'Niet ingevuld';

function ClothingSizes(props) {
    const { classes, t } = props;
    const [edit, setEdit] = useState(false);
    const { users, account, shirtSizes, clothingSizes } = useStore();
    const { selectedUser = { sizes: {} } } = users;

    useEffect(() => {
        if (users.loading) {
            setEdit(false);
        }
    }, [users.loading]);

    function validate() {
        if (selectedUser.sizes) {
            if (selectedUser.sizes.height) {
                if (!selectedUser.newHeight) {
                    return 'true';
                }
            }

            if (parseInt(selectedUser.newHeight) < 50 || parseInt(selectedUser.newHeight) > 230) {
                return 'true';
            }

            if (selectedUser.sizes.chest) {
                if (!selectedUser.newChest) {
                    return 'true';
                }
            }

            if (parseInt(selectedUser.newChest) < 50 || parseInt(selectedUser.newChest) > 160) {
                return 'true';
            }

            if (selectedUser.sizes.waist) {
                if (!selectedUser.newWaist) {
                    return 'true';
                }
            }

            if (parseInt(selectedUser.newWaist) < 40 || parseInt(selectedUser.newWaist) > 160) {
                return 'true';
            }

            if (selectedUser.sizes.hipSize) {
                if (!selectedUser.newHipSize) {
                    return 'true';
                }
            }

            if (parseInt(selectedUser.newHipSize) < 50 || parseInt(selectedUser.newHipSize) > 160) {
                return 'true';
            }

            if (selectedUser.sizes.headSize) {
                if (!selectedUser.newHeadSize) {
                    return 'true';
                }
            }

            if (parseInt(selectedUser.newHeadSize) < 35 || parseInt(selectedUser.newHeadSize) > 70) {
                return 'true';
            }

            if (selectedUser.sizes.clothingSize && !selectedUser.newClothingSize) {
                return 'true';
            }

            if (!selectedUser.newShirtSize) {
                return 'true';
            }
        }
    }

    return (
        <>
            <h2 style={{ margin: 0 }}>{t('form.labels.clothingInfo')}</h2>
            <EditableCard
                canSave={!selectedUser.error}
                editable={props.editable}
                edit={edit}
                onEdit={edit => {
                    if (edit) {
                        account.success = '';
                        account.error = '';
                        selectedUser.newHeight = selectedUser.sizes.height;
                        selectedUser.newChest = selectedUser.sizes.chest;
                        selectedUser.newWaist = selectedUser.sizes.waist;
                        selectedUser.newHipSize = selectedUser.sizes.hipSize;
                        selectedUser.newHeadSize = selectedUser.sizes.headSize;
                        selectedUser.newClothingSize = selectedUser.sizes.clothingSize;
                        selectedUser.newShirtSize = selectedUser.sizes.shirtSize;
                        selectedUser.newClothingFrozen = selectedUser.clothingFrozen;
                        setEdit(true);
                    }
                }}
                onSave={() => {
                    account.success = '';
                    account.error = '';
                    account
                        .saveClothingSizes(selectedUser)
                        .then(() => {
                            selectedUser.sizes.height = selectedUser.newHeight;
                            selectedUser.sizes.chest = selectedUser.newChest;
                            selectedUser.sizes.waist = selectedUser.newWaist;
                            selectedUser.sizes.hipSize = selectedUser.newHipSize;
                            selectedUser.sizes.headSize = selectedUser.newHeadSize;
                            selectedUser.sizes.clothingSize = selectedUser.newClothingSize;
                            selectedUser.sizes.shirtSize = selectedUser.newShirtSize;
                            selectedUser.clothingFrozen = selectedUser.newClothingFrozen;
                            setEdit(false);
                        })
                        .catch(e => setEdit(false));
                }}
            >
                {({ edit }) => {
                    if (!edit) {
                        return (
                            <>
                                {account.level === 0 && !account.canEditClothingSizes && (
                                    <div
                                        style={{
                                            background: '#ff9800',
                                            color: '#fff',
                                            padding: '10px 15px',
                                            marginBottom: 10,
                                        }}
                                    >
                                        {t('info.clothing.cannotEdit')} <br />
                                        {t('info.clothing.wantToEdit')}
                                    </div>
                                )}
                                {account.level === 1 && selectedUser.clothingFrozen && (
                                    <div
                                        style={{
                                            background: '#ff9800',
                                            color: '#fff',
                                            padding: '10px 15px',
                                            marginBottom: 10,
                                        }}
                                    >
                                        {t('info.clothing.cannotBeEdited')}
                                    </div>
                                )}
                                <span className={classes.label}>{t('form.labels.height')}</span>
                                {selectedUser.sizes.height || t(notPresent)}
                                <span className={classes.label}>{t('form.labels.chest')}</span>
                                {selectedUser.sizes.chest || t(notPresent)}
                                <span className={classes.label}>{t('form.labels.waist')}</span>
                                {selectedUser.sizes.waist || t(notPresent)}
                                <span className={classes.label}>{t('form.labels.hipSize')}</span>
                                {selectedUser.sizes.hipSize || t(notPresent)}
                                <span className={classes.label}>{t('form.labels.headSize')}</span>
                                {selectedUser.sizes.headSize || t(notPresent)}
                                <span className={classes.label}>{t('form.labels.clothingSize')}</span>
                                {selectedUser.sizes.clothingSize || t(notPresent)}
                                <span className={classes.label}>{t('form.labels.shirtSize')}</span>
                                {selectedUser.sizes.shirtSize || t(notPresent)}
                            </>
                        );
                    }

                    return (
                        <>
                            <Inputs.Input
                                fullWidth
                                type="number"
                                label={t('form.labels.height')}
                                required={selectedUser.sizes.height !== undefined}
                                validations={[{ type: 'between', min: 50, max: 230 }]}
                                value={selectedUser.newHeight}
                                onChange={value => (selectedUser.newHeight = value)}
                                onBlur={() => (selectedUser.error = validate())}
                            />
                            <Inputs.Input
                                fullWidth
                                type="number"
                                label={t('form.labels.chest')}
                                required={selectedUser.sizes.chest !== undefined}
                                validations={[{ type: 'between', min: 50, max: 160 }]}
                                value={selectedUser.newChest}
                                onChange={value => (selectedUser.newChest = value)}
                                onBlur={() => (selectedUser.error = validate())}
                            />
                            <Inputs.Input
                                fullWidth
                                type="number"
                                label={t('form.labels.waist')}
                                required={selectedUser.sizes.waist !== undefined}
                                validations={[{ type: 'between', min: 40, max: 160 }]}
                                value={selectedUser.newWaist}
                                onChange={value => (selectedUser.newWaist = value)}
                                onBlur={() => (selectedUser.error = validate())}
                            />
                            <Inputs.Input
                                fullWidth
                                type="number"
                                label={t('form.labels.hipSize')}
                                required={selectedUser.sizes.hipSize !== undefined}
                                validations={[{ type: 'between', min: 50, max: 160 }]}
                                value={selectedUser.newHipSize}
                                onChange={value => (selectedUser.newHipSize = value)}
                                onBlur={() => (selectedUser.error = validate())}
                            />
                            <Inputs.Input
                                fullWidth
                                type="number"
                                label={t('form.labels.headSize')}
                                required={selectedUser.sizes.headSize !== undefined}
                                validations={[{ type: 'between', min: 35, max: 70 }]}
                                value={selectedUser.newHeadSize}
                                onChange={value => (selectedUser.newHeadSize = value)}
                                onBlur={() => (selectedUser.error = validate())}
                            />
                            <Inputs.Input
                                fullWidth
                                label={t('form.labels.clothingSize')}
                                type="select"
                                value={selectedUser.newClothingSize}
                                required={selectedUser.sizes.clothingSize !== undefined}
                                onChange={v => (selectedUser.newClothingSize = v)}
                                onBlur={() => (selectedUser.error = validate())}
                                options={clothingSizes.autoComplete}
                            />
                            <Inputs.Input
                                fullWidth
                                label={t('form.labels.shirtSize')}
                                type="select"
                                value={selectedUser.newShirtSize}
                                required={selectedUser.sizes.shirtSize !== undefined}
                                onChange={v => (selectedUser.newShirtSize = v)}
                                onBlur={() => (selectedUser.error = validate())}
                                options={shirtSizes.autoComplete}
                            />
                            {account.level === 1 && (
                                <Inputs.Input
                                    fullWidth
                                    label={t('form.labels.freeze')}
                                    type="checkbox"
                                    value={selectedUser.newClothingFrozen}
                                    onChange={v => (selectedUser.newClothingFrozen = v)}
                                />
                            )}
                        </>
                    );
                }}
            </EditableCard>
        </>
    );
}

export default withTranslation()(withStyles(profileStyles)(observer(ClothingSizes)));
