/* istanbul ignore file */
import React, { useEffect, useState } from 'react';
import Page from 'components/Page.jsx';
import CustomInput from 'components/CustomInput/CustomInput';
import { withStyles } from '@material-ui/styles';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import ContentEditable from 'react-contenteditable';
import Button from 'components/CustomButtons/Button.jsx';
import { Delete } from '@material-ui/icons';
import { Success } from 'components/Notifications';

const mailStyle = {
    content: {
        background: 'white',
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: '10px 15px',
        margin: '26px 0',
    },
    list: {
        listStyle: 'none',
        margin: 0,
        padding: 0,
        '& > li': {
            padding: '5px 10px',
            background: 'rgba(0,0,0,.1)',
            borderRadius: 5,
            border: '1px solid rgba(0,0,0,.15)',
            marginBottom: 3,
        },
    },
    close: {
        float: 'right',
        '&:hover': {
            cursor: 'pointer',
            color: 'black',
        },
    },
};

function Mail(props) {
    const { classes } = props;
    const { mail } = useStore();
    const [addresses, setAddresses] = useState([]);

    let matches = mail.html ? mail.html.match(/\{\w+\}/g) : [];
    matches = matches
        .map(match => match.replace('{', '').replace('}', ''))
        .filter(v => !['uuid', 'signoutlink'].includes(v));

    useEffect(() => {
        mail.getTemplates();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        mail.getTemplate();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [mail.template]);

    return (
        <Page title="Verstuur mail" loading={mail.loading}>
            <Success message={mail.success} onClose={() => (mail.success = '')} />
            <div className={classes.content}>
                <CustomInput
                    type="select"
                    labelText="Mail-template"
                    formControlProps={{
                        fullWidth: true,
                    }}
                    inputProps={{
                        useValue: true,
                        native: true,
                        value: mail.template,
                        onChange: e => (mail.template = e.target.value),
                        name: 'template',
                        inputProps: {
                            name: 'template',
                            id: 'template_YS',
                        },
                        options: mail.templates,
                    }}
                />
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                    {mail.html && (
                        <div>
                            <h2>Email</h2>
                            <ContentEditable
                                style={{ maxWidth: 620 }}
                                html={mail.html} // innerHTML of the editable div
                                onChange={e => (mail.html = e.target.value)}
                            />
                        </div>
                    )}
                    {matches.length > 0 && (
                        <div style={{ width: 440 }}>
                            <h2>Dynamische velden</h2>
                            {matches.map(match => (
                                <CustomInput
                                    key={match}
                                    labelText={match}
                                    formControlProps={{
                                        fullWidth: true,
                                    }}
                                    inputProps={{
                                        value: mail[match],
                                        onBlur: e => {
                                            mail.html = mail.html.replace(`{${match}}`, e.target.value);
                                        },
                                        name: 'template',
                                        inputProps: {
                                            name: 'template',
                                            id: 'template_YS',
                                        },
                                    }}
                                />
                            ))}
                        </div>
                    )}
                    {mail.html && !matches.length && (
                        <div style={{ width: 440 }}>
                            <h2>Onderwerp</h2>
                            <CustomInput
                                labelText="Onderwerp"
                                formControlProps={{
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    value: mail.subject,
                                    onChange: e => (mail.subject = e.target.value),
                                    name: 'subject',
                                    inputProps: {
                                        name: 'subject',
                                        id: 'subject_YS',
                                    },
                                }}
                            />
                            <h2>Geadresseerden</h2>
                            {addresses.length === 0 && (
                                <p>Nog geen geadresseerden, gelieve een mailadres toe te voegen</p>
                            )}
                            {addresses.length > 0 && (
                                <ul className={classes.list}>
                                    {addresses.map(address => (
                                        <li>
                                            {address}
                                            <span
                                                className={classes.close}
                                                onClick={() => setAddresses(addresses.filter(a => a !== address))}
                                            >
                                                <Delete />
                                            </span>
                                        </li>
                                    ))}
                                </ul>
                            )}
                            <CustomInput
                                labelText="E-mailadres"
                                formControlProps={{
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    value: mail.email,
                                    onChange: e => (mail.email = e.target.value),
                                    name: 'email',
                                    inputProps: {
                                        name: 'email',
                                        id: 'email_YS',
                                    },
                                }}
                            />
                            <Button
                                color="success"
                                onClick={() => {
                                    mail.email && setAddresses([...addresses, mail.email]);
                                    mail.email = '';
                                }}
                            >
                                Voeg toe
                            </Button>
                            <Button
                                color="success"
                                onClick={async () => {
                                    await mail.send(addresses);
                                    setAddresses([]);
                                }}
                            >
                                Verstuur email(s)
                            </Button>
                        </div>
                    )}
                </div>
            </div>
        </Page>
    );
}

export default withStyles(mailStyle)(observer(Mail));
