import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import useReactRouter from 'use-react-router';

import inviteUsersStyle from './invite.style.jsx';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import CsvDropzone from 'components/Dropzone/CsvDropzone.jsx';
import Checkbox from '@material-ui/core/Checkbox';
import { ArrowBackIos } from '@material-ui/icons';

import CardFooter from 'components/Card/CardFooter.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Toolbar from '../../components/Toolbar/Toolbar.jsx';
import Loader from '../../components/Loader.jsx';
import CustomInput from 'components/CustomInput/CustomInput.jsx';

function InviteUsers(props) {
    const { classes } = props;
    const { account, ui } = useStore();
    const history = useHistory();
    const [addressList, setAddressList] = useState([]);
    const [email, setEmail] = useState('');
    const [success, setSuccess] = useState('');

    useEffect(() => {
        return () => {
            account.success = null;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div style={{ flexGrow: 1 }}>
            {ui.isMobile && (
                <div className={classes.header}>
                    <ArrowBackIos onClick={() => history.replace('/medewerkers')} />
                    <span>Verstuur uitnodigingen</span>
                </div>
            )}
            <Toolbar title="Verstuur uitnodigingen" />
            {success && (
                <div
                    style={{
                        padding: '10px 15px',
                        background: '#54ab25',
                        color: '#fff',
                    }}
                >
                    {success}
                </div>
            )}
            <Loader loading={account.loading}>
                <div className={classes.form}>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={5}>
                            <h1>Upload .csv</h1>
                            <CsvDropzone
                                onData={data => {
                                    data = data.split('\n');
                                    data.shift();
                                    data = data
                                        .map(addr => ({
                                            checked: addr && addr.includes('@') && addr.includes('.'),
                                            email: addr.trim(),
                                        }))
                                        .filter(({ email }) => email);
                                    setAddressList(data);
                                }}
                                className={classes.dropzone}
                            />
                        </GridItem>
                        <GridItem xs={12} sm={12} md={2}></GridItem>
                        <GridItem xs={12} sm={12} md={5}>
                            <h1>Manuele ingave</h1>
                            <CustomInput
                                labelText="E-mailadres"
                                id="em_FVD"
                                formControlProps={{
                                    error: email && (email.length < 6 || !email.includes('@') || !email.includes('.')),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    autoComplete: 'off',
                                    required: true,
                                    type: 'text',
                                    value: email,
                                    onChange: e => setEmail(e.target.value),
                                }}
                            />
                            <Button
                                disabled={!email || (email.length < 6 || !email.includes('@') || !email.includes('.'))}
                                onClick={async () => {
                                    setAddressList([{ email, checked: true }].concat(addressList));
                                }}
                                color="primary"
                                size="md"
                            >
                                Voeg toe
                            </Button>
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <CardFooter className={classes.cardFooter}>
                                <Button
                                    disabled={addressList.length === 0}
                                    onClick={async () => {
                                        try {
                                            await account.invite(addressList).then(() => {
                                                const sendTo = addressList.filter(({ checked }) => checked).length;
                                                sendTo > 1 && setSuccess(`${sendTo} mails werden uitgestuurd`);
                                                sendTo === 1 && setSuccess(`1 mail werd uitgestuurd`);
                                                setAddressList([]);
                                                setEmail('');
                                            });
                                        } catch (e) {}
                                    }}
                                    color="primary"
                                    size="md"
                                >
                                    Verstuur emails
                                </Button>
                            </CardFooter>
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <h1>Verstuur naar:</h1>
                            {addressList.length === 0 && <h2>Nog geen e-mailadressen toegevoegd</h2>}
                            {addressList.length > 0 && (
                                <ul className={classes.addressList}>
                                    {addressList.map((address, key, addressList) => (
                                        <li {...{ key }}>
                                            <span>
                                                <Checkbox
                                                    checked={address.checked}
                                                    onChange={e => {
                                                        addressList[key].checked = e.target.checked;
                                                        setAddressList([].concat(addressList));
                                                    }}
                                                />
                                            </span>
                                            <span>{address.email}</span>
                                        </li>
                                    ))}
                                </ul>
                            )}
                        </GridItem>
                    </GridContainer>
                </div>
            </Loader>
        </div>
    );
}

InviteUsers.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(inviteUsersStyle)(observer(InviteUsers));
