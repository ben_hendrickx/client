import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/styles';
import GridContainer from 'components/Grid/GridContainer';
import GridItem from 'components/Grid/GridItem';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../mobx';
import useReactRouter from 'use-react-router';
import EditableCard from 'components/EditableCard';
import profileStyles from './profile.style.jsx';
import { AccountCircle } from '@material-ui/icons';
import ImageDropzone from 'components/Dropzone/ImageDropzone.jsx';
import CustomInput from 'components/CustomInput/CustomInput';
import moment from 'moment';
import Address from 'components/Inputs/Address.jsx';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import Page from 'components/Page';
import Button from 'components/CustomButtons/Button.jsx';
import { Success } from 'components/Notifications';
import ClothingSizes from './Components/ClothingSizes';
import parse from 'html-react-parser';
import { Trans, withTranslation } from 'react-i18next';

function Profile(props) {
    const { style, classes, noName = false, t } = props;

    const { users, ui, general, account, shirtSizes, clothingSizes, countries } = useStore();
    const { match, history } = useReactRouter();

    const [editPicture, setEditPicture] = useState(false);
    const [editContactDetails, setEditContactDetails] = useState(false);
    const [editPrivacy, setEditPrivacy] = useState(false);
    const [editInformation, setEditInformation] = useState(false);
    const [editRemarks, setEditRemarks] = useState(false);

    const { selectedUser = { sizes: {} } } = users;

    const notPresent = 'Niet ingevuld';
    const {
        firstName,
        lastName,
        date_of_birth,
        sex,
        nationality,
        origin,
        phoneNumber,
        cellPhone,
        email,
        secondaryEmail,
        streetName,
        houseNumber,
        bus,
        zipCode,
        cityName,
        owner_remarks,
    } = selectedUser;

    function validateInformation() {
        if (selectedUser.newFirstName && selectedUser.newFirstName.length < 2) {
            return 'Voornaam is ongeldig';
        }

        if (selectedUser.newLastName && selectedUser.newLastName.length < 2) {
            return 'Achternaam is ongeldig';
        }

        if (selectedUser.newDateOfBirth && !selectedUser.newDateOfBirth.isValid()) {
            return 'Geboortedatum is geen geldige datum';
        }

        if (selectedUser.newNationality && selectedUser.newNationality.length < 2) {
            return 'Nationaliteit is ongeldig';
        }

        return null;
    }

    function validateContactDetails() {
        if (selectedUser.newPhoneNumber && selectedUser.newPhoneNumber.length < 9) {
            return 'Telefoonnummer is ongeldig';
        }

        if (selectedUser.newCellPhone && selectedUser.newCellPhone.length < 9) {
            return 'G.S.M.-nummer is ongeldig';
        }

        if (selectedUser.newEmail) {
            if (
                selectedUser.newEmail.length < 6 ||
                !selectedUser.newEmail.includes('@') ||
                !selectedUser.newEmail.includes('.')
            ) {
                return 'E-mailadres is geen geldig mailadres';
            }
        }

        if (selectedUser.newSecondaryEmail) {
            if (
                selectedUser.newSecondaryEmail.length < 6 ||
                !selectedUser.newSecondaryEmail.includes('@') ||
                !selectedUser.newSecondaryEmail.includes('.')
            ) {
                return 'Tweede e-mailadres is geen geldig mailadres';
            }
        }

        if (selectedUser.newEmail || selectedUser.newSecondaryEmail) {
            if (!selectedUser.newPhoneNumber && !selectedUser.newCellPhone) {
                return 'Telefoonnummer of G.S.M.-nummer moet ingevuld zijn.';
            }
        }

        return null;
    }

    const ProfileContent = isOwnProfile => (
        <>
            <Success message={account.success} onClose={() => (account.success = '')} />
            <GridContainer style={style}>
                <GridItem xs={12} sm={12} md={4}>
                    {(isOwnProfile || account.rights.canReadUsersPicture) && (
                        <>
                            <h2 style={{ margin: 0 }}>{t('form.labels.profilePicture')}</h2>
                            <EditableCard
                                editable={isOwnProfile || account.rights.canUpdateUsersPicture}
                                style={{ textAlign: 'center' }}
                                edit={editPicture}
                                onEdit={edit => {
                                    if (edit) {
                                        account.success = '';
                                        account.error = '';
                                        delete selectedUser.newImage;
                                        setEditPicture(true);
                                    }
                                }}
                                onSave={() => {
                                    account.success = '';
                                    account.error = '';
                                    account
                                        .saveImage(selectedUser)
                                        .then(() => {
                                            selectedUser.image = selectedUser.newImage;
                                            delete selectedUser.newImage;
                                            setEditPicture(false);
                                        })
                                        .catch(() => setEditPicture(false));
                                }}
                            >
                                {({ edit }) => {
                                    if (edit) {
                                        return (
                                            <ImageDropzone
                                                style={{ maxWidth: 200, margin: '0 auto', maxHeight: 200 }}
                                                className={classes.dropzone}
                                                onData={data => (selectedUser.newImage = data)}
                                                edit
                                                store={selectedUser}
                                            />
                                        );
                                    }
                                    return selectedUser.image ? (
                                        <img
                                            alt="User"
                                            src={selectedUser.image}
                                            height="200"
                                            style={{ background: 'rgba(160,160,160,.6)' }}
                                        />
                                    ) : (
                                        <AccountCircle
                                            style={{
                                                fontSize: 200,
                                                background: 'rgba(160,160,160,.6)',
                                                color: 'white',
                                            }}
                                        />
                                    );
                                }}
                            </EditableCard>
                        </>
                    )}
                    {(isOwnProfile || account.rights.canReadUsersPersonal) && (
                        <>
                            <h2 style={{ margin: 0 }}>{t('form.labels.personalData')}</h2>
                            <EditableCard
                                editable={isOwnProfile || account.rights.canUpdateUsersPersonal}
                                edit={editInformation}
                                onEdit={edit => {
                                    account.success = '';
                                    account.error = '';
                                    if (edit) {
                                        selectedUser.newFirstName = firstName;
                                        selectedUser.newLastName = lastName;
                                        selectedUser.newDateOfBirth = moment(date_of_birth);
                                        selectedUser.newSex = ['Man', 'Vrouw', 'X'][sex];
                                        selectedUser.newNationality = nationality;
                                        selectedUser.newOrigin = origin;
                                        setEditInformation(true);
                                    }
                                }}
                                onSave={() => {
                                    selectedUser.error = validateInformation();
                                    account.success = '';
                                    account.error = '';
                                    if (selectedUser.error) {
                                        return;
                                    }
                                    account
                                        .saveInformation(selectedUser)
                                        .then(() => {
                                            selectedUser.firstName = selectedUser.newFirstName;
                                            selectedUser.lastName = selectedUser.newLastName;
                                            selectedUser.date_of_birth = selectedUser.newDateOfBirth.toISOString();
                                            selectedUser.sex = ['Man', 'Vrouw', 'X'].indexOf(selectedUser.newSex);
                                            selectedUser.nationality = selectedUser.newNationality;
                                            selectedUser.origin = selectedUser.newOrigin;
                                            setEditInformation(false);
                                        })
                                        .catch(e => setEditInformation(false));
                                }}
                            >
                                {({ edit }) => {
                                    if (!edit) {
                                        return (
                                            <>
                                                <span className={classes.label}>{t('form.labels.firstName')}</span>{' '}
                                                {firstName}
                                                <span className={classes.label}>{t('form.labels.lastName')}</span>{' '}
                                                {lastName}
                                                <span className={classes.label}>
                                                    {t('form.labels.dateOfBirth')}
                                                </span>{' '}
                                                {new Date(date_of_birth).toLocaleDateString('nl-BE', {
                                                    weekday: 'long',
                                                    year: 'numeric',
                                                    month: 'long',
                                                    day: 'numeric',
                                                })}
                                                <span className={classes.label}>{t('form.labels.sex')}</span>{' '}
                                                {['Man', 'Vrouw', 'X'][sex || 0]}
                                                <span className={classes.label}>{t('form.labels.origin')}</span>{' '}
                                                {nationality}
                                                <span className={classes.label}>
                                                    {t('form.labels.countryOfBirth')}
                                                </span>{' '}
                                                {origin}
                                            </>
                                        );
                                    }

                                    return (
                                        <>
                                            <CustomInput
                                                labelText={t('form.labels.firstName')}
                                                id="fN_FVD"
                                                formControlProps={{
                                                    error:
                                                        !!selectedUser.newFirstName &&
                                                        selectedUser.newFirstName.length < 2,
                                                    required: true,
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    color: 'primary',
                                                    name: 'fname',
                                                    autoComplete: 'given-name',
                                                    required: true,
                                                    type: 'text',
                                                    value: selectedUser.newFirstName,
                                                    onChange: e => (selectedUser.newFirstName = e.target.value),
                                                    onBlur: () => (selectedUser.error = validateInformation()),
                                                }}
                                            />
                                            <CustomInput
                                                labelText={t('form.labels.lastName')}
                                                id="lN_FVD"
                                                formControlProps={{
                                                    error:
                                                        !!selectedUser.newLastName &&
                                                        selectedUser.newLastName.length < 2,
                                                    required: true,
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    color: 'primary',
                                                    type: 'text',
                                                    name: 'lname',
                                                    autoComplete: 'family-name',
                                                    value: selectedUser.newLastName,
                                                    onChange: e => (selectedUser.newLastName = e.target.value),
                                                    onBlur: () => (selectedUser.error = validateInformation()),
                                                }}
                                            />
                                            <CustomInput
                                                type="date"
                                                labelText={`${t('form.labels.dateOfBirth')} *`}
                                                formControlProps={{
                                                    error:
                                                        !!selectedUser.newDateOfBirth &&
                                                        !selectedUser.newDateOfBirth.isValid(),
                                                    required: true,
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    id: 'dob_FVD',
                                                    maxDate: new Date(),
                                                    maxDateMessage: 'Gelieve een datum te selecteren in het verleden',
                                                    minDateMessage:
                                                        'Gelieve een datum te kiezen die maximum 100 jaar geleden is',
                                                    minDate: new Date(Date.now() - 100 * 365 * 24 * 60 * 60 * 1000),
                                                    inputProps: {
                                                        id: 'dob_FVD',
                                                    },
                                                    onBlur: () => (selectedUser.error = validateInformation()),
                                                    value: selectedUser.newDateOfBirth || null,
                                                    onChange: date => (selectedUser.newDateOfBirth = date),
                                                }}
                                            />
                                            <CustomInput
                                                type="select"
                                                labelText={t('form.labels.sex')}
                                                formControlProps={{
                                                    required: true,
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    native: true,
                                                    value: selectedUser.newSex,
                                                    onChange: e => (selectedUser.newSex = e.target.value),
                                                    name: 'sex',
                                                    inputProps: {
                                                        name: 'sex',
                                                        id: 'sex_FVD',
                                                    },
                                                    options: [
                                                        { label: t('form.options.male'), value: 0 },
                                                        { label: t('form.options.female'), value: 1 },
                                                        { label: t('form.options.genderX'), value: 2 },
                                                    ],
                                                }}
                                            />
                                            <CustomInput
                                                labelText={t('form.labels.origin')}
                                                id="origin_FVD"
                                                formControlProps={{
                                                    error:
                                                        !!selectedUser.newNationality &&
                                                        selectedUser.newNationality.length < 2,
                                                    required: true,
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    color: 'primary',
                                                    type: 'text',
                                                    autoComplete: 'off',
                                                    value: selectedUser.newNationality,
                                                    onChange: e => (selectedUser.newNationality = e.target.value),
                                                    onBlur: () => (selectedUser.error = validateInformation()),
                                                }}
                                            />
                                            <CustomInput
                                                type="select"
                                                labelText={t('form.labels.countryOfBirth')}
                                                formControlProps={{
                                                    required: true,
                                                    fullWidth: true,
                                                }}
                                                inputProps={{
                                                    native: true,
                                                    value: selectedUser.newOrigin,
                                                    onChange: e => {
                                                        return (selectedUser.newOrigin = e.target.value);
                                                    },
                                                    inputProps: {
                                                        name: 'bCountry',
                                                        id: 'bCountry_FVD',
                                                    },
                                                    options: countries.autoComplete,
                                                    name: 'country',
                                                    autoComplete: 'country',
                                                }}
                                            />
                                        </>
                                    );
                                }}
                            </EditableCard>
                        </>
                    )}
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    {(isOwnProfile || account.rights.canReadUsersContact || account.rights.canReadUsersAddress) && (
                        <>
                            {isOwnProfile || account.rights.canReadUsersContact ? (
                                <h2 style={{ margin: 0 }}>{t('form.labels.contactDetails')}</h2>
                            ) : (
                                <h2 style={{ margin: 0 }}>{t('form.labels.addressInfo')}</h2>
                            )}
                            <EditableCard
                                editable={
                                    isOwnProfile ||
                                    account.rights.canUpdateUsersContact ||
                                    account.rights.canUpdateUsersAddress
                                }
                                edit={editContactDetails}
                                onCancel={() => {
                                    selectedUser.bus = selectedUser.newAddress.bus;
                                    setEditContactDetails(false);
                                }}
                                onEdit={edit => {
                                    if (edit) {
                                        account.success = '';
                                        account.error = '';
                                        selectedUser.newPhoneNumber = phoneNumber;
                                        selectedUser.newCellPhone = cellPhone;
                                        selectedUser.newEmail = email;
                                        selectedUser.newSecondaryEmail = secondaryEmail;
                                        selectedUser.newAddress = {
                                            streetName,
                                            houseNumber,
                                            zipCode,
                                            cityName,
                                            bus,
                                        };
                                        delete selectedUser.bus;
                                        setEditContactDetails(true);
                                    }
                                }}
                                onSave={() => {
                                    selectedUser.error = validateContactDetails();
                                    account.success = '';
                                    account.error = '';
                                    if (selectedUser.error) {
                                        return;
                                    }
                                    account
                                        .saveContactDetails(selectedUser)
                                        .then(() => {
                                            let goHome = false;
                                            if (account.level === 0 && selectedUser.email !== selectedUser.newEmail) {
                                                goHome = true;
                                            }

                                            const {
                                                streetName,
                                                houseNumber,
                                                bus,
                                                zipCode,
                                                cityName,
                                            } = selectedUser.newAddress;
                                            selectedUser.streetName = streetName;
                                            selectedUser.houseNumber = houseNumber;
                                            selectedUser.bus = bus;
                                            selectedUser.zipCode = zipCode;
                                            selectedUser.cityName = cityName;
                                            selectedUser.phoneNumber = selectedUser.newPhoneNumber;
                                            selectedUser.cellPhone = selectedUser.newCellPhone;
                                            selectedUser.email = selectedUser.newEmail;
                                            selectedUser.secondaryEmail = selectedUser.newSecondaryEmail;
                                            setEditContactDetails(false);
                                            goHome && (account.email = '');
                                            goHome && (account.password = '');
                                            goHome && history.push('/');
                                        })
                                        .catch(e => setEditContactDetails(false));
                                }}
                            >
                                {({ edit }) => {
                                    if (!edit) {
                                        return (
                                            <>
                                                {(isOwnProfile || account.rights.canReadUsersContact) && (
                                                    <>
                                                        <span className={classes.label}>
                                                            {t('form.labels.phoneNumber')}
                                                        </span>{' '}
                                                        {phoneNumber || t(notPresent)}
                                                        <span className={classes.label}>
                                                            {t('form.labels.cellPhone')}
                                                        </span>{' '}
                                                        {cellPhone || t(notPresent)}
                                                        <span className={classes.label}>
                                                            {t('form.labels.email')}
                                                        </span>{' '}
                                                        {email}
                                                        <span className={classes.label}>
                                                            {t('form.labels.secondaryEmail')}
                                                        </span>{' '}
                                                        {secondaryEmail || t(notPresent)}
                                                    </>
                                                )}
                                                {(isOwnProfile || account.rights.canReadUsersAddress) && (
                                                    <>
                                                        <span className={classes.label}>
                                                            {t('form.labels.address')}
                                                        </span>{' '}
                                                        {`${selectedUser.streetName} ${selectedUser.houseNumber}${
                                                            selectedUser.bus ? selectedUser.bus : ''
                                                        }, ${selectedUser.zipCode} ${selectedUser.cityName}`}
                                                    </>
                                                )}
                                            </>
                                        );
                                    }

                                    return (
                                        <>
                                            {(isOwnProfile || account.rights.canUpdateUsersContact) && (
                                                <>
                                                    <CustomInput
                                                        labelText={t('form.labels.phoneNumber')}
                                                        id="pN_FVD"
                                                        formControlProps={{
                                                            error:
                                                                selectedUser.newPhoneNumber &&
                                                                selectedUser.newPhoneNumber.length < 9,
                                                            fullWidth: true,
                                                        }}
                                                        inputProps={{
                                                            color: 'primary',
                                                            type: 'text',
                                                            value: selectedUser.newPhoneNumber,
                                                            name: 'phone',
                                                            autoComplete: 'tel',
                                                            onChange: e =>
                                                                (selectedUser.newPhoneNumber = e.target.value),
                                                            onBlur: () =>
                                                                (selectedUser.error = validateContactDetails()),
                                                        }}
                                                    />

                                                    <CustomInput
                                                        labelText={t('form.labels.cellPhone')}
                                                        id="cpN_FVD"
                                                        formControlProps={{
                                                            error:
                                                                selectedUser.newCellPhone &&
                                                                selectedUser.newCellPhone.length < 9,
                                                            fullWidth: true,
                                                        }}
                                                        inputProps={{
                                                            color: 'primary',
                                                            type: 'text',
                                                            name: 'mobile',
                                                            autoComplete: 'tel',
                                                            value: selectedUser.newCellPhone,
                                                            onChange: e => (selectedUser.newCellPhone = e.target.value),
                                                            onBlur: () =>
                                                                (selectedUser.error = validateContactDetails()),
                                                        }}
                                                    />
                                                    <CustomInput
                                                        labelText={t('form.labels.email')}
                                                        id="ma_FVD"
                                                        formControlProps={{
                                                            error:
                                                                (selectedUser.newEmail &&
                                                                    (selectedUser.newEmail.length < 6 ||
                                                                        !selectedUser.newEmail.includes('@') ||
                                                                        !selectedUser.newEmail.includes('.'))) ||
                                                                (selectedUser.error &&
                                                                    selectedUser.error.includes('E-mail')),
                                                            required: true,
                                                            fullWidth: true,
                                                        }}
                                                        inputProps={{
                                                            color: 'primary',
                                                            type: 'email',
                                                            name: 'username',
                                                            autoComplete: 'username',
                                                            value: selectedUser.newEmail,
                                                            onChange: e => (selectedUser.newEmail = e.target.value),
                                                            onBlur: () =>
                                                                (selectedUser.error = validateContactDetails()),
                                                        }}
                                                    />
                                                    <CustomInput
                                                        labelText={t('form.labels.secondaryEmail')}
                                                        id="secMa_FVD"
                                                        formControlProps={{
                                                            error:
                                                                (selectedUser.newSecondaryEmail &&
                                                                    (selectedUser.newSecondaryEmail.length < 6 ||
                                                                        !selectedUser.newSecondaryEmail.includes('@') ||
                                                                        !selectedUser.newSecondaryEmail.includes(
                                                                            '.'
                                                                        ))) ||
                                                                (selectedUser.error &&
                                                                    selectedUser.error.includes('weede')),
                                                            fullWidth: true,
                                                        }}
                                                        inputProps={{
                                                            color: 'primary',
                                                            type: 'email',
                                                            autoComplete: 'off',
                                                            value: selectedUser.newSecondaryEmail,
                                                            onChange: e =>
                                                                (selectedUser.newSecondaryEmail = e.target.value),
                                                            onBlur: () =>
                                                                (selectedUser.error = validateContactDetails()),
                                                        }}
                                                    />
                                                </>
                                            )}
                                            {(isOwnProfile || account.rights.canUpdateUsersAddress) &&
                                                selectedUser.newAddress && (
                                                    <>
                                                        <Address store={selectedUser.newAddress} />
                                                    </>
                                                )}
                                        </>
                                    );
                                }}
                            </EditableCard>
                        </>
                    )}
                    {(isOwnProfile || account.rights.canReadUsersOther) && (
                        <>
                            <h2 style={{ margin: 0 }}>{t('form.labels.otherInfo')}</h2>
                            <EditableCard
                                editable={isOwnProfile || account.rights.canUpdateUsersOther}
                                edit={editPrivacy}
                                onEdit={edit => {
                                    if (edit) {
                                        account.success = '';
                                        account.error = '';
                                        selectedUser.newPictures = (selectedUser.settings || {}).pictures;
                                        setEditPrivacy(true);
                                    }
                                }}
                                onSave={() => {
                                    account.success = '';
                                    account.error = '';
                                    if (selectedUser.error) {
                                        return;
                                    }
                                    account
                                        .savePrivacyFlag(selectedUser)
                                        .then(() => {
                                            selectedUser.settings &&
                                                (selectedUser.settings.pictures = selectedUser.newPictures);
                                            setEditPrivacy(false);
                                        })
                                        .catch(e => setEditPrivacy(false));
                                }}
                            >
                                {({ edit }) => {
                                    if (!edit) {
                                        return (
                                            <>
                                                <span className={classes.label}>{t('form.labels.privacy')}: </span>{' '}
                                                {(selectedUser.settings || {}).pictures ? 'Ja' : 'Nee'}
                                                <span className={classes.label}>{t('form.labels.pin')}</span>{' '}
                                                {(selectedUser.settings || {}).pin}
                                                {[1, 2].includes(account.level) && (
                                                    <span className={classes.label}>{t('form.labels.since')}</span>
                                                )}
                                                {[1, 2].includes(account.level)
                                                    ? new Date(selectedUser.creationDt).toLocaleDateString('nl-BE', {
                                                          weekday: 'long',
                                                          year: 'numeric',
                                                          month: 'long',
                                                          day: 'numeric',
                                                      })
                                                    : null}
                                            </>
                                        );
                                    }

                                    return (
                                        <>
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        color="primary"
                                                        checked={selectedUser.newPictures}
                                                        onChange={e => (selectedUser.newPictures = e.target.checked)}
                                                        value={selectedUser.newPictures}
                                                        inputProps={{
                                                            'aria-label': 'Privacy',
                                                        }}
                                                    />
                                                }
                                                style={{ alignItems: 'start' }}
                                                label={
                                                    <p>
                                                        <Trans i18nKey="privacy">
                                                            Hierbij verklaar ik dat foto’s en video’s waarop ik
                                                            duidelijk in beeld sta door het Lokaal Bestuur Geel gebruikt
                                                            mogen worden:
                                                            <br />
                                                            - in publicaties, magazines of brochures van Lokaal Bestuur
                                                            Geel
                                                            <br />
                                                            - in de stedelijke rubriek in Deze Week en Rondom
                                                            <br />
                                                            - op de websites van Lokaal Bestuur Geel, op sociale media
                                                            van Lokaal Bestuur Geel (Facebook, Twitter, Instagram…), in
                                                            de digitale nieuwsbrieven van Lokaal Bestuur Geel
                                                            <br />
                                                            <br />
                                                            Indien u nog geen 16 jaar bent, moet een ouder of voogd
                                                            toestemming geven voor het gebruiken van foto- en
                                                            beeldmateriaal. Dit kan door een e-mail te sturen naar
                                                            {process.env.REACT_APP_EMAIL}. Gelieve de naam van het kind
                                                            duidelijk te vermelden in de mail. Wenst u niet langer dat
                                                            uw foto- en beeldmateriaal wordt gebruikt, laat dit dan
                                                            weten via
                                                            {process.env.REACT_APP_EMAIL} of aan de balie van Toerisme
                                                            Geel.
                                                        </Trans>
                                                    </p>
                                                }
                                            />
                                        </>
                                    );
                                }}
                            </EditableCard>
                        </>
                    )}
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    {(isOwnProfile || account.rights.canReadUsersClothing) && (
                        <ClothingSizes
                            editable={
                                (!!account.canEditClothingSizes && isOwnProfile) ||
                                (account.level === 1 && account.rights.canUpdateUsersClothing)
                            }
                        />
                    )}
                    {account.rights.canReadUsersRemarks && (
                        <EditableCard
                            editable={account.rights.canUpdateUsersRemarks}
                            edit={editRemarks}
                            onEdit={edit => {
                                if (edit) {
                                    account.success = '';
                                    account.error = '';
                                    selectedUser.newRemarks = selectedUser.owner_remarks;
                                    setEditRemarks(true);
                                }
                            }}
                            onSave={() => {
                                account.success = '';
                                account.error = '';
                                if (selectedUser.error) {
                                    return;
                                }
                                account
                                    .saveRemarks(selectedUser)
                                    .then(() => {
                                        selectedUser.owner_remarks = selectedUser.newRemarks;
                                        setEditRemarks(false);
                                    })
                                    .catch(e => setEditRemarks(false));
                            }}
                        >
                            {({ edit }) => {
                                if (!edit) {
                                    return (
                                        <>
                                            <span className={classes.label}>{t('form.labels.remarks')}</span>{' '}
                                            {parse((owner_remarks || t(notPresent)).replace(/\n/g, '<br />'))}
                                        </>
                                    );
                                }

                                return (
                                    <CustomInput
                                        labelText={t('form.labels.remarks')}
                                        id="remarks_FVD"
                                        formControlProps={{
                                            fullWidth: true,
                                        }}
                                        inputProps={{
                                            inputProps: {
                                                rows: 5,
                                            },
                                            multiline: true,
                                            color: 'primary',
                                            type: 'text',
                                            autoComplete: 'off',
                                            value: selectedUser.newRemarks || ' ',
                                            onChange: e => (selectedUser.newRemarks = e.target.value),
                                        }}
                                    />
                                );
                            }}
                        </EditableCard>
                    )}
                </GridItem>
            </GridContainer>
            {account.rights.canDeleteUsers && !match.params.registrationId && (
                <Button
                    onClick={() =>
                        general.addModal({
                            title: t('modal.title.user.delete'),
                            body: t('modal.body.user.delete'),
                            buttonText: t('form.button.delete'),
                            onConfirm: async () => {
                                await users.delete(selectedUser.id);
                                history.push('/medewerkers');
                                users.resource.success = 'Profiel werd succesvol verwijderd';
                            },
                        })
                    }
                    color="danger"
                >
                    {t('form.button.delete')}
                </Button>
            )}
        </>
    );

    useEffect(() => {
        if (users.loading) {
            setEditInformation(false);
            setEditPicture(false);
            setEditPrivacy(false);
            setEditRemarks(false);
            setEditContactDetails(false);
        }
    }, [users.loading]);

    useEffect(() => {
        async function fetchData() {
            const oldUserId = users.selectedUser?.id;
            const newUserId = parseInt(match?.params?.id || account.id);

            countries.fetch();
            await shirtSizes.fetch();
            clothingSizes.fetch();

            if (!match.params.registrationId && (!oldUserId || oldUserId !== newUserId)) {
                await users.fetchUser(newUserId);
            }
        }

        fetchData();

        return () => {
            account.error = '';
            account.success = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.id]);

    if (!noName && account.level === 0) {
        return (
            <div className={classes.container} style={{ flexGrow: 1 }}>
                <Page title={`${selectedUser.firstName || ''} ${selectedUser.lastName || ''}`} loading={users.loading}>
                    {account.shouldVerify && (
                        <div
                            style={{
                                padding: '10px 15px',
                                marginTop: ui.isMobile ? 30 : 0,
                                background: '#ff9800',
                                color: '#fff',
                                marginBottom: 20,
                                display: 'flex',
                                justifyContent: 'space-between',
                                flexDirection: ui.isMobile ? 'column' : 'row',
                            }}
                        >
                            <div>
                                U hebt zich sinds geruime tijd niet meer aangemeld, gelieve te bevestigen of al uw
                                gegevens nog correct zijn.
                                <br />
                                U kan de gegevens hieronder aanpassen indien deze zijn verouderd.
                                <br />
                                Zodra alles up-to-date is, gelieve te bevestigen door op 'Bevestig gegevens' te klikken.
                            </div>
                            <Button
                                onClick={async () => {
                                    await account.verify();
                                    account.shouldVerify = false;
                                    let modal;
                                    const closeModal = () => modal.close();
                                    modal = general.addModal({
                                        title: `Gegevens bevestigd`,
                                        body: (
                                            <>
                                                <p>
                                                    Bedankt om uw gegevens te bevestigen! Dit helpt onze interne
                                                    medewerkers enorm vooruit voor de organisatie van de projecten.
                                                    <br />
                                                    <br />U kan hieronder klikken op 'Projecten' om u in te schrijven
                                                    voor een project
                                                </p>
                                                <Button
                                                    onClick={() => {
                                                        closeModal();
                                                        ui.isMobile
                                                            ? history.replace('/projecten')
                                                            : history.replace('/');
                                                    }}
                                                    color="primary"
                                                >
                                                    Ga naar projecten
                                                </Button>
                                            </>
                                        ),
                                    });
                                    account.success = 'Bedankt om uw gegevens te bevestigen';
                                }}
                                style={{ marginLeft: '20px' }}
                                color="success"
                                size="md"
                            >
                                Bevestig gegevens
                            </Button>
                        </div>
                    )}
                    {ProfileContent(true)}
                </Page>
            </div>
        );
    }

    if ((match.params.id || account.id) && !users.loading && !selectedUser.firstName) {
        return (
            <Page title="Gebruiker niet gevonden" containerStyle={style}>
                <div className={classes.content}>
                    <h2 style={{ margin: 0 }}>Deze gebruiker kon niet worden gevonden</h2>
                </div>
                <Button
                    onClick={() => {
                        history.push(ui.isMobile ? '/' : '/projecten');
                    }}
                    style={{ marginLeft: '20px' }}
                    color="primary"
                    size="md"
                >
                    Ga terug naar overzicht
                </Button>
            </Page>
        );
    }

    return ProfileContent();
}

export default withTranslation()(withStyles(profileStyles)(observer(Profile)));
