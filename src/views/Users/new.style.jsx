const newUserStyle = {
    form: {
        maxWidth: 1040,
        margin: '0 10px',
        '& h1': {
            color: '#444444',
            fontSize: '1.5em',
        },
        '& h2': {
            color: '#999',
            fontSize: '0.8em',
            lineHeight: '1em',
            paddingBottom: 17,
        },
    },
    cardFooter: {
        paddingTop: '0rem',
        border: '0',
        borderRadius: '6px',
        justifyContent: 'center !important',
    },
    dropzone: {
        marginTop: 27,
        border: `2px dashed #888`,
        textAlign: 'center',
        color: '#888',
        borderRadius: '5px',
        fontSize: '0.9em',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& > div': {
            minHeight: '200px',
            width: '80%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    },
};

export default newUserStyle;
