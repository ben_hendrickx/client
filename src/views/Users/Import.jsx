import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import inviteUsersStyle from './invite.style.jsx';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import CsvDropzone from 'components/Dropzone/CsvDropzone.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import UserRow from 'components/User/Row.jsx';
import Page from 'components/Page.jsx';
import { Success } from 'components/Notifications.jsx';
import { Checkbox } from '@material-ui/core';

const RegisterInProject = withStyles({ row: { display: 'flex', alignItems: 'center' } })(function RegisterInProject({
    classes,
    projects,
    onSelect,
}) {
    const [projectId, setProjectId] = useState(0);

    return (
        <>
            <p>Wenst u de geïmporteerde deelnemers in te schrijven in een project?</p>
            {projects.map(project => (
                <div className={classes.row} key={project.id}>
                    <Checkbox
                        checked={project.id === projectId}
                        onChange={() => {
                            setProjectId(project.id);
                            onSelect(project.id);
                        }}
                    />
                    <div key={`project-${project.id}`}>
                        <span>{project.name}</span>
                    </div>
                </div>
            ))}
        </>
    );
});

function InviteUsers(props) {
    const { classes } = props;
    const { account, general, users: usersStore } = useStore();
    const [userList, setUserList] = useState([]);
    const [success, setSuccess] = useState('');

    useEffect(() => {
        return () => {
            account.success = null;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page title="Importeer medewerkers">
            <div className={classes.form}>
                <h1>Upload .csv</h1>
                <Success message={success} onClose={() => setSuccess('')} />
                <CsvDropzone
                    onData={data => {
                        data = data.includes('\r\n') ? data.split('\r\n') : data.split('\n');
                        let heading;

                        while (!heading) {
                            const row = data.shift();
                            if (row.toLowerCase().includes('voornaam')) {
                                heading = row;
                            }
                        }
                        const columns = heading.split(';');
                        const associationColumnIndex = columns.findIndex(v => v?.toLowerCase() === 'vereniging') + 1;
                        const vereniging = columns[associationColumnIndex];

                        data = data
                            .map(row => {
                                const userData = row.split(';');
                                const user = {};
                                for (let i in userData) {
                                    const value = userData[i];
                                    let key = (columns[i] || '')
                                        .trim()
                                        .replace(/\s/g, '_')
                                        .replace(/-/g, '')
                                        .toLowerCase();

                                    if (key.includes('verantwoordelijke')) {
                                        key = 'verantwoordelijke';
                                    }
                                    user[key] = value;
                                }

                                const dateOfBirth = (user.geboortedatum || '').split('/');
                                const newDateOfBirth = [dateOfBirth[1], dateOfBirth[0], dateOfBirth[2]].join('/');
                                user.geboortedatum = newDateOfBirth;
                                user.vereniging = vereniging;

                                return user;
                            })
                            .filter(({ mailadres }) => mailadres);
                        setUserList(data);
                    }}
                    className={classes.dropzone}
                />
                {userList.length > 0 && (
                    <div style={{ marginTop: 10, paddingBottom: 10, height: 'calc(100vh - 500px)', overflowY: 'auto' }}>
                        {userList.map(
                            (
                                {
                                    voornaam,
                                    naam,
                                    geboortedatum,
                                    geslacht,
                                    nationaliteit,
                                    telefoonnummer,
                                    gsm_nummer,
                                    mailadres,
                                    bevestig_mailadres,
                                    tweede_mailadres,
                                    straatnaam,
                                    huisnummer,
                                    busnummer,
                                    postcode,
                                    gemeente,
                                    tshirt_maat,
                                    groepsverantwoordelijke,
                                },
                                i
                            ) => (
                                <UserRow
                                    small
                                    firstName={voornaam}
                                    lastName={naam}
                                    email={mailadres}
                                    date_of_birth={false}
                                    watch={false}
                                    actions={[
                                        {
                                            tooltip: 'Niet importeren',
                                            icon: 'delete',
                                            callback: () => {
                                                const newUserList = [...userList];
                                                newUserList.splice(i, 1);
                                                setUserList(newUserList);
                                            },
                                        },
                                    ]}
                                />
                            )
                        )}
                    </div>
                )}
                <CardFooter className={classes.cardFooter}>
                    <Button
                        disabled={userList.length === 0}
                        onClick={async () => {
                            try {
                                const { users, projects } = await account.import(userList);
                                const imported = users.length;
                                if (imported !== userList.length) {
                                    setSuccess(
                                        `${imported} medewerkers werden geïmporteerd, ${userList.length -
                                            imported} medewerkers hadden reeds een profiel`
                                    );
                                } else if (imported > 1) {
                                    setSuccess(`${imported} medewerkers werden geïmporteerd`);
                                } else if (imported === 1) {
                                    setSuccess(`1 medewerker werd geïmporteerd`);
                                }

                                setUserList([]);

                                if (imported && projects.length) {
                                    let modal;
                                    modal = general.addModal({
                                        title: 'Schrijf in voor project',
                                        body: (
                                            <RegisterInProject
                                                projects={projects}
                                                onSelect={projectId => {
                                                    usersStore.projectIdToRegister = projectId;
                                                }}
                                            />
                                        ),
                                        buttonText: 'Schrijf in',
                                        onConfirm: async () => {
                                            modal.close();
                                            await usersStore.registerMultiple(users);
                                            setSuccess(
                                                `${imported} medewerkers werden ingeschreven in het project ${
                                                    projects.find(({ id }) => id === usersStore.projectIdToRegister)
                                                        .name
                                                }`
                                            );
                                        },
                                    });
                                }
                            } catch (e) {}
                        }}
                        color="primary"
                        size="md"
                    >
                        Importeer
                    </Button>
                </CardFooter>
            </div>
        </Page>
    );
}

InviteUsers.propTypes = {
    classes: PropTypes.object,
};

export default withStyles(inviteUsersStyle)(observer(InviteUsers));
