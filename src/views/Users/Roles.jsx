import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import ListRow from 'components/ListRow';
import { observer } from 'mobx-react-lite';
import Role from 'components/Role';
import Inputs from 'components/Inputs';

function RolesOverview() {
    const { projectRoles, users } = useStore();

    function rowRenderer({ key, index, style }) {
        const role = projectRoles.resource.filtered[index];
        const icon = role.type;

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <Role role={role} scene cast identifier type description project icon={icon} />
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            apiPrefix={`/user/${users.selectedUser.id}`}
            store={projectRoles.resource}
            rowRenderer={rowRenderer}
            heightOffset={400}
            filters={
                <>
                    <Inputs.roles.filters.project />
                    {projectRoles.resource.filters.project !== undefined && (
                        <Inputs.roles.filters.scene projectId={projectRoles.resource.filters.project} />
                    )}
                </>
            }
        />
    );
}

export default observer(RolesOverview);
