import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';
import { AccountBalance, ArtTrack, Assignment, CameraRoll, PanTool, Repeat } from '@material-ui/icons';
import registrationsDetailStyle from './detail.style.jsx';
import useReactRouter from 'use-react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Page from 'components/Page.jsx';
import TabView from 'components/TabView.jsx';
import Profile from './Profile.jsx';
import RegistrationsOverview from './Registrations.jsx';
import RolesOverview from './Roles.jsx';
import ScenesOverview from './Scenes.jsx';
import RepetitionsOverview from './Repetitions.jsx';
import Rights from 'views/Users/Rights.jsx';
import { withTranslation } from 'react-i18next';

function UserDetail(props) {
    const { classes, t } = props;
    const { users, account, projects } = useStore();
    const { match, history } = useReactRouter();

    const { selectedUser = { sizes: {} } } = users;

    useEffect(() => {
        account.success = '';
        if (!users.selectedUser) {
            users.fetchUser(match.params.id || account.id);
        }
        //TODO: check switching profile
        // eslint-disable-next-line react-hooks/exhaustive-deps

        return () => {
            if (!projects.id) {
                delete users.selectedUser;
            }
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [match.params.id]);

    return (
        <div className={classes.container} style={{ flexGrow: 1 }}>
            <Page
                loading={!users.selectedUser || users.loading}
                title={`${selectedUser.firstName || ''} ${selectedUser.lastName || ''}`}
            >
                <TabView
                    tabs={[
                        {
                            label: t('page.title.overview'),
                            icon: <AccountBalance />,
                            onClick: () => history.push(`/medewerker/${match.params.id}/overzicht`),
                        },
                        account.rights.canReadUserRegistrations && {
                            label: t('page.title.registrations'),
                            icon: <Assignment />,
                            onClick: () => history.push(`/medewerker/${match.params.id}/inschrijvingen`),
                        },
                        account.rights.canReadUserRoles && {
                            label: t('page.title.roles'),
                            icon: <ArtTrack />,
                            onClick: () => history.push(`/medewerker/${match.params.id}/rollen`),
                        },
                        account.rights.canReadUserScenes && {
                            label: t('page.title.scenes'),
                            icon: <CameraRoll />,
                            onClick: () => history.push(`/medewerker/${match.params.id}/scenes`),
                        },
                        account.rights.canReadUserRepetitions && {
                            label: t('page.title.repetitions'),
                            icon: <Repeat />,
                            onClick: () => history.push(`/medewerker/${match.params.id}/repetities`),
                        },
                        account.rights.canReadUserRights && {
                            label: t('page.title.accessRoles'),
                            icon: <PanTool />,
                            onClick: () => history.push(`/medewerker/${match.params.id}/toegangsrollen`),
                        },
                    ]}
                    routes={[
                        { path: '/medewerker/:id/overzicht', component: () => <Profile style={{ marginTop: 20 }} /> },
                        account.rights.canReadUserRegistrations && {
                            path: '/medewerker/:id/inschrijvingen',
                            component: RegistrationsOverview,
                        },
                        account.rights.canReadUserRoles && { path: '/medewerker/:id/rollen', component: RolesOverview },
                        account.rights.canReadUserScenes && {
                            path: '/medewerker/:id/scenes',
                            component: ScenesOverview,
                        },
                        account.rights.canReadUserRepetitions && {
                            path: '/medewerker/:id/repetities',
                            component: RepetitionsOverview,
                        },
                        account.rights.canReadUserRights && {
                            path: '/medewerker/:id/toegangsrollen',
                            component: Rights,
                        },
                    ]}
                />
            </Page>
        </div>
    );
}

UserDetail.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(registrationsDetailStyle)(observer(UserDetail)));
