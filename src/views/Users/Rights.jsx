import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import useReactRouter from 'use-react-router';
import { Delete } from '@material-ui/icons';
import './rights.scss';
import ResourceOverview from 'components/ResourceOverview';
import ListRow from 'components/ListRow';
import ListRowActions from 'components/ListRowActions';
import { Tooltip } from '@material-ui/core';
import Inputs from 'components/Inputs';
import { withTranslation } from 'react-i18next';

const AccessRoleModalContent = observer(function AccessRoleModalContent() {
    const { accessRoles } = useStore();

    useEffect(() => {
        async function fetchData() {
            accessRoles.data = (await accessRoles.resource.fetchAndReturn()).data.map(({ id, name }) => ({
                value: id,
                label: name,
            }));
        }

        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <>
            <Inputs.Input
                fullWidth
                label="Toegangsrol"
                type="select"
                value={accessRoles.selected}
                onChange={v => (accessRoles.selected = v)}
                options={accessRoles.data}
            />
        </>
    );
});

function Rights({ t }) {
    const { users, accessRoles, general } = useStore();
    const { match } = useReactRouter();

    useEffect(() => {
        return () => {
            users.success = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function rowRenderer({ key, index, style }) {
        const accessRole = accessRoles.resource.filtered[index];

        const { id, name } = accessRole;

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span>{name}</span>
                    <ListRowActions>
                        <Tooltip title={t('tooltip.accessRole.deleteFromUser')} placement="top">
                            <Delete
                                onClick={() => {
                                    general.addModal({
                                        title: t('modal.title.accessRole.deleteFromUser'),
                                        body: t('modal.body.accessRole.deleteFromUser'),
                                        buttonText: t('form.button.delete'),
                                        onConfirm: async () => {
                                            await accessRoles.resource.delete(`/user/${match.params.id}`, id);
                                            await accessRoles.resource.fetch(`/user/${match.params.id}`);
                                        },
                                    });
                                }}
                            />
                        </Tooltip>
                    </ListRowActions>
                </ListRow>
            </li>
        );
    }

    return (
        <>
            <ResourceOverview
                apiPrefix={`/user/${match.params.id}`}
                store={accessRoles.resource}
                rowRenderer={rowRenderer}
                heightOffset={400}
                actions={[
                    //TODO: check rights and remove level property
                    {
                        icon: 'add',
                        onClick: () => {
                            general.addModal({
                                title: t('modal.title.accessRole.addToUser'),
                                body: <AccessRoleModalContent />,
                                buttonText: t('form.button.add'),
                                onConfirm: async () => {
                                    await accessRoles.addToUser(match.params.id);
                                    setTimeout(async () => {
                                        accessRoles.resource.success = t('success.accessRole.addedToUser');
                                        await accessRoles.resource.fetch(`/user/${match.params.id}`);
                                    }, 1000);
                                },
                            });
                        },
                        tooltip: t('tooltip.accessRole.addToUser'),
                    },
                ]}
            />
        </>
    );
}

export default withTranslation()(observer(Rights));
