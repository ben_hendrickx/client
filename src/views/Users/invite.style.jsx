import { primaryColor, white, primaryCardHeader } from '../../assets/jss/material-kit-react.jsx';

const inviteUsersStyle = {
    row: {
        display: 'flex',
        alignItems: 'center',
    },
    header: {
        ...primaryCardHeader,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    dropzone: {
        marginTop: 27,
        border: `2px dashed #888`,
        textAlign: 'center',
        color: '#888',
        borderRadius: '5px',
        fontSize: '0.9em',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        '& > div': {
            minHeight: '200px',
            width: '80%',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
    },
    form: {
        maxWidth: 1040,
        margin: '0 10px',
        '& h1': {
            color: '#444444',
            fontSize: '1.5em',
        },
        '& h2': {
            color: '#999',
            fontSize: '0.8em',
            lineHeight: '1em',
            paddingBottom: 17,
        },
    },
    cardFooter: {
        padding: '15px 0',
    },
    addressList: {
        listStyle: 'none',
        margin: '0',
        padding: '0',
        display: 'flex',
        flexWrap: 'wrap',
        '& > li': {
            position: 'relative',
            display: 'flex',
            background: white,
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
            padding: '0 15px',
            margin: '10px',
            maxWidth: '45%',
            boxSizing: 'border-box',
            '& span.highlightOnHover:hover': {
                cursor: 'pointer',
                fontWeight: 500,
                '& svg': {
                    color: primaryColor,
                },
            },
            '& > span': {
                display: 'inline-flex',
                alignItems: 'center',
                '&.big': {
                    minWidth: 400,
                },
            },
            '& svg': {
                marginRight: '15px',
            },
        },
    },
};

export default inviteUsersStyle;
