import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import Repetition from 'components/Repetitions/Repetition';
import Inputs from 'components/Inputs';

function RepetitionsOverview() {
    const { repetitions, users } = useStore();

    function rowRenderer({ key, index, style }) {
        const repetition = repetitions.resource.filtered[index];

        return (
            <li key={key}>
                <Repetition repetition={repetition} />
            </li>
        );
    }

    return <>
        <h3 className="showPrint">Repetities van {users.selectedUser.lastName} {users.selectedUser.firstName}</h3>
        <ResourceOverview
            apiPrefix={`/user/${users.selectedUser.id}`}
            store={repetitions.resource}
            rowRenderer={rowRenderer}
            heightOffset={400}
            noAutoSizer
            filters={
                <Inputs.repetitions.filters.past />
            }
        />
    </>;
}

export default observer(RepetitionsOverview);
