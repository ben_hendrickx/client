import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import withStyles from '@material-ui/core/styles/withStyles';

import newUserStyle from './new.style.jsx';
import { useHistory } from 'react-router';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import CustomInput from 'components/CustomInput/CustomInput.jsx';
import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import ImageDropzone from 'components/Dropzone/ImageDropzone.jsx';
import Address from 'components/Inputs/Address.jsx';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import CardFooter from 'components/Card/CardFooter.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Page from 'components/Page.jsx';
import spinner from '../../assets/img/spinner.gif';
import { Trans, withTranslation } from 'react-i18next';

function NewProject(props) {
    const { classes, t } = props;
    const { profile, countries, account, shirtSizes, projects } = useStore();
    const history = useHistory();

    useEffect(() => {
        countries.fetch();
        shirtSizes.fetch();

        return () => {
            profile.reset();
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const notAllFilled =
        !profile.privacy ||
        !profile.firstName ||
        !profile.lastName ||
        !profile.dateOfBirth ||
        !profile.sex ||
        !profile.countryOfBirth ||
        !profile.origin ||
        !profile.tShirt ||
        !profile.streetName ||
        !profile.houseNumber ||
        !profile.cityName ||
        !profile.zipCode ||
        (!profile.phone && !profile.cell);
    const disabled = !!profile.success || !!profile.error || notAllFilled;

    function validate(byClick = false) {
        if (profile.firstName && profile.firstName.length < 2) {
            return 'Voornaam is ongeldig';
        }

        if (profile.lastName && profile.lastName.length < 2) {
            return 'Achternaam is ongeldig';
        }

        if (profile.dateOfBirth && !profile.dateOfBirth.isValid()) {
            return 'Geboortedatum is geen geldige datum';
        }

        if (profile.origin && profile.origin.length < 2) {
            return 'Nationaliteit is ongeldig';
        }

        if (
            profile.phone &&
            (profile.phone.length < 9 ||
                !/^((\+|00)\d{1,3}\s?|0)(\d\s?\d{3}|\d{2}\s?\d{2})(\s?\d{2}){2}$/.test(
                    profile.phone.replace(/(\/|\.)/g, '')
                ))
        ) {
            return 'Telefoonnummer is ongeldig';
        }

        if (
            profile.cell &&
            (profile.cell.length < 9 ||
                !/^((\+|00)\d{1,3}\s?|0)\d(60|[6789]\d)(\s?\d{2}){3}$/.test(profile.cell.replace(/(\/|\.)/g, '')))
        ) {
            return 'G.S.M.-nummer is ongeldig';
        }

        if (profile.secondaryMail) {
            if (
                profile.secondaryMail.length < 6 ||
                !profile.secondaryMail.includes('@') ||
                !profile.secondaryMail.includes('.')
            ) {
                return 'Tweede e-mailadres is geen geldig mailadres';
            }
        }

        if (byClick && notAllFilled) {
            let response = 'Gelieve de ontbrekende velden in te vullen (';
            const missing = [];
            !profile.firstName && missing.push('Voornaam');
            !profile.lastName && missing.push('Achternaam');
            !profile.dateOfBirth && missing.push('Geboortedatum');
            !profile.sex && missing.push('Geslacht');
            !profile.origin && missing.push('Nationaliteit');
            !profile.countryOfBirth && missing.push('Geboorteland');
            !profile.phone && !profile.cell && missing.push('Telefoonnummer/G.S.M.-nummer');
            !profile.streetName &&
                !profile.houseNumber &&
                !profile.zipCode &&
                !profile.cityName &&
                missing.push('Adres');
            !profile.tShirt && missing.push('T-shirt maat');
            !profile.privacy && missing.push('Privacy-verklaring');
            return response + missing.join(', ') + ')';
        }

        return null;
    }

    return (
        <div style={{ flexGrow: 1 }}>
            <Page title="Nieuw profiel">
                {profile.error && (
                    <div
                        style={{
                            padding: '10px 15px',
                            background: '#f44336',
                            color: '#fff',
                        }}
                    >
                        {profile.error}
                    </div>
                )}
                <div className={classes.form}>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={5}>
                            <h1>Profielfoto</h1>
                            <ImageDropzone
                                className={classes.dropzone}
                                store={profile}
                                onData={data => (profile.image = data)}
                            />
                            <h2>{t('form.hint.profilePicture')}</h2>
                            <h1>Persoonsgegevens</h1>
                            <CustomInput
                                labelText="Voornaam"
                                id="voornaam"
                                formControlProps={{
                                    error: profile.firstName && profile.firstName.length < 2,
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    autoComplete: 'off',
                                    required: true,
                                    type: 'text',
                                    value: profile.firstName,
                                    onChange: e => (profile.firstName = e.target.value),
                                    onBlur: () => (profile.error = validate()),
                                }}
                            />
                            <CustomInput
                                labelText="Achternaam"
                                id="achternaam"
                                formControlProps={{
                                    error: profile.lastName && profile.lastName.length < 2,
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    type: 'text',
                                    autoComplete: 'off',
                                    value: profile.lastName,
                                    onChange: e => (profile.lastName = e.target.value),
                                    onBlur: () => (profile.error = validate()),
                                }}
                            />
                            <CustomInput
                                type="date"
                                labelText="Geboortedatum *"
                                formControlProps={{
                                    error: profile.dateOfBirth && !profile.dateOfBirth.isValid(),
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    id: 'geboortedatum',
                                    maxDate: new Date(),
                                    maxDateMessage: 'Gelieve een datum te selecteren in het verleden',
                                    minDateMessage: 'Gelieve een datum te kiezen die maximum 100 jaar geleden is',
                                    minDate: new Date(Date.now() - 100 * 365 * 24 * 60 * 60 * 1000),
                                    onBlur: () => (profile.error = validate()),
                                    value: profile.dateOfBirth || null,
                                    onChange: date => (profile.dateOfBirth = date),
                                }}
                            />
                            <CustomInput
                                type="select"
                                labelText="Geslacht"
                                formControlProps={{
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    inputProps: {
                                        id: 'geslacht',
                                    },
                                    native: true,
                                    value: profile.sex,
                                    onChange: e => (profile.sex = e.target.value),
                                    options: [{ label: 'Man', value: 0 }, { label: 'Vrouw', value: 1 }],
                                }}
                            />
                            <CustomInput
                                labelText="Nationaliteit"
                                id="nationaliteit"
                                formControlProps={{
                                    error: profile.origin && profile.origin.length < 2,
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    type: 'text',
                                    autoComplete: 'off',
                                    value: profile.origin,
                                    onChange: e => (profile.origin = e.target.value),
                                    onBlur: () => (profile.error = validate()),
                                }}
                            />
                            <CustomInput
                                type="select"
                                labelText="Geboorteland"
                                formControlProps={{
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    id: 'geboorteland',
                                    native: true,
                                    value: profile.countryOfBirth,
                                    onChange: e => {
                                        return (profile.countryOfBirth = e.target.value);
                                    },
                                    options: countries.autoComplete,
                                }}
                            />
                        </GridItem>
                        <GridItem xs={false} sm={false} md={1}></GridItem>
                        <GridItem xs={12} sm={12} md={6}>
                            <h1>Contactgegevens</h1>
                            <CustomInput
                                labelText="Telefoonnummer"
                                id="telefoonnummer"
                                formControlProps={{
                                    error:
                                        profile.phone &&
                                        (profile.phone.length < 9 ||
                                            !/^((\+|00)\d{1,3}\s?|0)(\d\s?\d{3}|\d{2}\s?\d{2})(\s?\d{2}){2}$/.test(
                                                profile.phone.replace(/(\/|\.)/g, '')
                                            )),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    type: 'text',
                                    autoComplete: 'off',
                                    value: profile.phone,
                                    onChange: e => (profile.phone = e.target.value),
                                    onBlur: () => (profile.error = validate()),
                                }}
                            />
                            <CustomInput
                                labelText="G.S.M.-nummer"
                                formControlProps={{
                                    error:
                                        profile.cell &&
                                        (profile.cell.length < 9 ||
                                            !/^((\+|00)\d{1,3}\s?|0)\d(60|[6789]\d)(\s?\d{2}){3}$/.test(
                                                profile.cell.replace(/(\/|\.)/g, '')
                                            )),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    id: 'gsm_nummer',
                                    color: 'primary',
                                    type: 'text',
                                    autoComplete: 'off',
                                    value: profile.cell,
                                    onChange: e => (profile.cell = e.target.value),
                                    onBlur: () => (profile.error = validate()),
                                }}
                            />
                            <CustomInput
                                labelText="Tweede mailadres"
                                id="tweede_mailadres"
                                formControlProps={{
                                    error:
                                        profile.secondaryMail &&
                                        (profile.secondaryMail.length < 6 ||
                                            !profile.secondaryMail.includes('@') ||
                                            !profile.secondaryMail.includes('.')),
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    color: 'primary',
                                    type: 'email',
                                    autoComplete: 'new-password',
                                    value: profile.secondaryMail,
                                    onChange: e => (profile.secondaryMail = e.target.value),
                                    onBlur: () => (profile.error = validate()),
                                }}
                            />
                            <h1>Adresgegevens</h1>
                            <Address store={profile} />
                            <h1>Overige gegevens</h1>
                            <CustomInput
                                type="select"
                                labelText="T-shirt maat"
                                formControlProps={{
                                    required: true,
                                    fullWidth: true,
                                }}
                                inputProps={{
                                    id: 't-shirt_maat',
                                    native: true,
                                    value: profile.tShirt,
                                    onChange: e => (profile.tShirt = e.target.value),
                                    options: shirtSizes.autoComplete,
                                }}
                            />
                        </GridItem>
                    </GridContainer>
                    <CardFooter className={classes.divider}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    color="primary"
                                    checked={profile.privacy}
                                    onChange={e => (profile.privacy = e.target.checked)}
                                    value={profile.privacy}
                                    inputProps={{
                                        'aria-label':
                                            'Privacyverklaring van stad Geel m.b.t. bescherming van de persoonlijke gegevens ',
                                    }}
                                />
                            }
                            style={{ alignItems: 'start' }}
                            label={
                                <p>
                                    Ik bevestig{' '}
                                    <a target="_blank" rel="noopener noreferrer" href={process.env.REACT_APP_PRIVACY}>
                                        de privacyverklaring van {process.env.REACT_APP_CLIENT} m.b.t. bescherming van
                                        gegevens
                                    </a>{' '}
                                    gelezen te hebben
                                </p>
                            }
                        />
                    </CardFooter>
                    <CardFooter className={classes.cardFooter}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    color="primary"
                                    checked={profile.pictures}
                                    onChange={e => (profile.pictures = e.target.checked)}
                                    value={profile.pictures}
                                    inputProps={{
                                        'aria-label': 'Privacy',
                                    }}
                                />
                            }
                            style={{ alignItems: 'start' }}
                            label={
                                <p>
                                    <Trans i18nKey="privacy">
                                        Hierbij verklaar ik dat foto’s en video’s waarop ik duidelijk in beeld sta door
                                        het Lokaal Bestuur Geel gebruikt mogen worden:
                                        <br />
                                        - in publicaties, magazines of brochures van Lokaal Bestuur Geel
                                        <br />
                                        - in de stedelijke rubriek in Deze Week en Rondom
                                        <br />
                                        - op de websites van Lokaal Bestuur Geel, op sociale media van Lokaal Bestuur
                                        Geel (Facebook, Twitter, Instagram…), in de digitale nieuwsbrieven van Lokaal
                                        Bestuur Geel
                                        <br />
                                        <br />
                                        Indien u nog geen 16 jaar bent, moet een ouder of voogd toestemming geven voor
                                        het gebruiken van foto- en beeldmateriaal. Dit kan door een e-mail te sturen
                                        naar {process.env.REACT_APP_EMAIL}. Gelieve de naam van het kind duidelijk te
                                        vermelden in de mail. Wenst u niet langer dat uw foto- en beeldmateriaal wordt
                                        gebruikt, laat dit dan weten via {process.env.REACT_APP_EMAIL} of aan de balie
                                        van Toerisme Geel.
                                    </Trans>
                                </p>
                            }
                        />
                    </CardFooter>
                    <CardFooter className={classes.cardFooter}>
                        <Button
                            color={`${disabled || account.loading ? 'disabled' : 'primary'}`}
                            onClick={async () => {
                                if (disabled) {
                                    profile.error = validate(true);
                                    return;
                                }
                                try {
                                    const userId = await profile.create();
                                    await account.fetchProfiles();
                                    if (userId !== undefined) {
                                        await account.changeProfile(userId);
                                    }
                                    history.replace('/projecten');
                                    setTimeout(() => {
                                        projects.success = 'Het nieuwe profiel werd succesvol aangemaakt';
                                    }, 500);
                                } catch (e) {}
                            }}
                            size="md"
                        >
                            {account.loading ? <img alt="spinner" src={spinner} height={25} /> : 'Maak profiel'}
                        </Button>
                    </CardFooter>
                </div>
            </Page>
        </div>
    );
}

NewProject.propTypes = {
    classes: PropTypes.object,
};

export default withTranslation()(withStyles(newUserStyle)(observer(NewProject)));
