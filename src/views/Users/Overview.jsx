import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';
import UserRow from '../../components/User/Row.jsx';
import Inputs from 'components/Inputs';
import Import from './Import';
import { withTranslation } from 'react-i18next';

function Filters() {
    return (
        <>
            <Inputs.users.filters.sex />
            <Inputs.users.filters.minAge />
            <Inputs.users.filters.maxAge />
        </>
    );
}

function UsersOverview({ t }) {
    const { ui, account, users, general } = useStore();

    function rowRenderer({ key, index, style }) {
        const user = users.resource.data[index];

        return (
            <UserRow
                key={key}
                style={{ ...(style || {}), height: 'auto', width: '100%', margin: '10px 0px 10px 0px' }}
                onClick={() => {}}
                {...(user || {})}
                actions={[
                    {
                        developer: true,
                        tooltip: 'Delete user',
                        icon: 'delete',
                        callback: () => {},
                    },
                    // TODO: support this again
                    // account.rights.canCreateRegistrations && {
                    //     tooltip: 'Schrijf in voor project',
                    //     icon: 'note_add',
                    //     callback: () => {},
                    // },
                ]}
            />
        );
    }

    return (
        <ResourceOverview
            store={users.resource}
            rowRenderer={rowRenderer}
            heightOffset={260}
            rowHeight={ui.isMobile ? 180 : 80}
            actions={[
                //TODO: check rights and remove level property
                //TODO: make this page work properly --> { icon: 'mail', route: '/medewerkers/mail', level: 1, tooltip: 'Verstuur mail' },
                //TODO: add export right to database
                account.rights.canExportUsers && {
                    icon: 'getApp',
                    href: `/api/reports/users.xlsx`,
                    tooltip: t('tooltip.report.download'),
                    addition: users.filters.getString(),
                },
                // TODO: support this again
                account.rights.canCreateUsers && {
                    icon: 'importExport',
                    tooltip: t('tooltip.import.users'),
                    onClick: () => {
                        general.addModal({
                            type: 'window',
                            body: <Import />,
                        });
                    },
                },
                account.rights.canCreateUsers && {
                    icon: 'getApp',
                    tooltip: `${t('tooltip.download.template')}: ${t('tooltip.import.users').toLowerCase()}`,
                    href: `/api/templates/import_users.xlsx`,
                },
            ]}
            backend
            filters={<Filters />}
            sort={<Inputs.users.sort />}
        />
    );
}

export default withTranslation()(observer(UsersOverview));
