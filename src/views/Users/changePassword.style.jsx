import { white } from '../../assets/jss/material-kit-react.jsx';

const changePasswordStyle = {
    card: {
        position: 'relative',
        '& img': {
            maxWidth: '100%',
        },
        background: white,
        boxShadow:
            '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',
        padding: '10px 15px',
        margin: '26px 0',
        '& > span': {
            marginTop: 10,
        },
        '& > span:first-child': {
            marginTop: 0,
        },
    },
    cardFooter: {
        padding: 0,
    },
};

export default changePasswordStyle;
