import React, { useEffect } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import { useStore } from '../../mobx';
import { observer } from 'mobx-react-lite';

import GridContainer from 'components/Grid/GridContainer.jsx';
import GridItem from 'components/Grid/GridItem.jsx';
import { LockOutlined } from '@material-ui/icons';
import { useHistory } from 'react-router';
import InputAdornment from '@material-ui/core/InputAdornment';
import CustomInput from 'components/CustomInput/CustomInput.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import Button from 'components/CustomButtons/Button.jsx';
import Page from 'components/Page.jsx';

import changePasswordStyle from './changePassword.style.jsx';
import { Success } from 'components/Notifications';
import { Danger } from 'components/Notifications';
import { withTranslation } from 'react-i18next';

function ChangePassword(props) {
    const { classes, t } = props;
    const { account } = useStore();
    const history = useHistory();

    useEffect(() => {
        account.resetNotifications();
        account.password = '';
        account.confirmPassword = '';

        return () => {
            account.resetNotifications();
            account.password = '';
            account.confirmPassword = '';
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page title={t('page.title.changePassword')}>
            <Success message={account.success} onClose={() => (account.success = '')} />
            <Danger message={account.error} onClose={() => (account.error = '')} />
            <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                    <form className={classes.card} style={{ textAlign: 'center' }}>
                        <input
                            style={{ display: 'none' }}
                            type="text"
                            autoComplete="username email"
                            id="username"
                            name="email"
                            value={account.email}
                        />
                        <CustomInput
                            labelText={t('form.labels.password')}
                            id="password"
                            formControlProps={{
                                fullWidth: true,
                            }}
                            inputProps={{
                                color: 'primary',
                                autoComplete: 'new-password',
                                required: true,
                                type: 'password',
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <LockOutlined className={classes.inputIconsColor} />
                                    </InputAdornment>
                                ),
                                value: account.password,
                                onChange: e => (account.password = e.target.value),
                            }}
                        />

                        <CustomInput
                            labelText={t('form.labels.confirmPassword')}
                            id="confirmPassword"
                            formControlProps={{
                                fullWidth: true,
                            }}
                            inputProps={{
                                color: 'primary',
                                autoComplete: 'new-password',
                                required: true,
                                type: 'password',
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <LockOutlined className={classes.inputIconsColor} />
                                    </InputAdornment>
                                ),
                                value: account.confirmPassword,
                                onChange: e => (account.confirmPassword = e.target.value),
                            }}
                        />
                        <CardFooter className={classes.cardFooter}>
                            <Button
                                onClick={() =>
                                    account.setPassword(account.email, account.token).then(() => {
                                        let timeout = setTimeout(() => {
                                            history.goBack();
                                        }, 3000);
                                        history.listen(() => clearTimeout(timeout));
                                    })
                                }
                                color="primary"
                                size="md"
                                disabled={
                                    !account.password ||
                                    !account.confirmPassword ||
                                    account.password !== account.confirmPassword ||
                                    account.password.length < 6
                                }
                            >
                                {t('form.button.confirm')}
                            </Button>
                        </CardFooter>
                    </form>
                </GridItem>
            </GridContainer>
        </Page>
    );
}

export default withTranslation()(withStyles(changePasswordStyle)(observer(ChangePassword)));
