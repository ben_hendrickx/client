import React from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import ListRow from 'components/ListRow';
import { observer } from 'mobx-react-lite';
import Scene from 'components/Scene';
import Inputs from 'components/Inputs';

function ScenesOverview() {
    const { scenes, users } = useStore();

    function rowRenderer({ key, index, style }) {
        const scene = scenes.resource.filtered[index];

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <Scene scene={scene} identifier type description project />
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            apiPrefix={`/user/${users.selectedUser.id}`}
            store={scenes.resource}
            rowRenderer={rowRenderer}
            heightOffset={400}
            filters={<Inputs.scenes.filters.project />}
        />
    );
}

export default observer(ScenesOverview);
