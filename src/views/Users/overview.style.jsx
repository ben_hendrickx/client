import { primaryCardHeader } from '../../assets/jss/material-kit-react.jsx';

const projectsOverviewStyle = {
    container: {
        flexGrow: 1,
        minWidth: '100%',
        '& h2': {
            textAlign: 'center',
            fontSize: '1.5em',
        },
    },
    header: {
        ...primaryCardHeader,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: '15px',
        paddingBottom: '15px',
        boxShadow: '0px 5px 10px -3px rgba(68,68,68,0.2)',
        '& > img': {
            maxHeight: '40px',
            marginRight: '20px',
        },
        '& > span': {
            fontWeight: 'bold',
            fontStyle: 'italic',
        },
        '& svg:first-child': {
            position: 'absolute',
            left: '10px',
        },
        '& svg:last-child': {
            position: 'absolute',
            right: '10px',
        },
    },
    imageOverlay: {
        background: 'rgba(0,0,0,0.6)',
        borderRadius: 5,
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& img': {
            cursor: 'pointer',
            maxWidth: '90vw',
            maxHeight: '90vh',
        },
    },
    users: {
        listStyle: 'none',
        margin: '0',
        padding: '10px 0',
    },
    overlay: {
        background: 'rgba(0,0,0,0.6)',
        borderRadius: 5,
        padding: 20,
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100vw',
        height: '100vh',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        userSelect: 'none',
        zIndex: 999,
        '& > div': {
            padding: '20px 30px',
            borderRadius: '5px',
            background: '#ffffff',
            boxShadow:
                '2px 1px 5px -1px rgba(0,0,0,0.8), 0px 0px 1px 0px rgba(0,0,0,0.74), 0px 3px 5px -2px rgba(0,0,0,0.72)',
            '& h1': {
                fontSize: '1.5rem',
            },
        },
    },
    project: {
        margin: '5px 0',
        borderRadius: 3,
        border: '1px solid rgba(0,0,0,.1)',
        cursor: 'pointer',
        padding: '5px 10px !important',
        boxShadow: 'none !important',
        display: 'flex',
        justifyContent: 'space-between',
        '&:hover': {
            background: 'rgba(0,0,0,.05)',
        },
        '& span': {
            display: 'flex',
            alignItems: 'center',
            marginRight: 10,
        },
    },
};

export default projectsOverviewStyle;
