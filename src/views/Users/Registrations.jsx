import React, { useEffect } from 'react';
import ResourceOverview from 'components/ResourceOverview';
import { useStore } from '../../mobx';
import ListRow from 'components/ListRow';
import { observer } from 'mobx-react-lite';
import moment from 'moment';
import { FolderOpen, Lock, LockOpen } from '@material-ui/icons';

function RegistrationsOverview() {
    const { registrations, users } = useStore();

    useEffect(() => {
        registrations.resource.findIn = 'project';
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    function rowRenderer({ key, index, style }) {
        const { project } = registrations.resource.filtered[index];

        const { name, openFrom, closedFrom, closedOffStage } = project;

        let endDate = closedFrom ? closedFrom : closedOffStage;
        closedOffStage && moment(closedFrom).isBefore(new Date(closedOffStage)) && (endDate = closedOffStage);

        return (
            <li key={key} style={style}>
                <ListRow key={key}>
                    <span style={{ width: 200 }}>
                        <FolderOpen /> {name}
                    </span>
                    {openFrom && (
                        <span>
                            <LockOpen /> {moment(openFrom).format('DD/MM/YYYY HH:mm')}
                        </span>
                    )}
                    {endDate && (
                        <span>
                            <Lock /> {moment(endDate).format('DD/MM/YYYY HH:mm')}
                        </span>
                    )}
                </ListRow>
            </li>
        );
    }

    return (
        <ResourceOverview
            apiPrefix={`/user/${users.selectedUser.id}`}
            store={registrations.resource}
            rowRenderer={rowRenderer}
            heightOffset={550}
        />
    );
}

export default observer(RegistrationsOverview);
