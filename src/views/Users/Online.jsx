import { useStore } from '../../mobx';
import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import Page from 'components/Page';
import Row from 'components/User/Row';

function Online() {
    const { statistics } = useStore();

    useEffect(() => {
        statistics.getOnlineUsers();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Page title="Online medewerkers" loading={statistics.loading}>
            {statistics.online.map(u => (
                <Row {...u} showImage={false} details={false} style={{ paddingLeft: 15 }} />
            ))}
        </Page>
    );
}

export default observer(Online);
