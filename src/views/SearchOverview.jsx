import React from 'react';
import { useHistory } from 'react-router';
import Page from 'components/Page';

import { useStore } from '../mobx';
import { withStyles } from '@material-ui/styles';
import { primaryCardHeader } from '../assets/jss/material-kit-react.jsx';
import { observer } from 'mobx-react-lite';
import { withTranslation } from 'react-i18next';

function pad(value, length) {
    if ((value || '').toString().length >= length) {
        return value;
    }

    return pad('0' + value, length);
}

const searchOverviewStyle = {
    type: {
        '& header': {
            ...primaryCardHeader,
            padding: '10px 20px',
            fontSize: '1.2em',
        },
        '& > div': {
            padding: '10px 20px',
            background: '#fff',
            boxShadow:
                '0px 1px 3px 0px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 2px 1px -1px rgba(0,0,0,0.12)',

            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            flexWrap: 'wrap',
            '& > div': {
                minWidth: '20%',
                flexGrow: 1,
                flexShrink: 1,
                flexBasis: 0,
                background: 'rgba(0,0,0,0.05)',
                transition: 'all linear 0.1s',
                borderRadius: 2,
                border: '1px solid rgba(0,0,0,0.15)',
                marginBottom: 10,
                marginRight: 10,
                padding: '5px 10px',
                '&:hover': {
                    cursor: 'pointer',
                    background: 'rgba(0,0,0,0.1)',
                    border: '1px solid rgba(0,0,0,0.2)',
                },
            },
        },
    },
};

function SearchOverview(props) {
    const { classes, t } = props;
    const { account, general } = useStore();
    const history = useHistory();

    return (
        <Page title={t('page.title.search')}>
            {!general.hasSearchResults && <h2>{t('errors.noSearchResults')}</h2>}
            {/* eslint-disable-next-line */}
            {Object.entries(general.searchResults).map(([type, results]) => {
                switch (type) {
                    case 'user':
                        return (
                            <section className={classes.type}>
                                <header>{t('resource.users')}</header>
                                <div>
                                    {results.map(user => (
                                        <div
                                            key={`user-${user.id}`}
                                            onClick={e => {
                                                history.push('/medewerker/' + user.id + '/overzicht');
                                            }}
                                        >
                                            {user.firstName} {user.lastName}
                                        </div>
                                    ))}
                                </div>
                            </section>
                        );
                    case 'project':
                        return account.rights.canReadProjects ? (
                            <section className={classes.type}>
                                <header>{t('resource.projects')}</header>
                                <div>
                                    {results.map(project => (
                                        <div
                                            key={`project-${project.id}`}
                                            onClick={e => {
                                                history.push('/projecten/' + project.id + '/overzicht');
                                            }}
                                        >
                                            {project.name}
                                        </div>
                                    ))}
                                </div>
                            </section>
                        ) : null;
                    case 'article':
                        return (
                            <section className={classes.type}>
                                <header>{t('resource.articles')}</header>
                                <div>
                                    {results.map(article => (
                                        <div
                                            key={`article-${article.id}`}
                                            onClick={e => {
                                                history.push('/kledijbeheer/artikel/' + article.id);
                                            }}
                                        >
                                            {pad(article.id, 6)} {article.clothing.name} (Maat: {article.size.size})
                                        </div>
                                    ))}
                                </div>
                            </section>
                        );
                    default:
                        break;
                }
            })}
        </Page>
    );
}

export default withTranslation()(withStyles(searchOverviewStyle)(observer(SearchOverview)));
