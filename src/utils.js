import moment from 'moment';

function getTime(date) {
    return moment(date).format('HH:mm');
}

export { getTime };
