import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { useHistory, withRouter } from 'react-router';

import { observer } from 'mobx-react-lite';
import { StoreProvider, useStore } from './mobx';
import { createBrowserHistory } from 'history';
import { Router, Route } from 'react-router-dom';
import { Anonymous, Authenticated, LoggedOff, LoggedIn } from 'components/Routes.jsx';

import Dashboard from 'views/Dashboard/Dashboard.jsx';
import FullPageLoader from 'components/FullPageLoader.jsx';
import Modal from 'components/Modal/Modal.jsx';
import { NotificationContainer } from 'react-notifications';

import 'assets/scss/material-kit-react.scss?v=1.7.0'; //TODO: rewrite
import 'react-notifications/lib/notifications.css';
import 'react-chat-elements/dist/main.css';
import 'index.scss';

import { Close, Launch } from '@material-ui/icons';
import { primaryColor, primaryColorDark } from 'assets/jss/material-kit-react';
import './i18n';

const Modals = withRouter(
    observer(function(props) {
        const { general } = useStore();

        const minimizedModals = general.modals?.filter(modal => modal.minimized);
        const openModals = general.modals?.filter(modal => !modal.minimized);

        useEffect(() => {
            window.addEventListener('popstate', () => {
                general.modals.forEach(modal => modal.minimize());
            });
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, []);

        useEffect(() => {
            function handler(e) {
                const modals = general.modals || [];
                const topModal = modals[modals.length - 1];
                if (topModal && e.code && e.code.toLowerCase().includes('escape')) {
                    topModal.close();
                }
            }

            window.addEventListener('keyup', handler);

            return () => {
                window.removeEventListener('keyup', handler);
            };
        }, [general.modals]);

        return (
            <>
                {openModals?.map((modal, index) => <Modal modal={modal} index={index} />)}
                {props.children}
                <div
                    style={{
                        position: 'fixed',
                        top: !!minimizedModals?.length ? 'calc(100vh - 30px)' : '100vh',
                        bottom: 0,
                        left: 205,
                        right: 0,
                        display: 'flex',
                        flexDirection: 'row',
                        zIndex: 9999,
                        height: 30,
                    }}
                >
                    {minimizedModals?.map((modal, index) => <MinimizedModal modal={modal} />)}
                </div>
            </>
        );
    })
);

const MinimizedModal = ({ modal }) => (
    <div
        style={{
            background: 'white',
            maxWidth: 250,
            textAlign: 'center',
            paddingLeft: 10,
            position: 'relative',
            height: 30,
            marginRight: 10,
            boxShadow: '1px 1px 2px 1px rgb(0 0 0 / 40%)',
            fontSize: '14px',
            color: '#222',
            display: 'flex',
            alignItems: 'center',
        }}
    >
        <span
            style={{
                maxWidth: 150,
                overflow: 'hidden',
                display: 'block',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
            }}
        >
            {modal.title}
        </span>
        <div
            style={{
                cursor: 'pointer',
                background: `linear-gradient(180deg, ${primaryColor}, ${primaryColorDark})`,
                color: 'white',
                marginLeft: 20,
                padding: '6px 10px 0px',
            }}
        >
            <Launch onClick={() => modal.maximize()} style={{ fontSize: 16, marginRight: 10 }} />
            <Close onClick={() => modal.close()} style={{ fontSize: 16 }} className="close" />
        </div>
    </div>
);

const AppManager = observer(function(props) {
    const { account, general } = useStore();
    const history = useHistory();

    useEffect(() => {
        if (props.verifyToken) {
            account.verifyToken().catch(() => {
                console.error('Going back to main screen because verifying token failed');
                history.push('/');
            });
        }

        if ((window.location.search || '').includes('emailConfirmed=true')) {
            account.success = 'Bedankt voor het bevestigen van uw e-mailadres, u kan zich nu aanmelden';
        }

        if ((window.location.search || '').includes('projectAgain=true')) {
            account.success = 'Bedankt voor je feedback, u kan zich nu aanmelden om u in te schrijven';
        }

        if ((window.location.search || '').includes('projectAgain=false')) {
            account.success = 'Wij danken je voor je feedback!';
        }

        if ((window.location.search || '').includes('unsubscribed=true')) {
            account.success = 'U heeft zich uitgeschreven voor alle mails van fanvandimpna.be';
        }

        function handler() {
            if (general.searchOpen) {
                general.searchOpen = false;
            }
        }
        window.addEventListener('click', handler);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (!account.loggedIn) {
            function handler(e) {
                if (account.loggedIn) {
                    window.removeEventListener(e.type, handler);
                }

                if (e.code && e.code.toLowerCase().includes('enter')) {
                    !account.loggedIn && account.login();
                }
            }
            window.addEventListener('keyup', handler);
        }

        if (!account.level && account.shouldVerify) {
            history.replace('/profiel');
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [account.loggedIn, account.shouldVerify, account.id]);

    if (account.loggedIn === null || (account.loggedIn && !account.id)) {
        return <FullPageLoader />;
    }

    return props.children;
});

AppManager.propTypes = {
    children: PropTypes.node,
};

const hist = createBrowserHistory();

const App = function() {
    const { ui } = useStore();
    return (
        <Router history={hist}>
            <AppManager verifyToken>
                <Modals>
                    <Anonymous>
                        <LoggedOff />
                    </Anonymous>
                    <Authenticated>
                        <Route exact={ui.isMobile} path="/" component={Dashboard} />
                        {ui.isMobile && <LoggedIn />}
                        <NotificationContainer />
                    </Authenticated>
                </Modals>
            </AppManager>
        </Router>
    );
};

ReactDOM.render(
    <StoreProvider>
        <App />
    </StoreProvider>,
    document.getElementById('root')
);
