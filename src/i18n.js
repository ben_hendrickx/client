import i18n from 'i18next';
import HttpApi from 'i18next-http-backend';
import { apiUrl } from './mobx';
import { initReactI18next } from 'react-i18next';

i18n.use(HttpApi)
    .use(initReactI18next)
    .init({
        lng: window.localStorage.getItem('FVD_LANG'),
        fallbackLng: 'nl',
        debug: false,
        keySeparator: '.',
        interpolation: { escapeValue: false },
        react: { useSuspense: false },
        backend: { loadPath: `${apiUrl}/api/locales/{{lng}}` },
    });

export default i18n;
