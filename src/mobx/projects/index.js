import { observable } from 'mobx';
import moment from 'moment';

import copy from './copy';

import resourceGenerator from '../shared/resource';
import register from './register';
import form from '../shared/form';

export default function projects(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request, globalStore } = dependencies;

    return observable({
        copyOnStage: true,
        async importRoleScheme(data) {
            this.loading = true;
            await request(`/api/project/${this.resource.item.id}/import/scheme`, 'POST', data);
            await globalStore.overview.fetch(this.resource.item.id);
            this.loading = false;
        },
        async importRoleSchemeUsers(data) {
            await request(`/api/project/${this.resource.item.id}/import/scheme/users`, 'POST', data);
        },
        resource: resource({
            single: 'project',
            plural: 'projects',
            labels: {
                single: 'project',
                plural: 'projecten',
            },
            filters: {
                inactive: 0,
            },
        }),
        accessRoles: {
            resource: resource({
                single: 'accessRole',
                plural: 'accessRoles',
                labels: {
                    single: 'Toegangsrol',
                    plural: 'Toegangsrollen',
                },
            }),
            data: {},
            get config() {
                return {
                    sections: [{ id: 'information' }],
                    fields: [
                        {
                            id: 'name',
                            required: true,
                            type: 'text',
                            translationKey: 'form.labels.name',
                            section: 'information',
                        },
                        {
                            id: 'description',
                            type: 'text',
                            required: true,
                            label: 'Omschrijving',
                            section: 'information',
                        },
                        {
                            id: 'accessRoleId',
                            required: true,
                            type: 'select',
                            label: 'Toegangsrol',
                            section: 'information',
                            options: globalStore.accessRoles.resource.data
                                .filter(v => v)
                                .map(({ id, name }) => ({ label: name, value: id })),
                        },
                    ],
                };
            },
            get form() {
                return form(this.config, this.data);
            },
            users: [],
            async fetchUsers(accessRoleId) {
                this.users = (await request(
                    `/api/project/${globalStore.projects.id}/accessRole/${accessRoleId}/users`
                )).data.map(({ userId }) => userId);
            },
            async saveUsers(accessRoleId) {
                await request(
                    `/api/project/${globalStore.projects.id}/accessRole/${accessRoleId}/users`,
                    'POST',
                    this.users
                );
            },
        },
        formData: {},
        get config() {
            return {
                sections: [
                    {
                        id: 'information',
                        translationKey: 'form.sections.projectInfo',
                    },
                    {
                        id: 'registrations',
                        translationKey: 'form.sections.registrations',
                        title: 'Inschrijvingen',
                    },
                    {
                        id: 'access-roles',
                        translationKey: 'form.sections.roleScheme',
                        title: 'Rollenschema',
                    },
                ],
                fields: [
                    {
                        id: 'name',
                        translationKey: 'form.labels.name',
                        type: 'text',
                        required: true,
                        validations: [{ type: 'length', min: 3, max: 255 }],
                        section: 'information',
                        //TODO: add support for hint property
                    },
                    {
                        id: 'projectClosed',
                        translationKey: 'form.labels.project.end',
                        type: 'datetime',
                        required: true,
                        validations: [
                            //TODO: add validation for dates
                        ],
                        section: 'information',
                    },
                    process.env.REACT_APP_IS_FVD === 'true' && {
                        id: 'extended',
                        label: 'Uitgebreide versie (voor ommegang)',
                        type: 'checkbox',
                        section: 'information',
                    },
                    {
                        id: 'auditions_enabled',
                        translationKey: 'form.labels.project.auditions',
                        type: 'checkbox',
                        section: 'information',
                    },
                    this.formData.auditions_enabled && {
                        id: 'auditionHint',
                        translationKey: 'form.labels.project.auditionHint',
                        type: 'text',
                        validations: [{ type: 'length', min: 10, max: 255 }],
                        section: 'information',
                    },
                    {
                        id: 'openFrom',
                        translationKey: 'form.labels.project.registrations.open',
                        type: 'datetime',
                        required: true,
                        validations: [
                            //TODO: add validation for dates
                        ],
                        section: 'registrations',
                    },
                    {
                        id: 'closedFrom',
                        translationKey: 'form.labels.project.registrations.onStage',
                        type: 'datetime',
                        required: true,
                        validations: [
                            //TODO: add validation for dates
                        ],
                        section: 'registrations',
                    },
                    {
                        id: 'closedOffStage',
                        translationKey: 'form.labels.project.registrations.offStage',
                        type: 'datetime',
                        required: true,
                        validations: [
                            //TODO: add validation for dates
                        ],
                        section: 'registrations',
                    },
                    {
                        id: 'editRegistrationClosed',
                        translationKey: 'form.labels.project.registrations.edit',
                        type: 'datetime',
                        validations: [
                            //TODO: add validation for dates
                        ],
                        section: 'registrations',
                    },
                    {
                        id: 'deleteRegistrationClosed',
                        translationKey: 'form.labels.project.registrations.delete',
                        type: 'datetime',
                        validations: [
                            //TODO: add validation for dates
                        ],
                        section: 'registrations',
                    },
                    {
                        id: 'rolesOpen',
                        translationKey: 'form.labels.project.roleScheme.open',
                        type: 'datetime',
                        validations: [
                            //TODO: add validation for dates
                        ],
                        section: 'access-roles',
                    },
                    {
                        id: 'rolesClosed',
                        translationKey: 'form.labels.project.roleScheme.closed',
                        type: 'datetime',
                        validations: [
                            //TODO: add validation for dates
                        ],
                        section: 'access-roles',
                    },
                ],
            };
        },
        get form() {
            return form(this.config, this.formData);
        },

        data: [],
        filter: '',

        copy: copy(dependencies),
        register: register(dependencies),

        showInactive: false,

        partsOfTown: [],

        name: '',

        //COMPUTING FUNCTIONS
        get autoComplete() {
            return this.resource.filtered.map(({ id, name }) => ({ label: name, value: id }));
        },
        get filteredData() {
            const midnightYesterday = new Date().setHours(0, 0, 0, 0);
            const midnight = midnightYesterday + 24 * 60 * 60 * 1000;

            const projects = this.data
                .filter(v => v)
                .filter(({ openFrom, closedFrom, closedOffStage }) => {
                    if (this.showInactive) {
                        return (
                            !openFrom ||
                            (closedFrom &&
                                moment(closedFrom)
                                    .add(1, 'day')
                                    .isSameOrBefore(midnightYesterday) &&
                                closedOffStage &&
                                moment(closedOffStage)
                                    .add(1, 'day')
                                    .isSameOrBefore(midnightYesterday))
                        );
                    }

                    if (!openFrom) {
                        return false;
                    }

                    return (
                        (closedFrom &&
                            moment(closedFrom)
                                .add(1, 'day')
                                .isSameOrAfter(midnight)) ||
                        (closedOffStage &&
                            moment(closedOffStage)
                                .add(1, 'day')
                                .isSameOrAfter(midnight))
                    );
                });

            if (!this.filter) {
                return projects;
            }

            return projects.filter(({ name }) => name.toLowerCase().includes(this.filter.toLowerCase()));
        },

        //HELPER FUNCTIONS
        reset() {
            this.id = null;
            this.name = null;
            this.title = null;
            this.subtitle = null;
            this.openFrom = null;
            this.closedFrom = null;
            this.closedOffStage = null;
            this.rolesOpen = null;
            this.rolesClosed = null;
            this.auditionHint = null;
            this.extended = false;
            this.auditions_enabled = false;
            this.editRegistrationClosed = null;
            this.deleteRegistrationClosed = null;
            this.rights = null;
        },
        hasRight(name) {
            return (
                [1, 2, 3].includes(globalStore.account.level) ||
                (this.rights && this.rights[name] !== undefined && this.rights[name] > 0)
            );
        },
        hasEditRight(name) {
            return (
                [1, 2].includes(globalStore.account.level) ||
                (this.hasRight(name) && this.rights && this.rights[name] > 1)
            );
        },
        hasAllRights(name) {
            return (
                [1, 2].includes(globalStore.account.level) ||
                (this.hasEditRight(name) && this.rights && this.rights[name] > 2)
            );
        },

        // DATA FUNCTIONS
        async fetchProject(id) {
            try {
                this.loading = true;
                const project = await request(`/api/project/${id}`);

                this.id = project.id;

                this.resource.item = {
                    ...project,
                };

                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async save() {
            const { name } = this;

            const method = this.id ? 'PUT' : 'POST';

            const url = `/api/project${this.id ? `/${this.id}` : 's'}`;
            await request(url, method, {
                name,
                title: this.title,
                subtitle: this.subtitle,
                openFrom: moment(this.openFrom),
                closedFrom: moment(this.closedFrom),
                closedOffStage: moment(this.closedOffStage),
                auditionHint: this.auditionHint,
                extended: this.extended,
                auditions_enabled: this.auditions_enabled,
                rolesOpen: this.rolesOpen ? moment(this.rolesOpen) : null,
                rolesClosed: this.rolesClosed ? moment(this.rolesClosed) : null,
                editRegistrationClosed: this.editRegistrationClosed ? moment(this.editRegistrationClosed) : null,
                deleteRegistrationClosed: this.deleteRegistrationClosed ? moment(this.deleteRegistrationClosed) : null,
            });

            this.success = this.id ? `Project ${name} werd gewijzigd` : `Project ${name} werd aangemaakt`;
        },
        async delete(id) {
            try {
                this.loading = true;
                await request(`/api/project/${id}`, 'DELETE');
            } catch (e) {
                console.error(e);
            }
            this.loading = false;
        },
        // TODO: rename these functions
        async fetchUsersForProject(projectId) {
            const response = await request(`/api/project/${projectId}/users`);
            return response.data;
        },
        async fetchRolesForProject(projectId) {
            const response = await request(`/api/project/${projectId}/roles`);
            return response;
        },
        async fetchGroupsForProject(projectId) {
            const response = await request(`/api/project/${projectId}/groups`);
            return response.data;
        },
        async fetchScenesForProject(projectId) {
            const response = await request(`/api/project/${projectId}/scenes`);
            return response.data;
        },

        // TODO: try to move these functions
        async fetchPartsOfTown() {
            if (!this.partsOfTown?.length) {
                const partsOfTown = await request('/api/parts-of-town');
                partsOfTown.unshift({ id: '0', name: 'Ik woon niet in Geel' });
                this.partsOfTown = partsOfTown.map(({ id, name }) => ({ label: name, value: id }));
            }
        },
    });
}
