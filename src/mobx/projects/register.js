import React from 'react';
import form from '../shared/form';
import matenFiche from '../../assets/img/matenfiche.jpg';
import moment from 'moment';

export default function({ globalStore, request }) {
    return {
        error: '',
        success: '',
        data: {},
        get config() {
            let rolesOptions;

            const project = globalStore.projects.resource.item;

            if (this.data.location === 1) {
                rolesOptions = project.extended
                    ? globalStore.roles.onStageAutoComplete
                    : globalStore.roles.onStageAutoComplete.filter(({ for_extended }) => !for_extended);
            } else if (this.data.location === 0) {
                rolesOptions = project.extended
                    ? globalStore.roles.offStageAutoComplete
                    : globalStore.roles.offStageAutoComplete.filter(({ for_extended }) => !for_extended);
            }

            return {
                sections: [
                    {
                        id: 'general',
                        translationKey: 'form.sections.general',
                    },
                    {
                        id: 'preferred',
                        translationKey: 'form.sections.preferred',
                    },
                    {
                        id: 'other',
                        translationKey: 'form.sections.other',
                    },
                    {
                        id: 'clothing',
                        translationKey: 'form.sections.clothing',
                    },
                ],
                fields: [
                    {
                        id: 'location',
                        translationKey: 'form.labels.registration.location',
                        type: 'select',
                        section: 'general',
                        required: true,
                        options: [
                            moment(project.closedFrom).isAfter(moment()) && {
                                label: 'On-stage',
                                value: 1,
                            },
                            moment(project.closedOffStage).isAfter(moment()) && {
                                label: 'Off-stage',
                                value: 0,
                            },
                        ],
                    },
                    project.auditions_enabled &&
                        this.data.location && {
                            id: 'auditioning',
                            translationKey: 'form.labels.registration.auditioning',
                            section: 'general',
                            type: 'checkbox',
                        },
                    project.auditions_enabled &&
                        project.auditionHint &&
                        this.data.location && {
                            section: 'general',
                            component: () => (
                                <small style={{ marginLeft: 30, marginTop: -14, display: 'block' }}>
                                    ({project.auditionHint})
                                </small>
                            ),
                        },
                    this.data.location !== undefined && {
                        id: 'roles',
                        translationKey:
                            this.data.location === 0
                                ? 'form.labels.registration.offStagePreference'
                                : 'form.labels.registration.onStagePreference',
                        type: 'select',
                        multi: true,
                        section: 'preferred',
                        required: true,
                        options: rolesOptions,
                    },
                    this.data.location !== undefined && {
                        section: 'preferred',
                        component: () => (
                            <small style={{ marginTop: -14, display: 'block' }}>
                                Je kan meerdere voorkeuren selecteren
                                <br />
                                door de <strong>CTRL-toets</strong> op Windows of de <strong>Command-toets</strong> op Mac
                                <br />
                                ingedrukt te houden tijdens het selecteren
                            </small>
                        ),
                    },
                    process.env.REACT_APP_IS_FVD &&
                        project.extended && {
                            id: 'partOfTown',
                            label: 'Deeldorp',
                            required: true,
                            type: 'select',
                            section: 'other',
                            options: globalStore.projects.partsOfTown,
                        },
                    {
                        id: 'talents',
                        translationKey: 'form.labels.registration.talents',
                        type: 'text',
                        section: 'other',
                    },
                    {
                        id: 'remarks',
                        translationKey: 'form.labels.registration.remarks',
                        type: 'text',
                        section: 'other',
                    },
                    !globalStore.account.canEditClothingSizes && {
                        section: 'clothing',
                        component: () => (
                            <div
                                style={{
                                    background: '#ff9800',
                                    color: '#fff',
                                    padding: '10px 15px',
                                }}
                            >
                                Voorlopig kan je geen kleding-maten aanpassen. <br />
                                Wens je dit toch te doen stuur dan een mail naar {process.env.REACT_APP_EMAIL}
                            </div>
                        ),
                    },
                    this.data.location === 1 && {
                        id: 'height',
                        translationKey: 'form.labels.height',
                        type: 'number',
                        section: 'clothing',
                        required: globalStore.account.canEditClothingSizes,
                        disabled: !globalStore.account.canEditClothingSizes,
                    },
                    this.data.location === 1 && {
                        id: 'chest',
                        translationKey: 'form.labels.chest',
                        type: 'number',
                        section: 'clothing',
                        required: globalStore.account.canEditClothingSizes,
                        disabled: !globalStore.account.canEditClothingSizes,
                    },
                    this.data.location === 1 && {
                        id: 'waist',
                        translationKey: 'form.labels.waist',
                        type: 'number',
                        section: 'clothing',
                        required: globalStore.account.canEditClothingSizes,
                        disabled: !globalStore.account.canEditClothingSizes,
                    },
                    this.data.location === 1 && {
                        id: 'hipSize',
                        translationKey: 'form.labels.hipSize',
                        type: 'number',
                        section: 'clothing',
                        required: globalStore.account.canEditClothingSizes,
                        disabled: !globalStore.account.canEditClothingSizes,
                    },
                    this.data.location === 1 && {
                        id: 'headSize',
                        translationKey: 'form.labels.headSize',
                        type: 'number',
                        section: 'clothing',
                        required: globalStore.account.canEditClothingSizes,
                        disabled: !globalStore.account.canEditClothingSizes,
                    },
                    this.data.location === 1 && {
                        id: 'clothingSize',
                        translationKey: 'form.labels.clothingSize',
                        type: 'select',
                        section: 'clothing',
                        required: globalStore.account.canEditClothingSizes,
                        disabled: !globalStore.account.canEditClothingSizes,
                        options: globalStore.clothingSizes.autoComplete,
                    },
                    {
                        id: 'shirtSize',
                        translationKey: 'form.labels.shirtSize',
                        type: 'select',
                        section: 'clothing',
                        required: globalStore.account.canEditClothingSizes,
                        disabled: !globalStore.account.canEditClothingSizes,
                        options: globalStore.shirtSizes.autoComplete,
                    },
                    {
                        section: 'clothing',
                        component: () => (
                            <span
                                id="reference"
                                onClick={() => (globalStore.ui.imagePreview = { src: matenFiche })}
                                style={{
                                    cursor: 'pointer',
                                    textDecoration: 'underline',
                                    fontSize: '13px',
                                }}
                            >
                                Klik hier voor een referentiefiche voor het opmeten van de juiste maten.
                            </span>
                        ),
                    },
                ],
            };
        },
        get form() {
            return form(this.config, this.data);
        },
        async save(apiPrefix, body, userId) {
            body = { ...this.data };
            body.clothingSize &&
                (body.clothingSize = globalStore.clothingSizes.autoComplete.find(
                    ({ value }) => parseInt(value) === parseInt(body.clothingSize)
                )?.label);
            await request(`/api/project/${globalStore.projects.id}/user/${userId}`, 'POST', body);
            globalStore.users.resource.success = 'Medewerker werd succesvol ingeschreven';
            delete globalStore.users.selectedUser?.id;
        },
    };
}
