import { v4 as uuidv4 } from 'uuid';

export default function({ request, socket }) {
    return {
        to: null,
        onStage: true,
        offStage: false,
        appearing: false,
        copying: false,
        async copy(projectId) {
            this.copying = true;
            try {
                const uuid = uuidv4();
                this.uuid = uuid;
                await request(`/api/project/${projectId}/copy`, 'POST', { uuid });
            } catch (e) {
                console.error(e);
            }
            setTimeout(() => {
                this.copying = false;
                this.success = 'Project succesvol gedupliceerd';
                this.progress = 0;
            }, 1000);
        },
        async copyRegistrations(projectId) {
            this.copying = true;
            try {
                const uuid = uuidv4();
                this.uuid = uuid;
                await request(`/api/project/${projectId}/registrations/copy`, 'POST', {
                    uuid,
                    to: this.to,
                    onStage: this.onStage,
                    offStage: this.offStage,
                    appearing: this.appearing,
                });
            } catch (e) {
                console.error(e);
            }
            setTimeout(() => {
                this.copying = false;
                this.success = 'Inschrijvingen succesvol gekopiëerd';
                this.progress = 0;
            }, 1000);
        },
        progress: 0,
        uuid: '',
        text: '',
        listen() {
            const updateProgress = ({ uuid, progress, text }) => {
                if (uuid === this.uuid) {
                    this.progress = progress * 100;
                    this.text = Math.floor(progress * 100) + `% - ${text}`;
                }
            };

            socket.on('copy_project_progress', updateProgress);
            socket.on('copy_registrations_progress', updateProgress);

            return () => {};
        },
    };
}
