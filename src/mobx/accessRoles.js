import { observable } from 'mobx';
import resourceGenerator from './shared/resource';

export default function registrations(dependencies) {
    const resource = resourceGenerator(dependencies);

    const { request } = dependencies;

    return observable({
        rights: [],
        async fetchRights() {
            this.rights = ((await request(`/api/access_roles/rights`)) || {}).data;
        },
        async copy(id) {
            await request(`/api/access_role/${id}/copy`);
        },
        async fetchAccessRole(id) {
            const accessRole = await request(`/api/access_role/${id}`);

            this.rights = accessRole.rights || [];
            delete accessRole.rights;
            this.resource.form.data = accessRole;
        },
        async addToUser(userId, apiPrefix = '') {
            await request(`/api${apiPrefix}/user/${userId}/access_roles`, 'POST', this.selected);
        },
        resource: resource({
            single: 'access_role',
            plural: 'access_roles',
            labels: {
                single: 'toegangsrol',
                plural: 'toegangsrollen',
            },
            form: {
                sections: [
                    {
                        id: 'information',
                        title: 'Informatie',
                    },
                ],
                fields: [
                    {
                        id: 'name',
                        translationKey: 'form.labels.name',
                        type: 'text',
                        required: true,
                        validations: [{ type: 'length', min: 3, max: 255 }],
                        section: 'information',
                    },
                    {
                        id: 'description',
                        translationKey: 'form.labels.description',
                        type: 'text',
                        required: true,
                        validations: [{ type: 'length', min: 10, max: 255 }],
                        section: 'information',
                    },
                ],
            },
        }),
    });
}
