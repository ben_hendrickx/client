import React from 'react';
import { observable } from 'mobx';

export default function ui({ request, apiUrl }) {
    return observable({
        isMobile: window.innerWidth < 960,
        title: <>Welkom op ons inschrijfsysteem</>,
        subtitle: (
            <>
                Op dit moment zijn er geen projecten actief waarop ingeschreven kan worden.
                <br />
                <br />
                Tot binnenkort,
                <br />
                {process.env.REACT_APP_CLIENT}
            </>
        ),
        async fetch() {
            try {
                const { title, subtitle } = await request(`/api/general/info`);

                this.title = title || this.title;
                this.subtitle = subtitle || this.subtitle;
            } catch (e) {}
        },
    });
}
