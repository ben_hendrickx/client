import { observable } from 'mobx';

import resourceGenerator from './shared/resource';

export default function users(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request, globalStore } = dependencies;

    return observable({
        resource: resource({
            single: 'user',
            plural: 'users',
            labels: {
                single: 'medewerker',
                plural: 'medewerkers',
            },
            sort: 'lastName|ASC',
        }),
        get birthYearAutocomplete() {
            return this.resource.filtered
                .reduce((acc, user) => {
                    const birthYear = new Date(user.date_of_birth).getFullYear();

                    if (!acc.includes(birthYear)) {
                        acc.push(birthYear);
                    }

                    return acc;
                }, [])
                .sort((a, b) => a - b)
                .map(birthYear => ({
                    label: birthYear,
                    value: birthYear,
                }));
        },

        data: [],
        filter: '',
        name: '',
        filters: {
            sex: '',
            hasPicture: '',
            age: '',
            birthYear: '',
            linked: '',
            reset: function() {
                this.sex = '';
                this.hasPicture = '';
                this.minAge = '';
                this.maxAge = '';
                this.linked = '';
            },
            getString: function() {
                const data = [];

                if (this.sex) {
                    data.push(`sex=${this.sex}`);
                }

                if (this.minAge) {
                    data.push(`minAge=${this.minAge}`);
                }

                if (this.maxAge) {
                    data.push(`maxAge=${this.maxAge}`);
                }

                return data.join('&');
            },
        },
        sort: 'A-Z',
        set(data) {
            this.data = data;
        },
        get ageOptions() {
            return this.data
                .reduce((acc, { date_of_birth }) => {
                    const birthYear = new Date(date_of_birth).getFullYear();
                    const age = new Date().getFullYear() - birthYear;

                    if (!acc.find(({ value }) => value === age)) {
                        acc.push({ label: age, value: age });
                    }

                    return acc;
                }, [])
                .sort((a, b) => {
                    return a.value - b.value;
                });
        },
        get birthYearOptions() {
            return this.data
                .reduce((acc, { date_of_birth }) => {
                    const birthYear = new Date(date_of_birth).getFullYear();

                    if (!acc.find(({ value }) => value === birthYear)) {
                        acc.push({ label: birthYear, value: birthYear });
                    }

                    return acc;
                }, [])
                .sort((a, b) => {
                    return a.value - b.value;
                });
        },
        get filteredData() {
            const sortFunction = (a, b) => {
                const field = ['A-Z', 'Z-A'].includes(this.sort)
                    ? 'lastName'
                    : this.sort.toLowerCase().includes('leeftijd')
                    ? 'date_of_birth'
                    : 'creationDt';
                const ASC =
                    field === 'date_of_birth'
                        ? this.sort.includes('aflopend')
                        : this.sort === 'A-Z' || this.sort.includes('oplopend');

                const aField = field === 'date_of_birth' ? new Date(a[field]).getTime() : a[field].toLowerCase();
                const bField = field === 'date_of_birth' ? new Date(b[field]).getTime() : b[field].toLowerCase();

                if (aField < bField) {
                    return ASC ? -1 : 1;
                }

                if (aField > bField) {
                    return ASC ? 1 : -1;
                }

                if (field === 'firstName') {
                    if (a.lastName.toLowerCase() < b.lastName.toLowerCase()) {
                        return ASC ? -1 : 1;
                    }

                    if (a.lastName.toLowerCase() > b.lastName.toLowerCase()) {
                        return ASC ? 1 : -1;
                    }
                }

                return 0;
            };

            if (
                !this.filter &&
                !this.filters.sex &&
                !this.filters.hasPicture &&
                !this.filters.minAge &&
                !this.filters.maxAge &&
                !this.filters.linked
            ) {
                return this.data.slice().sort(sortFunction);
            }

            return this.data
                .filter(
                    ({
                        id: userId,
                        firstName,
                        lastName,
                        email,
                        sex,
                        image,
                        date_of_birth,
                        owner_remarks,
                        registration,
                    }) => {
                        const birthYear = new Date(date_of_birth).getFullYear();
                        const age = new Date().getFullYear() - birthYear;

                        if (this.filters.linked === 'Ja') {
                            if (!globalStore.groups.users.find(({ id }) => id === userId)) {
                                return false;
                            }
                        }

                        if (this.filters.linked === 'Neen') {
                            if (globalStore.groups.users.find(({ id }) => id === userId)) {
                                return false;
                            }
                        }

                        if (this.filters.sex !== '') {
                            if (!(this.filters.sex === 'Man' ? sex === 0 : sex === 1)) {
                                return false;
                            }
                        }

                        if (this.filters.hasPicture !== '') {
                            if (!(this.filters.hasPicture === 'Ja' ? !!image : !image)) {
                                return false;
                            }
                        }

                        if (this.filters.maxAge !== '') {
                            if (Number(this.filters.maxAge) < Number(age)) {
                                return false;
                            }
                        }

                        if (this.filters.minAge !== '') {
                            if (Number(this.filters.minAge) > Number(age)) {
                                return false;
                            }
                        }

                        if ((this.filter || '').toLowerCase().indexOf('opm:') === 0) {
                            return (
                                (owner_remarks || '')
                                    .toLowerCase()
                                    .includes(this.filter.toLowerCase().replace('opm:', '')) ||
                                ((registration && registration.owner_remarks) || '')
                                    .toLowerCase()
                                    .includes(this.filter.toLowerCase().replace('opm:', ''))
                            );
                        }

                        return [firstName, lastName, email]
                            .join(' ')
                            .toLowerCase()
                            .includes((this.filter || '').toLowerCase());
                    }
                )
                .sort(sortFunction);
        },
        async fetch() {
            this.loading = true;
            this.data = await request(`/api/users?level=0`);
            this.loading = false;
        },
        async fetchUser(id) {
            this.loading = true;
            const user = await request(`/api/user/${id}`);
            !user.sizes && (user.sizes = {}); //TODO: remove this
            this.selectedUser = user;
            this.loading = false;
        },
        async fetchUserForRegistration(registrationId) {
            this.loading = true;
            const user = await request(`/api/registration/${registrationId}/user`);
            !user.sizes && (user.sizes = {}); //TODO: remove this
            this.selectedUser = user;
            this.loading = false;
        },
        async fetchSizes(userId) {
            this.selectedUser.sizes = await request(`/api/user/${userId}/sizes`);
        },
        async fetchSettings(userId) {
            this.selectedUser.settings = await request(`/api/user/${userId}/settings`);
        },
        async registerMultiple(userIds) {
            await request(`/api/project/${this.projectIdToRegister}/users`, 'POST', userIds);
        },
        async register(projectId, userId) {
            this.loading = true;
            try {
                await request(`/api/project/${projectId}/user/${userId}`, 'POST', {
                    roles: this.roles,
                    auditioning: this.audition || false,
                    talents: this.talents || '',
                    remarks: this.remarks || '',
                    partOfTown: this.partOfTown || '',
                    height: this.height,
                    chest: this.chest,
                    waist: this.waist,
                    hipSize: this.hipSize,
                    headSize: this.headSize,
                    clothingSize: this.clothingSize,
                    shirtSize: this.tShirt,
                });
                this.loading = false;
            } catch (e) {
                this.setNotifications({ error: e });
                this.loading = false;
                throw e;
            }
        },
        async delete(userId) {
            await request(`/api/user/${userId}`, 'DELETE');
        },
    });
}
