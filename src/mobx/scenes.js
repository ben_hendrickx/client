import { observable } from 'mobx';

import resourceGenerator from './shared/resource';

export default function countries(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request, axios, apiUrl, globalStore } = dependencies;

    return observable({
        resource: resource({
            single: 'scene',
            plural: 'scenes',
            labels: {
                single: 'scène',
                plural: 'scènes',
            },
            filters: {},
        }),
        groups: {
            resource: resource({
                single: 'group',
                plural: 'groups',
                labels: {
                    single: 'scènegroep',
                    plural: 'groepen van scènes',
                },
                filters: {},
            }),
        },

        loading: false,
        sceneGroups: [],
        //groups: [],
        roles: [],
        users: [],
        get groupsAutocomplete() {
            return this.sceneGroups.map(({ id, name }) => ({ label: name, value: id }));
        },
        get autoComplete() {
            return this.resource.data
                .filter(v => v)
                .map(({ id, name, identifier }) => ({ label: `${identifier} - ${name}`, value: id }));
        },
        async fetchGroups(projectId) {
            this.loading = true;
            this.sceneGroups = ((await request(`/api/project/${projectId}/scenes/groups`)) || []).sort((a, b) => {
                return a.identifier - b.identifier;
            });
            this.loading = false;
        },
        async fetchScene(id) {
            try {
                this.loading = true;
                const scene = await request(`/api/scene/${id}`);
                this.id = scene.id;
                this.name = scene.name;
                this.description = scene.description;
                this.identifier = scene.identifier;
                this.groupId = scene.groupId;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async fetchSceneGroup(id) {
            try {
                this.loading = true;
                const scene = await request(`/api/scenes/group/${id}`);
                this.id = scene.id;
                this.name = scene.name;
                this.description = scene.description;
                this.identifier = scene.identifier;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        reset() {
            delete this.id;
            delete this.name;
            delete this.description;
            delete this.identifier;
            delete this.groupId;
            this.sceneGroups = [];
            this.roles = [];
            this.users = [];
        },
        async fetchGroupsForScene(sceneId) {
            try {
                this.loading = true;
                this.sceneGroups = (await axios.request({
                    method: 'GET',
                    url: `${apiUrl}/api/scene/${sceneId}/groups`,
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                })).data;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async fetchRolesForScene(sceneId) {
            try {
                this.loading = true;
                this.roles = (await axios.request({
                    method: 'GET',
                    url: `${apiUrl}/api/scene/${sceneId}/roles`,
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                })).data;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async deleteGroupFromScene(groupId) {
            try {
                this.loading = true;
                await axios.request({
                    method: 'DELETE',
                    url: `${apiUrl}/api/scene/${this.id}/groups`,
                    data: {
                        groupId,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                await this.fetchGroupsForScene(this.id);
                this.loading = false;
                this.success = 'Groep werd succesvol verwijderd';
            } catch (e) {
                console.error(e);
            }
        },
        async delete(sceneId) {
            try {
                this.loading = true;
                await axios.request({
                    method: 'DELETE',
                    url: `${apiUrl}/api/scene/${sceneId}`,
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async deleteRoleFromScene(roleId) {
            try {
                this.loading = true;
                await axios.request({
                    method: 'DELETE',
                    url: `${apiUrl}/api/scene/${this.id}/roles`,
                    data: {
                        roleId,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                await this.fetchRolesForScene(this.id);
                this.loading = false;
                this.success = 'Rol werd succesvol verwijderd';
            } catch (e) {
                console.error(e);
            }
        },
        async save(group) {
            const { name, description, identifier } = this;

            const method = this.id ? 'PUT' : 'POST';
            let url = `/api/scenes`;
            group && (url += '/group');
            url += `${this.id ? `/${this.id}` : ''}`;
            await request(url, method, {
                name,
                description,
                identifier,
                projectId: globalStore.projects.id,
                groupId: this.groupId || 0,
            });

            if (group) {
                this.resource.success = this.id
                    ? `Scène-groep ${name} werd gewijzigd`
                    : `Scène-groep ${name} werd aangemaakt`;
            } else {
                this.resource.success = this.id ? `Scène ${name} werd gewijzigd` : `Scène ${name} werd aangemaakt`;
            }

            this.resource.data = [];
            this.resource.status = [];
            await this.resource.fetch(`/project/${globalStore.projects.id}`);
        },
        async saveGroups() {
            try {
                this.loading = true;
                await axios.request({
                    method: 'POST',
                    url: `${apiUrl}/api/scene/${this.id}/groups/add`,
                    data: {
                        groupId: this.groupId,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                await this.fetchGroupsForScene(this.id);
                this.loading = false;
                this.success = 'Groep werd succesvol toegevoegd';
            } catch (e) {
                console.error(e);
            }
        },
        async saveRoles() {
            try {
                this.loading = true;
                await axios.request({
                    method: 'POST',
                    url: `${apiUrl}/api/scene/${this.id}/roles/add`,
                    data: {
                        roleId: this.roleId,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                await this.fetchRolesForScene(this.id);
                this.loading = false;
                this.success = 'Rol werd succesvol toegevoegd';
            } catch (e) {
                console.error(e);
            }
        },
    });
}
