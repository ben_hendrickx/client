import { observable } from 'mobx';
import resourceGenerator from './shared/resource';

export default function casts(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request, axios, globalStore, apiUrl } = dependencies;
    return observable({
        resource: resource({
            single: 'cast',
            plural: 'casts',
            labels: {
                single: 'cast',
                plural: 'casts',
            },
            form: {
                sections: [
                    {
                        id: 'information',
                    }
                ],
                fields: [
                    {
                        id: 'name',
                        translationKey: 'form.labels.name',
                        type: 'text',
                        required: true,
                        validations: [{ type: 'length', min: 3, max: 30 }],
                        section: 'information',
                        //TODO: add support for hint property
                    },
                ],
            },
        }),
        async fetchCast(id) {
            try {
                this.loading = true;
                const cast = await request(`/api/cast/${id}`);
                this.resource.form.data = cast;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        checked: [],

        data: [],
        loading: false,
        users: {
            data: [],
            loading: false,
        },
        async fetch() {
            if (this.data?.length) {
                return this.data;
            }

            this.loading = true;
            const response = await request('/api/casts');
            const data = response.data.sort((a, b) => {
                if (a.name < b.name) {
                    return -1;
                }
                if (a.name > b.name) {
                    return 1;
                }
                return 0;
            });
            this.data = data;
            this.loading = false;
            return data;
        },
        async save() {
            const { name } = this;

            const method = this.id ? 'PUT' : 'POST';
            const url = `${apiUrl}/api/casts${this.id ? `/${this.id}` : ''}`;
            await axios.request({
                method,
                url,
                data: {
                    name,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${globalStore.account.token}`,
                },
            });

            this.success = this.id ? `Cast ${name} werd gewijzigd` : `Cast ${name} werd aangemaakt`;

            await this.fetch();
        },
        get filteredData() {
            if (!this.filter) {
                return this.data;
            }

            return this.data.filter(v => v).filter(({ name }) => name.toLowerCase().includes(this.filter.toLowerCase()));
        },
    });
}
