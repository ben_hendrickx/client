import { observable } from 'mobx';

import resourceGenerator from './shared/resource';

export default function countries(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request, globalStore } = dependencies;

    return observable({
        resource: resource({
            single: 'project_role',
            plural: 'project_roles',
            labels: {
                single: 'rol',
                plural: 'rollen',
            },
            sort: 'identifier|ASC',
            form: {
                sections: [
                    {
                        id: 'information',
                    },
                ],
                fields: [
                    {
                        id: 'identifier',
                        translationKey: 'form.labels.identifier',
                        required: true,
                        type: 'text',
                        section: 'information',
                    },
                    {
                        id: 'name',
                        translationKey: 'form.labels.roles.name',
                        required: true,
                        type: 'text',
                        section: 'information',
                    },
                    {
                        id: 'description',
                        translationKey: 'form.labels.description',
                        type: 'text',
                        section: 'information',
                    },
                    {
                        id: 'speaking',
                        translationKey: 'form.labels.roles.speaking',
                        type: 'checkbox',
                        section: 'information',
                    },
                ],
            },
        }),
        async saveUsers() {
            await request(
                `/api/project/${globalStore.projects.id}/project_role/${this.id}/users`,
                'PUT',
                globalStore.casts.checked
            );
        },
        async fetchUsers(roleId) {
            const users = await request(`/api/project/${globalStore.projects.id}/project_role/${roleId}/users`);
            globalStore.casts.checked = users;
            return !!users.length;
        },

        loading: false,
        data: [],
        selectedRoles: [],
        selectedGroups: [],
        async switchCast() {
            return await request(`/api/project_roles/switch`, 'POST', {
                userId: globalStore.users.selectedUser.id,
                castId: this.selectedCast,
                roleIds: this.selectedRoles,
                groupIds: this.selectedGroups,
            });
        },
        get autoComplete() {
            return this.resource.data
                .filter(v => v)
                .map(({ id, name, identifier }) => ({ label: `${identifier} - ${name}`, value: id }));
        },
        async fetch(projectId, userId, past = true, open = false) {
            this.data = [];
            this.loading = true;

            const { data } = await request(
                `/api${projectId ? `/project/${projectId}` : ''}${
                    userId ? `/user/${userId}` : ''
                }/project_roles?past=${past}&open=${open}`
            );

            this.data = data;

            this.loading = false;
        },
        async fetchRole(id) {
            try {
                this.loading = true;
                const role = await request(`/api/project_role/${id}`);
                this.id = role.id;
                this.name = role.name;
                this.description = role.description;
                this.speaking = role.speaking;
                this.identifier = role.identifier;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        reset() {
            delete this.id;
            delete this.name;
            delete this.description;
            delete this.speaking;
            delete this.identifier;
            delete this.data;
        },
        async delete(roleId) {
            try {
                this.loading = true;
                await request(`/api/project_role/${this.id}`, 'DELETE');
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async save() {
            const { name, description = '', speaking, identifier } = this;

            const method = this.id ? 'PUT' : 'POST';
            const url = `/api/project_roles${this.id ? `/${this.id}` : ''}`;
            await request(url, method, {
                name,
                description,
                speaking: speaking || false,
                identifier,
                projectId: globalStore.projects.id,
            });

            this.success = this.id ? `Rol ${name} werd gewijzigd` : `Rol ${name} werd aangemaakt`;

            await this.fetch();
        },
        get filteredData() {
            const filter = this.filter;
            const scene = this.filters.scene;

            if (!filter && !scene) {
                return this.data;
            }

            return this.data
                .filter(v => v)
                .filter(({ name, identifier, scenes = [] }) => {
                    if (filter) {
                        if (
                            !name.toLowerCase().includes(filter.toLowerCase()) &&
                            !identifier.toLowerCase().includes(filter.toLowerCase())
                        ) {
                            return false;
                        }
                    }

                    if (scene) {
                        if (!scenes.includes(parseInt(scene))) {
                            return false;
                        }
                    }

                    return true;
                });
        },
        filters: {
            scene: '',
        },
        filter: '',
    });
}
