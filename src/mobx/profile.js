/* eslint-disable no-console */
import { observable } from 'mobx';
import moment from 'moment';

export default function profile({ axios, globalStore, apiUrl, request }) {
    return observable({
        async create() {
            try {
                const { userId } = await request('/api/profile', 'POST', {
                    email: globalStore.account.email,
                    firstName: this.firstName,
                    lastName: this.lastName,
                    date_of_birth: moment(this.dateOfBirth).format('YYYY-MM-DD'),
                    nationality: this.origin,
                    origin: this.countryOfBirth,
                    sex: this.sex,
                    phoneNumber: this.phone,
                    cellPhone: this.cell,
                    secondaryEmail: this.secondaryMail,
                    tShirt: this.tShirt,
                    streetName: this.streetName,
                    houseNumber: this.houseNumber,
                    cityName: this.cityName,
                    zipCode: this.zipCode,
                    image: this.image,
                    privacy: this.privacy,
                    pictures: this.pictures || false,
                });

                this.error = null;
                return userId;
            } catch (e) {
                this.error = typeof e.response === 'object' && e.response.data ? e.response.data : e.message;
            }
        },
        reset() {
            delete this.firstName;
            delete this.lastName;
            delete this.dateOfBirth;
            delete this.origin;
            delete this.countryOfBirth;
            delete this.sex;
            delete this.phone;
            delete this.cell;
            delete this.secondaryMail;
            delete this.tShirt;
            delete this.streetName;
            delete this.houseNumber;
            delete this.cityName;
            delete this.zipCode;
            delete this.image;
            delete this.privacy;
            delete this.pictures;
        },
    });
}
