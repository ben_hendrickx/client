import { observable } from 'mobx';

export default function(form, data = {}) {
    return observable({
        data,
        ...form,
        reset() {
            Object.keys(this.data).forEach(key => {
                delete this.data[key];
            });
        },
        get invalidFields() {
            return this.fields
                .filter(field => field)
                .filter(field => {
                    if (!field.required || field.component) {
                        return false;
                    }

                    if (field.type === 'address') {
                        return (
                            !this.data.streetName || !this.data.houseNumber || !this.data.zipCode || !this.data.cityName
                        );
                    }

                    if (field.type === 'select' && field.multi) {
                        return !this.data[field.id] || this.data[field.id].length <= 0;
                    }

                    return ['', undefined, null].includes(this.data[field.id]);
                });
        },
        get isValid() {
            return this.invalidFields.length <= 0;
        },
        get invalidMessage() {
            return `Gelieve de verplichte velden in te vullen (${this.invalidFields
                .map(({ label }) => label)
                .join(', ')})`;
        },
    });
}
