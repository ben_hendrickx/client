import { observable } from 'mobx';
import moment from 'moment';
import formStore from './form';
import dateService from '../../services/date';

function getNestedValues(object) {
    const values = Object.values(object).reduce((acc, value) => {
        // Ignore filtering base64 strings
        if (typeof value === 'string' && value.startsWith('data:')) {
            return acc;
        }

        if ([null, undefined].includes(value)) {
            return acc;
        }

        if (typeof value !== 'object') {
            return acc.concat([(value || '').toString().toLowerCase()]);
        }

        if (Array.isArray(value)) {
            return acc.concat(value.map(value => value.toString().toLowerCase()));
        }

        return acc.concat(getNestedValues(value).map(value => value.toString().toLowerCase()));
    }, []);

    return values;
}

export default function({ request, globalStore, apiUrl }) {
    return function(config = {}) {
        const { single, plural, labels, filters, form, findIn, sort } = config;

        return observable({
            findIn,

            reset(keepFilters) {
                !keepFilters &&
                    (this.filters = {
                        ...filters,
                    });
                this.data = [];
                this.status = [];
                this.total = 0;
                this.success = '';
                this.loading = false;
                this.offset = 0;
                this.batchSize = 10;
                this.item = {};
                this.checked = [];
                this.form.reset();
            },

            resource: {
                get single() {
                    return labels.single;
                },
                get singleUpperCased() {
                    return labels.single[0].toUpperCase() + labels.single.slice(1);
                },
                get plural() {
                    return labels.plural;
                },
            },

            loading: false,
            offset: 0,
            batchSize: 10,
            total: 0,
            data: [],
            status: [],
            item: {},
            checked: [],
            filterValues: {},
            async fetch(apiPrefix = '', setLoading = true) {
                setLoading && (this.loading = true);
                const { data, total, offset = 0, items, filters } = await this.fetchAndReturn(apiPrefix);
                this.data.length = items !== undefined ? items : data.length;
                this.status.length = items !== undefined ? items : data.length;
                filters && (this.filterValues = filters);

                for (let i = 0; i < data.length; i++) {
                    this.data[offset + i] = data[i];
                    this.status[offset + i] = 'loaded';
                }

                this.items = items;
                this.total = total;
                setLoading && (this.loading = false);
            },
            async fetchAndReturn(apiPrefix = '') {
                return await request(`/api${apiPrefix}/${plural}${this.filterQuery}`);
            },
            async save(apiPrefix = '', body = this.form.data) {
                const data = Object.entries(body).reduce((acc, [key, value]) => {
                    switch (typeof value) {
                        case 'string':
                            if (!['houseNumber', 'zipCode'].includes(key) && dateService().isDate(value)) {
                                acc[key] = moment(value); //dateService().getDate(value).format('YYYY-MM-DD HH:mm:00');
                            } else {
                                acc[key] = value;
                            }
                            break;
                        // case 'object':
                        //     if (moment.isMoment(value)) {
                        //         acc[key] = dateService().getDate(value).format('YYYY-MM-DD HH:mm:00');
                        //     } else {
                        //         acc[key] = value;
                        //     }
                        //     break;
                        case 'object':
                            if (key === 'files') {
                                Object.entries(value).forEach(([_, value]) => {
                                    globalStore.general.uploadFile(value);
                                });
                                acc[key] = Object.values(value).map(f => f.name);
                            } else {
                                acc[key] = value;
                            }
                            break;
                        default:
                            acc[key] = value;
                            break;
                    }
                    return acc;
                }, {});

                const url = body.id ? `/api${apiPrefix}/${single}/${body.id}` : `/api${apiPrefix}/${plural}`;
                const method = body.id ? 'PUT' : 'POST';
                const action = body.id ? 'gewijzigd' : 'aangemaakt';

                await request(url, method, data);
                this.success = `${this.resource.singleUpperCased} werd succesvol ${action}`;
            },
            async delete(apiPrefix = '', toDelete, deleteOrArchive = 'verwijderd') {
                if (Array.isArray(toDelete)) {
                    await this.deleteChecked(apiPrefix, toDelete);
                } else {
                    await request(`/api${apiPrefix}/${single}/${toDelete}`, 'DELETE');
                    this.success = `${this.resource.singleUpperCased} werd succesvol ${deleteOrArchive}`;
                }
            },
            // This function should be used when there is more than one attribute to search for the to be deleted records
            // eg. { userId: x, projectId: x} to only delete users from a specific project
            async deleteWithConfig(apiPrefix = '', config = {}) {
                await request(`/api${apiPrefix}/${plural}`, 'DELETE', config);
            },

            // This function receives an array of id's to delete
            // TODO: check if toDelete property is really necessary
            // TODO: check if this function is being used?
            async deleteChecked(apiPrefix = '', toDelete, deleteOrArchive = 'verwijderd') {
                await this.deleteWithConfig(apiPrefix, toDelete);
                if (this.checked.length === 1) {
                    this.success = `${this.resource.singleUpperCased} werd succesvol verwijderd`;
                } else {
                    this.success = `${this.checked.length} ${this.resource.plural} werden succesvol ${deleteOrArchive}`;
                }
            },

            sort,
            filters: {
                ...filters,
            },
            get isFiltering() {
                return Object.values(this.filters).some(v => {
                    return Array.isArray(v) ? !!v.length : ![null, undefined, ''].includes(v);
                });
            },
            get filterParams() {
                return this.isFiltering
                    ? Object.entries(this.filters).filter(([key, value]) =>
                          Array.isArray(value)
                              ? !!value.length
                              : key !== 'plain' && !['', undefined, null].includes(value)
                      )
                    : [];
            },
            get exportUrl() {
                return `${apiUrl}/api/reports/${plural}.xlsx`;
            },
            get exportUrlWithFilters() {
                const params = this.filterParams;

                this.sort && params.push(['sort', this.sort]);
                params.push(['token', globalStore.account.token]);

                return `${this.exportUrl}?${params.map(([key, value]) => `${key}=${value}`).join('&')}`;
            },
            get exportUrlWithoutFilters() {
                return `${this.exportUrl}?token=${globalStore.account.token}`;
            },
            get filterQuery() {
                const params = this.filterParams;

                this.sort && params.push(['sort', this.sort]);
                this.offset !== undefined && params.push(['offset', this.offset]);
                this.batchSize !== undefined && params.push(['items', this.batchSize]);

                return `${params.length ? '?' : ''}${params.map(([key, value]) => `${key}=${value}`).join('&')}`;
            },
            get filtered() {
                let data = this.data.filter(v => !!v);

                if (!this.isFiltering || !this.filters.plain) {
                    return data;
                }

                const filters = this.filters.plain?.toLowerCase().split(' ');

                return data.filter(item => {
                    const values = getNestedValues(this.findIn ? item[this.findIn] : item);
                    return filters.every(filterPart => {
                        return values.some(v => v.includes(filterPart));
                    });
                });
            },

            form: formStore(form),
        });
    };
}
