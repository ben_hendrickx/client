/* istanbul ignore file */
import React from 'react';
import { observable } from 'mobx';

import resourceGenerator from '../shared/resource';
import form from '../shared/form';
import { secondaryColor } from 'assets/jss/material-kit-react';

export default function projectUsers(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { globalStore, request } = dependencies;

    return observable({
        selectedRows: [],

        resource: resource({
            single: 'project_user',
            plural: 'project_users',
            labels: {
                single: 'deelnemer',
                plural: 'deelnemers',
            },
        }),
        transfer: {
            resource: resource({
                single: 'transfer',
                plural: 'transfer',
                labels: {
                    single: 'deelnemer',
                    plural: 'deelnemers',
                },
            }),
            async save(projectId) {
                const body = this.resource.checked
                    .map(v => v.split('|').map(v => (v ? parseInt(v) : v)))
                    .reduce((acc, [sceneId, roleId, groupId, castId, userId]) => {
                        acc.push({
                            sceneId,
                            roleId,
                            groupId,
                            castId,
                            userId,
                        });
                        return acc;
                    }, []);

                await request(`/api/project/${projectId}/transfer`, 'POST', body);
            },
        },
        articles: {
            data: {},
            item: {},
            items: [],
            get form() {
                return form(this.config, this.data);
            },
            get config() {
                return {
                    sections: [
                        {
                            id: 'general',
                        },
                        {
                            id: 'remarks',
                        },
                    ],
                    fields: [
                        {
                            id: 'articleId',
                            label: 'Artikel-nummer',
                            type: 'text',
                            section: 'general',
                            onChange: () => delete this.data.statusId,
                        },
                        this.data.description && {
                            section: 'general',
                            component: () => (
                                <small
                                    style={{
                                        display: 'block',
                                        marginTop: -17,
                                        fontSize: '15px',
                                        background: '#fdc300',
                                        padding: '5px 10px',
                                        color: 'white',
                                        borderRadius: '0 0 3px 3px',
                                    }}
                                >
                                    <b>{this.data.description}</b>
                                </small>
                            ),
                        },
                        {
                            section: 'general',
                            component: () => (
                                <div
                                    style={{
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                        marginTop: 30,
                                    }}
                                >
                                    <div
                                        style={{
                                            height: 0,
                                            borderTop: `1px solid ${secondaryColor}`,
                                            width: 'calc(50% - 20px)',
                                        }}
                                    ></div>
                                    <div style={{ padding: '0 10px', color: secondaryColor }}>OF</div>
                                    <div
                                        style={{
                                            height: 0,
                                            borderTop: `1px solid ${secondaryColor}`,
                                            width: 'calc(50% - 20px)',
                                        }}
                                    ></div>
                                </div>
                            ),
                        },
                        {
                            id: 'statusId',
                            label: 'Status',
                            type: 'select',
                            section: 'general',
                            options: [
                                { label: 'Nog knippen', value: 1 },
                                { label: 'Doorgegeven om te knippen', value: 2 },
                                { label: 'Geknipt', value: 3 },
                                { label: 'In herstelling', value: 4 },
                                { label: 'Aan te passen', value: 5 },
                                { label: 'Eigen kledij', value: 5 },
                            ],
                            onChange: () => delete this.data.articleId,
                        },
                        {
                            id: 'remarks',
                            label: 'Opmerkingen',
                            type: 'text',
                            section: 'remarks',
                        },
                    ],
                };
            },
            async getOtherScenes(projectUser, articleUuid) {
                //TODO: ACCEPT TYPE
                let url = `/api/project_user/${projectUser.uuid}/scenes`;
                if (articleUuid) {
                    url += `?articleUuid=${articleUuid}`;
                }
                const otherScenes = await request(url);

                if (!otherScenes.length) {
                    return [];
                }

                return otherScenes;
            },
            async delete(projectUser, articleUuid, callback = () => {}) {
                await request(`/api/project_user/${projectUser.uuid}/article/${articleUuid}`, 'DELETE', {
                    projectUserUuids: this.data.other,
                });
                callback();
            },
            async add(projectUser, callback = () => {}) {
                const { success, title, message, data = {} } = await request(
                    `/api/project_user/${projectUser.uuid}/articles`,
                    'POST',
                    { ...projectUser, ...this.data }
                );

                if (!success) {
                    globalStore.general.addModal({
                        title,
                        body: message,
                        onConfirm: async () => {
                            this.data.force = true;
                            await request(`/api/project_user/${projectUser.uuid}/articles`, 'POST', {
                                ...projectUser,
                                ...data,
                                ...this.data,
                            });
                            this.data = {};
                            callback();
                        },
                    });
                } else {
                    this.data = {};
                    callback();
                }
            },
            resource: resource({
                single: 'article',
                plural: 'articles',
                labels: {
                    single: 'artikel',
                    plural: 'artikels',
                },
            }),
        },
        async delete(indexes = []) {
            await Promise.all(
                indexes.map(index => {
                    const { projectId, sceneId, roleId, groupId, castId, userId } = this.resource.filtered[index];
                    return this.resource.deleteWithConfig(undefined, {
                        projectId,
                        sceneId,
                        roleId,
                        groupId,
                        castId,
                        userId,
                    });
                })
            );

            if (indexes.length === 1) {
                this.resource.success = 'Deelnemer werd succesvol verwijderd';
            } else {
                this.resource.success = `${indexes.length} deelnemers werden succesvol verwijderd`;
            }

            this.resource.data = [];
            this.resource.status = [];
            this.resource.fetch();
            this.resource.checked = [];
        },
        async fit(id) {
            await request(`/api/project_user/${id}`, 'PUT', { status: 1 });
        },
        async ready(id) {
            await request(`/api/project_user/${id}`, 'PUT', { status: 2 });
        },
    });
}
