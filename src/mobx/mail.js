/* istanbul ignore file */
import { observable } from 'mobx';

export default function general({ request }) {
    return observable({
        loading: true,
        templates: [],
        async getTemplates() {
            this.loading = true;
            this.templates = (await request('/api/mail/templates')).templates;
            this.loading = false;
        },
        async getTemplate() {
            this.html = (await request(`/api/mail/template/${this.template}`)).template;
        },
        async send(addresses) {
            this.loading = true;
            await request('/api/mail/send', 'POST', { addresses, html: this.html, subject: this.subject });
            this.loading = false;
            this.success = addresses.length > 1 ? 'Mails werden succesvol verzonden' : 'Mail werd succesvol verzonden';
            this.html = '';
            this.template = '';
            this.email = '';
        },
    });
}
