import { observable } from 'mobx';

export default function statistics({ request }) {
    return observable({
        loading: false,
        online: [],
        async getOnlineUsers() {
            this.loading = true;
            this.online = await request('/api/statistics/users/online');
            this.loading = false;
        },
    });
}
