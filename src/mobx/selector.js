import { observable } from 'mobx';

export default function general({ globalStore, request }) {
    return observable({
        casts: [],
        async fetchCasts() {
            const casts = await globalStore.casts.fetch();
            this.casts = casts.sort((a, b) => {
                if (a.name < b.name) {
                    return -1;
                }
                if (a.name > b.name) {
                    return 1;
                }
                return 0;
            });
        },
        users: [],
        checkedUsers: [],
        async fetchUsers(projectId) {
            const users = await globalStore.projects.fetchUsersForProject(projectId);

            this.users = users.sort((a, b) => {
                if (a.lastName < b.lastName) {
                    return -1;
                }
                if (a.lastName > b.lastName) {
                    return 1;
                }
                return 0;
            });
        },

        roles: [],
        checkedRoles: [],
        async fetchRoles(projectId) {
            const casts = await globalStore.casts.fetch();
            const roles = await globalStore.projects.fetchRolesForProject(projectId);

            this.roles = roles
                .sort((a, b) => {
                    if (a.identifier < b.identifier) {
                        return -1;
                    }
                    if (a.identifier > b.identifier) {
                        return 1;
                    }
                    return 0;
                })
                .map(role => ({ ...role, casts: [...casts] }));
        },

        groups: [],
        checkedGroups: [],
        async fetchGroups(projectId) {
            const casts = await globalStore.casts.fetch();
            const groups = await globalStore.projects.fetchGroupsForProject(projectId);

            this.groups = groups
                .sort((a, b) => {
                    if (a.identifier < b.identifier) {
                        return -1;
                    }
                    if (a.identifier > b.identifier) {
                        return 1;
                    }
                    return 0;
                })
                .map(group => ({ ...group, casts: [...casts] }));
        },

        scenes: [],
        checkedScenes: [],
        async fetchScenes(projectId) {
            const casts = await globalStore.casts.fetch();
            let scenes = await globalStore.projects.fetchScenesForProject(projectId);

            scenes = scenes.reduce((acc, scene) => {
                if (!scene.scenes) {
                    acc.push(scene);
                } else {
                    acc = [...acc, ...scene.scenes];
                }
                return acc;
            }, []);

            this.scenes = scenes
                .sort((a, b) => {
                    if (a.identifier < b.identifier) {
                        return -1;
                    }
                    if (a.identifier > b.identifier) {
                        return 1;
                    }
                    return 0;
                })
                .map(scene => ({ ...scene, casts: [...casts] }));
        },

        sceneRoles: [],
        async fetchSceneRoles() {
            const response = await request('/api/scene_roles');
            this.sceneRoles = response.data;
            return response.data;
        },

        sceneGroups: [],
        async fetchSceneGroups() {
            const response = await request('/api/scene_groups');
            this.sceneGroups = response.data;
            return response.data;
        },

        checkedSceneRoles: [],
        getProjectRolesForScene(sceneId) {
            const roleIdsForScene = this.sceneRoles
                .filter(sceneRole => sceneRole.sceneId === sceneId)
                .map(sceneRole => sceneRole.project_role_id);
            return this.roles.filter(role => roleIdsForScene.includes(role.id));
        },

        checkedSceneGroups: [],
        getGroupsForScene(sceneId) {
            const groupIdsForScene = this.sceneGroups
                .filter(sceneGroup => sceneGroup.sceneId === sceneId)
                .map(sceneRole => sceneRole.groupId);
            return this.groups.filter(group => groupIdsForScene.includes(group.id));
        },

        get uniqueCheckedScenes() {
            return [...this.checkedScenes, ...this.checkedSceneRoles, ...this.checkedSceneGroups].reduce((acc, row) => {
                row.sceneId && !acc.includes(row.sceneId) && acc.push(row.sceneId);
                return acc;
            }, []).length;
        },

        get uniqueCheckedSceneRoles() {
            const extraRoles = this.checkedScenes.reduce((acc, checkedScene) => {
                acc = [
                    ...acc,
                    ...this.getProjectRolesForScene(checkedScene.sceneId).map(role => ({ roleId: role.id })),
                ];
                return acc;
            }, []);
            return [...extraRoles, ...this.checkedScenes, ...this.checkedSceneRoles, ...this.checkedSceneGroups].reduce(
                (acc, row) => {
                    row.roleId && !acc.includes(row.roleId) && acc.push(row.roleId);
                    return acc;
                },
                []
            ).length;
        },

        get uniqueCheckedSceneGroups() {
            const extraGroups = this.checkedScenes.reduce((acc, checkedScene) => {
                acc = [...acc, ...this.getGroupsForScene(checkedScene.sceneId).map(group => ({ groupId: group.id }))];
                return acc;
            }, []);
            return [
                ...extraGroups,
                ...this.checkedScenes,
                ...this.checkedSceneRoles,
                ...this.checkedSceneGroups,
            ].reduce((acc, row) => {
                row.groupId && !acc.includes(row.groupId) && acc.push(row.groupId);
                return acc;
            }, []).length;
        },

        get uniqueCheckedCasts() {
            return [...this.checkedScenes, ...this.checkedSceneRoles, ...this.checkedSceneGroups].reduce((acc, row) => {
                row.castId && !acc.includes(row.castId) && acc.push(row.castId);
                return acc;
            }, []).length;
        },

        calculate(data) {
            const { checked, sceneId, roleId, groupId, castId } = data;

            //Check all
            if (!sceneId && !roleId && !groupId && !castId) {
                this.checkedSceneRoles = [];
                this.checkedSceneGroups = [];
                this.checkedScenes = [];
                if (checked) {
                    this.casts.forEach(cast =>
                        this.scenes.forEach(scene => this.checkedScenes.push({ sceneId: scene.id, castId: cast.id }))
                    );
                }
            }

            //Check cast
            if (castId && !sceneId && !roleId && !groupId) {
                this.checkedSceneRoles = this.checkedSceneRoles.filter(
                    checkedSceneRole => checkedSceneRole.castId !== castId
                );
                this.checkedSceneGroups = this.checkedSceneGroups.filter(
                    checkedSceneGroup => checkedSceneGroup.castId !== castId
                );
                this.checkedScenes = this.checkedScenes.filter(checkedScene => checkedScene.castId !== castId);
                if (checked) {
                    this.scenes.forEach(scene => this.checkedScenes.push({ sceneId: scene.id, castId }));
                }
            }

            //Main click on scene
            if (sceneId && !roleId && !groupId && !castId) {
                this.checkedSceneRoles = this.checkedSceneRoles.filter(
                    checkedSceneRole => checkedSceneRole.sceneId !== sceneId
                );
                this.checkedSceneGroups = this.checkedSceneGroups.filter(
                    checkedSceneGroup => checkedSceneGroup.sceneId !== sceneId
                );
                this.checkedScenes = this.checkedScenes.filter(checkedScene => checkedScene.sceneId !== sceneId);
                //Checked, so check all casts and all underlying groups and roles
                if (checked) {
                    this.casts.forEach(cast => this.checkedScenes.push({ sceneId, castId: cast.id }));
                }
            }

            //Click on scene cast
            if (sceneId && castId && !roleId && !groupId) {
                this.checkedSceneRoles = this.checkedSceneRoles.filter(
                    checkedSceneRole => checkedSceneRole.sceneId !== sceneId || checkedSceneRole.castId !== castId
                );
                this.checkedSceneGroups = this.checkedSceneGroups.filter(
                    checkedSceneGroup => checkedSceneGroup.sceneId !== sceneId || checkedSceneGroup.castId !== castId
                );
                this.checkedScenes = this.checkedScenes.filter(
                    checkedScene => checkedScene.sceneId !== sceneId || checkedScene.castId !== castId
                );
                if (checked) {
                    this.checkedScenes.push({ sceneId, castId });
                }
            }

            //Main click on role
            if (sceneId && roleId && !groupId && !castId) {
                this.checkedSceneRoles = this.checkedSceneRoles.filter(
                    checkedSceneRole => checkedSceneRole.sceneId !== sceneId || checkedSceneRole.roleId !== roleId
                );
                //Checked, so check all casts from this role
                if (checked) {
                    this.casts.forEach(cast => this.checkedSceneRoles.push({ sceneId, roleId, castId: cast.id }));
                } else {
                    const isSceneChecked = this.checkedScenes.some(checkedScene => checkedScene.sceneId === sceneId);
                    if (isSceneChecked) {
                        this.roles.forEach(role => {
                            const sceneHasRole = globalStore.repetitions.selectorData.scenes
                                .find(({ id }) => id === sceneId)
                                ?.roles?.some(({ id }) => id === role.id);
                            sceneHasRole &&
                                role.id !== roleId &&
                                this.casts.forEach(cast =>
                                    this.checkedSceneRoles.push({
                                        sceneId,
                                        roleId: role.id,
                                        castId: cast.id,
                                    })
                                );
                        });

                        this.groups.forEach(group => {
                            const sceneHasGroup = globalStore.repetitions.selectorData.scenes
                                .find(({ id }) => id === sceneId)
                                ?.groups?.some(({ id }) => id === group.id);
                            sceneHasGroup &&
                                this.casts.forEach(cast =>
                                    this.checkedSceneGroups.push({
                                        sceneId,
                                        groupId: group.id,
                                        castId: cast.id,
                                    })
                                );
                        });
                    }

                    this.checkedScenes = this.checkedScenes.filter(checkedScene => checkedScene.sceneId !== sceneId);
                }
            }

            //Click on role cast
            if (sceneId && roleId && castId && !groupId) {
                this.checkedSceneRoles = this.checkedSceneRoles.filter(
                    checkedSceneRole =>
                        checkedSceneRole.sceneId !== sceneId ||
                        checkedSceneRole.roleId !== roleId ||
                        checkedSceneRole.castId !== castId
                );
                if (checked) {
                    this.checkedSceneRoles.push({ sceneId, roleId, castId });
                } else {
                    const isSceneCheckedForCast = this.checkedScenes.some(
                        checkedScene => checkedScene.sceneId === sceneId && checkedScene.castId === castId
                    );
                    if (isSceneCheckedForCast) {
                        this.roles.forEach(role => {
                            const sceneHasRole = globalStore.repetitions.selectorData.scenes
                                .find(({ id }) => id === sceneId)
                                ?.roles?.some(({ id }) => id === role.id);
                            const hasRole = this.checkedSceneRoles.some(
                                sceneRole =>
                                    sceneRole.sceneId === sceneId &&
                                    sceneRole.roleId === role.id &&
                                    sceneRole.castId === castId
                            );
                            sceneHasRole &&
                                !hasRole &&
                                role.id !== roleId &&
                                this.checkedSceneRoles.push({
                                    sceneId,
                                    roleId: role.id,
                                    castId,
                                });
                        });

                        this.groups.forEach(group => {
                            const sceneHasGroup = globalStore.repetitions.selectorData.scenes
                                .find(({ id }) => id === sceneId)
                                ?.groups?.some(({ id }) => id === group.id);
                            const hasGroup = this.checkedSceneGroups.some(
                                sceneGroup =>
                                    sceneGroup.sceneId === sceneId &&
                                    sceneGroup.groupId === group.id &&
                                    sceneGroup.castId === castId
                            );
                            sceneHasGroup &&
                                !hasGroup &&
                                this.checkedSceneGroups.push({
                                    sceneId,
                                    groupId: group.id,
                                    castId,
                                });
                        });
                    }

                    this.checkedScenes = this.checkedScenes.filter(
                        checkedScene => checkedScene.sceneId !== sceneId || checkedScene.castId !== castId
                    );
                }
            }

            //Main click on group
            if (sceneId && groupId && !roleId && !castId) {
                this.checkedSceneGroups = this.checkedSceneGroups.filter(
                    checkedSceneGroup => checkedSceneGroup.sceneId !== sceneId || checkedSceneGroup.groupId !== groupId
                );
                //Checked, so check all casts from this group
                if (checked) {
                    this.casts.forEach(cast => this.checkedSceneGroups.push({ sceneId, groupId, castId: cast.id }));
                } else {
                    const isSceneChecked = this.checkedScenes.some(checkedScene => checkedScene.sceneId === sceneId);
                    if (isSceneChecked) {
                        this.roles.forEach(role => {
                            const sceneHasRole = globalStore.repetitions.selectorData.scenes
                                .find(({ id }) => id === sceneId)
                                ?.roles?.some(({ id }) => id === role.id);
                            sceneHasRole &&
                                this.casts.forEach(cast =>
                                    this.checkedSceneRoles.push({
                                        sceneId,
                                        roleId: role.id,
                                        castId: cast.id,
                                    })
                                );
                        });

                        this.groups.forEach(group => {
                            const sceneHasGroup = globalStore.repetitions.selectorData.scenes
                                .find(({ id }) => id === sceneId)
                                ?.groups?.some(({ id }) => id === group.id);
                            sceneHasGroup &&
                                group.id !== groupId &&
                                this.casts.forEach(cast =>
                                    this.checkedSceneGroups.push({
                                        sceneId,
                                        groupId: group.id,
                                        castId: cast.id,
                                    })
                                );
                        });
                    }

                    this.checkedScenes = this.checkedScenes.filter(checkedScene => checkedScene.sceneId !== sceneId);
                }
            }

            //Click on group cast
            if (sceneId && groupId && castId && !roleId) {
                this.checkedSceneGroups = this.checkedSceneGroups.filter(
                    checkedSceneGroup =>
                        checkedSceneGroup.sceneId !== sceneId ||
                        checkedSceneGroup.groupId !== groupId ||
                        checkedSceneGroup.castId !== castId
                );
                if (checked) {
                    this.checkedSceneGroups.push({ sceneId, groupId, castId });
                } else {
                    const isSceneCheckedForCast = this.checkedScenes.some(
                        checkedScene => checkedScene.sceneId === sceneId && checkedScene.castId === castId
                    );
                    if (isSceneCheckedForCast) {
                        this.roles.forEach(role => {
                            const sceneHasRole = globalStore.repetitions.selectorData.scenes
                                .find(({ id }) => id === sceneId)
                                ?.roles?.some(({ id }) => id === role.id);
                            const hasRole = this.checkedSceneRoles.some(
                                sceneRole =>
                                    sceneRole.sceneId === sceneId &&
                                    sceneRole.roleId === role.id &&
                                    sceneRole.castId === castId
                            );
                            sceneHasRole &&
                                !hasRole &&
                                this.checkedSceneRoles.push({
                                    sceneId,
                                    roleId: role.id,
                                    castId,
                                });
                        });

                        this.groups.forEach(group => {
                            const sceneHasGroup = globalStore.repetitions.selectorData.scenes
                                .find(({ id }) => id === sceneId)
                                ?.groups?.some(({ id }) => id === group.id);
                            const hasGroup = this.checkedSceneGroups.some(
                                sceneGroup =>
                                    sceneGroup.sceneId === sceneId &&
                                    sceneGroup.groupId === group.id &&
                                    sceneGroup.castId === castId
                            );
                            sceneHasGroup &&
                                !hasGroup &&
                                group.id !== groupId &&
                                this.checkedSceneGroups.push({
                                    sceneId,
                                    groupId: group.id,
                                    castId,
                                });
                        });
                    }

                    this.checkedScenes = this.checkedScenes.filter(
                        checkedScene => checkedScene.sceneId !== sceneId || checkedScene.castId !== castId
                    );
                }
            }
        },

        get checkedData() {
            const data = [
                ...this.checkedGroups,
                ...this.checkedRoles,
                ...this.checkedScenes,
                ...this.checkedSceneRoles,
                ...this.checkedSceneGroups,
            ];
            this.checkedUsers.forEach(checkedUser => data.push({ userId: checkedUser }));
            return data;
        },

        get allChecked() {
            return this.checkedScenes.length === this.scenes.length * this.casts.length;
        },

        get castChecked() {
            const castScenes = this.checkedScenes.reduce((acc, checkedScene) => {
                if (!acc[checkedScene.castId]) {
                    acc[checkedScene.castId] = 0;
                }
                acc[checkedScene.castId]++;
                return acc;
            }, {});

            for (let cast in castScenes) {
                if (castScenes[cast] === this.scenes.length) {
                    return true;
                }
            }

            return false;
        },
    });
}
