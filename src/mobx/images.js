import { observable } from 'mobx';

export default function ui({ axios, apiUrl, globalStore }) {
    return observable({
        images: {},
        async fetch(user) {
            const image =
                (await axios.request({
                    method: 'POST',
                    url: `${apiUrl}/api/image`,
                    data: {
                        ...user,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                    },
                })).data || '';

            this.images[user.id] = image;
        },
    });
}
