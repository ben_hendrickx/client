import { observable } from 'mobx';

export default function({ request, globalStore }) {
    function hasRight(rights, { right: rightName, read, create, update, delete: remove, subRight }) {
        return (rights || []).some(right => {
            let hasRight;
            if (right.name === rightName) {
                if (subRight !== undefined) {
                    return right.read && right.subRights.some(({ name, access }) => name === subRight && access);
                }

                read !== undefined && (hasRight = right.read === read);
                create !== undefined && (hasRight = right.create === create);
                update !== undefined && (hasRight = right.update === update);
                remove !== undefined && (hasRight = right.delete === remove);
                return hasRight;
            }

            return false;
        });
    }

    return observable({
        loading: true,
        rights: [],
        projectRights: {},
        async fetch() {
            this.loading = true;
            const rights = await request(`/api/user/${globalStore.account.id}/rights`);
            this.rights = rights;
            this.loading = false;
        },
        async fetchRightsForProject(projectId) {
            globalStore.projects.id = projectId;
            this.loading = true;
            const rights = await request(`/api/project/${projectId}/user/${globalStore.account.id}/rights`);
            this.projectRights[projectId] = rights;
            this.loading = false;
            // delete globalStore.projects.id;
        },
        hasRight(config, includeProjectRights = true) {
            const right = hasRight(this.rights, config);
            const projectRight = hasRight(this.projectRights[globalStore.projects.id], config);
            return right || (includeProjectRights && projectRight);
        },

        //////////////
        // RIGHTS FOR THREADS
        //////////////
        get canReadThreads() {
            return this.hasRight({ right: 'threads', read: true });
        },
        get canCreateThreads() {
            return this.hasRight({ right: 'threads', create: true });
        },
        get canUpdateThreads() {
            return this.hasRight({ right: 'threads', update: true });
        },
        get canDeleteThreads() {
            return this.hasRight({ right: 'threads', delete: true });
        },

        //////////////
        // RIGHTS FOR THREAD ACTIONS
        //////////////
        get canReadThreadActions() {
            return this.hasRight({ right: 'thread_actions', read: true });
        },
        get canCreateThreadActions() {
            return this.hasRight({ right: 'thread_actions', create: true });
        },
        get canUpdateThreadActions() {
            return this.hasRight({ right: 'thread_actions', update: true });
        },
        get canDeleteThreadActions() {
            return this.hasRight({ right: 'thread_actions', delete: true });
        },

        //////////////
        // RIGHTS FOR PROJECTS
        //////////////
        get canReadProjects() {
            return this.hasRight({ right: 'projects', read: true });
        },
        get canCreateProjects() {
            return this.hasRight({ right: 'projects', create: true });
        },
        get canUpdateProjects() {
            return this.hasRight({ right: 'projects', update: true });
        },
        get canDeleteProjects() {
            return this.hasRight({ right: 'projects', delete: true });
        },
        get canExportProjects() {
            return this.hasRight({ right: 'projects', subRight: 'projects_export' });
        },
        get canManageProjectRights() {
            return this.hasRight({ right: 'projects', subRight: 'rights' });
        },
        get canMakeBadges() {
            return this.hasRight({ right: 'projects', subRight: 'badges' });
        },
        get canTransferClothing() {
            return this.hasRight({ right: 'projects', subRight: 'transfer' });
        },

        //////////////
        // RIGHTS FOR USERS
        //////////////
        get canReadUsers() {
            return this.hasRight({ right: 'users', read: true });
        },
        get canCreateUsers() {
            return this.hasRight({ right: 'users', create: true });
        },
        get canUpdateUsers() {
            return this.hasRight({ right: 'users', update: true });
        },
        get canDeleteUsers() {
            return this.hasRight({ right: 'users', delete: true });
        },
        get canReadUsersPicture() {
            return this.hasRight({ right: 'users', subRight: 'picture' });
        },
        get canUpdateUsersPicture() {
            return (
                this.hasRight({ right: 'users', update: true }) &&
                this.hasRight({ right: 'users', subRight: 'picture' })
            );
        },
        get canReadUsersPersonal() {
            return this.hasRight({ right: 'users', subRight: 'personal' });
        },
        get canUpdateUsersPersonal() {
            return (
                this.hasRight({ right: 'users', update: true }) &&
                this.hasRight({ right: 'users', subRight: 'personal' })
            );
        },
        get canReadUsersContact() {
            return this.hasRight({ right: 'users', subRight: 'contact' });
        },
        get canUpdateUsersContact() {
            return (
                this.hasRight({ right: 'users', update: true }) &&
                this.hasRight({ right: 'users', subRight: 'contact' })
            );
        },
        get canReadUsersAddress() {
            return this.hasRight({ right: 'users', subRight: 'address' });
        },
        get canUpdateUsersAddress() {
            return (
                this.hasRight({ right: 'users', update: true }) &&
                this.hasRight({ right: 'users', subRight: 'address' })
            );
        },
        //TODO: change to privacy
        get canReadUsersOther() {
            return this.hasRight({ right: 'users', subRight: 'other' });
        },
        get canUpdateUsersOther() {
            return (
                this.hasRight({ right: 'users', update: true }) && this.hasRight({ right: 'users', subRight: 'other' })
            );
        },
        get canReadUsersClothing() {
            return this.hasRight({ right: 'users', subRight: 'clothing' });
        },
        get canUpdateUsersClothing() {
            return (
                this.hasRight({ right: 'users', update: true }) &&
                this.hasRight({ right: 'users', subRight: 'clothing' })
            );
        },
        get canReadUsersRemarks() {
            return this.hasRight({ right: 'users', subRight: 'remarks' });
        },
        get canUpdateUsersRemarks() {
            return (
                this.hasRight({ right: 'users', update: true }) &&
                this.hasRight({ right: 'users', subRight: 'remarks' })
            );
        },
        get canReadUserRoles() {
            return this.hasRight({ right: 'users', subRight: 'roles' });
        },
        get canReadUserRegistrations() {
            return this.hasRight({ right: 'users', subRight: 'registrations' });
        },
        get canReadUserGroups() {
            return this.hasRight({ right: 'users', subRight: 'groups' });
        },
        get canReadUserScenes() {
            return this.hasRight({ right: 'users', subRight: 'scenes' });
        },
        get canReadUserRepetitions() {
            return this.hasRight({ right: 'users', subRight: 'repetitions' });
        },
        get canReadUserRights() {
            return this.hasRight({ right: 'users', subRight: 'rights' });
        },
        get canExportUsers() {
            return this.hasRight({ right: 'users', subRight: 'export_users' });
        },

        //////////////
        // RIGHTS FOR REGISTRATIONS
        //////////////
        get canReadRegistrations() {
            return this.hasRight({ right: 'registrations', read: true });
        },
        get canCreateRegistrations() {
            return this.hasRight({ right: 'registrations', create: true });
        },
        get canUpdateRegistrations() {
            return this.hasRight({ right: 'registrations', update: true });
        },
        get canDeleteRegistrations() {
            return this.hasRight({ right: 'registrations', delete: true });
        },
        get canExportRegistrations() {
            return this.hasRight({ right: 'registrations', subRight: 'registrations_export' });
        },
        get canReadRegistrationsGeneral() {
            return this.hasRight({ right: 'registrations', subRight: 'general' });
        },
        get canReadRegistrationsPreferred() {
            return this.hasRight({ right: 'registrations', subRight: 'preferred' });
        },
        get canReadRegistrationsOther() {
            return this.hasRight({ right: 'registrations', subRight: 'other' });
        },
        get canCopyRegistrations() {
            return this.hasRight({ right: 'registrations', subRight: 'copy' });
        },

        //////////////
        // RIGHTS FOR ROLES
        //////////////
        get canReadRoles() {
            return this.hasRight({ right: 'roles', read: true });
        },
        get canCreateRoles() {
            return this.hasRight({ right: 'roles', create: true });
        },
        get canUpdateRoles() {
            return this.hasRight({ right: 'roles', update: true });
        },
        get canDeleteRoles() {
            return this.hasRight({ right: 'roles', delete: true });
        },
        get canExportRoles() {
            return this.hasRight({ right: 'roles', subRight: 'export' });
        },
        get canUpdateRoleUsers() {
            return this.hasRight({ right: 'roles', subRight: 'addusers' });
        },

        //////////////
        // RIGHTS FOR GROUPS
        //////////////
        get canReadGroups() {
            return this.hasRight({ right: 'groups', read: true });
        },
        get canCreateGroups() {
            return this.hasRight({ right: 'groups', create: true });
        },
        get canUpdateGroups() {
            return this.hasRight({ right: 'groups', update: true });
        },
        get canDeleteGroups() {
            return this.hasRight({ right: 'groups', delete: true });
        },
        get canExportGroups() {
            return this.hasRight({ right: 'groups', subRight: 'export' });
        },
        get canUpdateGroupUsers() {
            return this.hasRight({ right: 'groups', subRight: 'addusers' });
        },

        //////////////
        // RIGHTS FOR SCENES
        //////////////
        get canReadScenes() {
            return this.hasRight({ right: 'scenes', read: true });
        },
        get canCreateScenes() {
            return this.hasRight({ right: 'scenes', create: true });
        },
        get canUpdateScenes() {
            return this.hasRight({ right: 'scenes', update: true });
        },
        get canDeleteScenes() {
            return this.hasRight({ right: 'scenes', delete: true });
        },
        get canUpdateSceneUsers() {
            return this.hasRight({ right: 'scenes', subRight: 'addusers' });
        },
        get canExportScenes() {
            return this.hasRight({ right: 'scenes', subRight: 'export' });
        },

        //////////////
        // RIGHTS FOR REPETITIONS
        //////////////
        get canReadRepetitions() {
            return this.hasRight({ right: 'repetitions', read: true });
        },
        get canCreateRepetitions() {
            return this.hasRight({ right: 'repetitions', create: true });
        },
        get canUpdateRepetitions() {
            return this.hasRight({ right: 'repetitions', update: true });
        },
        get canDeleteRepetitions() {
            return this.hasRight({ right: 'repetitions', delete: true });
        },
        get canExportRepetitions() {
            return this.hasRight({ right: 'repetitions', subRight: 'export' });
        },
        get canOpenCloseRepetitions() {
            return this.hasRight({ right: 'repetitions', subRight: 'openclose' });
        },
        get canPinRepetitions() {
            return this.hasRight({ right: 'repetitions', subRight: 'pin' });
        },
        get canPresentRepetitions() {
            return this.hasRight({ right: 'repetitions', subRight: 'present' });
        },

        //////////////
        // RIGHTS FOR CASTS
        //////////////
        get canReadCasts() {
            return this.hasRight({ right: 'casts', read: true });
        },
        get canCreateCasts() {
            return this.hasRight({ right: 'casts', create: true });
        },
        get canUpdateCasts() {
            return this.hasRight({ right: 'casts', update: true });
        },
        get canDeleteCasts() {
            return this.hasRight({ right: 'casts', delete: true });
        },

        //////////////
        // RIGHTS FOR REPETITIONTYPES
        //////////////
        get canReadRepetitionTypes() {
            return this.hasRight({ right: 'repetitiontypes', read: true });
        },
        get canCreateRepetitionTypes() {
            return this.hasRight({ right: 'repetitiontypes', create: true });
        },
        get canUpdateRepetitionTypes() {
            return this.hasRight({ right: 'repetitiontypes', update: true });
        },
        get canDeleteRepetitionTypes() {
            return this.hasRight({ right: 'repetitiontypes', delete: true });
        },

        //////////////
        // RIGHTS FOR LOCATIONS
        //////////////
        get canReadLocations() {
            return this.hasRight({ right: 'locations', read: true });
        },
        get canCreateLocations() {
            return this.hasRight({ right: 'locations', create: true });
        },
        get canUpdateLocations() {
            return this.hasRight({ right: 'locations', update: true });
        },
        get canDeleteLocations() {
            return this.hasRight({ right: 'locations', delete: true });
        },

        //////////////
        // RIGHTS FOR ARTICLES
        //////////////
        get canReadArticles() {
            return this.hasRight({ right: 'articles', read: true });
        },
        get canCreateArticles() {
            return this.hasRight({ right: 'articles', create: true });
        },
        get canUpdateArticles() {
            return this.hasRight({ right: 'articles', update: true });
        },
        get canDeleteArticles() {
            return this.hasRight({ right: 'articles', delete: true });
        },
        get canExportArticles() {
            return this.hasRight({ right: 'articles', subRight: 'export' });
        },

        //////////////
        // RIGHTS FOR ORDERS
        //////////////
        get canReadOrders() {
            return this.hasRight({ right: 'orders', read: true });
        },
        get canCreateOrders() {
            return this.hasRight({ right: 'orders', create: true });
        },
        get canUpdateOrders() {
            return this.hasRight({ right: 'orders', update: true });
        },
        get canDeleteOrders() {
            return this.hasRight({ right: 'orders', delete: true });
        },

        //////////////
        // RIGHTS FOR CLOTHINGSIZES
        //////////////
        get canReadClothingSizes() {
            return this.hasRight({ right: 'sizes', read: true });
        },
        get canCreateClothingSizes() {
            return this.hasRight({ right: 'sizes', create: true });
        },
        get canUpdateClothingSizes() {
            return this.hasRight({ right: 'sizes', update: true });
        },
        get canDeleteClothingSizes() {
            return this.hasRight({ right: 'sizes', delete: true });
        },
        get canExportClothingSizes() {
            return this.hasRight({ right: 'sizes', subRight: 'export' });
        },

        //////////////
        // RIGHTS FOR PATTERNS
        //////////////
        get canReadPatterns() {
            return this.hasRight({ right: 'patterns', read: true });
        },
        get canCreatePatterns() {
            return this.hasRight({ right: 'patterns', create: true });
        },
        get canUpdatePatterns() {
            return this.hasRight({ right: 'patterns', update: true });
        },
        get canDeletePatterns() {
            return this.hasRight({ right: 'patterns', delete: true });
        },
        get canExportPatterns() {
            return this.hasRight({ right: 'patterns', subRight: 'export' });
        },

        //////////////
        // RIGHTS FOR FABRICS
        //////////////
        get canReadFabrics() {
            return this.hasRight({ right: 'fabrics', read: true });
        },
        get canCreateFabrics() {
            return this.hasRight({ right: 'fabrics', create: true });
        },
        get canUpdateFabrics() {
            return this.hasRight({ right: 'fabrics', update: true });
        },
        get canDeleteFabrics() {
            return this.hasRight({ right: 'fabrics', delete: true });
        },
        get canExportFabrics() {
            return this.hasRight({ right: 'fabrics', subRight: 'export' });
        },

        //////////////
        // RIGHTS FOR COLORS
        //////////////
        get canReadColors() {
            return this.hasRight({ right: 'colors', read: true });
        },
        get canCreateColors() {
            return this.hasRight({ right: 'colors', create: true });
        },
        get canUpdateColors() {
            return this.hasRight({ right: 'colors', update: true });
        },
        get canDeleteColors() {
            return this.hasRight({ right: 'colors', delete: true });
        },
        get canExportColors() {
            return this.hasRight({ right: 'colors', subRight: 'export' });
        },

        //////////////
        // RIGHTS FOR RACKS
        //////////////
        get canReadRacks() {
            return this.hasRight({ right: 'racks', read: true });
        },
        get canCreateRacks() {
            return this.hasRight({ right: 'racks', create: true });
        },
        get canUpdateRacks() {
            return this.hasRight({ right: 'racks', update: true });
        },
        get canDeleteRacks() {
            return this.hasRight({ right: 'racks', delete: true });
        },
        get canExportRacks() {
            return this.hasRight({ right: 'racks', subRight: 'export' });
        },

        //////////////
        // RIGHTS FOR CATEGORIES
        //////////////
        get canReadCategories() {
            return this.hasRight({ right: 'categories', read: true });
        },
        get canCreateCategories() {
            return this.hasRight({ right: 'categories', create: true });
        },
        get canUpdateCategories() {
            return this.hasRight({ right: 'categories', update: true });
        },
        get canDeleteCategories() {
            return this.hasRight({ right: 'categories', delete: true });
        },

        //////////////
        // RIGHTS FOR ACCESSROLES
        //////////////
        get canReadAccessRoles() {
            return this.hasRight({ right: 'rights', read: true });
        },
        get canCreateAccessRoles() {
            return this.hasRight({ right: 'rights', create: true });
        },
        get canUpdateAccessRoles() {
            return this.hasRight({ right: 'rights', update: true });
        },
        get canDeleteAccessRoles() {
            return this.hasRight({ right: 'rights', delete: true });
        },

        //////////////
        // RIGHTS FOR SETTINGS
        //////////////
        get canReadSettings() {
            return this.hasRight({ right: 'settings', read: true });
        },
        get canUpdateSettings() {
            return this.hasRight({ right: 'settings', update: true });
        },

        get canSeeCalendar() {
            return this.canReadRepetitions && this.hasRight({ right: 'calendar', read: true });
        },

        //////////////
        // RIGHTS FOR NOTIFICATIONS
        //////////////
        get canReadNotificationsGlobal() {
            return this.hasRight({ right: 'notifications', read: true }, false);
        },
        get canReadNotifications() {
            return this.hasRight({ right: 'notifications', read: true });
        },
        get canUpdateNotifications() {
            return this.hasRight({ right: 'notifications', update: true });
        },
        get canDeleteNotifications() {
            return this.hasRight({ right: 'notifications', delete: true });
        },
        get canReadNotificationsNewUsers() {
            return this.hasRight({ right: 'notifications', subRight: 'new_users' });
        },
        get canReadNotificationsRegistrationUpdated() {
            return this.hasRight({ right: 'notifications', subRight: 'registration_updated' });
        },
        get canReadNotificationsRegistrationDeleted() {
            return this.hasRight({ right: 'notifications', subRight: 'registration_deleted' });
        },
        get canReadNotificationsRepetitionSignoff() {
            return this.hasRight({ right: 'notifications', subRight: 'repetition_signoff' });
        },
        get canReadNotificationsClothingChanged() {
            return this.hasRight({ right: 'notifications', subRight: 'clothing_changed' });
        },
        get canReadNotificationsContactdetailsChanged() {
            return this.hasRight({ right: 'notifications', subRight: 'contactdetails_changed' });
        },
        get canReadNotificationsClothingdetailsChanged() {
            return this.hasRight({ right: 'notifications', subRight: 'clothingdetails_changed' });
        },
        get canReadNotificationsUserinfoChanged() {
            return this.hasRight({ right: 'notifications', subRight: 'userinfo_changed' });
        },

        //////////////
        // RIGHTS FOR NOTIFICATIONS
        //////////////
        get canReadMessages() {
            return this.hasRight({ right: 'messages', read: true });
        },
        get canCreateMessages() {
            return this.hasRight({ right: 'messages', create: true });
        },
        get canUpdateMessages() {
            return this.hasRight({ right: 'messages', update: true });
        },
        get canDeleteMessages() {
            return this.hasRight({ right: 'messages', delete: true });
        },
        get canReadMessagesRead() {
            return this.hasRight({ right: 'messages', subRight: 'messages_read' });
        },
    });
}
