/* eslint-disable no-console */
import { observable } from 'mobx';
import moment from 'moment';
import rights from './rights';

export default function account(dependencies) {
    const { request, localStorageService, socket, globalStore, tokenService } = dependencies;

    socket.on('USER_IMAGE', img => {
        globalStore.profile.picture = img;
    });

    socket.on('logout', () => {
        console.error('Server disconnected');
        globalStore.account.loggedIn = false;
        window.location.href = '/';
    });

    function socketInterval(callback, duration) {
        let interval = setInterval(() => {
            if (socket.connected) {
                callback();
                clearInterval(interval);
            }
        }, duration);
    }

    return observable({
        rights: rights(dependencies),
        email: localStorageService.get('FVD_REMEMBER', false) ? localStorageService.get('FVD_EMAIL', '') : '',
        password: localStorageService.get('FVD_REMEMBER', false) ? localStorageService.get('FVD_PWD', '') : '',
        _remember: localStorageService.get('FVD_REMEMBER', false),
        loggedIn: null,
        token: localStorageService.get('FVD_TOKEN', null),
        profiles: [],
        loading: false,
        set({
            id,
            email,
            firstName,
            lastName,
            level,
            image,
            password,
            confirmPassword,
            token,
            loggedIn,
            shouldVerify,
            canEditClothingSizes,
        }) {
            id !== undefined && (this.id = id);
            email !== undefined && (this.email = email);
            firstName !== undefined && (this.firstName = firstName);
            lastName !== undefined && (this.lastName = lastName);
            level !== undefined && (this.level = level);
            image !== undefined && (globalStore.profile.picture = image);
            password !== undefined && (this.password = password);
            confirmPassword !== undefined && (this.confirmPassword = confirmPassword);
            token !== undefined && tokenService.set(token);
            token !== undefined && (this.token = localStorageService.set('FVD_TOKEN', token));
            loggedIn !== undefined && (this.loggedIn = loggedIn);
            shouldVerify !== undefined && (this.shouldVerify = shouldVerify);
            canEditClothingSizes !== undefined && (this.canEditClothingSizes = canEditClothingSizes);
        },
        setNotifications({ warn, error, success } = {}) {
            this.warn = warn || '';
            this.error = error || '';
            this.success = success || '';
        },
        resetNotifications() {
            this.setNotifications({});
        },
        async verify() {
            await request(`/api/verify/${this.id}`);
        },
        async import(users) {
            return await request(`/api/import`, 'POST', { users });
        },
        async login() {
            this.loading = true;
            try {
                const {
                    token,
                    firstName,
                    lastName,
                    level,
                    email,
                    id,
                    image,
                    shouldVerify,
                    canEditClothingSizes,
                } = await request('/api/auth', 'POST', {
                    email: this.email,
                    password: this.password,
                });

                localStorageService.set('FVD_EMAIL', this.email);
                localStorageService.set('FVD_PWD', this.password);

                this.set({
                    id,
                    email,
                    firstName,
                    lastName,
                    level,
                    image,
                    password: '',
                    token,
                    loggedIn: true,
                    shouldVerify,
                    canEditClothingSizes,
                });
                this.fetchProfiles(email);
                this.rights.fetch();
                this.resetNotifications();
                this.loading = false;
                socket.connected && socket.emit('connected', email);
            } catch (e) {
                if (e) {
                    this.setNotifications({
                        error: e || 'Er ging iets mis, gelieve de pagina te vernieuwen en opnieuw te proberen',
                    });
                }
                this.loading = false;
            }
        },
        async signup() {
            this.loading = true;
            try {
                await request('/api/signup', 'POST', {
                    firstName: this.firstName,
                    lastName: this.lastName,
                    date_of_birth: moment(this.dateOfBirth).format('YYYY-MM-DD'),
                    nationality: this.origin,
                    origin: this.countryOfBirth,
                    sex: this.sex,
                    phoneNumber: this.phone,
                    cellPhone: this.cell,
                    email: this.mail,
                    secondaryEmail: this.secondaryMail,
                    tShirt: this.tShirt,
                    streetName: this.streetName,
                    houseNumber: this.houseNumber,
                    bus: this.bus || '',
                    cityName: this.cityName,
                    zipCode: this.zipCode,
                    image: this.image,
                    privacy: this.privacy,
                    pictures: this.pictures || 0,
                    pwd: this.password,
                });

                this.set({
                    email: '',
                    password: '',
                    confirmPassword: '',
                });

                this.setNotifications({
                    success:
                        'Bedankt voor uw registratie! U ontvangt dadelijk een mail met daarin een link om uw mailadres te bevestigen',
                });
                this.loading = false;
            } catch (e) {
                this.setNotifications({ error: e });
                this.loading = false;
                throw e;
            }
        },
        async forgotPassword() {
            this.loading = true;
            try {
                await request('/api/forgot-password', 'POST', { email: this.email });
                this.setNotifications({ success: 'U ontvangt zo een mail met een link om uw wachtwoord te wijzigen' });
                this.loading = false;
            } catch (e) {
                this.setNotifications({ error: e });
                this.loading = false;
                throw e;
            }
        },
        async setPassword(email, token) {
            try {
                await request('/api/change-password', 'POST', { token, email, password: this.password });
                !this.loggedIn &&
                    this.setNotifications({ success: 'Uw wachtwoord werd succesvol gewijzigd, u kan nu aanmelden' });
                this.loggedIn &&
                    this.setNotifications({
                        success:
                            'Uw wachtwoord werd succesvol gewijzigd, u wordt zo doorverwezen naar de vorige pagina',
                    });
            } catch (e) {
                this.setNotifications({ error: e });
                throw e;
            }
        },
        async fetchSizes(userId) {
            this.loading = true;
            try {
                const user = await request(`/api/user/${userId || this.id}/sizes`);

                if (!user) {
                    this.loading = false;
                    return;
                }

                this.tShirt = user.shirtSize;
                this.height = user.height;
                this.chest = user.chest;
                this.waist = user.waist;
                this.hipSize = user.hipSize;
                this.headSize = user.headSize;
                this.clothingSize = user.clothingSize;
                this.loading = false;
            } catch (e) {
                this.setNotifications({ error: e });
                this.loading = false;
                throw e;
            }
        },
        async changeProfile(userId) {
            try {
                const { level, firstName, lastName, token, id, image, shouldVerify, canEditClothingSizes } =
                    (await request(`/api/auth/switchProfile/${userId}`)) || {};

                token && tokenService.set(token);
                this.token = localStorageService.set('FVD_TOKEN', token);
                this.shouldVerify = shouldVerify;
                this.id = id;
                this.level = level;
                this.firstName = firstName;
                this.lastName = lastName;
                this.canEditClothingSizes = canEditClothingSizes;

                globalStore.profile.picture = image;

                this.fetchProfiles();
                globalStore.account.rights.fetch();
            } catch (e) {
                console.error(e);
            }
        },
        async invite(addresses) {
            this.loading = true;
            addresses = addresses.filter(({ checked }) => checked);
            addresses = addresses.map(({ email }) => email);

            try {
                await request(`/api/mail`, 'POST', { to: addresses });
            } catch (e) {}
            this.loading = false;
        },
        async fetchProfiles(email) {
            if (email) {
                this.email = email;
            }
            try {
                const users = await request(`/api/user/${this.id}/profiles`);

                this.profiles = users.filter(u => {
                    return u.firstName !== this.firstName || u.lastName !== this.lastName;
                });
            } catch (e) {
                console.error(e);
            }
        },
        reset() {
            this.firstName = '';
            this.lastName = '';
            this.email = '';
            this.dateOfBirth = '';
            this.origin = '';
            this.countryOfBirth = '';
            this.sex = '';
            this.phone = '';
            this.cell = '';
            this.mail = '';
            this.confirmMail = '';
            this.secondaryMail = '';
            this.tShirt = '';
            this.streetName = '';
            this.houseNumber = '';
            this.cityName = '';
            this.zipCode = '';
            this.image = '';
            this.privacy = '';
            this.pictures = '';
            this.password = '';
            this.confirmPassword = '';
            this.shouldVerify = '';
            globalStore.registrations.data = [];
            globalStore.projects.data = [];
        },
        logout() {
            request('/api/logout', 'POST', {});
            tokenService.clear();
            localStorageService.remove('FVD_TOKEN');
            this.loggedIn = false;
            this.reset();
            this.email = localStorageService.get('FVD_EMAIL', '');
            this.password = localStorageService.get('FVD_PWD', '');
        },
        set remember(v) {
            this._remember = localStorageService.set('FVD_REMEMBER', v);
        },
        get remember() {
            return this._remember;
        },
        async verifyToken() {
            return new Promise((resolve, reject) => {
                if (this.token) {
                    socketInterval(() => {
                        socket.emit('VERIFY_TOKEN', this.token);
                    }, 300);
                } else {
                    this.loggedIn = false;
                }

                socket.on('error', e => console.error(e));

                socket.on('connection', this.verifyToken);

                socket.on('VERIFY_TOKEN', data => {
                    if (data.status && data.status === 404) {
                        this.loggedIn = false;
                        this.token = null;
                        tokenService.clear();
                        localStorageService.remove('FVD_TOKEN');
                        this.reset();
                        return reject('Token expired');
                    }

                    this.loggedIn = !!data;
                    this.firstName = data.firstName;
                    this.lastName = data.lastName;
                    this.email = data.email;
                    this.level = data.level;
                    this.id = data.id;
                    this.shouldVerify = data.shouldVerify;
                    this.canEditClothingSizes = data.canEditClothingSizes;

                    socket.connected && data.email && socket.emit('connected', data.email);

                    this.rights.fetch();
                    this.fetchProfiles();
                    return resolve();
                });
            });
        },
        async saveImage(user) {
            try {
                await request(`/api/image`, 'PUT', {
                    image: user.newImage,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    email: user.email,
                });
                globalStore.profile.picture = user.newImage;
                this.success =
                    this.level === 1
                        ? 'Deze profielfoto werd succesvol gewijzigd'
                        : 'Uw profielfoto werd succesvol gewijzigd';

                delete globalStore.images.images[user.id];
            } catch (e) {
                this.error =
                    typeof e.response === 'object' && e.response.data
                        ? typeof e.response.data === 'object'
                            ? e.response.data.message
                            : e.response.data
                        : e.message;
                this.error === 'Bad request' && (this.error = null);
            }
        },
        async saveContactDetails(user) {
            try {
                await request(`/api/user/${user.id}/contact-details`, 'PUT', {
                    phoneNumber: user.newPhoneNumber,
                    cellPhone: user.newCellPhone,
                    email: user.email,
                    newEmail: user.newEmail,
                    secondaryEmail: user.newSecondaryEmail,
                    streetName: user.newAddress.streetName,
                    houseNumber: user.newAddress.houseNumber,
                    bus: user.newAddress.bus,
                    zipCode: user.newAddress.zipCode,
                    cityName: user.newAddress.cityName,
                });

                if (this.level === 0 && user.email !== user.newEmail) {
                    this.token = null;
                    this.loggedIn = false;
                    tokenService.clear();
                    localStorageService.remove('FVD_TOKEN');
                    this.success =
                        'Uw contactgegevens werden succesvol gewijzigd, gelieve u nu aan te melden met uw nieuw e-mailadres';
                } else {
                    this.success =
                        this.level === 1
                            ? `De contactgegevens van ${user.firstName} ${user.lastName} werden aangepast`
                            : 'Uw contactgegevens werden succesvol gewijzigd';
                }
            } catch (e) {
                this.error =
                    typeof e.response === 'object' && e.response.data
                        ? typeof e.response.data === 'object'
                            ? e.response.data.message
                            : e.response.data
                        : e.message;
                throw new Error();
            }
        },
        async saveInformation(user) {
            try {
                await request(`/api/information`, 'PUT', {
                    id: user.id,
                    firstName: user.newFirstName,
                    lastName: user.newLastName,
                    date_of_birth: moment(user.newDateOfBirth).format('YYYY-MM-DD'),
                    sex: ['Man', 'Vrouw', 'X'].indexOf(user.newSex),
                    nationality: user.newNationality,
                    origin: user.newOrigin,
                });
                this.success =
                    this.level === 1
                        ? `De persoonsgegevens van ${user.firstName} ${user.lastName} werden aangepast`
                        : 'Uw persoonsgegevens werden succesvol gewijzigd';
            } catch (e) {
                this.error =
                    typeof e.response === 'object' && e.response.data
                        ? typeof e.response.data === 'object'
                            ? e.response.data.message
                            : e.response.data
                        : e.message;
                throw new Error();
            }
        },
        async saveClothingSizes(user) {
            try {
                await request(`/api/clothing-sizes`, 'PUT', {
                    id: user.id,
                    height: user.newHeight || null,
                    chest: user.newChest || null,
                    waist: user.newWaist || null,
                    hipSize: user.newHipSize || null,
                    headSize: user.newHeadSize || null,
                    clothingSize: user.newClothingSize || null,
                    shirtSize: user.newShirtSize || null,
                    clothingFrozen: user.newClothingFrozen || false,
                });
                this.success =
                    this.level === 1
                        ? `De kledij-gegevens van ${user.firstName} ${user.lastName} werden aangepast`
                        : 'Uw kledij-gegevens werden succesvol gewijzigd';
            } catch (e) {
                this.error =
                    typeof e.response === 'object' && e.response.data
                        ? typeof e.response.data === 'object'
                            ? e.response.data.message
                            : e.response.data
                        : e.message;
                throw new Error();
            }
        },
        async saveRemarks(user) {
            try {
                await request(`/api/user/${user.id}/remarks`, 'PUT', {
                    remarks: user.newRemarks || null,
                });
                this.success = `De opmerking werd aangepast`;
            } catch (e) {
                this.error =
                    typeof e.response === 'object' && e.response.data
                        ? typeof e.response.data === 'object'
                            ? e.response.data.message
                            : e.response.data
                        : e.message;
                throw new Error();
            }
        },
        async savePrivacyFlag(user) {
            try {
                await request(`/api/privacy`, 'PUT', {
                    id: user.id,
                    pictures: user.newPictures,
                });
                this.success = 'Uw privacy-gegevens werden succesvol gewijzigd';
            } catch (e) {
                this.error =
                    typeof e.response === 'object' && e.response.data
                        ? typeof e.response.data === 'object'
                            ? e.response.data.message
                            : e.response.data
                        : e.message;
                throw new Error();
            }
        },
    });
}
