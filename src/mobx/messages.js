import { observable } from 'mobx';

import resourceGenerator from './shared/resource';

export default function messages(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request } = dependencies;

    return observable({
        resource: resource({
            single: 'message',
            plural: 'messages',
            labels: {
                single: 'bericht',
                plural: 'berichten',
            },
            form: {
                sections: [
                    {
                        id: 'information',
                        translationKey: 'form.sections.information',
                    },
                    {
                        id: 'files',
                        translationKey: 'form.sections.files',
                    },
                ],
                fields: [
                    {
                        id: 'title',
                        translationKey: 'form.labels.title',
                        type: 'text',
                        required: true,
                        validations: [{ type: 'length', min: 3, max: 255 }],
                        section: 'information',
                    },
                    {
                        id: 'description',
                        translationKey: 'form.labels.description',
                        type: 'wysiwyg',
                        required: true,
                        validations: [{ type: 'length', min: 10, max: 255 }],
                        section: 'information',
                    },
                    {
                        id: 'files',
                        translationKey: 'form.labels.files',
                        type: 'file',
                        section: 'files',
                        multiple: true,
                    },
                ],
            },
        }),
        getUnreadMessageCount: async function() {
            const { data } = await request(`/api/messages/unread`);
            this.unread = data;
        },
        getMessage: async function(id) {
            this.resource.item = await request(`/api/messages/${id}`);
        },
        openedByUser: async function(userId, id) {
            await request(`/api/messages/${id}/opened?userId=${userId}`, 'POST');
        },
        markRead: async function(userId, id) {
            await request(`/api/messages/${id}/read?userId=${userId}`, 'POST');
        },
    });
}
