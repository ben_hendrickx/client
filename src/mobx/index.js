import PropTypes from 'prop-types';
import { useLocalStore } from 'mobx-react-lite';
import React from 'react';
import io from 'socket.io-client';
import axiosInstanceBuilder from '../services/axiosInstanceBuilder';
import account from './account';
import profile from './profile.js';
import ui from './ui.js';
import general from './general';
import projects from './projects';
import registrations from './registrations';
import users from './users';
import countries from './countries';
import shirtSizes from './shirtSizes';
import clothingSizes from './clothingSizes';
import roles from './roles';
import overview from './overview';
import projectRoles from './projectRoles';
import scenes from './scenes';
import groups from './groups';
import casts from './casts';
import repetitions from './repetitions';
import types from './types';
import locations from './locations';
import images from './images';
import notifications from './notifications';
import threads from './threads';
import clothingManagement from './clothingManagement/index.js';
import projectUsers from './retributions/projectUsers.js';
import articles from './clothingManagement/articles.js';
import mail from './mail';
import selector from './selector';
import statistics from './statistics';
import accessRoles from './accessRoles';
import settings from './settings';
import messages from './messages';

import localStorageService from '../services/localStorage';
import token from 'services/token';

const tokenService = token();

export const apiUrl = `${window.location.protocol}//${window.location.hostname}:${process.env.REACT_APP_PORT || 9998}`;
let store;

const request = async function(url, method = 'GET', body, options = {}) {
    const axios = axiosInstanceBuilder();

    const headers = {
        'Content-Type': 'application/json',
        ...(options.headers || {}),
    };

    delete options.headers;

    let token = tokenService.get();
    token && (headers.Authorization = `Bearer ${token}`);

    if (!(body instanceof FormData) && typeof body === 'object' && !Array.isArray(body)) {
        body = Object.entries(body)
            .filter(([, v]) => v !== undefined)
            .reduce((acc, [key, value]) => {
                acc[key] = value;
                return acc;
            }, {});
    }

    try {
        const response = await axios.request({
            method,
            url: `${apiUrl}${url}`,
            data: body,
            headers,
            ...options,
        });
        return response.data;
    } catch (e) {
        let error;
        if (typeof e === 'object' && typeof e.response === 'object' && e.response.data) {
            error = typeof e.response.data === 'object' ? e.response.data.message : e.response.data;
        } else {
            error = e.message;
        }
        console.error(error);
        throw error;
    }
};

const dependencies = {
    axios: axiosInstanceBuilder(),
    request,
    io,
    localStorageService: localStorageService(),
    apiUrl,
    tokenService,
};

export const globalStore = function() {
    store = {
        general,
        account,
        ui,
        users,
        projects,
        registrations,
        countries,
        profile,
        shirtSizes,
        clothingSizes,
        roles,
        overview,
        projectRoles,
        scenes,
        groups,
        casts,
        repetitions,
        types,
        locations,
        images,
        mail,
        selector,
        statistics,
        accessRoles,
        notifications,
        threads,
        clothingManagement,
        articles,
        projectUsers,
        settings,
        messages,
    };

    dependencies.globalStore = store;
    !dependencies.socket && (dependencies.socket = io(apiUrl, { path: '/websocket', reconnection: true }));

    return Object.entries(store).reduce((acc, [key, value]) => {
        acc[key] = value(dependencies);
        return acc;
    }, store);
};

const storeContext = React.createContext({});

export const StoreProvider = ({ children }) => {
    return <storeContext.Provider value={useLocalStore(globalStore)}>{children}</storeContext.Provider>;
};

StoreProvider.propTypes = {
    children: PropTypes.node,
};

//@TODO test if we can provide a selector and return a partial part of the store
export const useStore = storeName => {
    const store = React.useContext(storeContext);
    if (!store) {
        // this is especially useful in TypeScript so you don't need to be checking for null all the time
        throw new Error('You have forgot to use StoreProvider, shame on you.');
    }

    if (!storeName) {
        return store;
    }

    let storeToReturn = store;
    const pieces = storeName.split('.');
    pieces.forEach(piece => (storeToReturn = storeToReturn[piece]));
    return storeToReturn;
};
