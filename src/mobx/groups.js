import { observable } from 'mobx';

import resourceGenerator from './shared/resource';

export default function countries(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { axios, apiUrl, globalStore, request } = dependencies;

    return observable({
        resource: resource({
            single: 'group',
            plural: 'groups',
            labels: {
                single: 'groep',
                plural: 'groepen',
            },
            sort: 'identifier|ASC',
            form: {
                sections: [{ id: 'information' }],
                fields: [
                    {
                        type: 'text',
                        id: 'identifier',
                        required: true,
                        section: 'information',
                        translationKey: 'form.labels.identifier',
                    },
                    {
                        type: 'text',
                        id: 'name',
                        required: true,
                        section: 'information',
                        translationKey: 'form.labels.name',
                    },
                    {
                        type: 'text',
                        id: 'description',
                        section: 'information',
                        translationKey: 'form.labels.description',
                    },
                ],
            },
        }),
        async saveUsers() {
            await request(
                `/api/project/${globalStore.projects.id}/group/${this.id}/users`,
                'PUT',
                globalStore.casts.checked
            );
        },
        async fetchUsers(groupId) {
            const users = await request(`/api/project/${globalStore.projects.id}/group/${groupId}/group_users`);
            globalStore.casts.checked = users;
            return !!users.length;
        },
        async copy(groupId) {
            try {
                this.loading = true;
                await request(`/api/group/${groupId}/copy`);
                this.resource.success = 'Groep werd succesvol gekopiëerd';
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },

        loading: false,
        data: [],
        users: [],
        groups: [],
        get autoComplete() {
            return this.resource.data
                .filter(v => v)
                .map(({ id, name, identifier }) => ({ label: `${identifier} - ${name}`, value: id }));
        },
        reset() {
            delete this.id;
            delete this.name;
            delete this.description;
            delete this.identifier;
            delete this.data;
            delete this.users;
            delete this.groups;
        },
        async fetch(projectId, userId, past = true, open = false) {
            this.data = [];
            this.loading = true;

            const { data } = await request(
                `/api${projectId ? `/project/${projectId}` : ''}${
                    userId ? `/user/${userId}` : ''
                }/groups?past=${past}&open=${open}`
            );

            this.data = data;

            this.loading = false;
        },
        async fetchGroup(id, castId) {
            try {
                let url = `${apiUrl}/api/group/${id}`;
                if (castId) {
                    url += `/${castId}`;
                }
                this.loading = true;
                const group =
                    (await axios.request({
                        method: 'GET',
                        url,
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                    })).data || {};
                this.id = group.id;
                this.name = group.name;
                this.description = group.description;
                this.users = group.users || [];
                this.groups = group.groups || [];
                this.identifier = group.identifier;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async linkGroupToGroup(groupId) {
            try {
                this.loading = true;
                await axios.request({
                    method: 'POST',
                    url: `${apiUrl}/api/group/${this.id}/link_group`,
                    data: {
                        groupId,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                this.loading = false;
                this.success = 'Groep succesvol gekoppeld';
            } catch (e) {
                console.error(e);
            }
        },
        async unlinkGroupToGroup(groupId) {
            try {
                this.loading = true;
                await axios.request({
                    method: 'DELETE',
                    url: `${apiUrl}/api/group/${this.id}/link_group`,
                    data: {
                        groupId,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                this.loading = false;
                this.success = 'Groep succesvol losgekoppeld';
            } catch (e) {
                console.error(e);
            }
        },
        get filteredData() {
            if (!this.filter) {
                return this.data;
            }

            return this.data
                .filter(v => v)
                .filter(({ name }) => name.toLowerCase().includes(this.filter.toLowerCase()));
        },
    });
}
