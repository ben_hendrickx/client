import { observable } from 'mobx';

export default function overview({ request, globalStore }) {
    return observable({
        loading: null,
        projects: 0,
        employees: 0,
        online: 0,
        registrations: 0,
        onStage: 0,
        offStage: 0,
        articles: 0,
        rentals: 0,
        groups: 0,
        scenes: 0,
        roles: 0,
        repetitions: {
            total: 0,
            future: 0,
            past: 0,
        },
        async fetch(id) {
            this.loading = true;
            try {
                const data = await request(`/api/statistics/overview${id ? `/${id}` : ''}`);

                data.projects !== undefined && (this.projects = data.projects);
                data.employees !== undefined && (this.employees = data.employees);
                data.online !== undefined && (this.online = data.online);
                data.articles !== undefined && (this.articles = data.articles);
                data.rentals !== undefined && (this.rentals = data.rentals);
                data.registrations !== undefined && (this.registrations = data.registrations);
                data.onStage !== undefined && (this.onStage = data.onStage);
                data.offStage !== undefined && (this.offStage = data.offStage);
                data.groups !== undefined && (this.groups = data.groups);
                data.scenes !== undefined && (this.scenes = data.scenes);
                data.roles !== undefined && (this.roles = data.roles);
                data.repetitions !== undefined && (this.repetitions = data.repetitions);
            } catch (e) {
                console.error(e);
            }
            this.loading = false;
        },
    });
}
