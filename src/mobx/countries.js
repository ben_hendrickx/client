import { observable } from 'mobx';

export default function countries({ request }) {
    return observable({
        loading: false,
        data: [],
        async fetch() {
            if (this.data.length) {
                return;
            }

            this.loading = true;
            const data = await request(`/api/countries`);

            this.data = data;
            this.loading = false;
        },
        get autoComplete() {
            return this.data.filter(v => v)
                .map(({ name, id }) => ({
                    label: name,
                    value: id,
                }))
                .sort((a, b) => {
                    if (a.label < b.label) {
                        return -1;
                    }
                    if (a.label > b.label) {
                        return 1;
                    }
                    return 0;
                });
        },
    });
}
