/* istanbul ignore file */
import { observable } from 'mobx';
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

import categories from './categories';
import resourceGenerator from '../shared/resource';

function pad(value, length) {
    if ((value || '').toString().length >= length) {
        return value;
    }

    return pad('0' + value, length);
}

export default function clothingManagement(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { globalStore, request, socket } = dependencies;

    return observable({
        sizes: {
            resource: resource({
                single: 'clothingsize',
                plural: 'clothingsizes',
                labels: {
                    single: 'kledij-maat',
                    plural: 'kledij-maten',
                },
                filters: {
                    archived: 0,
                },
            }),
            get autoComplete() {
                return this.resource.data
                    .filter(v => v)
                    .map(({ id, size }) => ({
                        label: size,
                        value: id,
                    }));
            },
        },
        fabrics: {
            resource: resource({
                single: 'fabric',
                plural: 'fabrics',
                labels: {
                    single: 'stof',
                    plural: 'stoffen',
                },
                filters: {
                    archived: 0,
                },
            }),
            get autoComplete() {
                return this.resource.data
                    .filter(v => v)
                    .map(({ id, type }) => ({
                        label: type,
                        value: id,
                    }));
            },
        },
        patterns: {
            resource: resource({
                single: 'pattern',
                plural: 'patterns',
                labels: {
                    single: 'motief',
                    plural: 'motieven',
                },
                filters: {
                    archived: 0,
                },
            }),
            get autoComplete() {
                return this.resource.data
                    .filter(v => v)
                    .map(({ id, name }) => ({
                        label: name,
                        value: id,
                    }));
            },
        },
        colors: {
            resource: resource({
                single: 'color',
                plural: 'colors',
                labels: {
                    single: 'kleur',
                    plural: 'kleuren',
                },
                filters: {
                    archived: 0,
                },
            }),
            get autoComplete() {
                return this.resource.data
                    .filter(v => v)
                    .map(({ id, name }) => ({
                        label: name,
                        value: id,
                    }));
            },
        },
        racks: {
            resource: resource({
                single: 'rack',
                plural: 'racks',
                labels: {
                    single: 'rek',
                    plural: 'rekken',
                },
                filters: {
                    archived: 0,
                },
            }),
            get autoComplete() {
                return this.resource.data
                    .filter(v => v)
                    .map(({ id, description }) => ({
                        label: `${id} - ${description}`,
                        value: id,
                    }));
            },
        },

        orders: {
            checked: [],
            resource: resource({
                single: 'order',
                plural: 'orders',
                labels: {
                    single: 'bestelling',
                    plural: 'bestellingen',
                },
                filters: {
                    billed: 0,
                },
            }),
        },

        categories: categories(dependencies),

        active: true,
        loading: false,
        rackArticles: [],
        clothingSizes: [],
        items: [],
        progress: 0,
        text: '',
        order: {
            items: [],
        },
        amount: 1,
        filters: {},
        listen() {
            socket.on('clothing_item_progress', ({ uuid, progress, text }) => {
                if (uuid === this.uuid) {
                    this.progress = progress * 100;
                    this.text = Math.floor(progress * 100) + `% - ${text}`;
                }
            });

            return () => {};
        },
        listen_single_order(listenId) {
            socket.on('clothing_received_item', ({ orderId, id, amount, washing }) => {
                if (orderId === parseInt(listenId)) {
                    const item = this.orders.resource.form.data.items.find(item => item.id === id);
                    item.returned = amount;
                    item.washing = washing;
                }
            });
        },
        receiveItem(orderId, id, receive = true, accessoire = false) {
            socket.emit('clothing_receive_item', {
                orderId: parseInt(orderId),
                id,
                amount: !accessoire && !receive ? 0 : this.amount,
                receive,
            });
        },
        async import(items) {
            this.importing = true;
            this.progress = 0.1;
            //const promises = [];

            const uuid = uuidv4();
            this.uuid = uuid;

            await request('/api/clothing/import', 'POST', { items, uuid });

            // while (items.length > 100) {
            //     const part = items.splice(0, 100);
            //     promises.push(await request('POST', '/api/clothing/import', { items: part }));
            // }

            // await Promise.all(promises);
            this.importing = false;
        },
        async getClothingSizes() {
            this.loading = true;
            this.clothingSizes = (await request('/api/clothing/articleClothingSizes')) || [];
            this.loading = false;
        },
        async getSingleClothingSize(id) {
            this.loading = true;
            const { size } = await request('/api/clothing/clothingSizes/' + id);
            this.size = size;
            this.loading = false;
        },
        async getPatterns() {
            this.loading = true;
            this.patterns = (await request('/api/clothing/patterns')) || [];
            this.loading = false;
        },
        async getSinglePattern(id) {
            this.loading = true;
            const { name } = await request('/api/clothing/pattern/' + id);
            this.patternName = name;
            this.loading = false;
        },
        async getFabrics() {
            this.loading = true;
            this.fabrics = (await request('/api/clothing/fabrics')) || [];
            this.loading = false;
        },
        async getSingleFabric(id) {
            this.loading = true;
            const { type } = await request('/api/clothing/fabrics/' + id);
            this.fabricType = type;
            this.loading = false;
        },
        async getColors() {
            this.loading = true;
            this.colors = (await request('/api/clothing/colors')) || [];
            this.loading = false;
        },
        async getSingleColor(id) {
            this.loading = true;
            const { name } = await request('/api/clothing/color/' + id);
            this.colorName = name;
            this.loading = false;
        },
        async getRacks() {
            this.loading = true;
            this.racks = (await request('/api/clothing/racks')) || [];
            this.loading = false;
        },
        async getArticle(id) {
            this.loading = true;
            const data = (await request(`/api/article/${id}`)) || {};
            this.active = data.active;
            this.clothingId = data.clothing.id;
            this.clothingSizeId = data.size.id;
            this.patternId = data.pattern.id;
            this.fabricId = data.fabric.id;
            this.colorId = data.color.id;
            this.rackId = data.rackId;
            this.tags = (data.tags || '').split('|').filter(v => v);
            this.loading = false;
        },
        async getRack(id) {
            this.loading = true;
            const data = (await request(`/api/clothing/rack/${id}`)) || {};
            this.rackId = data.id;
            this.rackDescription = data.description;
            this.loading = false;
        },
        async getArticlesOnRack(id) {
            this.loading = true;
            this.rackArticles = (await request(`/api/clothing/rack/${id}/articles`)) || [];
            this.loading = false;
        },
        async getClothing() {
            this.loading = true;
            this.clothing = (await request('/api/clothing')) || [];
            this.loading = false;
        },
        get filteredRacks() {
            if (!this.rackFilter) {
                return this.racks;
            }

            return this.racks.filter(item => {
                if (
                    item.id
                        .toString()
                        .toLowerCase()
                        .includes(this.rackFilter.toLowerCase()) ||
                    item.description
                        .toString()
                        .toLowerCase()
                        .includes(this.rackFilter.toLowerCase())
                ) {
                    return true;
                }

                return false;
            });
        },
        get filteredFabrics() {
            if (!this.fabricFilter) {
                return this.fabrics;
            }

            return this.fabrics.filter(item => {
                if (
                    item.type
                        .toLowerCase()
                        .toString()
                        .includes(this.fabricFilter.toLowerCase())
                ) {
                    return true;
                }

                return false;
            });
        },
        get filteredClothingSizes() {
            if (!this.clothingSizesFilter) {
                return this.clothingSizes;
            }

            return this.clothingSizes.filter(item => {
                if (
                    item.size
                        .toLowerCase()
                        .toString()
                        .includes(this.clothingSizesFilter.toLowerCase())
                ) {
                    return true;
                }

                return false;
            });
        },
        get filteredPatterns() {
            if (!this.patternFilter) {
                return this.patterns;
            }

            return this.patterns.filter(item => {
                if (
                    item.name
                        .toLowerCase()
                        .toString()
                        .includes(this.patternFilter.toLowerCase())
                ) {
                    return true;
                }

                return false;
            });
        },
        get filteredColors() {
            if (!this.colorFilter) {
                return this.colors;
            }

            return this.colors.filter(item => {
                if (
                    item.name
                        .toLowerCase()
                        .toString()
                        .includes(this.colorFilter.toLowerCase())
                ) {
                    return true;
                }

                return false;
            });
        },
        get filteredArticles() {
            if (
                !this.articleFilter &&
                !this.filters.clothingId &&
                !this.filters.clothingSizeId &&
                !this.filters.fabricId &&
                !this.filters.patternId &&
                !this.filters.colorId &&
                !this.filters.rack
            ) {
                return this.articles;
            }

            function pad(value, length) {
                if ((value || '').toString().length >= length) {
                    return value;
                }

                return pad('0' + value, length);
            }

            return this.articles.filter(item => {
                if (this.filters.clothingId && item.clothing.id !== parseInt(this.filters.clothingId)) return false;
                if (this.filters.clothingSizeId && item.size.id !== parseInt(this.filters.clothingSizeId)) return false;
                if (this.filters.fabricId && item.fabric.id !== parseInt(this.filters.fabricId)) return false;
                if (this.filters.patternId && item.pattern.id !== parseInt(this.filters.patternId)) return false;
                if (this.filters.colorId && item.color.id !== parseInt(this.filters.colorId)) return false;
                if (this.filters.rack && item.rackId !== parseInt(this.filters.rack)) return false;

                if (this.articleFilter) {
                    if (
                        pad(item.id.toString(), 6).includes(this.articleFilter.toLowerCase()) ||
                        item.clothing.name
                            .toLowerCase()
                            .toString()
                            .includes(this.articleFilter.toLowerCase()) ||
                        (item.tags && item.tags.toLowerCase().includes(this.articleFilter.toLowerCase()))
                    ) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            });
        },
        get filteredRackArticles() {
            if (
                !this.articleFilter &&
                !this.filters.clothingId &&
                !this.filters.clothingSizeId &&
                !this.filters.fabricId &&
                !this.filters.patternId &&
                !this.filters.colorId
            ) {
                return this.rackArticles;
            }

            function pad(value, length) {
                if ((value || '').toString().length >= length) {
                    return value;
                }

                return pad('0' + value, length);
            }

            return this.rackArticles.filter(item => {
                if (this.filters.clothingId && item.clothing.id !== parseInt(this.filters.clothingId)) return false;
                if (this.filters.clothingSizeId && item.size.id !== parseInt(this.filters.clothingSizeId)) return false;
                if (this.filters.fabricId && item.fabric.id !== parseInt(this.filters.fabricId)) return false;
                if (this.filters.patternId && item.pattern.id !== parseInt(this.filters.patternId)) return false;
                if (this.filters.colorId && item.color.id !== parseInt(this.filters.colorId)) return false;

                if (this.articleFilter) {
                    if (
                        pad(item.id.toString(), 6).includes(this.articleFilter.toLowerCase()) ||
                        item.clothing.name
                            .toLowerCase()
                            .toString()
                            .includes(this.articleFilter.toLowerCase()) ||
                        (item.tags && item.tags.toLowerCase().includes(this.articleFilter.toLowerCase()))
                    ) {
                        return true;
                    } else {
                        return false;
                    }
                }

                return true;
            });
        },
        get price() {
            if (!this.to || !this.from) {
                return 0;
            }

            return this.orders.resource.form.data.items
                .reduce((acc, item) => {
                    acc +=
                        (item.clothing.category.price *
                            ((new Date(this.to).getTime() - new Date(this.from).getTime()) / 1000 / 60 / 60 / 24)) /
                        30;
                    return acc;
                }, 0)
                .toFixed(2);
        },
        async createSize() {
            this.loading = true;
            this.data =
                (await request('/api/clothing/clothingSizes', 'POST', {
                    size: this.size,
                })) || [];
            this.success = `Kledij-maat ${this.size} werd aangemaakt`;
            this.size = '';
            this.loading = false;
        },
        async updateSize(id) {
            this.loading = true;
            this.data =
                (await request('/api/clothing/clothingSizes/' + id, 'PUT', {
                    size: this.size,
                })) || [];
            this.success = `Kledij-maat werd gewijzigd`;
            this.size = '';
            this.loading = false;
        },
        async createFabric() {
            this.loading = true;
            this.data =
                (await request('/api/clothing/fabric', 'POST', {
                    type: this.fabricType,
                })) || [];
            this.success = `Stof ${this.fabricType} werd aangemaakt`;
            this.fabricType = '';
            this.loading = false;
        },
        async updateFabric(id) {
            this.loading = true;
            this.data =
                (await request('/api/clothing/fabric/' + id, 'PUT', {
                    type: this.fabricType,
                })) || [];
            this.success = `Stof werd gewijzigd`;
            this.fabricType = '';
            this.loading = false;
        },
        async createPattern() {
            this.loading = true;
            this.data =
                (await request('/api/clothing/pattern', 'POST', {
                    name: this.patternName,
                })) || [];
            this.success = `Motief ${this.patternName} werd aangemaakt`;
            this.patternName = '';
            this.loading = false;
        },
        async updatePattern(id) {
            this.loading = true;
            this.data =
                (await request('/api/clothing/pattern/' + id, 'PUT', {
                    name: this.patternName,
                })) || [];
            this.success = `Motief werd gewijzigd`;
            this.patternName = '';
            this.loading = false;
        },
        async createColor() {
            this.loading = true;
            this.data =
                (await request('/api/clothing/color', 'POST', {
                    name: this.colorName,
                })) || [];
            this.success = `Kleur ${this.colorName} werd aangemaakt`;
            this.colorName = '';
            this.loading = false;
        },
        async updateColor(id) {
            this.loading = true;
            this.data =
                (await request('/api/clothing/color/' + id, 'PUT', {
                    name: this.colorName,
                })) || [];
            this.success = `Kleur werd gewijzigd`;
            this.colorName = '';
            this.loading = false;
        },
        async saveArticle(id) {
            const method = id ? 'PUT' : 'POST';
            const url = id ? `/api/article/${id}` : '/api/article';
            this.loading = true;
            this.data =
                (await request(url, method, {
                    clothingId: parseInt(this.clothingId),
                    clothingSizeId: parseInt(this.clothingSizeId),
                    fabricId: parseInt(this.fabricId || 0),
                    patternId: parseInt(this.patternId || 0),
                    colorId: parseInt(this.colorId || 0),
                    rackId: parseInt(this.rackId || 0),
                    tags: this.tags || [],
                    active: this.active === false ? false : true,
                })) || [];
            this.success = `Artikel ${id ? pad(id, 6) : this.data.id} werd ${id ? 'gewijzigd' : 'aangemaakt'}`;
            this.clothingId = '';
            this.clothingSizeId = '';
            this.fabricId = '';
            this.patternId = '';
            this.colorId = '';
            this.tags = [];
            this.loading = false;
        },
        async saveRack(id) {
            const method = id ? 'PUT' : 'POST';
            const url = id ? `/api/clothing/rack/${id}` : '/api/clothing/rack';
            this.loading = true;
            this.data =
                (await request(url, method, {
                    id: parseInt(this.rackId),
                    description: this.rackDescription,
                })) || [];
            this.success = `Rek ${id ? id : this.data.id} werd ${id ? 'gewijzigd' : 'aangemaakt'}`;
            this.rackId = '';
            this.rackDescription = '';
            this.loading = false;
        },
        async getOrders() {
            this.loading = true;
            this.orders = (await request('/api/clothing/orders')) || [];
            this.loading = false;
        },
        async getPrices() {
            const { creationDt, internal, periods, items } = this.orders.resource.form.data;
            const { price, replacementPrice } = await request('/api/orders/price', 'POST', {
                startDt: creationDt ? moment(creationDt) : moment(),
                internal,
                periods,
                items,
            });
            this.orders.resource.form.data.price = price;
            this.orders.resource.form.data.replacementPrice = replacementPrice;
        },
        async getOrder(id) {
            this.loading = true;
            const {
                internal,
                type,
                name,
                socialSecurity,
                companyName,
                uniqueNumber,
                isFor,
                bankAccount,
                cellPhone,
                email,
                fromDt,
                toDt,
                periods,
                info,
                streetName,
                houseNumber,
                zipCode,
                cityName,
                bus,
                items,
                billed,
                price,
                replacementPrice,
                creationDt,
            } = (await request('/api/order/' + id)) || {};

            this.orders.resource.form.data = {
                creationDt,
                internal,
                type,
                name,
                socialSecurity,
                companyName,
                uniqueNumber,
                isFor,
                bankAccount,
                cellPhone,
                email,
                fromDt: moment(fromDt),
                toDt: moment(toDt),
                periods: periods.toString(),
                info,
                streetName,
                houseNumber,
                zipCode,
                cityName,
                bus,
                items,
                billed,
                price,
                replacementPrice,
            };

            this.loading = false;
        },
        get filteredOrders() {
            if (!this.orderFilter) {
                return this.orders;
            }

            return this.orders.filter(item => {
                if (!item.id.toString().includes(this.orderFilter.toLowerCase())) {
                    return false;
                }

                return true;
            });
        },
        get autoComplete() {
            return globalStore.articles.resource.filtered.map(({ id }) => ({ label: id.toString(), value: id }));
        },
        get autoCompleteRacks() {
            return this.racks.resource.filtered.map(({ id, description }) => ({
                label: `${id} - ${description}`,
                value: id,
            }));
        },
        get autoCompleteClothing() {
            return this.clothing.resource.filtered.map(({ id, name }) => ({ label: name, value: id }));
        },
        get autoCompleteClothingSizes() {
            return this.sizes.resource.filtered
                .map(({ id, size }) => ({ label: size.toString(), value: id }))
                .sort((a, b) => {
                    if (!isNaN(parseInt(a.label))) {
                        if (!isNaN(parseInt(b.label))) {
                            return parseInt(a.label) - parseInt(b.label);
                        }
                    }
                    return a.label > b.label ? 1 : a.label < b.label ? -1 : 0;
                });
        },
        get autoCompleteFabrics() {
            return this.fabrics.resource.filtered.map(({ id, type }) => ({ label: type, value: id }));
        },
        get autoCompletePatterns() {
            return this.patterns.resource.filtered.map(({ id, name }) => ({ label: name, value: id }));
        },
        get autoCompleteColors() {
            return this.colors.resource.filtered.map(({ id, name }) => ({ label: name, value: id }));
        },
        get autoCompleteInStock() {
            return this.articles
                .filter(({ stock }) => stock)
                .map(({ size, clothing, id }) => ({
                    label: `${id}: ${clothing.name} (Maat: ${size.size})`,
                    value: id,
                }));
        },
        async addArticle(id) {
            const { data } = this.orders.resource.form;

            !data.items && (data.items = []);

            let item = data.items.find(item => item.id === parseInt(id));
            item && item.amount++;

            if (!item) {
                globalStore.articles.resource.filters.text = id;
                const { offset, items } = globalStore.articles.resource;
                delete globalStore.articles.resource.offset;
                delete globalStore.articles.resource.items;
                const articles = await globalStore.articles.resource.fetchAndReturn();
                item = {
                    ...(articles.data.find(i => i.id === parseInt(id)) || {}),
                    amount: 1,
                };
                if (!item.id) {
                    throw new Error('Artikel niet gevonden');
                }
                data.items.push(item);
                globalStore.articles.resource.offset = offset;
                globalStore.articles.resource.items = items;
            }

            this.id = '';
            delete globalStore.articles.resource.filters.text;
        },
        removeItem(id) {
            const { data } = this.orders.resource.form;
            data.items = data.items.filter(i => i.id !== id);
        },
        async createRental() {
            try {
                const { data } = this.orders.resource.form;
                const rentalId =
                    (await request('/api/orders', 'POST', {
                        internal: data.internal || false,
                        name: data.name,
                        cellPhone: data.cellPhone,
                        email: data.email,
                        fromDt: moment(data.fromDt).format('YYYY-MM-DD'),
                        toDt: moment(data.toDt).format('YYYY-MM-DD'),
                        periods: data.periods,
                        streetName: data.streetName,
                        houseNumber: data.houseNumber,
                        zipCode: data.zipCode,
                        cityName: data.cityName,
                        items: data.items,
                        isFor: data.isFor,
                        uniqueNumber: data.uniqueNumber,
                        bankAccount: data.bankAccount,
                        socialSecurity: data.socialSecurity,
                        type: parseInt(data.type),
                        companyName: data.companyName,
                        info: data.info,
                    })).rentalId || undefined;

                this.success = 'Uitlenen van artikel(s) is gelukt';

                return rentalId;
            } catch (e) {
                this.error = e;
            }
        },
        selectedArticles: [],
        async receiveSelectedItems(id) {
            socket.emit('clothing_receive_items', {
                orderId: parseInt(id),
                ids: this.selectedArticles,
            });
        },
        async updateRental(id, body) {
            try {
                const { data } = this.orders.resource.form;
                await request(
                    `/api/order/${id}`,
                    'PUT',
                    body || {
                        internal: data.internal || false,
                        name: data.name,
                        cellPhone: data.cellPhone,
                        email: data.email,
                        fromDt: moment(data.fromDt).format('YYYY-MM-DD'),
                        toDt: moment(data.toDt).format('YYYY-MM-DD'),
                        periods: data.periods,
                        streetName: data.streetName,
                        houseNumber: data.houseNumber,
                        zipCode: data.zipCode,
                        cityName: data.cityName,
                        items: data.items,
                        isFor: data.isFor,
                        uniqueNumber: data.uniqueNumber,
                        bankAccount: data.bankAccount,
                        socialSecurity: data.socialSecurity,
                        type: parseInt(data.type),
                        companyName: data.companyName,
                        info: data.info,
                    }
                );

                this.success = 'Bestelling werd succesvol gewijzigd';
            } catch (e) {
                this.error = e;
            }
        },
        async return() {
            try {
                await request('/api/article/' + this.artNumber, 'PUT', {
                    active: true,
                    stock: 1,
                    washing: false,
                    rackId: this.rack,
                });

                this.success = 'Artikel staat gemarkeerd als "In rek"';
                this.artNumber = '';
                this.rack = '';
            } catch (e) {
                this.error = e;
            }
        },
        async washing() {
            try {
                await request('/api/article/' + this.artNumber, 'PUT', {
                    active: true,
                    stock: 1,
                    washing: true,
                    rackId: 0,
                });

                this.success = 'Artikel staat gemarkeerd als "Naar wasserij"';
                this.artNumber = '';
            } catch (e) {
                this.error = e;
            }
        },
        async remove() {
            try {
                await request('/api/article/' + this.artNumber, 'DELETE');
                this.success = 'Artikel werd verwijderd';
                this.artNumber = '';
            } catch (e) {
                this.error = e;
            }
        },
    });
}
