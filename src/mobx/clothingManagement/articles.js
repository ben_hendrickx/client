/* istanbul ignore file */
import { observable } from 'mobx';
import resourceGenerator from '../shared/resource';
import form from '../shared/form';

export default function clothingManagement(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { globalStore, request } = dependencies;

    return observable({
        suggestions: [],
        async getSuggestions() {
            // TODO: remap this to /api/articles/tags
            this.suggestions = (await request('/api/clothing/tags')) || [];
        },
        data: {},
        resource: resource({
            single: 'article',
            plural: 'articles',
            labels: {
                single: 'artikel',
                plural: 'artikels',
            },
        }),
        get form() {
            return form(this.config, this.data);
        },
        get config() {
            return {
                sections: [
                    {
                        id: 'information',
                    },
                ],
                fields: [
                    {
                        id: 'description',
                        translationKey: 'form.labels.description',
                        type: 'text',
                        required: true,
                        section: 'information',
                    },
                    {
                        id: 'categoryId',
                        translationKey: 'form.labels.clothing.category',
                        type: 'select',
                        required: true,
                        section: 'information',
                        options: globalStore.clothingManagement.categories.autoComplete,
                        //TODO: add support for hint property
                    },
                    {
                        id: 'clothingSizeId',
                        translationKey: 'form.labels.clothing.clothingSize',
                        type: 'select',
                        required: true,
                        section: 'information',
                        options: globalStore.clothingManagement.sizes.autoComplete,
                        //TODO: add support for hint property
                    },
                    {
                        id: 'patternId',
                        translationKey: 'form.labels.clothing.pattern',
                        type: 'select',
                        section: 'information',
                        options: globalStore.clothingManagement.patterns.autoComplete,
                        //TODO: add support for hint property
                    },
                    {
                        id: 'fabricId',
                        translationKey: 'form.labels.clothing.fabric',
                        type: 'select',
                        section: 'information',
                        options: globalStore.clothingManagement.fabrics.autoComplete,
                        //TODO: add support for hint property
                    },
                    {
                        id: 'colorId',
                        translationKey: 'form.labels.clothing.color',
                        type: 'select',
                        section: 'information',
                        options: globalStore.clothingManagement.colors.autoComplete,
                        //TODO: add support for hint property
                    },
                    {
                        id: 'rackId',
                        translationKey: 'form.labels.clothing.rack',
                        type: 'select',
                        section: 'information',
                        options: globalStore.clothingManagement.racks.autoComplete,
                        //TODO: add support for hint property
                    },
                    {
                        id: 'tags',
                        type: 'tags',
                        section: 'information',
                        options: this.suggestions,
                        onChange: v => {
                            this.data.accessoire = v.includes('accessoire');
                        },
                        //TODO: add support for hint property
                    },
                    {
                        id: 'accessoire',
                        translationKey: 'form.labels.clothing.accessoire',
                        type: 'checkbox',
                        section: 'information',
                        checked: () => this.data.tags?.includes('accessoire'),
                        onChange: v => {
                            if (!this.data.tags) {
                                this.data.tags = [];
                            }

                            if (v) {
                                !this.data.tags.includes('accessoire') &&
                                    (this.data.tags = ['accessoire', ...this.data.tags]);
                            } else {
                                this.data.tags = this.data.tags.filter(v => v !== 'accessoire');
                            }
                        },
                    },
                    globalStore.account.rights.canDeleteArticles && {
                        id: 'active',
                        translationKey: 'form.labels.clothing.isActive',
                        type: 'checkbox',
                        section: 'information',
                    },
                    {
                        id: 'washing',
                        translationKey: 'form.labels.clothing.isWashing',
                        type: 'checkbox',
                        section: 'information',
                    },
                ],
            };
        },
        async save() {
            const url = this.data.id ? `/api/article/${this.data.id}` : `/api/articles`;
            const method = this.data.id ? 'PUT' : 'POST';
            const action = this.data.id ? 'gewijzigd' : 'aangemaakt';
            const body = { ...this.data };
            delete body.id;
            delete body.accessoire;
            let { id } = await request(url, method, body);
            id !== undefined && (id += ' ');
            id === undefined && (id = this.data.id + ' ');
            id === undefined && (id = ' ');
            this.resource.success = `Artikel ${id}werd succesvol ${action}`;
        },
    });
}
