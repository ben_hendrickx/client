/* istanbul ignore file */
import { observable } from 'mobx';

import resourceGenerator from '../shared/resource';
import moment from 'moment';

export default function(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request } = dependencies;

    return observable({
        resource: resource({
            single: 'category',
            plural: 'categories',
            labels: {
                single: 'categorie',
                plural: 'categorieën',
            },
        }),
        filters: {
            plain: '',
        },
        priceFilters: {
            active: 1,
        },
        get filteredPrices() {
            if ([null, undefined, ''].includes(this.priceFilters.active)) {
                return this.prices;
            }

            if (this.priceFilters.active) {
                return [this.currentPrice(this.prices)];
            }

            return this.prices.filter(price => {
                if (this.priceFilters.active === 0) {
                    const startDt = new Date(price.startDt).getTime();
                    const endDt = price.endDt ? new Date(price.endDt).getTime() : null;

                    return (endDt && endDt < Date.now()) || startDt > Date.now();
                }

                return true;
            });
        },
        async createPrice(id) {
            this.loading = true;
            await request(`/api/category/${id}/prices`, 'POST', {
                startDt: (this.fromDt ? moment(this.fromDt).startOf('day') : moment(new Date())).format(
                    'YYYY-MM-DD HH:mm:ss'
                ),
                endDt: this.toDt
                    ? moment(this.toDt)
                          .startOf('day')
                          .format('YYYY-MM-DD HH:mm:ss')
                    : undefined,
                internalPrice: parseFloat(this.internalPrice),
                externalPrice: parseFloat(this.externalPrice),
                replacementPrice: parseFloat(this.replacementPrice),
            });

            this.success = `Prijs werd aangemaakt`;
            delete this.fromDt;
            delete this.toDt;
            delete this.internalPrice;
            delete this.externalPrice;
            delete this.replacementPrice;
            this.loading = false;
        },

        currentPrice(prices) {
            return prices
                ?.filter(price => {
                    return (
                        new Date(price.startDt).getTime() <= Date.now() &&
                        (!price.endDt || new Date(price.endDt).getTime() >= Date.now())
                    );
                })
                .sort((a, b) => {
                    const result = new Date(b.startDt).getTime() - new Date(a.startDt).getTime();

                    if (result === 0) {
                        return new Date(a.endDt).getTime() - new Date(b.endDt).getTime();
                    }

                    return result;
                })[0];
        },

        data: [],
        filter: '',
        get filtered() {
            if (!this.filter) {
                return this.data;
            }

            return this.data
                .filter(v => v)
                .filter(item => {
                    if (
                        !item.name
                            .toString()
                            .toLowerCase()
                            .includes(this.filter.toLowerCase())
                    ) {
                        return false;
                    }

                    return true;
                });
        },
        async fetch() {
            this.loading = true;
            this.data = ((await request('/api/categories')) || []).data.map(category => {
                return {
                    ...category,
                    get currentPrice() {
                        return category.prices
                            .filter(price => {
                                return (
                                    new Date(price.startDt).getTime() <= Date.now() &&
                                    (!price.endDt || new Date(price.endDt).getTime() >= Date.now())
                                );
                            })
                            .sort((a, b) => {
                                const result = new Date(b.startDt).getTime() - new Date(a.startDt).getTime();

                                if (result === 0) {
                                    return new Date(a.endDt).getTime() - new Date(b.endDt).getTime();
                                }

                                return result;
                            })[0];
                    },
                };
            });

            this.loading = false;
        },
        async fetchCategory(id) {
            this.loading = true;
            const data = await request(`/api/category/${id}`);

            this.id = data.id;
            this.name = data.name;
            this.prices = data.prices;

            this.loading = false;
        },
        async create() {
            this.loading = true;
            await request('/api/clothing/category', 'POST', {
                name: this.name,
                internalPrice: parseFloat(this.internalPrice),
                externalPrice: parseFloat(this.externalPrice),
                replacementPrice: parseFloat(this.replacementPrice),
            });

            this.resource.success = `Categorie ${this.name} werd aangemaakt`;
            this.name = '';
            this.internalPrice = '';
            this.externalPrice = '';
            this.replacementPrice = '';
            this.loading = false;
        },
        async save(id) {
            this.loading = true;
            this.data =
                (await request(`/api/category/${id}`, 'PUT', {
                    name: this.name,
                })) || [];

            this.resource.success = `Categorie werd gewijzigd`;
            this.name = '';
            this.loading = false;
        },
        get autoComplete() {
            return this.resource.data
                .filter(v => v)
                .map(({ id, name }) => ({
                    label: name,
                    value: id,
                }));
        },
    });
}
