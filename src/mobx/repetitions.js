import React from 'react';
import { observable } from 'mobx';
import moment from 'moment';

import resourceGenerator from './shared/resource';

export default function repetitions(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request, axios, globalStore, apiUrl, socket } = dependencies;

    return observable({
        async getAll(projectId) {
            let query = '';
            projectId && (query += `?projectId=${projectId}`);
            return await request(`/api/repetitions/all${query}`);
        },
        resource: resource({
            single: 'repetition',
            plural: 'repetitions',
            labels: {
                single: 'repetitie',
                plural: 'repetities',
            },
            filters: {
                past: 0,
            },
        }),

        loading: false,
        data: [],
        pinned: [],
        reset() {
            this.data = [];
            this.pinned = [];
            this.casts = [];
            this.scenes = [];
            this.pinning = [];
            this.presence = [];
            this.selectorData = {};

            delete this.id;
            delete this.name;
            delete this.description;
            delete this.identifier;
            delete this.startDt;
            delete this.endDt;
            delete this.date;
            delete this.start;
            delete this.end;
            delete this.register;
            delete this.notifications;
            delete this.allChecked;
            delete this.locations.value;
        },
        async fetch(projectId) {
            this.loading = true;
            const { data } = await request(`/api${projectId ? `/project/${projectId}` : ''}/repetitions`);
            this.data = data;
            this.loading = false;

            return this.data;
        },

        partsOfTown: [],
        filter: '',
        name: '',
        passed: false,
        locations: {
            data: [],
            value: '',
            get autoComplete() {
                return this.data
                    .filter(v => v)
                    .map(location => {
                        return { label: `${location.identifier}: ${location.name}`, value: location.id };
                    });
            },
        },
        types: {
            data: [],
            value: '',
            get autoComplete() {
                return this.data
                    .filter(v => v)
                    .map(type => {
                        return { label: type.name, value: type.id };
                    });
            },
        },
        casts: [],
        scenes: [],
        pinning: [],
        async fetchSameLocation(repetitionId) {
            const url = `${apiUrl}/api/repetitions/${repetitionId}/location`;
            this.loadingPinning = true;
            this.pinning =
                (await axios.request({
                    method: 'GET',
                    url,
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                })).data || [];
            this.loadingPinning = false;
        },
        async enterPin(repetitionId, pin, repetitionIds, present = true) {
            const url = `${apiUrl}/api/repetitions/${repetitionId}/enterPin`;
            try {
                const { success, firstName, lastName, message } =
                    (await axios.request({
                        method: 'POST',
                        url,
                        data: {
                            pin: pin || this.pin,
                            repetitionIds,
                            present,
                        },
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                    })).data || {};

                if (success) {
                    this.success = (
                        <>
                            Welkom,
                            <br />
                            <br />
                            <b>
                                {firstName} {lastName}
                            </b>
                        </>
                    );
                } else {
                    this.error = message;
                }
            } catch (e) {
                this.error = <b>Ongeldige pincode</b>;
            }
        },
        get all() {
            return (
                this.selectorData &&
                this.selectorData.scenes &&
                this.selectorData.scenes.every(({ checked }) => checked)
            );
        },
        get checkedUsers() {
            return this.selectorData.scenes.reduce((acc, scene) => {
                scene.groups.forEach(group => {
                    group.users.forEach(user => {
                        user.checked && !acc.includes(user.id) && acc.push(user.id);
                        !user.checked &&
                            user.casts.forEach(cast => cast.checked && !acc.includes(user.id) && acc.push(user.id));
                    });
                });

                scene.roles.forEach(role => {
                    role.users.forEach(user => {
                        user.checked && !acc.includes(user.id) && acc.push(user.id);
                        !user.checked &&
                            user.casts
                                .filter(v => v)
                                .forEach(cast => cast.checked && !acc.includes(user.id) && acc.push(user.id));
                    });
                });

                return acc;
            }, []).length;
        },
        isPartiallyChecked({ scene, group, role }) {
            function isGroupOrRoleChecked(group) {
                return (
                    group.checked ||
                    group.casts.some(({ checked }) => checked) ||
                    group.users.some(user => {
                        return user.checked || user.casts.filter(v => v).some(cast => cast.checked);
                    })
                );
            }

            if (scene) {
                return (
                    scene.groups.some(group => isGroupOrRoleChecked(group)) ||
                    scene.roles.some(role => isGroupOrRoleChecked(role))
                );
            }

            if (group) {
                return isGroupOrRoleChecked(group);
            }

            if (role) {
                return isGroupOrRoleChecked(role);
            }

            return false;
        },
        get checkedData() {
            return this.selectorData.scenes.reduce((acc, scene) => {
                scene.groups.forEach(group => {
                    group.casts.forEach(cast => {
                        if (scene.checked || group.checked || cast.checked) {
                            acc.push({ sceneId: scene.id, groupId: group.id, castId: cast.id });
                        } else {
                            group.users.forEach(user => {
                                user.casts.forEach(userCast => {
                                    if (userCast.id === cast.id) {
                                        (user.checked || cast.checked) &&
                                            acc.push({
                                                sceneId: scene.id,
                                                groupId: group.id,
                                                castId: cast.id,
                                                userId: user.id,
                                            });
                                    }
                                });
                            });
                        }
                    });
                });

                scene.roles.forEach(role => {
                    role.casts.forEach(cast => {
                        if (scene.checked || role.checked || cast.checked) {
                            acc.push({ sceneId: scene.id, roleId: role.id, castId: cast.id });
                        } else {
                            role.users.forEach(user => {
                                user.casts.forEach(userCast => {
                                    if (userCast.id === cast.id) {
                                        (user.checked || cast.checked) &&
                                            acc.push({
                                                sceneId: scene.id,
                                                roleId: role.id,
                                                castId: cast.id,
                                                userId: user.id,
                                            });
                                    }
                                });
                            });
                        }
                    });
                });

                return acc;
            }, []);
        },
        async fetchPresence(repetitionId) {
            const url = `${apiUrl}/api/repetitions/${repetitionId}/presence`;
            this.presence = (
                (await axios.request({
                    method: 'GET',
                    url,
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                })).data || []
            ).sort((a, b) => {
                const aL = a.lastName.toLowerCase();
                const bL = b.lastName.toLowerCase();

                return aL > bL ? 1 : aL < bL ? -1 : 0;
            });
        },
        async save() {
            const {
                identifier,
                description,
                name,
                date,
                start,
                end,
                locations: { value: locationId },
                types: { value: typeId },
                register,
                notifications,
            } = this;

            const [startH, startM] = moment(start)
                .format('HH:mm')
                .split(':')
                .map(v => parseInt(v));
            const [endH, endM] = moment(end)
                .format('HH:mm')
                .split(':')
                .map(v => parseInt(v));

            let startDt = moment(new Date(new Date(date).setHours(startH, startM, 0, 0)));
            let endDt = moment(new Date(new Date(date).setHours(endH, endM, 0, 0)));

            const method = this.id ? 'PUT' : 'POST';
            const url = `${apiUrl}/api/repetitions${this.id ? `/${this.id}` : ''}`;
            await axios.request({
                method,
                url,
                data: {
                    identifier,
                    description,
                    name,
                    startDt,
                    endDt,
                    locationId,
                    register,
                    notifications,
                    typeId,
                    projectId: globalStore.projects.id,
                    data: globalStore.selector.checkedData,
                    all: globalStore.selector.allChecked,
                    cast: globalStore.selector.castChecked,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${globalStore.account.token}`,
                },
            });

            this.resource.success = this.id ? `Repetitie ${name} werd gewijzigd` : `Repetitie ${name} werd aangemaakt`;

            await this.fetch();
        },
        async markPresent(repetitionId, present, userId) {
            const url = `${apiUrl}/api/repetitions/${repetitionId}/mark-presence`;
            const headers = {
                'Content-Type': 'application/json',
            };

            if (!userId) {
                headers.Authorization = `Bearer ${globalStore.account.token}`;
            }

            await axios.request({
                method: 'PUT',
                url,
                data: {
                    present,
                    userId,
                },
                headers,
            });

            if (!userId) {
                this.success = present
                    ? 'U bent weer aangemeld voor deze repetitie'
                    : 'U bent nu afgemeld voor deze repetitie';
                this.data = this.data.map(r => {
                    if (r.id !== repetitionId) {
                        return r;
                    }

                    r.presence = present;
                    return r;
                });
            } else {
                await this.fetch(undefined, globalStore.account.id);
            }
        },
        get autoComplete() {
            return this.data.filter(v => v).map(({ id, name }) => ({ label: name, value: id }));
        },
        get filteredData() {
            return this.data
                .filter(v => v)
                .filter(({ name, endDt }) => {
                    if (!this.passed) {
                        if (new Date(endDt).getTime() < Date.now()) {
                            return false;
                        }
                    }

                    return !this.filter || name.toLowerCase().includes(this.filter.toLowerCase());
                });
        },
        async copy(repetitionId) {
            try {
                this.loading = true;
                await axios.request({
                    method: 'GET',
                    url: `${apiUrl}/api/repetitions/copy/${repetitionId}`,
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async delete(repetitionId) {
            if (Array.isArray(repetitionId)) {
                try {
                    this.loading = true;
                    await axios.request({
                        method: 'DELETE',
                        url: `${apiUrl}/api/repetitions`,
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                        data: {
                            ids: repetitionId,
                        },
                    });
                    this.loading = false;
                } catch (e) {
                    console.error(e);
                }
            } else {
                try {
                    this.loading = true;
                    await axios.request({
                        method: 'DELETE',
                        url: `${apiUrl}/api/repetition/${repetitionId}`,
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                    });
                    this.loading = false;
                } catch (e) {
                    console.error(e);
                }
            }
        },

        async fetchSelectorData(projectId, repetitionId) {
            let url = `${apiUrl}/api/repetitions/selectorData?projectId=${projectId}`;

            repetitionId && (url += `&repetitionId=${repetitionId}`);

            try {
                this.loading = true;
                const selectorData =
                    (await axios.request({
                        method: 'GET',
                        url,
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                    })).data || {};

                selectorData.checked.forEach(i => {
                    if (!i.sceneId) {
                        if (i.userId) {
                            globalStore.selector.checkedUsers.push(i.userId);
                        } else if (i.groupId) {
                            globalStore.selector.checkedGroups.push(i);
                        } else if (i.roleId) {
                            globalStore.selector.checkedRoles.push(i);
                        }
                    } else {
                        if (i.roleId) {
                            globalStore.selector.checkedSceneRoles.push(i);
                        } else if (i.groupId) {
                            globalStore.selector.checkedSceneGroups.push(i);
                        } else {
                            globalStore.selector.checkedScenes.push(i);
                        }
                    }
                });

                this.selectorData = selectorData;
            } catch (e) {
                console.error(e);
            }
        },
        async fetchLocations() {
            try {
                this.locations.data =
                    (
                        (await axios.request({
                            method: 'GET',
                            url: `${apiUrl}/api/locations`,
                            headers: {
                                'Content-Type': 'application/json',
                                Authorization: `Bearer ${globalStore.account.token}`,
                            },
                        })).data || {}
                    ).data || [];
            } catch (e) {
                console.error(e);
            }
        },
        async fetchTypes() {
            try {
                this.types.data =
                    (
                        (await axios.request({
                            method: 'GET',
                            url: `${apiUrl}/api/types`,
                            headers: {
                                'Content-Type': 'application/json',
                                Authorization: `Bearer ${globalStore.account.token}`,
                            },
                        })).data || {}
                    ).data || [];
            } catch (e) {
                console.error(e);
            }
        },
        async open(ids) {
            try {
                this.loading = true;
                this.types.data =
                    (
                        (await axios.request({
                            method: 'PUT',
                            url: `${apiUrl}/api/repetitions/open`,
                            data: {
                                ids,
                            },
                            headers: {
                                'Content-Type': 'application/json',
                                Authorization: `Bearer ${globalStore.account.token}`,
                            },
                        })).data || {}
                    ).data || [];
            } catch (e) {
                console.error(e);
            }
            this.loading = false;
        },
        async close(ids) {
            try {
                this.loading = true;
                this.types.data =
                    (
                        (await axios.request({
                            method: 'PUT',
                            url: `${apiUrl}/api/repetitions/close`,
                            data: {
                                ids,
                            },
                            headers: {
                                'Content-Type': 'application/json',
                                Authorization: `Bearer ${globalStore.account.token}`,
                            },
                        })).data || {}
                    ).data || [];
            } catch (e) {
                console.error(e);
            }
            this.loading = false;
        },
        presence: [],
        get presenceFiltered() {
            if (!this.userFilter && !this.presenceFilter && !this.roleFilter) {
                return this.presence;
            }

            return this.presence.filter(user => {
                if (this.userFilter) {
                    if (
                        !user.firstName.toLowerCase().includes(this.userFilter.toLowerCase()) &&
                        !user.lastName.toLowerCase().includes(this.userFilter.toLowerCase())
                    ) {
                        return false;
                    }
                }

                if (this.roleFilter) {
                    if (user.isRoleUser && !parseInt(this.roleFilter)) {
                        return false;
                    }

                    if (!user.isRoleUser && parseInt(this.roleFilter)) {
                        return false;
                    }
                }

                if (!this.presenceFilter) {
                    return true;
                }

                if (this.presenceFilter) {
                    switch (parseInt(this.presenceFilter)) {
                        case 1:
                            return (
                                user.present ||
                                (!this.register &&
                                    new Date(this.startDt).getTime() + 15 * 60 * 1000 < Date.now() &&
                                    user.markedPresent)
                            );
                        case 0:
                            return !user.present && !user.markedPresent;
                        case 2:
                            return (
                                this.register &&
                                new Date(this.startDt).getTime() + 15 * 60 * 1000 < Date.now() &&
                                user.markedPresent &&
                                !user.present
                            );
                        case 3:
                            return (
                                new Date(this.startDt).getTime() + 15 * 60 * 1000 > Date.now() &&
                                user.markedPresent &&
                                !user.present
                            );
                        default:
                            return false;
                    }
                }
                return false;
            });
        },
        async fetchRepetition(id) {
            try {
                this.loading = true;
                const repetition =
                    (await axios.request({
                        method: 'GET',
                        url: `${apiUrl}/api/repetition/${id}`,
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                    })).data || {};
                this.id = repetition.id;
                this.name = repetition.name;
                this.description = repetition.description;
                this.identifier = repetition.identifier;
                this.startDt = repetition.startDt;
                this.endDt = repetition.endDt;
                this.date = new Date(repetition.startDt.split('T')[0]);
                this.start = repetition.startDt;
                this.end = repetition.endDt;
                this.register = repetition.register;
                this.notifications = repetition.notifications;
                this.allChecked = repetition.allChecked;
                this.locations.value = repetition.locationId;
                this.types.value = repetition.typeId;
                this.loading = false;
                return repetition;
            } catch (e) {
                console.error(e);
            }
        },
        async fetchScenes(id) {
            try {
                const scenes =
                    (await axios.request({
                        method: 'GET',
                        url: `${apiUrl}/api/repetition/${id}/scenes`,
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                    })).data || [];
                this.scenes = scenes;
            } catch (e) {
                console.error(e);
            }
        },
        async fetchPartsOfTown() {
            try {
                const partsOfTown =
                    (await axios.request({
                        method: 'GET',
                        url: `${apiUrl}/api/parts-of-town`,
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                    })).data || [];

                partsOfTown.unshift({ id: '', name: 'Ik woon niet in Geel' });
                this.partsOfTown = partsOfTown.map(({ id, name }) => ({ label: name, value: id }));
            } catch (e) {
                console.error(e);
            }
        },
        listen(repetitionId) {
            socket.off('repetition_presence');
            socket.emit('repetition_presence', { repetitionId });

            socket.on('repetition_presence', data => {
                if (parseInt(repetitionId) === data.repetitionId) {
                    this.presence = data.presence.sort((a, b) => {
                        if (a.lastName < b.lastName) {
                            return -1;
                        }
                        if (a.lastName > b.lastName) {
                            return 1;
                        }
                        if (a.firstName < b.firstName) {
                            return -1;
                        }
                        if (a.firstName > b.firstName) {
                            return 1;
                        }
                        return 0;
                    });
                }
            });
        },

        async fetchRepetitionsForUser(projectId, userId) {
            this.loading = true;
            this.repetitions = await request(`/api/project/${projectId}/user/${userId}/repetitions`);
            this.loading = false;
        },
    });
}
