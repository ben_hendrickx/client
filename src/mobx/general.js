import { observable } from 'mobx';
import { v4 as uuidV4 } from 'uuid';

export default function general({ request, globalStore }) {
    return observable({
        data: {},
        searchOpen: false,
        async uploadFile(file) {
            const formData = new FormData();
            formData.append('file', file, file.name);

            await request('/api/upload', 'POST', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });
        },
        async search(search) {
            this.data = {};

            if (!search) {
                return;
            }

            const results = await request('/api/general/search', 'POST', { search });

            const resultsPerType = results.reduce((acc, result) => {
                if (!acc[result.type]) {
                    acc[result.type] = [];
                }

                acc[result.type].push(result);
                return acc;
            }, {});

            this.data = resultsPerType;
            this.searchOpen = true;
        },
        get hasSearchResults() {
            return Object.keys(this.searchResults).length > 0;
        },
        get searchResults() {
            const results = { ...this.data };

            if (!globalStore.account.rights.canReadProjects) {
                delete results.project;
            }

            return results;
        },
        get resultCount() {
            return Object.values(this.searchResults).reduce((acc, results) => (acc += results.length), 0);
        },

        success: '',
        error: '',

        modals: [],
        addModal(data) {
            // TODO: add safe system for no duplicate modals
            // const modalWithSameTitle = this.modals.find(modal => data.title === modal.title);

            // if (modalWithSameTitle) {
            //     modalWithSameTitle.maximize();
            //     return;
            // }

            const id = uuidV4();
            data.close = async () => {
                const modalIndex = this.modals.findIndex(modal => id === modal.id);
                const modal = this.modals[modalIndex];
                if (modal) {
                    modal.onClose && (await modal.onClose());
                    this.modals.splice(this.modals.findIndex(modal => id === modal.id), 1);
                }
            };
            data.minimize = async () => {
                const modalIndex = this.modals.findIndex(modal => id === modal.id);
                const modal = this.modals[modalIndex];
                modal.onMinimize && (modal.data = { ...((await modal.onMinimize(modal)) || {}) });
                this.modals.splice(modalIndex, 1, { ...modal, minimized: true });
            };
            data.maximize = async () => {
                const modalIndex = this.modals.findIndex(modal => id === modal.id);
                const modal = this.modals[modalIndex];
                modal.onMaximize && (await modal.onMaximize(modal));
                this.modals.splice(modalIndex, 1, { ...modal, minimized: false });
            };
            data.setDisabled = async disabled => {
                const modalIndex = this.modals.findIndex(modal => id === modal.id);
                const modal = this.modals[modalIndex];
                this.modals.splice(modalIndex, 1, { ...modal, disabled });
            };
            this.modals.push({ ...data, id });
            return data;
        },
    });
}
