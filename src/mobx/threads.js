import { observable } from 'mobx';
import form from './shared/form';
import resourceGenerator from './shared/resource';

export default function threads(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { request, globalStore, socket } = dependencies;

    const { account, projects } = globalStore;

    const store = observable({
        message: '',
        threads: [],
        messages: [],
        listening: [],
        actions: {
            async fetch(id) {
                try {
                    const action = await request(`/api/threads/action/${id}`);
                    this.data = action;
                } catch (e) {
                    console.error(e);
                }
            },
            async getSuggestions() {
                const { data: users } = await request('/api/users?items=all&threads=true');
                this.suggestions = users.map(
                    ({ id, firstName, lastName, email }) => `${lastName} ${firstName} | ${email} (${id})`
                );
            },
            async save() {
                const url = this.data.id ? `/api/threads/action/${this.data.id}` : `/api/threads/actions`;
                const method = this.data.id ? 'PUT' : 'POST';
                const action = this.data.id ? 'gewijzigd' : 'aangemaakt';
                const body = { ...this.data };
                delete body.id;

                body.responsible = body.responsible?.map(responsible =>
                    parseInt(responsible.split('(')[1].replace(')', ''))
                );
                body.participants = body.participants?.map(participant =>
                    parseInt(participant.split('(')[1].replace(')', ''))
                );

                let { id } = await request(url, method, body);
                id !== undefined && (id += ' ');
                id === undefined && (id = this.data.id + ' ');
                id === undefined && (id = ' ');

                this.resource.success = `Gespreksonderwerp ${id}werd succesvol ${action}`;
                this.resource.data = [];
                this.resource.status = [];
                this.resource.fetch('/threads');
            },
            suggestions: [],
            resource: resource({
                single: 'action',
                plural: 'actions',
                labels: {
                    single: 'actie',
                    plural: 'acties',
                },
            }),
            data: {},
            get form() {
                return form(this.config, this.data);
            },
            get config() {
                return {
                    sections: [
                        {
                            id: 'information',
                            translationKey: 'form.sections.basicInfo',
                        },
                        {
                            id: 'users',
                            translationKey: 'form.sections.participants',
                        },
                    ],
                    fields: [
                        {
                            id: 'name',
                            translationKey: 'form.labels.thread.subject',
                            type: 'text',
                            required: true,
                            validations: [{ type: 'length', min: 3, max: 30 }],
                            section: 'information',
                        },
                        {
                            translationKey: 'form.labels.thread.firstMessage',
                            placeholder: 'form.placeholder.defaultMessage',
                            id: 'defaultMessage',
                            type: 'text',
                            section: 'information',
                        },
                        {
                            translationKey: 'form.labels.thread.responsible',
                            placeholder: 'form.placeholder.typeAName',
                            id: 'responsible',
                            type: 'tags',
                            section: 'users',
                            options: this.suggestions,
                            onChange: v => {
                                return v.map(v => {
                                    if (!v.includes('|')) {
                                        return v;
                                    }

                                    const [name, , id] = v
                                        // eslint-disable-next-line no-useless-escape
                                        .split(/[\(\|\)]/g)
                                        .filter(v => v)
                                        .map(v => v.trim());
                                    return `${name} (${id})`;
                                });
                            },
                        },
                        {
                            translationKey: 'form.labels.thread.participants',
                            placeholder: 'form.placeholder.typeAName',
                            id: 'participants',
                            type: 'tags',
                            section: 'users',
                            options: this.suggestions,
                            onChange: v => {
                                return v.map(v => {
                                    if (!v.includes('|')) {
                                        return v;
                                    }

                                    const [name, , id] = v
                                        // eslint-disable-next-line no-useless-escape
                                        .split(/[\(\|\)]/g)
                                        .filter(v => v)
                                        .map(v => v.trim());
                                    return `${name} (${id})`;
                                });
                            },
                        },
                    ],
                };
            },
        },
        async save(apiPrefix) {
            const url = this.data.id ? `/api${apiPrefix}/threads/${this.data.id}` : `/api${apiPrefix}/threads`;
            const method = this.data.id ? 'PUT' : 'POST';
            const body = { ...this.data };
            delete body.id;

            body.responsible = body.responsible?.map(responsible =>
                parseInt(responsible.split('(')[1].replace(')', ''))
            );
            body.participants = body.participants?.map(participant =>
                parseInt(participant.split('(')[1].replace(')', ''))
            );

            let { id } = await request(url, method, body);
            id !== undefined && (id += ' ');
            id === undefined && (id = this.data.id + ' ');
            id === undefined && (id = ' ');

            this.resource.data = [];
            this.resource.status = [];
            this.resource.fetch(apiPrefix);
        },
        data: {},
        get form() {
            return form(this.config, this.data);
        },
        get config() {
            return {
                sections: [
                    !this.data.id && {
                        id: 'information',
                        translationKey: 'form.sections.basicInfo',
                    },
                    {
                        id: 'users',
                        translationKey: 'form.sections.participants',
                    },
                ],
                fields: [
                    !this.data.id && {
                        id: 'actionId',
                        translationKey: 'form.labels.thread.subject',
                        type: 'select',
                        required: true,
                        section: 'information',
                        options: store.actions.resource.data
                            .filter(v => v)
                            .map(({ id, name }) => ({
                                label: name,
                                value: id,
                            })),
                        //TODO: add support for hint property
                    },
                    !this.data.id && {
                        id: 'projectId',
                        translationKey: 'form.labels.thread.project',
                        type: 'select',
                        section: 'information',
                        options: projects.resource.filtered.map(({ id, name }) => ({
                            label: name,
                            value: id,
                        })),
                        //TODO: add support for hint property
                    },
                    !this.data.id &&
                        store.data.actionId !== undefined && {
                            translationKey: 'form.labels.thread.firstMessage',
                            placeholder: 'form.placeholder.defaultMessage',
                            id: 'defaultMessage',
                            type: 'text',
                            section: 'information',
                        },
                    store.data.actionId !== undefined && {
                        translationKey: 'form.labels.thread.responsible',
                        placeholder: 'form.placeholder.typeAName',
                        id: 'responsible',
                        type: 'tags',
                        section: 'users',
                        options: this.actions.suggestions,
                        onChange: v => {
                            return v.map(v => {
                                if (!v.includes('|')) {
                                    return v;
                                }

                                const [name, , id] = v
                                    // eslint-disable-next-line no-useless-escape
                                    .split(/[\(\|\)]/g)
                                    .filter(v => v)
                                    .map(v => v.trim());
                                return `${name} (${id})`;
                            });
                        },
                    },
                    store.data.actionId !== undefined && {
                        translationKey: 'form.labels.thread.participants',
                        placeholder: 'form.placeholder.typeAName',
                        id: 'participants',
                        type: 'tags',
                        section: 'users',
                        options: this.actions.suggestions,
                        onChange: v => {
                            return v.map(v => {
                                if (!v.includes('|')) {
                                    return v;
                                }

                                const [name, , id] = v
                                    // eslint-disable-next-line no-useless-escape
                                    .split(/[\(\|\)]/g)
                                    .filter(v => v)
                                    .map(v => v.trim());
                                return `${name} (${id})`;
                            });
                        },
                    },
                ],
            };
        },
        resource: resource({
            single: 'thread',
            plural: 'threads',
            labels: {
                single: 'gesprek',
                plural: 'gesprekken',
            },
            filters: {
                archive: 0,
            },
        }),
        async upload() {
            const formData = new FormData();
            formData.append('file', this.selectedFile, this.selectedFile.name);

            await request('/api/upload', 'POST', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });
        },
        async send() {
            try {
                this.sending = true;
                this.iconPicker = false;

                this.selectedFile && (await this.upload());
                await request(`/api/user/${account.id}/threads/${this.selectedThread.id}/messages`, 'POST', {
                    message: this.message,
                    replyOn: this.replying?.id,
                    file: this.selectedFile?.name,
                });

                delete this.replying;
                delete this.selectedFile;
                delete this.message;
            } catch (e) {
            } finally {
                this.sending = false;
            }
        },
        async get(threadId) {
            const { messages, participants, responsible } = await request(
                `/api/user/${account.id}/threads/${threadId}`
            );

            const thread = this.resource.data.find(t => t.id === threadId);
            thread.messages = messages;
            thread.participants = participants;
            thread.responsible = responsible;

            socket.emit('message_read', { threadId, userId: account.id });
            socket.emit('threads_unread', globalStore.account.id);
        },
        isListening: false,
        listen() {
            if (!globalStore.account.id) {
                return;
            }

            socket.on('threads_unread', function(count) {
                globalStore.threads.unread = count;
            });
            socket.emit('threads_unread', globalStore.account.id);

            socket.on('message_received', message => {
                socket.emit('threads_unread', globalStore.account.id);
                const thread = this.resource.data.find(t => t.id === message.threadId);
                if (thread) {
                    thread.lastMessage = message;
                    !thread.messages && (thread.messages = []);
                    thread.messages.push(message);
                    thread.unread = true;
                    if (this.selectedThread?.id === thread.id) {
                        thread.unread = false;
                        socket.emit('message_read', { threadId: thread.id, userId: account.id });
                        socket.emit('threads_unread', globalStore.account.id);
                    }
                }
            });

            socket.on('thread_update', ({ threadId, status }) => {
                const thread = this.resource.data.find(t => t.id === threadId);
                if (thread) {
                    thread.unread = false;
                    thread.messages = thread.messages?.map(message => {
                        message.status = status;
                        return message;
                    });
                }
            });
        },
    });

    return store;
}
