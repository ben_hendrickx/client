import { observable } from 'mobx';
import resourceGenerator from './shared/resource';

export default function registrations(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { axios, globalStore, apiUrl } = dependencies;

    return observable({
        resource: resource({
            single: 'location',
            plural: 'locations',
            labels: {
                single: 'locatie',
                plural: 'locaties',
            },
            form: {
                sections: [
                    {
                        id: 'information',
                    }
                ],
                fields: [
                    {
                        id: 'name',
                        translationKey: 'form.labels.name',
                        type: 'text',
                        required: true,
                        validations: [{ type: 'length', min: 3, max: 30 }],
                        section: 'information',
                        //TODO: add support for hint property
                    },
                    {
                        id: 'identifier',
                        translationKey: 'form.labels.abbreviation',
                        type: 'text',
                        required: true,
                        validations: [{ type: 'length', min: 1, max: 5 }],
                        section: 'information',
                        //TODO: add support for hint property
                    },
                    {
                        id: 'address',
                        translationKey: 'form.labels.address',
                        type: 'address',
                        required: true,
                        section: 'information',
                        //TODO: add support for hint property
                    },
                ],
            },
        }),
        loading: false,
        async fetchLocation(id) {
            try {
                this.loading = true;
                const location =
                    (await axios.request({
                        method: 'GET',
                        url: `${apiUrl}/api/location/${id}`,
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${globalStore.account.token}`,
                        },
                    })).data || {};
                this.id = location.id;
                this.name = location.name;
                this.identifier = location.identifier;
                this.streetName = location.streetName;
                this.houseNumber = location.houseNumber;
                this.cityName = location.cityName;
                this.zipCode = location.zipCode;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
    });
}
