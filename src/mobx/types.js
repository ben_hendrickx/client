import { observable } from 'mobx';
import resourceGenerator from './shared/resource';

export default function registrations(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { axios, globalStore, apiUrl } = dependencies;
    return observable({
        resource: resource({
            single: 'type',
            plural: 'types',
            labels: {
                single: 'repetitie-type',
                plural: 'repetitie-types',
            },
            form: {
                sections: [
                    {
                        id: 'information',
                    },
                ],
                fields: [
                    {
                        id: 'name',
                        translationKey: 'form.labels.name',
                        type: 'text',
                        required: true,
                        validations: [{ type: 'length', min: 3, max: 30 }],
                        section: 'information',
                        //TODO: add support for hint property
                    },
                ],
            },
        }),

        data: [],
        loading: false,
        users: {
            data: [],
            loading: false,
        },
        async fetch() {
            let url = `${apiUrl}/api/types`;

            try {
                this.loading = true;
                this.data =
                    (
                        (await axios.request({
                            method: 'GET',
                            url,
                            headers: {
                                'Content-Type': 'application/json',
                                Authorization: `Bearer ${globalStore.account.token}`,
                            },
                        })).data || {}
                    ).data || [];
                this.loading = false;
                return this.data;
            } catch (e) {
                console.error(e);
            }
        },
        async save() {
            const { name } = this;

            const method = this.id ? 'PUT' : 'POST';
            const url = `${apiUrl}/api/type${this.id ? `/${this.id}` : 's'}`;
            await axios.request({
                method,
                url,
                data: {
                    name,
                },
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${globalStore.account.token}`,
                },
            });

            this.success = this.id ? `Repetitie-type ${name} werd gewijzigd` : `Repetitie-type ${name} werd aangemaakt`;

            await this.fetch();
        },
        get filteredData() {
            if (!this.filter) {
                return this.data;
            }

            return this.data
                .filter(v => v)
                .filter(({ name }) => name.toLowerCase().includes(this.filter.toLowerCase()));
        },
    });
}
