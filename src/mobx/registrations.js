import { observable } from 'mobx';

import resourceGenerator from './shared/resource';

export default function registrations(dependencies) {
    const resource = resourceGenerator(dependencies);
    const { axios, globalStore, apiUrl, request } = dependencies;

    return observable({
        resource: resource({
            single: 'registration',
            plural: 'registrations',
            labels: {
                single: 'inschrijving',
                plural: 'inschrijvingen',
            },
            sort: 'user.lastName|ASC',
        }),
        data: [],
        dataMissing: [],
        name: '',
        filter: '',
        filterMissing: '',
        filters: {
            onStage: '',
            auditioning: '',
            remarks: '',
            talents: '',
            reset: function() {
                this.onStage = '';
                this.auditioning = '';
                this.remarks = '';
                this.talents = '';
            },
            getString: function() {
                const data = [];

                if (this.onStage) {
                    data.push(`onStage=${this.onStage}`);
                }

                if (this.auditioning) {
                    data.push(`auditioning=${this.auditioning}`);
                }

                if (this.remarks) {
                    data.push(`remarks=${this.remarks}`);
                }

                if (this.talents) {
                    data.push(`talents=${this.talents}`);
                }

                return data.join('&');
            },
        },
        sort: 'A-Z',
        async saveOthers(registration) {
            try {
                await axios.request({
                    method: 'PUT',
                    url: `${apiUrl}/api/registrations/others`,
                    data: {
                        id: registration.id,
                        ownerRemarks: registration.newOwnerRemarks || undefined,
                        talents: registration.newTalents || undefined,
                        remarks: registration.newRemarks || undefined,
                        partOfTown: registration.newPartOfTown || undefined,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                this.success = `De gegevens werden succesvol aangepast`;
            } catch (e) {
                this.error =
                    typeof e.response === 'object' && e.response.data
                        ? typeof e.response.data === 'object'
                            ? e.response.data.message
                            : e.response.data
                        : e.message;
                throw new Error();
            }
        },
        async saveRoles() {
            let url = `${apiUrl}/api/registrations/${this.selectedRegistration.id}/roles`;

            try {
                await axios.request({
                    method: 'PUT',
                    url,
                    data: {
                        roles: this.selectedRoles,
                        auditioning: this.auditioning,
                    },
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                this.success = 'Gegevens succesvol gewijzigd';
            } catch (e) {
                this.error = e.message;
            }
        },
        async fetchRegistration(id) {
            try {
                this.loading = true;
                this.selectedRegistration = await request(`/api/registration/${id}`);
                globalStore.projects.resource.item = this.selectedRegistration.project;
                this.auditioning = this.selectedRegistration.auditioning;
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },
        async signOff(registrationId, userId) {
            let url = `${apiUrl}/api/registrations/signoff/${registrationId}`;

            userId && (url += '/' + userId);

            try {
                this.loading = true;
                await axios.request({
                    method: 'DELETE',
                    url,
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${globalStore.account.token}`,
                    },
                });
                this.loading = false;
            } catch (e) {
                console.error(e);
            }
        },

        // VERIFIED
        async fetchRolesForRegistration(registrationId) {
            const roles = await request(`/api/registration/${registrationId}/roles`);
            this.selectedRegistration.roles = roles;
        },
    });
}
