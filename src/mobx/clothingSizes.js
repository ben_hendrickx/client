import { observable } from 'mobx';

export default function countries({ request }) {
    return observable({
        loading: false,
        data: [],
        async fetch() {
            if (this.data.length) {
                return;
            }

            this.loading = true;
            const data = await request(`/api/sizes/clothing`);

            this.data = data;
            this.loading = false;
        },
        get autoComplete() {
            return this.data
                .filter(v => v)
                .map(({ size }) => ({
                    label: size,
                    value: size,
                }));
        },
    });
}
