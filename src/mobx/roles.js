import { observable } from 'mobx';

export default function countries({ request }) {
    return observable({
        loading: false,
        data: [],
        async fetch() {
            if (this.data.length) {
                return;
            }

            this.loading = true;

            const data = await request(`/api/roles`);

            this.data = data;

            this.loading = false;
        },
        get onStage() {
            return this.data.filter(v => v).filter(({ onStage }) => onStage);
        },
        get offStage() {
            return this.data.filter(v => v).filter(({ onStage }) => !onStage);
        },
        get autoComplete() {
            return this.data.filter(v => v).map(({ role, id }) => ({
                label: role,
                value: id,
            }));
        },
        get onStageAutoComplete() {
            return this.onStage.map(({ role, id, for_extended }) => ({
                label: role,
                value: id,
                for_extended,
            }));
        },
        get offStageAutoComplete() {
            return this.offStage.map(({ role, id, for_extended }) => ({
                label: role,
                value: id,
                for_extended,
            }));
        },
    });
}
