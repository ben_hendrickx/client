import { observable } from 'mobx';
import form from './shared/form';

export default function settings({ request }) {
    return observable({
        data: {
            title: '',
            description: '',
        },
        config: {
            sections: [{ id: 'general' }],
            fields: [
                {
                    id: 'title',
                    required: true,
                    type: 'text',
                    translationKey: 'form.labels.settings.title',
                    section: 'general',
                },
                {
                    id: 'description',
                    required: true,
                    type: 'text',
                    translationKey: 'form.labels.settings.description',
                    section: 'general',
                    inputProps: {
                        inputProps: {
                            rows: 5,
                        },
                        multiline: true,
                    },
                },
            ],
        },
        get form() {
            return form(this.config, this.data);
        },
        async save() {
            await request('/api/settings', 'PUT', this.data);
            this.success = 'Instellingen werden succesvol gewijzigd';
        },
        async fetch() {
            this.data = await request('/api/settings');
        },
    });
}
