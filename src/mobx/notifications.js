import { observable } from 'mobx';
import resourceGenerator from './shared/resource';

export default function notifications(dependencies) {
    const { globalStore, socket } = dependencies;
    const resource = resourceGenerator(dependencies);

    return observable({
        resource: resource({
            single: 'notification',
            plural: 'notifications',
            labels: {
                single: 'melding',
                plural: 'meldingen',
            },
            filters: {
                read: 0,
            },
        }),
        get categoryAutoComplete() {
            return [
                globalStore.account.rights.canReadNotificationsNewUsers && { label: 'Nieuwe gebruiker', value: 1 },
                globalStore.account.rights.canReadNotificationsRegistrationUpdated && {
                    label: 'Wijziging inschrijving',
                    value: 2,
                },
                globalStore.account.rights.canReadNotificationsRegistrationDeleted && {
                    label: 'Uitschrijving',
                    value: 3,
                },
                globalStore.account.rights.canReadNotificationsRepetitionSignoff && {
                    label: 'Afmelding voor repetitie',
                    value: 4,
                },
                globalStore.account.rights.canReadNotificationsClothingChanged && {
                    label: 'Wijzigingen in kledijbeheer',
                    value: 5,
                },
                globalStore.account.rights.canReadNotificationsContactdetailsChanged && {
                    label: 'Wijziging contactgegevens',
                    value: 6,
                },
                globalStore.account.rights.canReadNotificationsClothingdetailsChanged && {
                    label: 'Wijziging kledij-gegevens',
                    value: 7,
                },
                globalStore.account.rights.canReadNotificationsUserinfoChanged && {
                    label: 'Wijziging persoonsgegevens',
                    value: 8,
                },
            ].filter(v => v);
        },

        notifications: [],
        unread: [],
        markRead(ids) {
            !Array.isArray(ids) && (ids = [ids]);
            for (let id of ids) {
                socket.emit('notification_read', {
                    userId: globalStore.account.id,
                    notificationId: id,
                });
            }
        },
        markUnread(ids) {
            !Array.isArray(ids) && (ids = [ids]);
            for (let id of ids) {
                socket.emit('notification_unread', {
                    userId: globalStore.account.id,
                    notificationId: id,
                });
            }
        },
        remove(ids) {
            !Array.isArray(ids) && (ids = [ids]);
            for (let id of ids) {
                socket.emit('notification_removed', {
                    userId: globalStore.account.id,
                    notificationId: id,
                });
            }
        },
        listen_unread(projectId) {
            if (globalStore.account.id) {
                socket.on('notifications_unread', function(notifications) {
                    globalStore.notifications.unread = notifications;
                });
                socket.on(
                    'notification_read',
                    id => (globalStore.notifications.unread = globalStore.notifications.unread.filter(n => n.id !== id))
                );
                socket.on('notification_added', notification => {
                    if (!globalStore.notifications.unread.some(n => n.id === notification.id)) {
                        globalStore.notifications.unread.push(notification);
                    }
                });
                socket.emit('notifications_unread', { userId: globalStore.account.id, projectId });
            }
            return () => {
                socket.close();
            };
        },
        listen() {
            if (globalStore.account.id) {
                socket.on('notifications', function(notifications) {
                    globalStore.notifications.notifications = notifications;
                });
                socket.on('notification_removed', id => {
                    globalStore.notifications.unread = globalStore.notifications.unread.filter(n => n.id !== id);
                    globalStore.notifications.notifications = globalStore.notifications.notifications.filter(
                        n => n.id !== id
                    );
                });
                socket.on('notification_added', notification =>
                    globalStore.notifications.notifications.push(notification)
                );
                socket.emit('notifications', globalStore.account.id);
            }
            return () => {};
        },
    });
}
