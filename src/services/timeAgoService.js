import TimeAgo from 'javascript-time-ago';
import nl from 'javascript-time-ago/locale/nl';
import fr from 'javascript-time-ago/locale/fr';
import en from 'javascript-time-ago/locale/en';

TimeAgo.addLocale(nl);
TimeAgo.addLocale(fr);
TimeAgo.addLocale(en);

let timeAgo = timeAgoService().set(window.localStorage.getItem('FVD_LANG') || 'nl');

export default function timeAgoService() {
    function set(locale) {
        switch (locale) {
            case 'en':
                timeAgo = new TimeAgo('en-US');
                break;
            case 'fr':
                timeAgo = new TimeAgo('fr-FR');
                break;
            default:
                timeAgo = new TimeAgo('nl-BE');
                break;
        }
    }

    function get() {
        return timeAgo;
    }

    return {
        get,
        set,
    };
}
