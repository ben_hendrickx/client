function localStorageService() {
    function get(key, fallback = null) {
        const localStorageItem = localStorage.getItem(key);
        return localStorageItem ? JSON.parse(localStorageItem) : fallback;
    }

    function set(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
        return value;
    }

    function remove(key) {
        localStorage.removeItem(key);
    }

    return {
        get,
        set,
        remove,
    };
}

export default localStorageService;
