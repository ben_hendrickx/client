/* eslint-disable no-console */
import { REQUEST_PREFIX } from '../constants/rest';

export const logApiRequestToConsole = (prefix, message, content = null, status = 200) => {
    const statusColor = `color: ${status === 200 ? 'red' : 'green'};`;
    const prefixColor = `color: ${prefix === REQUEST_PREFIX ? 'blue' : 'orange'};`;

    if (content) {
        console.groupCollapsed(`%c ${prefix} %c ${message}`, prefixColor, statusColor);
        console.log(content);
        console.groupEnd();
    } else {
        console.log(`%c ${prefix} %c ${message}`, prefixColor, statusColor);
    }
};
