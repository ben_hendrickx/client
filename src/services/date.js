import moment from 'moment';

export default function dateService() {
    function isDate(value) {
        return moment(value, moment.ISO_8601, true).isValid();
    }

    return {
        isDate,
    };
}
