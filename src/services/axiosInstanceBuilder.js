import axios from 'axios';
import { logApiRequestToConsole } from './log';
import { REQUEST_PREFIX, RESPONSE_PREFIX } from '../constants/rest';

/**
 * Will build a axios instance and will enable logging when needed
 * @returns {AxiosInstance}
 */
export default function axiosInstanceBuilder() {
    const axiosInstance = axios.create();

    const ENABLE_API_LOGGING = process.env.REACT_APP_ENABLE_REST_LOGGING || 'true';

    if (ENABLE_API_LOGGING.toLocaleLowerCase() === 'true') {
        axiosInstance.interceptors.request.use(request => {
            const method = request.method.toUpperCase();
            const url = request.url;

            let message = `${method} ${url}`;

            if (request.params && typeof request.params === 'object') {
                const queryParams = request.params;

                Object.keys(queryParams).forEach((paramKey, index) => {
                    const separator = index === 0 ? '?' : '&';
                    message += `${separator}${paramKey}=${queryParams[paramKey]}`;
                });
            }

            logApiRequestToConsole(REQUEST_PREFIX, message, request.data);

            return request;
        });

        axiosInstance.interceptors.response.use(response => {
            const method = response.config.method.toUpperCase();
            const url = response.request.responseURL;

            const message = `${method} ${url}`;

            logApiRequestToConsole(RESPONSE_PREFIX, message, response.data, response.status);

            return response;
        });
    }

    return axiosInstance;
}
