import localStorageService from "./localStorage";

let token = localStorageService().get('FVD_TOKEN', null);

export default function tokenService() {
    function get() {
        return token;
    }

    function set(t) {
        token = t;
    }

    function clear() {
        token = undefined;
    }

    return {
        get,
        set,
        clear,
    }
}
