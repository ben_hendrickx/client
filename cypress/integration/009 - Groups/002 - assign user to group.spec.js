describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Assign user to group', () => {
        it('Add user', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(3).click();
            cy.wait(2000);
            cy.wait(1000).get('.edit-users').eq(0).click();
            cy.wait(3000);
            cy.get('select[name="geslacht"]').select("0");
            cy.wait(1000).get('select[name="met_foto"]').select("1");
            cy.wait(1000).get('select[name="min._leeftijd"]').select("30");
            cy.wait(1000).get('select[name="max._leeftijd"]').select("30");
            cy.wait(1000).get('select[name="sortering"]').select("creationDt|ASC");
            cy.wait(1000).get('select[name="sortering"]').select("date_of_birth|ASC");
            cy.wait(1000).get('select[name="met_foto"]').select('');
            cy.wait(1000).get('input[type="checkbox"]').eq(1).click();
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.wait(2000).contains("Medewerkers van groep 'Testgroep' werden succesvol gewijzigd");
        });
    });
});
