describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Duplicate group', () => {
        it('Duplicate group', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(3).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Dupliceer groep"]').click();
            cy.wait(1000);
            cy.get('button').contains('Kopiëer').click();
            cy.wait(1000).contains('Groep werd succesvol gekopiëerd');
        });
    });
});
