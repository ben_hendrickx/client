describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Create group', () => {
        it('Create group', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(3).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Maak groep aan"]').click();
            cy.wait(1000);
            cy.get('input[name="volgnummer"]').type('GR001');
            cy.get('input[name="naam"]').type('Testgroep');
            cy.get('button').contains('Sla op').click();
            cy.wait(2000);
            cy.wait(1000).contains('Groep werd succesvol aangemaakt');
        });
    });
});
