describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Delete project', () => {
        it('Delete Kerstmusical copy', () => {
            cy.wait(1000).get('.remove-filters').click();
            cy.wait(1000).get('.delete-project').eq(1).click();
            cy.wait(1000).get('button').contains('Verwijder').click();
            cy.contains('Project werd succesvol verwijderd');
        });
    });
});
