describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Link scene to group', () => {
        it('Link scene to group', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(4).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Bewerk scène"]').eq(0).click();
            cy.wait(1000);
            cy.get('select[name="gekoppeld"]').select('3');
            cy.get('button').contains('Sla op').click();
            cy.wait(2000);
            cy.wait(1000).contains('Scène Testscène werd gewijzigd');
        });
    });
});
