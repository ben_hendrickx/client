describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Assign role and group to scene', () => {
        it('Assign role and group to scene', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(4).click();
            cy.wait(2000);
            cy.wait(1000).get('.edit-groups-roles').click();
            cy.wait(2000);
            cy.wait(1000).get('select[name="group"]').select('85');
            cy.wait(1000).get('button').contains('Voeg groep toe').click();
            cy.wait(1000).contains("Groep werd succesvol toegevoegd");
            cy.wait(1000).get('select[name="role"]').select('182');
            cy.wait(1000).get('button').contains('Voeg rol toe').click();
            cy.wait(1000).contains("Rol werd succesvol toegevoegd");
            cy.wait(2000);
        });
    });
});
