describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Create scene', () => {
        it('Create scene', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(4).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Maak scène aan"]').click();
            cy.wait(1000);
            cy.get('input[name="volgnummer"]').type('SC001');
            cy.get('input[name="scène-naam"]').type('Testscène');
            cy.get('button').contains('Sla op').click();
            cy.wait(2000);
            cy.wait(1000).contains('Scène Testscène werd aangemaakt');
        });
    });
});
