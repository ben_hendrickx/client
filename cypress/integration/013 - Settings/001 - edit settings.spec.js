describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/instellingen')
    });

    describe('Edit settings', () => {
        it('Edit settings', () => {
            cy.wait(1000).get('input[name="titel_voor_op_de_hoofdpagina"]').type('test');
            cy.wait(1000).get('input[name="omschrijving_voor_op_de_hoofdpagina"]').type('test');
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.contains('Instellingen werden succesvol gewijzigd');
            cy.get('a').contains('Afmelden').click();
            cy.contains('test');
        });
    });
});
