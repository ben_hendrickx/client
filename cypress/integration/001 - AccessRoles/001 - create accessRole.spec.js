describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/toegangsrollen');
    });

    describe('Create accessRole', () => {
        it('Create accessRole', () => {
            cy.wait(1000).get('[title="Maak rol aan"]').click();
            cy.get('input[name="naam"]').type('test');
            cy.get('input[name="omschrijving"]').type('test');
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.contains('Toegangsrol werd succesvol aangemaakt');
            cy.wait(2000);
        });
    });
});
