describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/toegangsrollen');
    });

    describe('Duplicate accessRole', () => {
        it('Duplicate accessRole', () => {
            cy.wait(1000).get('[title="Dupliceer toegangsrol"]').eq(0).click();
            cy.wait(1000).get('button').contains('Dupliceer toegangsrol').click();
            cy.contains('Toegangsrol succesvol gedupliceerd');
            cy.wait(2000);
        });
    });
});
