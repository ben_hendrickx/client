describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/toegangsrollen');
    });

    describe('Delete accessRole', () => {
        it('Delete accessRole', () => {
            cy.wait(1000).get('[title="Verwijder toegangsrol"]').eq(1).click();
            cy.wait(1000).get('button').contains('Verwijder').click();
            cy.contains('Toegangsrol werd succesvol verwijderd');
            cy.wait(1000).get('[title="Verwijder toegangsrol"]').eq(1).click();
            cy.wait(1000).get('button').contains('Verwijder').click();
            cy.contains('Toegangsrol werd succesvol verwijderd');
            cy.wait(2000);
        });
    });
});
