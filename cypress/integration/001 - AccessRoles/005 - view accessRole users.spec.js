describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/toegangsrollen');
    });

    describe('View accessRole users', () => {
        it('View accessRole users', () => {
            cy.wait(1000).get('[title="Bekijk gebruikers"]').eq(0).click();
            cy.contains('test@fanvandimpna.be');
            cy.wait(2000);
        });
    });
});
