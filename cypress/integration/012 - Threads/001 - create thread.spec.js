describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/')
    });

    describe('Create thread', () => {
        it('Create thread', () => {
            cy.wait(1000).get('.threads').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(1).click();
            cy.wait(1000).get('[title="Nieuw onderwerp"]').click();
            cy.get('input[name="naam"]').type('Test');
            cy.get('input[name="standaardbericht"]').type('Test');
            cy.get('.react-tagsinput-input').eq(0).type('FVD Test');
            cy.get('li').contains('FVD Test').click();
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.contains('Gespreksonderwerp 11 werd succesvol aangemaakt');
            cy.wait(1000).get('div[role=tablist] > button').eq(0).click();
            cy.wait(1000).get('.new').click();
            cy.get('select[name="onderwerp"]').select("11");
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.get('.thread').eq(0).click();
            cy.contains('Gesprek: Test');
        });
    });
});
