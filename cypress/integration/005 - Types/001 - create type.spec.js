describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/repetitietypes')
    });

    describe('Create type', () => {
        it('Create type"', () => {
            cy.wait(1000).get('[title="Maak repetitie-type aan"]').click();
            cy.wait(1000);
            cy.get('input[name="naam"]').type('Test');
            cy.wait(1000).get('button').contains('Sla op').click();
        });
    });
});
