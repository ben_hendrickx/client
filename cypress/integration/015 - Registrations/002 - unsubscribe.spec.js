describe('As a user', () => {
    beforeEach(() => {
        cy.login('ben@kommaboard.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    it('Unsubscribe', () => {
        cy.wait(3000).get('.view-registration').click();
        cy.wait(2000).get('button').contains('Uitschrijven').click();
        cy.wait(1000).get('button').contains('Schrijf uit').click();
        cy.wait(2000);
    });
});
