describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Delete registration', () => {
        it('Delete registration', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(1).click();
            cy.wait(1000).get('.view-user').eq(0).click();
            cy.wait(2000).get('button').contains('Uitschrijven').click();
            cy.wait(1000);
            cy.wait(1000).get('button').contains('Schrijf uit').click();
            cy.wait(2000).contains('Ben Hendrickx werd uitgeschreven voor Kerstmusical');
        });
    });
});
