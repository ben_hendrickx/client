describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten/9/overzicht')
    });

    describe('Copy registrations', () => {
        it('Copy registrations', () => {
            cy.wait(1000).get('.copy-registrations').click();
            cy.wait(1000);
            cy.wait(1000).get('select[name="selecteer_project"]').select("10");
            cy.wait(2000).get('button').contains('Kopiëer').click();
            cy.wait(1000).contains('Inschrijvingen succesvol gekopiëerd');
        });
    });
});
