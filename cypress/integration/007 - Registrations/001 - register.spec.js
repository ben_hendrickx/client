describe('As a user', () => {
    beforeEach(() => {
        cy.login('ben@kommaboard.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten');
    });

    it('Register in project', () => {
        cy.wait(1000).get('span').contains('Inschrijven').click();
        cy.wait(1000);
        cy.get('select[name="waar_wil_je_meewerken?"]').select('1');
        cy.wait(1000);
        cy.get('select[name="on-stage_voorkeur"]').select('1');
        cy.get('input[name="lichaamslengte"]').type('180');
        cy.get('input[name="borstomtrek"]').type('50');
        cy.get('input[name="taille"]').type('50');
        cy.get('input[name="heupomtrek"]').type('50');
        cy.get('input[name="hoofdomtrek"]').type('50');
        cy.get('select[name="kledingmaat"]').select('34');
        cy.get('#reference').click();
        cy.wait(1000).get('.close').click();
        cy.wait(1000).get('button').contains('Sla op').click();
        cy.wait(2000);
    });

    it('Edit preferences', () => {
        cy.wait(1000).get('.view-registration').click();
        cy.wait(2000);
        cy.wait(1000).get('.edit').eq(1).click();
        cy.get('select[name="on-stage_voorkeur"]').select('2');
        cy.wait(1000).get('.save').eq(1).click();
        cy.contains('Gegevens succesvol gewijzigd');
        cy.wait(2000);
    });
});
