describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Check registrations', () => {
        it('Open tab "Inschrijvingen"', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(1).click();
            cy.wait(2000);
        });
    });
});
