xdescribe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Edit project', () => {
        it('Edit Kerstmusical', () => {
            cy.wait(1000).get('.edit-project').click();
            cy.wait(1000);
            cy.get('input[name="toegang_tot_rollenschema_vanaf"]').clear();
            cy.get('input[name="toegang_tot_rollenschema_toegestaan_tot"]').clear();
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.wait(2000).contains('Project werd succesvol gewijzigd');
        });
    });
});
