describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Copy project', () => {
        it('Copy Kerstmusical', () => {
            cy.wait(1000).get('.copy-project').click();
            cy.wait(1000);
            cy.get('button').contains('Dupliceer').click();
            cy.contains('Project werd succesvol gedupliceerd');
        });
    });
});
