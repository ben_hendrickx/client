import moment from 'moment';

describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Create project', () => {
        it('Create Kerstmusical', () => {
            cy.wait(1000).get('[title="Maak project aan"]').click();
            cy.wait(1000);
            cy.get('input[name="naam"]').type('Kerstmusical');
            cy.get('input[name="titel_voor_op_loginpagina"]').type('Kerstmusical');
            cy.get('input[name="omschrijving_voor_op_loginpagina"]').type('Kerstmusical');
            cy.get('input[name="project_loopt_uiterst_tot"]').type(moment().endOf('day').format('DD/MM/YYYY HH:mm'));
            cy.get('input[name="inschrijvingen_open_vanaf"]').type(moment().startOf('day').format('DD/MM/YYYY HH:mm'));
            cy.get('input[name="kan_on-stage_inschrijven_tot"]').type(moment().endOf('day').format('DD/MM/YYYY HH:mm'));
            cy.get('input[name="kan_off-stage_inschrijven_tot"]').type(moment().endOf('day').format('DD/MM/YYYY HH:mm'));
            cy.get('input[name="toegang_tot_rollenschema_vanaf"]').type(moment().startOf('day').format('DD/MM/YYYY HH:mm'));
            cy.get('input[name="toegang_tot_rollenschema_toegestaan_tot"]').type(moment().endOf('day').format('DD/MM/YYYY HH:mm'));
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.contains('Project werd succesvol aangemaakt');
        });
    });
});
