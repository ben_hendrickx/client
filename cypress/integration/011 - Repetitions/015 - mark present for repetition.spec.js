describe('As a user', () => {
    beforeEach(() => {
        cy.login('ben@kommaboard.be', 'FVD123');
        cy.viewport('macbook-15').visit('/repetities')
    });

    it('Mark present', () => {
        cy.wait(2000);
        cy.contains('Test (Test)');
        cy.get('button').contains('Ik kan niet aanwezig zijn').click();
        cy.contains('U bent nu afgemeld voor deze repetitie');
        cy.get('button').contains('Ik kan aanwezig zijn').click();
        cy.contains('U bent weer aangemeld voor deze repetitie');
    });
});
