describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Duplicate repetition', () => {
        it('Duplicate repetition', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(5).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Dupliceer repetitie"]').click();
            cy.wait(1000);
            cy.get('button').contains('Dupliceer repetitie').click();
            cy.wait(1000).contains('Repetitie succesvol gedupliceerd');
        });
    });
});
