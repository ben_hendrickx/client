describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Close repetition', () => {
        it('Close repetition', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(5).click();
            cy.wait(2000);
            cy.wait(1000).get('input[type="checkbox"]').eq(1).click();
            cy.get('button').contains('Sluit geselecteerde repetities').click();
            cy.wait(1000).get('button').contains('Sluit repetitie(s)').click();
            cy.contains('Repetities succesvol gesloten voor medewerkers');
        });
    });
});
