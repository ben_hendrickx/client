describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Open pin modal', () => {
        it('Open pin modal', () => {
            cy
                .wait(2000)
                .get('.view-project').click()
                .wait(1000)
                .get('div[role=tablist] > button').eq(5).click()
                .wait(2000)
                .get('[title="Ontgrendel repetitie"]').eq(0).click();
            cy
                .wait(1000)
                .get('.close').click()
                .get('[title="Overzicht aanwezigen"]').eq(0).click()
                .get('[title="Markeer als niet aanwezig"]').eq(0).click()
        });
    });
});
