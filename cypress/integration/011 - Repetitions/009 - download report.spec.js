describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('View repetition', () => {
        it('View repetition', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(5).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Overzicht aanwezigen"]').eq(0).click();
            cy.wait(1000).get('button[title="Download rapport"]').click();
            cy.wait(1000);
        });
    });
});
