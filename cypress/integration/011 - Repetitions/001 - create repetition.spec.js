import moment from 'moment';

describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Create repetition', () => {
        it('Create repetition', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(5).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Maak repetitie aan"]').click();
            cy.wait(2000);
            cy.wait(1000).get('select[name="type"]').select('25');
            cy.wait(1000).get('input[name="volgnummer"]').type('REP001');
            cy.get('input[name="naam"]').type('Repetitie');
            cy.get('input[name="datum_van_repetitie"]').type(moment().format('DDMMYYYY'));
            cy.get('input[name="start-uur"]').type(moment().add(30, 'minutes').format('HH:mm'));
            cy.get('input[name="eind-uur"]').type(moment().add(30, 'minutes').format('HH:mm'));
            cy.get('select[name="locatie"]').select('13');
            cy.get('input[type="checkbox"]').eq(0).click();
            cy.get('.open').eq(2).click();
            cy.get('.role-0').click();
            cy.get('button').contains('Sla op').click();
            cy.wait(2000);
            cy.wait(1000).contains('Repetitie Repetitie werd aangemaakt');
            cy.get('input[type="checkbox"]').click();
            cy.get('button').contains('Open geselecteerde repetities').click();
            cy.wait(1000).get('button').contains('Open repetitie(s)').click();
            cy.wait(1000).contains('Repetitie(s) succesvol geopend voor medewerkers');
            cy.wait(2000);
        });
    });
});
