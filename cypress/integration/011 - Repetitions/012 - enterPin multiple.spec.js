describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Open pinscreen', () => {
        it('Open pinscreen', () => {
            cy
                .wait(2000)
                .get('.view-project').click()
                .wait(1000)
                .get('div[role=tablist] > button').eq(5).click()
                .wait(2000)
                .get('[title="Overzicht aanwezigen"]').eq(0).click()
                .wait(1000)
                .get('.pin').invoke('text').then((pin) => {
                    const pin1 = pin.slice(0, 5);
                    const pin2 = pin.slice(5, 10);
                    cy
                        .visit('/pinscherm/422?repetitionIds=426')
                        .wait(2000)
                        .get('input').type(pin1)
                        .get('button').click();
                    cy
                        .contains('Welkom')
                        .wait(3000)
                        .get('input').type(pin1)
                        .get('button').click();
                    
                    cy
                        .contains('eerder al aangemeld')
                        .wait(3000)
                        .get('input').type(pin2)
                        .get('button').click();

                    cy.contains('Welkom').visit('/projecten')
                        .wait(2000)
                        .get('.view-project').click()
                        .wait(1000)
                        .get('div[role=tablist] > button').eq(5).click()
                        .wait(2000)
                        .get('[title="Overzicht aanwezigen"]').eq(0).click()
                        .wait(1000)
                        .get('.present').contains('2');
                });
        });
    });
});
