describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/pinscherm/422?repetitionIds=')
    });

    describe('Open pinscreen', () => {
        it('Open pinscreen', () => {
            cy.wait(1000).get('input').type('11111');
            cy.wait(2000);
            cy.wait(1000).get('button').click();
            cy.contains('Ongeldige pincode');
        });
    });
});
