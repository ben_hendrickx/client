describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Delete multiple repetitions', () => {
        it('Duplicate repetition 2 times', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(5).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Dupliceer repetitie"]').eq(0).click();
            cy.wait(1000);
            cy.get('button').contains('Dupliceer repetitie').click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Dupliceer repetitie"]').eq(0).click();
            cy.wait(1000);
            cy.get('button').contains('Dupliceer repetitie').click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Dupliceer repetitie"]').eq(0).click();
            cy.wait(1000);
            cy.get('button').contains('Dupliceer repetitie').click();
            cy.wait(1000).contains('Repetitie succesvol gedupliceerd');
        });

        it('Delete multiple repetitions', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(5).click();
            cy.wait(2000);
            cy.wait(1000).get('input[type="checkbox"]').eq(1).click();
            cy.get('input[type="checkbox"]').eq(2).click();
            cy.get('button').contains('Verwijder geselecteerde repetities').click();
            cy.wait(1000).get('button').contains(/^Verwijder$/).click();
            cy.contains('Repetities succesvol verwijderd');
        });
    });
});
