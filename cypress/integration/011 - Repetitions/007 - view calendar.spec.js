describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/')
    });

    describe('View calendar', () => {
        it('View calendar', () => {
            cy.wait(1000).get('.calendar').click();
            cy.contains('REP001');
        });
    });
});
