describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Edit repetition', () => {
        it('Edit repetition', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(5).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Bewerk repetitie"]').eq(0).click();
            cy.wait(1000);
            cy.get('input[name="naam"]').clear().type('Test');
            cy.get('.open').eq(0).click();
            cy.wait(1000).get('input[type="checkbox"]').eq(4).click();
            cy.get('.open').eq(0).click();
            cy.wait(1000).get('.group-0').click();
            cy.get('.open').eq(0).click();
            cy.wait(1000).get('.role-1').click();
            cy.get('.open').eq(0).click();
            cy.wait(1000).get('.user-0').click();
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.wait(1000).contains('Repetitie Test werd gewijzigd');
        });
    });
});
