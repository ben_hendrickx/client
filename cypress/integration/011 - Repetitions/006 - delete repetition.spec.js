describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Delete repetition', () => {
        it('Delete repetition', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(5).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Verwijder repetitie"]').eq(1).click();
            cy.wait(1000);
            cy.get('button').contains('Verwijder repetitie').click();
            cy.contains('Repetitie succesvol verwijderd');
        });
    });
});
