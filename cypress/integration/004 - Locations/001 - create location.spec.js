describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/locaties')
    });

    describe('Create location', () => {
        it('Create location"', () => {
            cy.wait(1000).get('[title="Maak locatie aan"]').click();
            cy.wait(1000);
            cy.get('input[name="naam"]').type('Test');
            cy.get('input[name="afkorting"]').type('TEST');
            cy.get('input[name="address"]').type('Molenaarstraat 10');
            cy.get('.suggestion-item').contains('Molenaarstraat 10').first().click();
            cy.wait(1000).get('button').contains('Sla op').click();
        });
    });
});
