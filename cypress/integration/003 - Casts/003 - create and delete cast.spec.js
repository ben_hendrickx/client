describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/casts')
    });

    describe('Create cast', () => {
        it('Create cast "Test"', () => {
            cy.wait(1000).get('[title="Maak cast aan"]').click();
            cy.wait(1000);
            cy.get('input[name="naam"]').type('Test');
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.contains('Cast werd succesvol aangemaakt');
            cy.get('[title="Verwijder cast"]').eq(1).click();
            cy.get('button').contains('Verwijder').click();
            cy.contains('Cast werd succesvol verwijderd');
        });
    });
});
