describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/casts')
    });

    describe('Create cast', () => {
        it('Create cast "Geel"', () => {
            cy.wait(1000).get('[title="Maak cast aan"]').click();
            cy.wait(1000);
            cy.get('input[name="naam"]').type('Geel');
            cy.wait(1000).get('button').contains('Sla op').click();
        });
    });
});
