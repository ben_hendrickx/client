describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/casts')
    });

    describe('Edit cast', () => {
        it('Edit cast "Geel"', () => {
            cy.wait(1000).get('[title="Bewerk cast"]').click();
            cy.wait(1000);
            cy.get('input[name="naam"]').type('_test');
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.contains('Cast werd succesvol gewijzigd');
        });
    });
});
