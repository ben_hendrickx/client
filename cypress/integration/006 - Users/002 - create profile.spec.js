describe('As a user', () => {
    beforeEach(() => {
        cy.login('ben@kommaboard.be', 'FVD123');
        cy.viewport('macbook-15').visit('/medewerkers/nieuw')
    });

    it('Create profile', () => {
        cy.wait(2000).get('#voornaam').type('Ben');
        cy.get('#achternaam').type('Hendrickx');
        cy.get('#geboortedatum').type('23021992');
        cy.get('#geslacht').select('Man');
        cy.get('#nationaliteit').type('belg');
        cy.get('#geboorteland').select('België');
        cy.get('#gsm_nummer').type('0476091967');
        cy.wait(1000).get('#ad_FVD').type('Molenaarstraat 10');
        cy.get('.suggestion-item').contains('Molenaarstraat 10').first().click();
        cy.get('#t-shirt_maat').select('140');
        cy.get('input[type="checkbox"]').first().click();
        cy.get('button').contains('Maak profiel').click();
        cy.wait(2000).contains('Ben Hendrickx');
    });
});
