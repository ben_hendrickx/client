describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/medewerkers')
    });

    describe('Import users', () => {
        it('Import users', () => {
            cy.wait(1000).get('[title="Importeer medewerkers"]').click();
            cy.fixture('import_users.csv').then(fileContent => {
                cy.get('.dropzone').attachFile('import_users.csv', {
                    subjectType: 'drag-n-drop',
                });
            });
            cy.contains('ben@relyus.be');
            cy.get('button').contains('Importeer').click();
            cy.contains('1 medewerker werd geïmporteerd');
        });
    });
});
