describe('As a user', () => {
    it('Create account', () => {
        cy.viewport('macbook-15').visit('/');
        cy.get('a[href="/account-aanmaken"]').click();
        cy.fixture('profile.jpeg').then(fileContent => {
            cy.get('.dropzone').attachFile('profile.jpeg', {
                subjectType: 'drag-n-drop',
            });
        });
        cy.get('input[name="voornaam"]').type('Test');
        cy.get('input[name="achternaam"]').type('Test');
        cy.get('input[name="geboortedatum"]').type('23021992');
        cy.get('select[name="geslacht"]').select('0');
        cy.get('input[name="nationaliteit"]').type('belg');
        cy.get('select[name="geboorteland"]').select('22');
        cy.get('input[name="g.s.m.-nummer"]').type('0476091967');
        cy.get('input[name="mailadres"]').type('ben@kommaboard.be');
        cy.get('input[name="bevestig_mailadres"]').type('ben@kommaboard.be');
        cy.get('input[name="address"]').type('Antwerpen');
        cy.get('.suggestion-item').contains('Antwerpen').first().click();
        cy.wait(1000).get('input[name="address"]').type('Molenaarstraat 10');
        cy.get('.suggestion-item').contains('Molenaarstraat 10').first().click();
        cy.get('select[name="t-shirt_maat"]').select('140');
        cy.get('input[name="wachtwoord"]').type('FVD123');
        cy.get('input[name="bevestig_wachtwoord"]').type('FVD123');
        cy.get('input[type="checkbox"]').first().click();
        cy.wait(1000).get('button').contains('Aanmelden').click();
        cy.wait(2000);
    });
});
