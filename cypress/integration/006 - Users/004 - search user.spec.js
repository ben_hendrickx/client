describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/')
    });

    describe('Search user', () => {
        it('Search user', () => {
            cy.wait(1000).get('#search').type('FVD');
            cy.wait(1000).get('.search-user-0').click();
            cy.wait(2000).contains('Test FVD');
        });
    });
});
