describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Create role', () => {
        it('Create role', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(2).click();
            cy.wait(2000);
            cy.wait(1000).get('[title="Maak rol aan"]').click();
            cy.wait(1000);
            cy.get('input[name="volgnummer"]').type('INR001');
            cy.get('input[name="rol-naam"]').type('Testrol');
            cy.get('input[type="checkbox"]').click();
            cy.get('button').contains('Sla op').click();
            cy.wait(2000);
            cy.wait(1000).contains('Rol werd succesvol aangemaakt');

            cy.wait(2000);
            cy.wait(1000).get('[title="Maak rol aan"]').click();
            cy.wait(1000);
            cy.get('input[name="volgnummer"]').type('INR002');
            cy.get('input[name="rol-naam"]').type('Testrol2');
            cy.get('input[type="checkbox"]').click();
            cy.get('button').contains('Sla op').click();
            cy.wait(2000);
            cy.wait(1000).contains('Rol werd succesvol aangemaakt');
        });
    });
});
