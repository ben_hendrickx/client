describe('As an admin', () => {
    beforeEach(() => {
        cy.login('test@fanvandimpna.be', 'FVD123');
        cy.viewport('macbook-15').visit('/projecten')
    });

    describe('Assign user to role', () => {
        it('Add user', () => {
            cy.wait(1000).get('.view-project').click();
            cy.wait(1000).get('div[role=tablist] > button').eq(2).click();
            cy.wait(2000);
            cy.wait(1000).get('.edit-users').eq(0).click();
            cy.wait(3000);
            cy.get('input[type="checkbox"]').eq(1).click();
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.wait(2000).contains("Medewerkers van rol 'Testrol' werden succesvol gewijzigd");

            cy.wait(2000);
            cy.wait(1000).get('.edit-users').eq(1).click();
            cy.wait(3000);
            cy.get('input[type="checkbox"]').eq(0).click();
            cy.wait(1000).get('button').contains('Sla op').click();
            cy.wait(2000).contains("Medewerkers van rol 'Testrol2' werden succesvol gewijzigd");
        });
    });
});
