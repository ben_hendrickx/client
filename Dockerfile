FROM node:14 as builder

ENV NODE_OPTIONS="--max-old-space-size=4096"

ENV PUBLIC_URL=/
ENV REACT_APP_API_ENDPOINT=/api
ENV REACT_APP_SOCKET_PATH=/websocket
ENV REACT_APP_API_HOST=server
ENV REACT_APP_PORT=443

WORKDIR /usr/src/app
COPY package.* /usr/src/app/
RUN npm install
RUN npm audit fix
COPY . /usr/src/app/
RUN npm run build

FROM --platform=linux/amd64 nginx:alpine

COPY --from=builder /usr/src/app/build /usr/share/nginx/html/
COPY default.conf /etc/nginx/conf.d/
